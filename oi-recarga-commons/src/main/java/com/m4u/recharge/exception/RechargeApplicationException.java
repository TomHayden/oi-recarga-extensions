package com.m4u.recharge.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

public class RechargeApplicationException extends Exception {

    private static final long serialVersionUID = 1L;

    protected String errorCode;

    public RechargeApplicationException(Exception e) {
        super(e);
    }

    public RechargeApplicationException(String msg) {
        super(msg);
    }

    public RechargeApplicationException(String msg, Exception e) {
        super(msg, e);
    }

    public RechargeApplicationException(String msg, Exception e, String errorCode) {
        super(msg, e);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
    
    public final void printStackTrace(PrintStream s) {
        String msg = getMessage();
        s.print(String.format("%s: %s", getClass().getName(), (msg == null) ? "" : msg));
    }

    public final void printStackTrace(PrintWriter s) {
        String msg = getMessage();
        s.print(String.format("%s: %s", getClass().getName(), (msg == null) ? "" : msg));
    }
}
