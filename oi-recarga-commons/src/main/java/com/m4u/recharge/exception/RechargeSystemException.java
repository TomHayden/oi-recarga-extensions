package com.m4u.recharge.exception;


public class RechargeSystemException extends Exception {

    private static final long serialVersionUID = 3258417248254374960L;

    protected String errorCode;

    public String getErrorCode() {
        return errorCode;
    }

    public RechargeSystemException(Exception e) {
        super(e);
    }

    public RechargeSystemException(String msg) {
        super(msg);
    }

    public RechargeSystemException(String msg, Exception e) {
        super(msg, e);
        if(msg.indexOf("Erro na criptografia dos dados") != -1){
        }
    }

    public RechargeSystemException(String msg, Exception e, String errorCode) {
        super(msg, e);
        this.errorCode = errorCode;
    }
}
