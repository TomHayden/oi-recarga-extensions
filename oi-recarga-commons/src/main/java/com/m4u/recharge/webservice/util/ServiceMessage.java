package com.m4u.recharge.webservice.util;

public enum ServiceMessage {

    CUSTOMER_NEW(1000, "Usuario novo."),

    CUSTOMER_REGISTERED_WITHOUT_RECHARGE(1001, "Usuario cadastrado sem recarga."),

    CUSTOMER_REGISTERED_WITH_RECHARGE(1002, "Usuario cadastrado com recarga."),

    REGISTER_PENDENT(1101, "Usuario com cadastro pendente."),

    CUSTOMER_BLACKLIST(1102, "Usuario na blacklist. O cadastro no servico de Recarga esta bloqueado."),

    CUSTOMER_TEMPORARY_BLOCK(1103, "Cadastro temporariamente bloqueado."),

    CUSTOMER_CHANGES_LIMIT_EXCEEDS(1104, "Impossivel realizar cadastro. Usuario excedeu limite de mudancas no mes."),

    REGISTER_INTERVAL_MIN(1105, "Cadastro nao permitido hoje. O prazo minimo para recadastramento e alteracao de CPF ainda nao foi atingido."),

    SUCCESS_REGISTER(2000, "Usuario cadastrado com sucesso."),

    CARD_REQUIRED(2100, "Cartao de credito e obrigatorio."),

    CARD_INVALID(2101, "Cartao de credito invalido."),

    //Nao utilizar, remover futuramente
    CARD_WAS_REGISTER(2102, "Nao e possivel cadastrar este cartao. Um cartao de credito fica vinculado a 1 Oi Pagador por um prazo minimo de 90 dias apos a ultima recarga."),

    EXPIRATION_DATE_REQUIRED(2200, "Data de validade obrigatoria."),

    EXPIRATION_DATE_INVALID(2201, "Data de validade invalida."),

    EXPIRATION_DATE_EXPIRED(2202, "Data de validade vencida."),

    CPF_REQUIRED(2300, "CPF obrigatorio. Informe apenas 11 numeros do seu CPF, sem pontos ou tracos."),

    CPF_INVALID(2301, "CPF invalido. Informe apenas 11 numeros do seu CPF, sem pontos ou tracos."),

    CPF_DUPLICATED(2302, "CPF Duplicado"),

    BIRTH_REQUIRED(2400, "Data de nascimento obrigatoria."),

    BIRTH_INVALID(2401, "Data de nascimento invalida."),

    BIRTH_DENIED(2402, "Idade nao permitida."),

    MSISDN_REQUIRED(2500, "Msisdn obrigatorio."),

    MSISDN_INVALID(2501, "Msisdn invalido."),

    ID_SOCIAL_NETWORK_REQUIRED(2502, "Id da Rede Social obrigatorio."),
    
    EMAIL_REQUIRED(2503, "Email obrigatorio"),
    
    EMAIL_INVALID(2504, "Email invalido"),
    
    SOCIAL_NETWORK_ID_ALREADY_ASSOCIATED(2505, "Id da Rede Social ja associado a outro cliente."),

    CARD_VALIDATION_MAX_LIMIT(2600, "Este cartao de credito atingiu o limite de telefones cadastrados. Favor adicione um novo cartao de credito para continuar."),

    CUSTOMER_ALREADY_REGISTERED(2700, "Cliente encontra-se com cadastro ativo."),

    SUCCESS_RECHARGE(3000, "Solicitacao de recarga realizada com sucesso."),

    PROCESSING_RECHARGE(3001, "Recarga da ultima solicitacao em processamento."),

    LAST_RECHARGE_FAULT(3002, "Recarga da ultima solicitacao finalizada com erro."),

    CUSTOMER_NOT_FOUND(3100, "Usuario nao cadastrado."),

    UNSUPPORTED_CARD(3101, "Cartao de credito nao suportado pela aplicacao."),

    PREPAID_BLACKLIST(3102, "Recebedor da recarga na blacklist. Nao e possivel realizar novas recargas para este numero."),

    CREDIT_CARD_BLACKLIST(3103, "Cartao de credito na blacklist. Nao e possivel efetuar recarga com o cartao de credito informado."),

    CREDIT_CARD_NOT_FOUND(3104, "Cartao de credito nao cadastrado."),

    PREPAID_NOT_FOUND(3105, "Recebedor da recarga nao encontrado."),

    RECHARGE_REPEAT_INTERVAL_MIN(3106, "Recarga nao permitida, intervalo minimo entre repeticao de recargas ainda nao atingido."),

    RECHARGE_INTERVAL_MIN(3115, "Recarga nao permitida, intervalo minimo entre recargas ainda nao atingido."),

    RECHARGE_MAX_VALUE_EXCEED(3107, "Recarga nao permitida, o valor maximo de recargas no mes foi excedido."),

    RECHARGE_MAX_QUANTITY_EXCEED(3108, "Recarga nao permitida, a quantidade de recargas permitidas no mes foi excedida."),

    RECHARGE_MIN_VALUE(3109, "Recarga nao permitida, o valor minimo para recarga nao foi atingido."),

    RECHARGE_TRANSACTION_MAX_VALUE(3110, "Recarga nao permitida, o valor maximo para recarga foi ultrapassado."),

    CARD_TYPE_REQUIRED(3111, "Bandeira do cartao obrigatoria."),

    CARD_END_NUMBER_REQUIRED(3112, "Ultimos 4 digitos do cartao obrigatorio."),

    CARD_END_NUMBER_INVALID(3113, "Ultimos 4 digitos do cartao invalido."),

    CVV_REQUIRED(3114, "CVV obrigatorio."),
    
    //Nao utilizar, remover futuramente.
    DEBIT_PAYMENT_TYPE_INVALID(3115, "Tipo de pagamento de debito invalido."),
    
    EXTERNAL_ID_DUPLICATED(3200, "External ID duplicado para este Canal."),
    
    SCHEDULED_RECHARGE_FOUND(3300, "Recarga programada encontrada."),
    
    SCHEDULED_RECHARGE_NOT_FOUND(3301, "Recarga programada nao encontrada."),

    SCHEDULED_RECHARGE_DOES_NOT_MATCH(3302, "Recarga programada com dados inconsistentes."),
    
    SCHEDULED_RECHARGE_ADDED(3400, "Recarga programada adicionada com sucesso."),
    
    SCHEDULED_RECHARGE_ALREADY_REGISTERED(3401, "Recarga programada ja cadastrada."),
    
    SCHEDULED_RECHARGE_REMOVED(3402, "Recarga programada cancelada com sucesso."),
    
    SCHEDULED_RECHARGE_ELEGIBILITY(3500, "Recarga programada valida para ciclo de recarga."),
    
    SCHEDULED_RECHARGE_NOT_ELEGIBILITY(3501, "Recarga programada nao valida para ciclo de recarga."),
    
    SCHEDULED_RECHARGE_WITH_INACTIVE_CARD(3502, "Recarga programada com cartao inativo."),
    
    SCHEDULED_RECHARGE_CALLBACK_RECEIVED(3503, "Callback de recarga programada recebido."),
    
    SCHEDULED_RECHARGE_WITH_INACTIVE_PREPAID(3504, "Recarga programada nao valida para ciclo de recarga."),
    
    SCHEDULED_RECHARGE_UPDATED(3600, "Recarga Programada atualizada com sucesso."),
    
    SCHEDULED_RECHARGE_MAX_DAY_CHANGES_REACHED(3601, "Nao foi possivel alterar a data da sua Recarga Programada, pois o limite de alteracoes no mes foi atingido."),
    
    SCHEDULED_RECHARGE_MAX_AMOUNT_CHANGES_REACHED(3602, "Nao foi possivel alterar o valor da sua Recarga Programada, pois o limite de alteracoes no mes foi atingido."),
    
    CREDIT_CARD_ADDED(4000, "Cartao de credito adicionado com sucesso."),

    CREDIT_CARD_REPLACED(4001, "Cartao de credito substituido com sucesso."),
    
    CREDIT_CARD_REMOVED(4002, "Cartao de credito removido com sucesso."),
    
    CREDIT_CARD_UPDATED(4003, "Cartao de credito atualizado com sucesso."),

    CREDIT_CARD_TYPE_NOT_MATCH(4100, "Bandeira do cartao nao bate com o numero passado."),

    CARD_CHANGE_LIMIT(4101, "O limite de alteracoes de cartao foi atingido."),

    CARD_SIZE_LIMIT(4102, "O limite de cartoes cadastrados foi atingido."),

    CARD_REGISTER_ON_USER(4103, "O cartao ja encontra-se cadastrado para o usuario."),
    
    TOKEN_SENT(4400,"Token enviado com sucesso."),
    
    TOKEN_NOT_SENT(4401,"Falha no envio do Token."),
    
    TOKEN_VALID(4500,"Token validado com sucesso."),
    
    TOKEN_INVALID(4501,"Token informado esta invalido."),
    
    TOKEN_REQUIRED(4502,"Token obrigatorio."),

    PREPAID_SUCCESS_ADDED(5000, "Recebedor adicionado com sucesso."),

    DDD_NOT_MATCH(5100, "O DDD do recebedor e diferente do titular."),

    PREPAID_REQUIRED(5101, "O recebedor de recarga obrigatorio."),

    PREPAID_INVALID(5102, "O recebedor de recarga invalido."),

    PREPAID_VALIDATION_LIMIT(5103, "Limite de recebedores atingido."),

    PREPAID_ALREADY_REGISTER(5104, "Recebedor ja encontra-se cadastrado."),
    
    PREPAID_VALIDATION_MAX_LIMIT(5105, "Recebedor ja encontra-se associado ao limite maximo de titulares."),
    
    PREPAID_SUCCESS_REMOVED(5500, "Recebedor removido com sucesso."),
    
    PREPAID_SUCCESS_UPDATED(5600, "Recebedor atualizado com sucesso."),

    PREPAID_UPDATES_LIMIT_REACHED(5700, "O limite para alteracoes de recebedores foi atingido."),
    
    PREPAID_REMOVES_LIMIT_REACHED(5800, "O limite para exclusoes de recebedores foi atingido."),

    REMOVE_CARD_SUCCESS(6000, "Cartao de credito removido com sucesso."),

    //Nao utilizar, remover futuramente
    CUSTOMER_DONT_HAVE_CARDS(6100, "O usuario nao possui cartoes cadastrados."),

    UNREGISTER_SUCCESS(7000, "Usuario cancelado com sucesso."),
    
    VALIDATION_REQUIRED(7100, "Pergunta/Resposta de validacao obrigatoria."),

    VALIDATION_FIRED(7101, "Erro na resposta de validacao. Por motivo de seguranca, o cadastro foi bloqueado."),

    VALIDATION_TRY(7102, "Erro na resposta de validacao. Em caso de 3 erros consecutivos, seu cadastro sera bloqueado."),
    
    VALIDATION_INVALID(7103, "Pergunta de validacao invalida."),

    TRANSACTION_HISTORY_SUCCESS(8000, "Historico de transacoes encontrado com sucesso."),

    TRANSACTION_HISTORY_NO_RESULT(8100, "Historico de transacoes nao encontrado."),

    TRANSACTION_HISTORY_INVALID_PERIOD(8101, "Periodo para consulta de transacoes nao informado/invalido."),

    TRANSACTION_STATUS_SUCCESS(8500, "Transacao externa encontrada."),

    TRANSACTION_STATUS_NO_RESULT(8501, "Transacao externa nao encontrada."),

    CHANNEL_UNSUPPORTED(8888, "O canal informado nao e suportado pela aplicacao."),

    INTERNAL_ERROR(9999, "Erro no processamento da requisicao."); 
    
    private final long code;
    private final String message;

    private ServiceMessage(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return code + " - " + message;
    }

    public static ServiceMessage getServiceMessage(long code) {
        for (ServiceMessage serviceMessage : ServiceMessage.values()) {
            if (serviceMessage.getCode() == code) {
                return serviceMessage;
            }
        }
        return null;
    }

}
