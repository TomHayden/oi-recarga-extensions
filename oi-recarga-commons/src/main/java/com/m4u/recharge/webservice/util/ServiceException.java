package com.m4u.recharge.webservice.util;

public class ServiceException extends Exception {
    
    private static final long serialVersionUID = 2073616545984979103L;

    private ServiceMessage serviceMessage;

    private boolean isToLog;

    private int logType;

    public ServiceException(ServiceMessage serviceMessage, boolean isToLog, int logType) {
        super(serviceMessage.getMessage());
        this.serviceMessage = serviceMessage;
        this.isToLog = isToLog;
        this.logType = logType;
    }

    public ServiceException(ServiceMessage serviceMessage, Throwable cause, boolean isToLog, int logType) {
        super(serviceMessage.getMessage(), cause);
        this.serviceMessage = serviceMessage;
        this.isToLog = isToLog;
        this.logType = logType;
    }

    public ServiceException(ServiceMessage serviceMessage, String message, boolean isToLog, int logType) {
        super(message);
        this.serviceMessage = serviceMessage;
        this.isToLog = isToLog;
        this.logType = logType;
    }

    public ServiceException(ServiceMessage serviceMessage, String message, Throwable t, boolean isToLog, int logType) {
        super(message, t);
        this.serviceMessage = serviceMessage;
        this.isToLog = isToLog;
        this.logType = logType;
    }

    public ServiceMessage getServiceMessage() {
        return serviceMessage;
    }

    public void setServiceMessage(ServiceMessage serviceMessage) {
        this.serviceMessage = serviceMessage;
    }

    public boolean isToLog() {
        return isToLog;
    }

    public void setToLog(boolean isToLog) {
        this.isToLog = isToLog;
    }

    public int getLogType() {
        return logType;
    }

    public void setLogType(int logType) {
        this.logType = logType;
    }

}
