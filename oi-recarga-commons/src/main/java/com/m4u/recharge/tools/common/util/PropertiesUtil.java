package com.m4u.recharge.tools.common.util;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesUtil {
    public static final Logger log = Logger.getLogger(PropertiesUtil.class);

    private String file;

    private Properties property = null;

    private long lastReloadTime = 0L;

    private long reloadTime = 0L;
    
    public PropertiesUtil(String file) {
        this.file = file;
    }

    public Properties loadProperty() {
        try {
            if (property == null || isTimeToReload()) {
                InputStream inStream = PropertiesUtil.class.getResourceAsStream(file);
                property = new Properties();
                property.load(inStream);
                lastReloadTime = System.currentTimeMillis();
                reloadTime = getInt("properties.reload.time", 0);
            }
        } catch (IOException e) {
            log.error("Error loading file .properties: " + file, e);
        }
        return property;
    }

    private boolean isTimeToReload() {
        return (System.currentTimeMillis() - lastReloadTime > reloadTime);
    }

    public String getString(String key) {
        property = loadProperty();
        if (property == null) {
            return null;
        }
        return property.getProperty(key).trim();
    }

    public String getString(String key, String defaultValue) {
        try {
            return getString(key);
        } catch (Exception e) {
            log.warn(String.format("Misconfigured or absent value for configuration '%s', returning default value: %s", key, defaultValue));
        }
        return defaultValue;
    }

    public boolean getBoolean(String key) {
        boolean result = false;
        property = loadProperty();
        if (property != null) {
            result = Boolean.parseBoolean(property.getProperty(key).trim());
        }
        return result;
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        try {
            return getBoolean(key);
        } catch (Exception e) {
            log.warn(String.format("Misconfigured or absent value for configuration '%s', returning default value: %s", key, defaultValue));
        }
        return defaultValue;
    }

    public int getInt(String key, int defaultValue) {
        String stringValue = null;
        try {
            stringValue = getString(key);
            if (stringValue == null) {
                return defaultValue;
            }
            return Integer.parseInt(stringValue);
        } catch (NumberFormatException numberFormatException) {
            log.warn("Misconfigured value for configuration '" + key + "', a numeric value was expected, but '" + stringValue + "' found.");
        } catch (Exception e) {
            log.warn(String.format("Misconfigured or absent value for configuration '%s', returning default value: %s", key, defaultValue));
        }
        return defaultValue;
    }

    public long getLong(String key, long defaultValue) {
        String stringValue = null;
        try {
            stringValue = getString(key);
            if (stringValue == null) {
                return defaultValue;
            }
            return Long.parseLong(stringValue);
        } catch (NumberFormatException numberFormatException) {
            log.warn("Misconfigured value for configuration '" + key + "', a numeric value was expected, but '" + stringValue + "' found.");
        } catch (Exception e) {
            log.warn(String.format("Misconfigured or absent value for configuration '%s', returning default value: %s", key, defaultValue));
        }
        return defaultValue;
    }

    public String[] getStringsCommaSeparated(String key) {
        if (property == null) {
            property = loadProperty();

            if (property == null) {
                return null;
            }
        }

        String tmp = property.getProperty(key);
        String[] strs = tmp.split(",");

        return strs;
    }
}