package com.m4u.recharge.restservice.commons.vo;

public class SimpleResponseRS {

    protected Long code;
    protected String message;

    public SimpleResponseRS(Long code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public Long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
