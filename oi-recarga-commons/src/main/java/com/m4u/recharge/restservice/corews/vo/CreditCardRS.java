package com.m4u.recharge.restservice.corews.vo;

public class CreditCardRS {

    protected String cardType;
    protected String last4Digits;
    protected String expirationDate;

    public CreditCardRS(String cardType, String last4Digits, String expirationDate) {
        super();
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.expirationDate = expirationDate;
    }

    public String getCardType() {
        return cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

}
