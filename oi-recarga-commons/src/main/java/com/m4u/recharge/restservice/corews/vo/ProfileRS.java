package com.m4u.recharge.restservice.corews.vo;

import java.util.List;

public class ProfileRS {

    protected Double minimumTopupAmount;
    protected Double topupMonthlyLimit;
    protected List<Double> topupAmountHint;
    protected Integer maximumPrepaidQuantity;
    protected Integer maximumPrepaidChange;
    protected Integer maximumCreditCardQuantity;
    protected Integer maximumCreditCardChange;

    public ProfileRS(Double minimumTopupAmount, Double topupMonthlyLimit, List<Double> topupAmountHint, Integer maximumPrepaidQuantity, Integer maximumPrepaidChange,
            Integer maximumCreditCardQuantity, Integer maximumCreditCardChange) {
        super();
        this.minimumTopupAmount = minimumTopupAmount;
        this.topupMonthlyLimit = topupMonthlyLimit;
        this.topupAmountHint = topupAmountHint;
        this.maximumPrepaidQuantity = maximumPrepaidQuantity;
        this.maximumPrepaidChange = maximumPrepaidChange;
        this.maximumCreditCardQuantity = maximumCreditCardQuantity;
        this.maximumCreditCardChange = maximumCreditCardChange;
    }

    public Double getMinimumTopupAmount() {
        return minimumTopupAmount;
    }

    public Double getTopupMonthlyLimit() {
        return topupMonthlyLimit;
    }

    public List<Double> getTopupAmountHint() {
        return topupAmountHint;
    }

    public Integer getMaximumPrepaidQuantity() {
        return maximumPrepaidQuantity;
    }

    public Integer getMaximumPrepaidChange() {
        return maximumPrepaidChange;
    }
    
    public Integer getMaximumCreditCardQuantity() {
        return maximumCreditCardQuantity;
    }

    public Integer getMaximumCreditCardChange() {
        return maximumCreditCardChange;
    }

}
