package com.m4u.recharge.restservice.corews.vo;

import javax.ws.rs.QueryParam;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;

public class HistoryRequestRS extends SimpleRequestRS {

    private @QueryParam("period") Integer month;

    public HistoryRequestRS() {
    }

    public HistoryRequestRS(Integer channel, String msisdn, Integer month) {
        super(channel, msisdn);
        this.month = month;
    }

    public Integer getMonth() {
        return month;
    }

    @Override
    public String toString() {
        return new StringBuffer("HistoryRequestRS [").append("channel=").append(getChannel()).append(", msisdn=").append(getMsisdn())
                .append(", month:").append(getMonth()).append("]").toString();
    }

}
