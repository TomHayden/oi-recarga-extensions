package com.m4u.recharge.restservice.commons.vo;

import javax.ws.rs.PathParam;

public class SimpleRequestRS {

    @PathParam("channel")
    protected Integer channel;

    @PathParam("msisdn")
    protected String msisdn;

    public SimpleRequestRS() {
    }

    public SimpleRequestRS(Integer channel, String msisdn) {
        super();
        this.channel = channel;
        this.msisdn = msisdn;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "SimpleRequest [channel=" + channel + ", msisdn=" + msisdn + "]";
    }
}
