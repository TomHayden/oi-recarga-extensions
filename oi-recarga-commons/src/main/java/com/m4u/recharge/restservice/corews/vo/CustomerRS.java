package com.m4u.recharge.restservice.corews.vo;

import java.util.List;

public class CustomerRS {

    protected String msisdn;
    protected String registerDate;
    protected Integer status;
    protected List<String> prepaids;
    protected List<CreditCardRS> creditCards;
    protected ProfileRS profile;
    protected ProfileUsageRS profileUsage;
    protected TopupTransactionRS lastTransaction;
    protected List<ScheduleRechargeRS> schedules;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    
    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<String> getPrepaids() {
        return prepaids;
    }

    public void setPrepaids(List<String> prepaids) {
        this.prepaids = prepaids;
    }

    public List<CreditCardRS> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(List<CreditCardRS> creditCards) {
        this.creditCards = creditCards;
    }

    public ProfileRS getProfile() {
        return profile;
    }

    public void setProfile(ProfileRS profile) {
        this.profile = profile;
    }

    public ProfileUsageRS getProfileUsage() {
        return profileUsage;
    }

    public void setProfileUsage(ProfileUsageRS profileUsage) {
        this.profileUsage = profileUsage;
    }

    public TopupTransactionRS getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(TopupTransactionRS lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    public List<ScheduleRechargeRS> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<ScheduleRechargeRS> schedules) {
        this.schedules = schedules;
    }

}
