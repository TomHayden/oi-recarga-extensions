package com.m4u.recharge.restservice.corews.vo;

public class TopupTransactionRS {

    protected String dateTime;
    protected String prepaid;
    protected Double amount;
    protected String cardType;
    protected String last4Digits;
    protected String nsu;
    protected Integer channel;

    public TopupTransactionRS(String dateTime, String prepaid, Double amount, String cardType, String last4Digits, String nsu,
            Integer channel) {
        super();
        this.dateTime = dateTime;
        this.prepaid = prepaid;
        this.amount = amount;
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.nsu = nsu;
        this.channel = channel;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCardType() {
        return cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public String getNsu() {
        return nsu;
    }

    public Integer getChannel() {
        return channel;
    }

}
