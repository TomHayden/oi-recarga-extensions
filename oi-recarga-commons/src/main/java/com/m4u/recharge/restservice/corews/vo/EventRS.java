package com.m4u.recharge.restservice.corews.vo;

public class EventRS {
    
    protected String dateTime;
    protected String description;
    protected String status;
    protected Double amount;
    protected String paymentType;
    protected String prepaid;
    protected String channel;
    protected String obs;
    
    public EventRS(String dateTime, String description, String status, Double amount, String paymentType, String prepaid, String channel,
            String obs) {
        super();
        this.dateTime = dateTime;
        this.description = description;
        this.status = status;
        this.amount = amount;
        this.paymentType = paymentType;
        this.prepaid = prepaid;
        this.channel = channel;
        this.obs = obs;
    }
    public String getDateTime() {
        return dateTime;
    }
    public String getDescription() {
        return description;
    }
    public String getStatus() {
        return status;
    }
    public Double getAmount() {
        return amount;
    }
    public String getPaymentType() {
        return paymentType;
    }
    public String getPrepaid() {
        return prepaid;
    }
    public String getChannel() {
        return channel;
    }
    public String getObs() {
        return obs;
    }

}
