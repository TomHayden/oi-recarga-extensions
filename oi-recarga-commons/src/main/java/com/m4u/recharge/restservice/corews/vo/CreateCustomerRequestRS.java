package com.m4u.recharge.restservice.corews.vo;

import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;

public class CreateCustomerRequestRS {

    @PathParam("channel")
    protected Integer channel;

    @FormParam("msisdn")
    protected String msisdn;

    @FormParam("cardNumber")
    protected String cardNumber;

    @FormParam("expirationDate")
    protected String expirationDate;

    @FormParam("cardToken")
    protected String cardToken;

    @FormParam("email")
    protected String email;

    @FormParam("socialNetworkId")
    protected String socialNetworkId;

    @FormParam("advertising")
    protected Boolean advertising;
    
    public CreateCustomerRequestRS() {
    }

    public CreateCustomerRequestRS(Integer channel, String msisdn, String cardNumber, String expirationDate, String cardToken,
            String email, String socialNetworkId, Boolean advertising) {
        super();
        this.channel = channel;
        this.msisdn = msisdn;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cardToken = cardToken;
        this.email = email;
        this.socialNetworkId = socialNetworkId;
        this.advertising = advertising;
    }

    public Integer getChannel() {
        return channel;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public String getCardToken() {
        return cardToken;
    }

    public String getEmail() {
        return email;
    }

    public String getSocialNetworkId() {
        return socialNetworkId;
    }

    public Boolean getAdvertising() {
        return advertising;
    }

    @Override
    public String toString() {
        
        // CUIDADO: NAO LOGAR cardNumber !!!
        
        return "CreateCustomerRequestRS [channel=" + channel + ", msisdn=" + msisdn + ", expirationDate=" + expirationDate + ", cardToken="
                + cardToken + ", email=" + email + ", socialNetworkId=" + socialNetworkId + ", advertising=" + advertising + "]";
    }

}