package com.m4u.recharge.restservice.corews.vo;

import javax.ws.rs.PathParam;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;

public class TopupStatusRequestRS extends SimpleRequestRS {

    private @PathParam("externalId") String externalId;

    public TopupStatusRequestRS() {
    }
    
    public TopupStatusRequestRS(Integer channel, String msisdn, String externalId) {
        super(channel, msisdn);
        this.externalId = externalId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String toString() {
        return new StringBuffer("TopupStatusRestRequest [").append("channel=").append(getChannel()).append(", msisdn=").append(getMsisdn())
                .append(", externalId:").append(getExternalId()).append("]").toString();
    }

}
