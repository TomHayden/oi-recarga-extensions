package com.m4u.recharge.restservice.corews.vo;

import javax.ws.rs.FormParam;
import javax.ws.rs.MatrixParam;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;
import com.m4u.recharge.util.RechargeUtil;

public class UpdateCreditCardRequestRS extends SimpleRequestRS {
    
    @MatrixParam("cardtype")
    private String cardType;

    @MatrixParam("last4digits")
    private String last4Digits;

    @FormParam("cardNumber")
    private String cardNumber;

    @FormParam("expirationDate")
    private String expirationDate;

    @FormParam("cardToken")
    private String cardToken;
    
    public UpdateCreditCardRequestRS() {
    }
            
    public UpdateCreditCardRequestRS(Integer channel, String msisdn, String cardType, String last4Digits, String cardNumber, String expirationDate) {
        super(channel, msisdn);
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
    }

    public UpdateCreditCardRequestRS(Integer channel, String msisdn, String cardType, String last4Digits, String cardToken) {
        super(channel, msisdn);
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.cardToken = cardToken;
    }
    
    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    @Override
    public String toString() {
        return "UpdateCreditCardRequestRS [cardType=" + cardType + ", last4Digits=" + last4Digits + ", cardNumber=" + RechargeUtil.hideCardNumber(cardNumber)
                + ", expirationDate=" + expirationDate + ", cardToken=" + cardToken + ", channel=" + channel + ", msisdn=" + msisdn + "]";
    }
    
    
}
