package com.m4u.recharge.restservice.corews.vo;

import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;

public class CustomerResponseRS extends SimpleResponseRS {

    protected CustomerRS customer;

    public CustomerResponseRS(Long code, String message, CustomerRS customer) {
        super(code, message);
        this.customer = customer;
    }

    public CustomerRS getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerRS customer) {
        this.customer = customer;
    }

}
