package com.m4u.recharge.restservice.corews.vo;

public class ScheduleRechargeRS {

    protected String id;
    protected String prepaid;
    protected String cardType;
    protected String last4Digits;
    protected Double amount;
    protected Byte day;

    public ScheduleRechargeRS(String id, String prepaid, String cardType, String last4Digits, Double amount, Byte day) {
        super();
        this.id = id;
        this.prepaid = prepaid;
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.amount = amount;
        this.day = day;
    }

    public String getId() {
        return id;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public String getCardType() {
        return cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public Double getAmount() {
        return amount;
    }

    public Byte getDay() {
        return day;
    }

}
