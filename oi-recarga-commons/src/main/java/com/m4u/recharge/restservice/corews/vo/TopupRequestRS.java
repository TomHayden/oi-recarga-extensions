package com.m4u.recharge.restservice.corews.vo;

import javax.ws.rs.FormParam;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;

public class TopupRequestRS extends SimpleRequestRS {
    
    @FormParam("prepaid")
    private String prepaid;

    @FormParam("amount")
    private Double amount;

    @FormParam("cardType")
    private String cardType;

    @FormParam("last4Digits")
    private String last4Digits;

    @FormParam("cvv")
    private String cvv;

    @FormParam("externalId")
    private String externalId;
    
    public TopupRequestRS() {
    }
            
    public TopupRequestRS(Integer channel, String msisdn, String prepaid, Double amount, String cardType, String last4Digits, String cvv,
            String externalId) {
        super(channel, msisdn);
        this.prepaid = prepaid;
        this.amount = amount;
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.cvv = cvv;
        this.externalId = externalId;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String toString() {
        return new StringBuffer("TopupRestRequest [").append("channel=").append(getChannel()).append(", msisdn=").append(getMsisdn())
                .append(", prepaid=").append(getPrepaid()).append(", amount=").append(getAmount()).append(", cardType")
                .append(getCardType()).append(", last4Digits=").append(getLast4Digits()).append(", externalId:").append(getExternalId())
                .append("]").toString();
    }
}
