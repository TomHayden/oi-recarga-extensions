package com.m4u.recharge.restservice.corews.vo;

import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;

public class TransactionStatusResponseRS extends SimpleResponseRS {

    protected TopupStatusRS topupStatus;

    public TransactionStatusResponseRS(Long code, String message, TopupStatusRS topupStatus) {
        super(code, message);
        this.topupStatus = topupStatus;
    }

    public TopupStatusRS getTopupStatus() {
        return topupStatus;
    }

}
