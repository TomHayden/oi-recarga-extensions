package com.m4u.recharge.restservice.corews.vo;

import javax.ws.rs.FormParam;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;

public class AddPrepaidRequestRS extends SimpleRequestRS {

    @FormParam("prepaid")
    private String prepaid;

    public AddPrepaidRequestRS() {
    }

    public AddPrepaidRequestRS(Integer channel, String msisdn, String prepaid) {
        super(channel, msisdn);
        this.prepaid = prepaid;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    @Override
    public String toString() {
        return "AddPrepaidRequestRS [prepaid=" + prepaid + ", channel=" + channel + ", msisdn=" + msisdn + "]";
    }

}
