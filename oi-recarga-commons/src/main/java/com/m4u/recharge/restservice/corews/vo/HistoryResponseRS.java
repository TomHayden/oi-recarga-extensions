package com.m4u.recharge.restservice.corews.vo;

import java.util.List;

import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;

public class HistoryResponseRS extends SimpleResponseRS {

    protected List<EventRS> events;

    public HistoryResponseRS(Long code, String message, List<EventRS> events) {
        super(code, message);
        this.events = events;
    }

    public List<EventRS> getEvents() {
        return events;
    }

}
