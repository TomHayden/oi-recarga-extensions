package com.m4u.recharge.restservice.corews.vo;

public class TopupStatusRS {

    protected String code;
    protected String description;
    
    public TopupStatusRS(String code, String description) {
        super();
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
