package com.m4u.recharge.restservice.corews.vo;

import java.util.List;

public class ProfileUsageRS {

    protected double topupAvailableBalance;
    protected List<Double> topupAvailableAmountHint;
    protected int prepaidQuantity;
    protected int prepaidChange;
    protected int creditCardQuantity;
    protected int creditCardChange;

    public ProfileUsageRS(double topupAvailableBalance, List<Double> topupAvailableAmountHint, int prepaidQuantity, int prepaidChange,
            int creditCardQuantity, int creditCardChange) {
        super();
        this.topupAvailableBalance = topupAvailableBalance;
        this.topupAvailableAmountHint = topupAvailableAmountHint;
        this.prepaidQuantity = prepaidQuantity;
        this.prepaidChange = prepaidChange;
        this.creditCardQuantity = creditCardQuantity;
        this.creditCardChange = creditCardChange;
    }

    public double getTopupAvailableBalance() {
        return topupAvailableBalance;
    }
    
    public List<Double> getTopupAvailableAmountHint() {
        return topupAvailableAmountHint;
    }

    public int getPrepaidQuantity() {
        return prepaidQuantity;
    }

    public int getPrepaidChange() {
        return prepaidChange;
    }

    public int getCreditCardQuantity() {
        return creditCardQuantity;
    }

    public int getCreditCardChange() {
        return creditCardChange;
    }
    
}
