package com.m4u.recharge.commons.enums;

public enum DateTypeEnum {

    MONTHS("MONTHS"), DAYS("DAYS"), YEARS("YEARS"), HOURS("HOURS"), MINUTES("MINUTES"), SECONDS("SECONDS"), WEEKS("WEEKS");

    private DateTypeEnum(String description) {
        this.description = description;
    }

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
