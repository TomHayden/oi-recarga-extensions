package com.m4u.recharge.commons.enums;


public enum PaymentType {

    CREDITCARD_VISA("VISA"), CREDITCARD_MASTERCARD("MASTERCARD"), CREDITCARD_DINERS("DINERS"), CREDITCARD_ELO("ELO"), CREDITCARD_HIPERCARD(
            "HIPERCARD"), CREDITCARD_AMEX("AMEX"), MPAYMENT_PAGGO("OI PAGGO"), MPAYMENT_PAGGO_SMS("PAGGO SMS"), MPAYMENT_MILAO("CC OI"), DEBITONLINE_ITAU(
            "ITAU SHOPLINE"), DEBITONLINE_BBRASIL("BBRASIL SHOPLINE"), DEBITONLINE_BRADESCO("BRADESCO SHOPLINE");

    private String description;

    PaymentType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public static PaymentType parse(String description) {
        for (PaymentType t : values()) {
            if (t.description.equals(description))
                return t;
        }
        throw new IllegalArgumentException("No matching constant for " + description);
    }

}
