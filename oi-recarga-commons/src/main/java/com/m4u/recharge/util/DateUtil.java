package com.m4u.recharge.util;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;

import com.m4u.recharge.commons.enums.DateTypeEnum;
import com.m4u.recharge.exception.RechargeApplicationException;

/**
 * @author raphael.castro
 * 
 */
public class DateUtil {

    private static DecimalFormat monthFormat = new DecimalFormat("00");

    private static Logger logger = Logger.getLogger(DateUtil.class);

    private static final String DEFAULT_FORMAT_DATE = "dd/MM/yyyy";

    public static final SimpleDateFormat FORMAT_DD_MM_YYYY = new SimpleDateFormat(DEFAULT_FORMAT_DATE);

    /**
     * Recebe
     * 
     * @param y
     * @return
     */
    public static int convertTwoDigitsYearToFour(char digitOne, char digitTwo) {
        String vv = String.valueOf(digitOne).concat(String.valueOf(digitTwo));
        int y = Integer.parseInt(vv);
        Calendar aux = Calendar.getInstance();
        int year = Integer.parseInt(String.valueOf(aux.get(Calendar.YEAR)).substring(2, 4));
        if (y < year) {
            return Integer.parseInt("20" + vv);
        } else {
            return Integer.parseInt("19" + vv);
        }
    }

    /**
     * Retorna a idade
     * 
     * @param birthDate
     * @return
     */
    public static int age(Calendar birthDate) {

        Calendar today = Calendar.getInstance();

        int res = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);
        if ((birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH))
                || (birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH) && birthDate.get(Calendar.DAY_OF_MONTH) > today
                        .get(Calendar.DAY_OF_MONTH))) {
            res--;
        }
        return res;
    }

    /**
     * Metodo obtem a diferenca em dias entre duas datas
     * 
     * @param startDate
     * @param endDate
     * @return
     */
    public static Long dateDiff(Date startDate, Date endDate) {

        Calendar date1 = new GregorianCalendar();
        date1.setTime(startDate);
        Calendar date2 = new GregorianCalendar();
        date2.setTime(endDate);

        // Get difference in milliseconds
        long diffMillis = date2.getTimeInMillis() - date1.getTimeInMillis();

        // Get difference in days
        long diffDays = diffMillis / (24 * 60 * 60 * 1000);

        return diffDays;
    }

    /**
     * Metodo obtem a diferenca em horas entre duas datas
     * 
     * @param startDate
     * @param endDate
     * @return
     */
    public static Long dateDiffInHours(Date startDate, Date endDate) {

        Calendar date1 = new GregorianCalendar();
        date1.setTime(startDate);
        Calendar date2 = new GregorianCalendar();
        date2.setTime(endDate);

        if (date1.after(date2)) {
            return null;
        }

        // Get difference in milliseconds
        long diffMillis = date2.getTimeInMillis() - date1.getTimeInMillis();

        // Get difference in hours
        long diffHours = diffMillis / (60 * 60 * 1000);

        return diffHours;
    }

    /**
     * Metodo obtem a diferenca em minutos entre duas datas
     * 
     * @param startDate
     * @param endDate
     * @return
     */
    public static Long dateDiffInMinutes(Date startDate, Date endDate) {

        Calendar date1 = new GregorianCalendar();
        date1.setTime(startDate);
        Calendar date2 = new GregorianCalendar();
        date2.setTime(endDate);

        if (date1.after(date2)) {
            return null;
        }

        // Get difference in milliseconds
        long diffMillis = date2.getTimeInMillis() - date1.getTimeInMillis();

        // Get difference in minutes
        long diffMinutes = diffMillis / (60 * 1000);

        return diffMinutes;
    }

    /**
     * Metodo obtem a diferenca em segundos entre duas datas
     * 
     * @param startDate
     * @param endDate
     * @return
     */
    public static Long dateDiffInSeconds(Date startDate, Date endDate) {
        
        Calendar date1 = new GregorianCalendar();
        date1.setTime(startDate);
        Calendar date2 = new GregorianCalendar();
        date2.setTime(endDate);
        
        // Get difference in milliseconds
        long diffMillis = date2.getTimeInMillis() - date1.getTimeInMillis();
        
        // Get difference in seconds
        long diffMinutes = diffMillis / (1000);
        
        return diffMinutes;
    }
    
    /**
     * Metodo retorna dia da semana em portugues correspondente a data passada como parametro
     * 
     * @param date
     * @return
     */
    public static String getDayOfWeek(Date date) {

        String dayOfWeek = null;
        Calendar calendar = GregorianCalendar.getInstance();

        if (date != null) {
            calendar.setTime(date);
            int dayCode = calendar.get(Calendar.DAY_OF_WEEK);

            switch (dayCode) {
            case 1:
                dayOfWeek = "domingo";
                break;
            case 2:
                dayOfWeek = "segunda-feira";
                break;
            case 3:
                dayOfWeek = "terca-feira";
                break;
            case 4:
                dayOfWeek = "quarta-feira";
                break;
            case 5:
                dayOfWeek = "quinta-feira";
                break;
            case 6:
                dayOfWeek = "sexta-feira";
                break;
            case 7:
                dayOfWeek = "sabado";
                break;
            }
        }

        return dayOfWeek;
    }

    /**
     * Metodo que retorna o nome do mes em portugues referente a uma data passada por parametro.
     * 
     * @param date
     * @return
     */
    public static String getMonthOfYear(Date date) {
        String monthOfYear = null;

        Calendar calendar = GregorianCalendar.getInstance();

        if (date != null) {
            calendar.setTime(date);
            int monthCode = calendar.get(Calendar.MONTH);

            switch (monthCode) {
            case 0:
                monthOfYear = "Janeiro";
                break;
            case 1:
                monthOfYear = "Fevereiro";
                break;
            case 2:
                monthOfYear = "Marco";
                break;
            case 3:
                monthOfYear = "Abril";
                break;
            case 4:
                monthOfYear = "Maio";
                break;
            case 5:
                monthOfYear = "Junho";
                break;
            case 6:
                monthOfYear = "Julho";
                break;
            case 7:
                monthOfYear = "Agosto";
                break;
            case 8:
                monthOfYear = "Setembro";
                break;
            case 9:
                monthOfYear = "Outubro";
                break;
            case 10:
                monthOfYear = "Novembro";
                break;
            case 11:
                monthOfYear = "Dezembro";
                break;
            }
        }

        return monthOfYear;

    }

    /**
     * Metodo retorna nova data baseada na soma de um quatidade de dias. Ex.: 30/12/2007 + 4 dias = 03/01/2008; Permite a diminuicao de dias
     * quando passados numeros negativos
     * 
     * @return
     */
    public static Date getDatePlusDays(Date date, int days) {
        // Obtem dia em milisegundos
        long tempSeconds = 1000;
        long tempMinutes = tempSeconds * 60;
        long tempHours = tempMinutes * 60;
        long oneDaysInMilliSeconds = tempHours * 24;
        // Obtem valor que representa a quantidade de dias
        long plusDays = oneDaysInMilliSeconds * days;
        Date newDate = new Date(date.getTime() + plusDays);
        return newDate;
    }

    /**
     * Converte uma String no formato dd/MM/yyyy para java.util.Date
     * 
     * @param date
     * @return
     * @throws ParseException
     */
    public static Date stringToDate(String date) throws ParseException {
        Date dateR = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat(DEFAULT_FORMAT_DATE);
        dateR = fmt.parse(date);
        return dateR;
    }

    /**
     * Converte uma String no formato passado como parametro para java.util.Date
     * 
     * @param date
     * @return
     * @throws ParseException
     */
    public static Date stringToDate(String date, String formato) throws ParseException {
        Date dateR = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat(formato);
        dateR = fmt.parse(date);

        return dateR;
    }

    /**
     * Converte uma Data java,util.Date para String no formato padrao dd/MM/yyyy
     * 
     * @param Date
     * @return
     * @throws ParseException
     */
    public static String dateToString(Date date) throws ParseException {
        SimpleDateFormat fmt = new SimpleDateFormat(DEFAULT_FORMAT_DATE);
        String dateR = fmt.format(date);
        return dateR;
    }

    /**
     * Converte uma Data java,util.Date para String no formato passado como parametro. Ex.: dd/MM/yyyy
     * 
     * @param Date
     * @return
     * @throws ParseException
     */
    public static String dateToString(Date date, String format) {
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        String dateR = fmt.format(date);
        return dateR;
    }

    /**
     * Retorna a ultima data do mes corrente
     * 
     * @param Date
     * @return Date
     */
    public static Date getLastDayOfCurrentMonth(Date data) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.add(Calendar.HOUR_OF_DAY, 23);
        calendar.add(Calendar.MINUTE, 59);
        calendar.add(Calendar.SECOND, 59);

        return calendar.getTime();
    }

    /**
     * Converte data em String no formato MMyy para o tipo java.util.Date
     * 
     * @param data
     * @return
     * @throws RechargeApplicationException
     */
    public static Date convertValidityCCardToDate(String data) throws RechargeApplicationException {

        try {

            if (data == null || data.length() == 0) {
                throw new RechargeApplicationException("Data de validade do cartao invalida");
            }
            if (data.length() != 4) {
                throw new RechargeApplicationException("Data de validade do cartao invalida");
            }

            String month = data.substring(0, 2);
            String year = data.substring(2, 4);

            int mm = Integer.parseInt(month);
            int yyyy = 2000 + Integer.parseInt(year);
            if (mm < 1 || mm > 12) {
                throw new RechargeApplicationException("O mes da validade do cartao esta incorreto");
            }

            Date validade = stringToDate(monthFormat.format(mm) + String.valueOf(yyyy), "MMyyyy");
            return getLastDayOfCurrentMonth(validade);

        } catch (RechargeApplicationException e) {
            throw e;
        } catch (Exception e) {
            throw new RechargeApplicationException("Erro ao fazer o parse da data de validade do cartao");
        }

    }

    /**
     * Converte parametros month (MM) e year (yyyy) em java.util.Date correspondente a validade de cartao.
     * 
     * @param month
     * @param year
     * @return
     * @throws RechargeApplicationException
     */
    public static Date convertValidityCCardToDate(int month, int year) throws RechargeApplicationException {

        try {

            if (month < 1 || month > 12) {
                throw new RechargeApplicationException("O mes da validade do cartao esta incorreto");
            }

            Date validade = stringToDate(monthFormat.format(month) + String.valueOf(year), "MMyyyy");
            return getLastDayOfCurrentMonth(validade);

        } catch (RechargeApplicationException e) {
            throw e;
        } catch (Exception e) {
            throw new RechargeApplicationException("Erro ao fazer o parse da data de validade do cartao");
        }

    }
    
    public static boolean validateDates(String date) {
        String dataSemBarra = date.replaceAll("/", "");
        if (!GenericValidator.isBlankOrNull(dataSemBarra)) {
            if (RechargeUtil.numbersOnly(dataSemBarra).length() < 8) {
                return false;
            }
            Pattern p = Pattern
                    .compile("^((((0?[1-9]|[12]\\d|3[01])(0?[13578]|1[02])((1[6-9]|[2-9]\\d)?\\d{2}))|((0?[1-9]|[12]\\d|30)(0?[13456789]|1[012])((1[6-9]|[2-9]\\d)?\\d{2}))|((0?[1-9]|1\\d|2[0-8])0?2((1[6-9]|[2-9]\\d)?\\d{2}))|(290?2((1[6-9]|[2-9]\\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\\d)?\\d{2}))|((0[1-9]|[12]\\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\\d)?\\d{2}))|((0[1-9]|1\\d|2[0-8])02((1[6-9]|[2-9]\\d)?\\d{2}))|(2902((1[6-9]|[2-9]\\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$");
            Matcher m = p.matcher(dataSemBarra);

            if (!m.matches()) {
                return false;
            }
        } else {
            return false;
        }
        DateTime givenDateTime = new DateTime(parseDate(dataSemBarra));
        int year = givenDateTime.getYear();
        if (year < 1900) {
            return false;
        }

        return true;
    }

    public static Date parseDate(String date) {
        Date givenDate = null;

        try {
            date = date.replace("/", "");
            givenDate = new SimpleDateFormat("ddMMyyyy").parse(date);

        } catch (Exception e) {
            logger.error(MessageFormat.format("Error during Date parsing [{0}].", date));
            throw new IllegalArgumentException(e.getMessage(), e);
        }

        return givenDate;
    }

    public static int getMonthFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        return month;
    }

    public static int getYearFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        return year;
    }

    public static int differenceBetweenDates(Date source, Date target, DateTypeEnum type) {
        int difference = 0;
        DateTime start = new DateTime(source);
        DateTime end = new DateTime(target);

        if (source.after(target)) {
            start = end;
            end = new DateTime(source);
        }

        switch (type) {
        case MONTHS:
            difference = Months.monthsBetween(start, end).getMonths();
            break;
        case DAYS:
            difference = Days.daysBetween(start, end).getDays();
            break;
        case YEARS:
            difference = Years.yearsBetween(start, end).getYears();
            break;
        case HOURS:
            difference = Hours.hoursBetween(start, end).getHours();
            break;
        case MINUTES:
            difference = Minutes.minutesBetween(start, end).getMinutes();
            break;
        case SECONDS:
            difference = Seconds.secondsBetween(start, end).getSeconds();
            break;
        case WEEKS:
            difference = Weeks.weeksBetween(start, end).getWeeks();
            break;
        }
        return difference;
    }

    public static Date getDateTime(Date date, DateTypeEnum type, int amount) {
        Calendar cal = Calendar.getInstance();
        Date data = new Date();
        switch (type) {
        case MONTHS:
            cal.add(Calendar.MONTH, amount);
            data = cal.getTime();
            break;
        case DAYS:
            cal.add(Calendar.DAY_OF_MONTH, amount);
            data = cal.getTime();
            break;
        case YEARS:
            cal.add(Calendar.YEAR, amount);
            data = cal.getTime();
            break;
        case HOURS:
            cal.add(Calendar.HOUR_OF_DAY, amount);
            data = cal.getTime();
            break;
        case MINUTES:
            cal.add(Calendar.MINUTE, amount);
            data = cal.getTime();
            break;
        case SECONDS:
            cal.add(Calendar.SECOND, amount);
            data = cal.getTime();
            break;
        case WEEKS:
            cal.add(Calendar.WEEK_OF_MONTH, amount);
            data = cal.getTime();
            break;
        }
        return data;
    }
    
}