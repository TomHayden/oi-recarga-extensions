/*
 * Created on Sep 9, 2009 by tom.hayden for Oi Recarga
 * 
 * Copyright M4U Solucoes 2001-2009
 */
package com.m4u.recharge.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.validator.GenericValidator;

import com.m4u.recharge.commons.enums.PaymentType;

/**
 * Esta classe deve conter funcoes auxiliares utilizadas em varios pontos do
 * sistema.
 * 
 * @author tom.hayden
 * 
 */
public class RechargeUtil {

    private static Locale ptBR = new Locale("pt", "BR");

    public static String decimalToString(double number) {
        return decimalToString(number, "###,###,##0.00");
    }

    public static String decimalToString(double number, String pattern) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(ptBR);
        DecimalFormat df = new DecimalFormat(pattern, symbols);
        return df.format(number);
    }

    public static String moneyToString(double money) {
        return moneyToString(money, "##0.00");
    }
    
    public static String moneyToString(double money, String pattern) {
        NumberFormat amountFormatter = NumberFormat.getCurrencyInstance(ptBR);
        return amountFormatter.format(money);
    }
    
    public static String numberWithZeroPadded(int number, String pattern) {
        DecimalFormat numberFormatter = new DecimalFormat(pattern);
        return numberFormatter.format(number);
    }
    
    public static String getValueFromText(String delimiters, String text) {
        text = text.trim();
        text = text.substring(0, text.length() - 3);
        return text.replaceAll(delimiters, "");
    }

    public static String getCVVFromText(String text) {
        text = text.trim();
        return text.substring(text.length() - 3, text.length());
    }

    public static String getPaymentType(String cardType) {
        if (cardType.equalsIgnoreCase("VISA")) {
            return PaymentType.CREDITCARD_VISA.getDescription();
        } else if (cardType.equalsIgnoreCase("Mastercard")) {
            return PaymentType.CREDITCARD_MASTERCARD.getDescription();
        } else if (cardType.equalsIgnoreCase("Diners")){
            return PaymentType.CREDITCARD_DINERS.getDescription();
        } else {
            return PaymentType.CREDITCARD_ELO.getDescription();
        }
    }

    public static String hideCardNumber(String cardNumber) {
        String result = "";

        if (cardNumber != null && cardNumber.length() > 0) {
            result = cardNumber.substring(0, 6) + "********" + cardNumber.substring(cardNumber.length() - 4, cardNumber.length());
        }
        return result;
    }

    public static String numbersOnly(String str) {
        String result = new String();
        String validChars = "0123456789";
        Character c;
        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);
            if (validChars.indexOf(c) >= 0) {
                result += c;
            }
        }
        return result;
    }

    public static double stringToDecimal(String number) {
        String valor = number.substring(0, number.length() - 2);
        String centavos = number.substring(number.length() - 2, number.length());
        number = valor + "." + centavos;
        return Double.parseDouble(number);
    }

    public static String truncate(String str, int length) {
        String result = null;
        if (str != null) {
            if (str.length() <= length) {
                result = str;
            } else {
                result = str.substring(0, length);
            }
        }
        return result;
    }
    
    public static String removeSpecialChars(String string) {
        string = string.trim();
        string = string.replaceAll("[() -\\./]", "");
        return string;
    }
    
    public static boolean validaCPF(String cpf) {

        if (!GenericValidator.isBlankOrNull(cpf) && GenericValidator.isLong(cpf) && GenericValidator.minLength(cpf, 11)) {
            if (cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222") || cpf.equals("33333333333")
                    || cpf.equals("44444444444") || cpf.equals("55555555555") || cpf.equals("66666666666") || cpf.equals("77777777777")
                    || cpf.equals("88888888888") || cpf.equals("99999999999"))
                return (false);

            int d1, d2;
            int digito1, digito2, resto;
            int digitoCPF;
            String nDigResult;

            d1 = d2 = 0;
            digito1 = digito2 = resto = 0;

            for (int nCount = 1; nCount < cpf.length() - 1; nCount++) {
                digitoCPF = Integer.valueOf(cpf.substring(nCount - 1, nCount)).intValue();

                // multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.
                d1 = d1 + (11 - nCount) * digitoCPF;

                // para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior.
                d2 = d2 + (12 - nCount) * digitoCPF;
            }
            ;

            // Primeiro resto da divisão por 11.
            resto = (d1 % 11);

            // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
            if (resto < 2)
                digito1 = 0;
            else
                digito1 = 11 - resto;

            d2 += 2 * digito1;

            // Segundo resto da divisão por 11.
            resto = (d2 % 11);

            // Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
            if (resto < 2)
                digito2 = 0;
            else
                digito2 = 11 - resto;

            // Digito verificador do CPF que está sendo validado.
            String nDigVerific = cpf.substring(cpf.length() - 2, cpf.length());

            // Concatenando o primeiro resto com o segundo.
            nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

            // comparar o digito verificador do cpf com o primeiro resto + o segundo resto.
            return nDigVerific.equals(nDigResult);
        }
        return false;
    }
    
    public static String getPrepaid(String ddd, String prepaid) {
        ddd = removeSpecialChars(ddd);
        prepaid = removeSpecialChars(prepaid);
        StringBuffer sb = new StringBuffer();
        return sb.append(ddd).append(prepaid).toString();
    }
    
}
