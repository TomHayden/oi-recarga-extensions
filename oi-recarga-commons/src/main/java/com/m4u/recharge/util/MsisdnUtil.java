package com.m4u.recharge.util;

public class MsisdnUtil {

    public static String formatMsisdn(String msisdn) {
        if (msisdn != null && (msisdn.length() == 10 || msisdn.length() == 11)) {
            return "55" + msisdn;
        } else {
            return msisdn;
        }
    }
    
}
