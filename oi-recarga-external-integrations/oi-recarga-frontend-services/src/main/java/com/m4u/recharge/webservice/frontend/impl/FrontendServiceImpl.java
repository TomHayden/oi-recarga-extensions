package com.m4u.recharge.webservice.frontend.impl;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.m4u.logging.ApplicationLogger;
import com.m4u.recharge.client.core.CoreRestConnectorFactory;
import com.m4u.recharge.client.core.restservice.schedule.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;
import com.m4u.recharge.restservice.corews.vo.AddPrepaidRequestRS;
import com.m4u.recharge.restservice.corews.vo.CreateCustomerRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupStatusRequestRS;
import com.m4u.recharge.restservice.corews.vo.UpdateCreditCardRequestRS;

@Path("/frontend/v3/channel/{channel}")
@Consumes(MediaType.TEXT_PLAIN)
public class FrontendServiceImpl {

    private static final Logger logger = Logger.getLogger(FrontendServiceImpl.class);

    @GET
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getCustomer(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn) {

        ApplicationLogger.step("CONSULTA_CLIENTE").channel(String.valueOf(channel)).param("msisdn", msisdn).log();

        SimpleRequestRS request = new SimpleRequestRS(channel, msisdn);

        logger.info("getCustomer request " + request);
        Response response = CoreRestConnectorFactory.createCoreRSConnector().getCustomer(request);

        return createRestResponse(response);
    }

    @POST
    @Path("/customers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createCustomer(@PathParam("channel") Integer channel, @FormParam("msisdn") String msisdn,
            @FormParam("cardNumber") String cardNumber, @FormParam("expirationDate") String expirationDate,
            @FormParam("cardToken") String cardToken, @FormParam("email") String email,
            @FormParam("socialNetworkId") String socialNetworkId, @FormParam("advertising") boolean advertising) {

        ApplicationLogger.step("CADASTRA_CLIENTE").channel(String.valueOf(channel)).customer(msisdn).ccToken(cardToken)
                .param("email", email).param("socialNetworkId", socialNetworkId).param("advertising", advertising).log();

        CreateCustomerRequestRS request = new CreateCustomerRequestRS(channel, msisdn, cardNumber, expirationDate, cardToken, email,
                socialNetworkId, advertising);

        Response response = CoreRestConnectorFactory.createCoreRSConnector().createCustomer(request);

        return createRestResponse(response);

    }

    @DELETE
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response cancelCustomer(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn) {

        ApplicationLogger.step("CANCELA_CADASTRO_CLIENTE").channel(String.valueOf(channel)).customer(msisdn).log();

        SimpleRequestRS request = new SimpleRequestRS(channel, msisdn);

        logger.info("cancelCustomer request " + request);
        Response response = CoreRestConnectorFactory.createCoreRSConnector().cancelCustomer(request);

        return createRestResponse(response);
    }

    @GET
    @Path("/customers/{msisdn}/topups/{externalId}/status")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getTopupStatusByExternalId(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @PathParam("externalId") String externalId) {

        ApplicationLogger.step("CONSULTA_STATUS_TRANSACAO").channel(String.valueOf(channel)).param("msisdn", msisdn)
                .param("externalId", externalId).log();
        TopupStatusRequestRS request = new TopupStatusRequestRS(channel, msisdn, externalId);

        logger.info("getTopupStatusByExternalId request " + request);
        Response response = CoreRestConnectorFactory.createCoreRSConnector().getTopupStatus(request);
        return createRestResponse(response);
    }

    @POST
    @Path("/customers/{msisdn}/topups")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response topupRequest(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @FormParam("prepaid") String prepaid, @FormParam("amount") Double amount, @FormParam("cardType") String cardType,
            @FormParam("last4Digits") String last4Digits, @FormParam("cvv") String cvv, @FormParam("externalId") String externalId) {

        ApplicationLogger.step("SOLICITACAO_RECARGA").channel(String.valueOf(channel)).param("msisdn", msisdn).param("prepaid", prepaid)
                .amount(amount).ccBrand(cardType).ccLast4Digits(last4Digits).param("externalId", externalId).log();
        TopupRequestRS request = new TopupRequestRS(channel, msisdn, prepaid, amount, cardType, last4Digits, cvv, externalId);

        logger.info("topupRequest request " + request);
        Response response = CoreRestConnectorFactory.createCoreRSConnector().topupRequest(request);
        return createRestResponse(response);

    }

    @POST
    @Path("/customers/{msisdn}/prepaids")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addPrepaid(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @FormParam("prepaid") String prepaid) {

        ApplicationLogger.step("CADASTRO_PREPAGO").channel(String.valueOf(channel)).param("msisdn", msisdn).param("prepaid", prepaid).log();
        AddPrepaidRequestRS request = new AddPrepaidRequestRS(channel, msisdn, prepaid);

        logger.info("addPrepaid request " + request);
        Response response = CoreRestConnectorFactory.createCoreRSConnector().addPrepaid(request);
        return createRestResponse(response);

    }

    @POST
    @Path("/customers/{msisdn}/schedules")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addScheduleRecharge(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @FormParam("prepaid") String prepaid, @FormParam("amount") Double amount, @FormParam("cardType") String cardType,
            @FormParam("last4Digits") String last4Digits, @FormParam("day") Byte day) {

        ApplicationLogger.step("CADASTRA_RECARGA_PROGRAMADA").channel(String.valueOf(channel)).customer(msisdn).param("prepaid", prepaid)
                .amount(amount).ccBrand(cardType).ccLast4Digits(last4Digits).param("day", day).log();

        AddScheduledRechargeRequest request = new AddScheduledRechargeRequest();
        request.setChannel(channel);
        request.setMsisdn(msisdn);
        request.setPrepaid(prepaid);
        request.setAmount(amount);
        request.setCardType(cardType);
        request.setLast4Digits(last4Digits);
        request.setDay(day);
        request.setSubscriptionType("AUTO_SERVICO");

        logger.info("addScheduleRecharge request " + request);

        Response response = CoreRestConnectorFactory.createSheduleServiceConnector().addScheduleRecharge(request);
        return createRestResponse(response);

    }

    @DELETE
    @Path("/customers/{msisdn}/schedules/{scheduleid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response removeScheduleById(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @PathParam("scheduleid") String scheduleId) {

        ApplicationLogger.step("REMOVE_RECARGA_PROGRAMADA").channel(String.valueOf(channel)).customer(msisdn)
                .param("scheduleId", scheduleId).log();

        RemoveScheduleByIdRequest request = new RemoveScheduleByIdRequest();
        request.setChannel(channel);
        request.setMsisdn(msisdn);
        request.setScheduleId(scheduleId);

        logger.info("removeScheduleId request " + request);

        Response response = CoreRestConnectorFactory.createSheduleServiceConnector().removeScheduleById(request);
        return createRestResponse(response);

    }

    @PUT
    @Path("/customers/{msisdn}/schedules/{scheduleid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateScheduledRecharge(@PathParam(value = "channel") Integer channel, @PathParam(value = "msisdn") String msisdn,
            @PathParam(value = "scheduleid") String scheduledId, @FormParam(value = "amount") Double amount,
            @FormParam(value = "day") Byte birthday, @FormParam(value = "cardType") String cardType,
            @FormParam(value = "last4Digits") String last4Digits) {

        ApplicationLogger.step("ATUALIZA_RECARGA_PROGRAMADA").channel(String.valueOf(channel)).customer(msisdn)
                .param("scheduleid", scheduledId).param("amount", amount).param("birthday", birthday).param("cardType", cardType)
                .param("last4Digits", last4Digits).log();

        UpdateScheduledRechargeRequest request = new UpdateScheduledRechargeRequest();
        request.setAmount(amount);
        request.setBirthday(birthday);
        request.setCardType(cardType);
        request.setChannel(channel);
        request.setLast4Digits(last4Digits);
        request.setMsisdn(msisdn);
        request.setScheduledId(scheduledId);

        logger.info("removeScheduleId request " + request);

        Response response = CoreRestConnectorFactory.createSheduleServiceConnector().updateScheduledRecharge(request);
        return createRestResponse(response);
    }

    @PUT
    @Path("/customers/{msisdn}/creditcards")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateCreditCard(@PathParam(value = "channel") Integer channel, @PathParam(value = "msisdn") String msisdn,
            @MatrixParam(value = "cardtype") String cardType, @MatrixParam(value = "last4digits") String last4Digits,
            @FormParam("cardNumber") String cardNumber, @FormParam("expirationDate") String expirationDate,
            @FormParam("cardToken") String cardToken) {

        ApplicationLogger.step("ATUALIZA_CARTAO_CREDITO").channel(String.valueOf(channel)).customer(msisdn)
                .param("last4Digits", last4Digits).log();

        UpdateCreditCardRequestRS request = new UpdateCreditCardRequestRS();
        request.setChannel(channel);
        request.setMsisdn(msisdn);
        request.setCardType(cardType);
        request.setLast4Digits(last4Digits);
        request.setCardNumber(cardNumber);
        request.setExpirationDate(expirationDate);
        request.setCardToken(cardToken);

        logger.info("updateCreditCard request " + request);

        Response response = CoreRestConnectorFactory.createCoreRSConnector().updateCreditCard(request);
        return createRestResponse(response);
    }

    private Response createRestResponse(Response response) {
        if (response != null) {
            String json = response.readEntity(String.class);
            return Response.status(response.getStatus()).type(MediaType.APPLICATION_JSON).entity(json).build();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

}