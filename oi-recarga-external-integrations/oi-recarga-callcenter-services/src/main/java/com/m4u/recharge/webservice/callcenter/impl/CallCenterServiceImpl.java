package com.m4u.recharge.webservice.callcenter.impl;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.m4u.logging.ApplicationLogger;
import com.m4u.recharge.client.core.CoreRestConnectorFactory;
import com.m4u.recharge.client.core.restservice.schedule.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleByIdRequest;
import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;
import com.m4u.recharge.restservice.corews.vo.HistoryRequestRS;

@Path("/callcenter/v1/channel/{channel}")
@Consumes(MediaType.TEXT_PLAIN)
public class CallCenterServiceImpl {

    private static final Logger logger = Logger.getLogger(CallCenterServiceImpl.class);

    @GET
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response getCustomer(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn) {

        ApplicationLogger.step("CONSULTA_CLIENTE").channel(String.valueOf(channel)).param("msisdn", msisdn).log();

        SimpleRequestRS request = new SimpleRequestRS(channel, msisdn);

        logger.info("getCustomer request " + request);
        Response response = CoreRestConnectorFactory.createCoreRSConnector().getCustomer(request);

        return createRestResponse(response);
    }

    @DELETE
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response cancelCustomer(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn) {

        ApplicationLogger.step("CANCELA_CADASTRO_CLIENTE").channel(String.valueOf(channel)).customer(msisdn).log();

        SimpleRequestRS request = new SimpleRequestRS(channel, msisdn);

        logger.info("cancelCustomer request " + request);
        Response response = CoreRestConnectorFactory.createCoreRSConnector().cancelCustomer(request);

        return createRestResponse(response);
    }

    @POST
    @Path("/customers/{msisdn}/schedules")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response addScheduleRecharge(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @FormParam("prepaid") String prepaid, @FormParam("amount") Double amount, @FormParam("cardType") String cardType,
            @FormParam("last4Digits") String last4Digits, @FormParam("day") Byte day) {

        ApplicationLogger.step("CADASTRA_RECARGA_PROGRAMADA").channel(String.valueOf(channel)).customer(msisdn).param("prepaid", prepaid)
                .amount(amount).ccBrand(cardType).ccLast4Digits(last4Digits).param("day", day).log();
        
        AddScheduledRechargeRequest request = new AddScheduledRechargeRequest();
        request.setChannel(channel);
        request.setMsisdn(msisdn);
        request.setPrepaid(prepaid);
        request.setAmount(amount);
        request.setCardType(cardType);
        request.setLast4Digits(last4Digits);
        request.setDay(day);
        
        logger.info("addScheduleRecharge request " + request);
        
        Response response = CoreRestConnectorFactory.createSheduleServiceConnector().addScheduleRecharge(request); 
        return createRestResponse(response);
        
    }    
    
    @DELETE
    @Path("/customers/{msisdn}/schedules/{scheduleid}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response removeScheduleById(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @PathParam("scheduleid") String scheduleId) {

        ApplicationLogger.step("REMOVE_RECARGA_PROGRAMADA").channel(String.valueOf(channel)).customer(msisdn)
                .param("scheduleId", scheduleId).log();

        RemoveScheduleByIdRequest request = new RemoveScheduleByIdRequest();
        request.setChannel(channel);
        request.setMsisdn(msisdn);
        request.setScheduleId(scheduleId);

        logger.info("removeScheduleId request " + request);

        Response response = CoreRestConnectorFactory.createSheduleServiceConnector().removeScheduleById(request);
        return createRestResponse(response);

    }

    @GET
    @Path("/customers/{msisdn}/history")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHistory(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @QueryParam("period") Integer month) {

        ApplicationLogger.step("CONSULTA_HISTORICO_CLIENTE").channel(String.valueOf(channel)).customer(msisdn).param("period", month).log();

        HistoryRequestRS request = new HistoryRequestRS(channel, msisdn, month);

        Response response = CoreRestConnectorFactory.createCoreRSConnector().getHistory(request);

        return createRestResponse(response);

    }

    private Response createRestResponse(Response response) {
        if (response != null) {
            String json = response.readEntity(String.class);
            // logger.info("getTopupStatus response " + json );
            return Response.status(response.getStatus()).type(MediaType.APPLICATION_JSON).entity(json).build();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

}
