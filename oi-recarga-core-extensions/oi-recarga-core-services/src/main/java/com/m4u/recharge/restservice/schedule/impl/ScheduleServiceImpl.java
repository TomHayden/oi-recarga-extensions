package com.m4u.recharge.restservice.schedule.impl;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.m4u.recharge.core.service.MainService;
import com.m4u.recharge.core.service.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.core.service.vo.GetScheduledRechargeResponse;
import com.m4u.recharge.core.service.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.core.service.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.restservice.commons.vo.RequestAndResponseConverter;
import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;
import com.m4u.recharge.util.MsisdnUtil;

@Path("/channel/{channel}")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class ScheduleServiceImpl {

    private static final Logger logger = Logger.getLogger(ScheduleServiceImpl.class);
    private RequestAndResponseConverter converter = new RequestAndResponseConverter();

    @POST
    @Path("/customers/{msisdn}/schedules")
    public Response addScheduleRecharge(@PathParam(value = "channel") Integer channel, @PathParam(value = "msisdn") String msisdn,
            @FormParam(value = "prepaid") String prepaid, @FormParam(value = "cardType") String cardType,
            @FormParam(value = "last4Digits") String last4Digits, @FormParam(value = "amount") Double amount,
            @FormParam(value = "day") Byte day, @FormParam(value = "subscriptionType") String subscriptionType) {

        msisdn = MsisdnUtil.formatMsisdn(msisdn);
        AddScheduledRechargeRequest addScheduledRechargeRequest = new AddScheduledRechargeRequest(channel, msisdn, prepaid, cardType,
                last4Digits, amount, day, subscriptionType);

        SimpleResponseRS response = getMainService().addScheduledRecharge(addScheduledRechargeRequest);

        return converter.createRestResponse(response);

    }

    @GET
    @Path("/schedules/{scheduleid}")
    public Response getCustomerScheduleById(@PathParam("scheduleid") String scheduleId) {

        GetScheduledRechargeResponse response = getMainService().getScheduleById(scheduleId);

        return converter.createRestResponse(response);

    }

    @PUT
    @Path("/customers/{msisdn}/schedules/{scheduleid}")
    public Response updateScheduledRecharge(@PathParam("channel") Integer channel, @PathParam("msisdn") String msisdn,
            @PathParam("scheduleid") String scheduledId, @FormParam(value = "amount") Double amount,
            @FormParam(value = "day") Byte birthday, @FormParam(value = "cardType") String cardType,
            @FormParam(value = "last4Digits") String last4Digits) {

        msisdn = MsisdnUtil.formatMsisdn(msisdn);
        UpdateScheduledRechargeRequest updateScheduledRecharge = new UpdateScheduledRechargeRequest(channel, msisdn, scheduledId, amount,
                birthday, cardType, last4Digits);

        SimpleResponseRS response = getMainService().updateScheduledRecharge(updateScheduledRecharge);

        return converter.createRestResponse(response);

    }

    @DELETE
    @Path("/customers/{msisdn}/schedules/{scheduleid}")
    public Response removeScheduleById(@PathParam("channel") Integer channelId, @PathParam("scheduleid") String scheduleId) {

        SimpleResponseRS response = getMainService().removeSchedule(scheduleId, channelId);

        return converter.createRestResponse(response);

    }

    @DELETE
    @Path("/schedules/{scheduleid}")
    public Response removeSchedule(@PathParam("channel") Integer channelId, @PathParam("scheduleid") String scheduleId) {

        SimpleResponseRS response = getMainService().removeSchedule(scheduleId, channelId);

        return converter.createRestResponse(response);

    }

    @PUT
    @Path("/schedules/{scheduleid}/evaluation")
    public Response verifyTopupElegibility(@PathParam(value = "channel") Integer channel, @FormParam(value = "cycleId") Long cycleId,
            @FormParam(value = "serviceId") String serviceId, @FormParam(value = "cycleStepId") String cycleStepId,
            @PathParam(value = "scheduleid") String scheduleId, @FormParam(value = "cardToken") String cardToken,
            @FormParam(value = "prepaid") String prepaid, @FormParam(value = "reloadAmount") Double reloadAmount) {

        SimpleResponseRS response = getMainService().verifyScheduleEligibitiy(channel, cycleId, serviceId, scheduleId, cycleStepId,
                cardToken, prepaid, reloadAmount);

        return converter.createRestResponse(response);

    }

    @PUT
    @Path("/schedules/{scheduleid}/notify")
    public Response proccessCallbackNotifications(@FormParam(value = "cycleId") Long cycleId,
            @FormParam(value = "serviceId") String serviceId, @PathParam(value = "scheduleid") String scheduleId,
            @FormParam(value = "cycleStepId") String cycleStepId, @FormParam(value = "description") String description,
            @FormParam(value = "payload") String payload, @FormParam(value = "created") String created) {

        ScheduledRechargeEventRequest request = new ScheduledRechargeEventRequest(cycleId, description, payload, scheduleId, cycleStepId, serviceId, created);
        SimpleResponseRS response = getMainService().processCallbackNotifications(request);
        return converter.createRestResponse(response);
    }

    private MainService getMainService() {
        InitialContext context;
        try {
            context = new InitialContext();
            return (MainService) context.lookup(MainService.JNDI_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Erro ao realizar lookup no JNDI: " + MainService.JNDI_NAME, e);
        }
    }

}
