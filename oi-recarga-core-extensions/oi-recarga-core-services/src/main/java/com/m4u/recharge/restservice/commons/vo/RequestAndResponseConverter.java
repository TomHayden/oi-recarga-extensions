package com.m4u.recharge.restservice.commons.vo;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.restservice.corews.impl.ResponseMapping;
import com.m4u.recharge.restservice.corews.vo.AddPrepaidRequestRS;
import com.m4u.recharge.restservice.corews.vo.CreateCustomerRequestRS;
import com.m4u.recharge.restservice.corews.vo.CreditCardRS;
import com.m4u.recharge.restservice.corews.vo.CustomerRS;
import com.m4u.recharge.restservice.corews.vo.CustomerResponseRS;
import com.m4u.recharge.restservice.corews.vo.ProfileRS;
import com.m4u.recharge.restservice.corews.vo.ProfileUsageRS;
import com.m4u.recharge.restservice.corews.vo.ScheduleRechargeRS;
import com.m4u.recharge.restservice.corews.vo.TopupRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupStatusRS;
import com.m4u.recharge.restservice.corews.vo.TopupStatusRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupTransactionRS;
import com.m4u.recharge.restservice.corews.vo.TransactionStatusResponseRS;
import com.m4u.recharge.restservice.corews.vo.UpdateCreditCardRequestRS;
import com.m4u.recharge.webservice.core.vo.AddPrepaidRequest;
import com.m4u.recharge.webservice.core.vo.CancelCustomerRequest;
import com.m4u.recharge.webservice.core.vo.CreateCustomerRequest;
import com.m4u.recharge.webservice.core.vo.CreateCustomerWithTokenRequest;
import com.m4u.recharge.webservice.core.vo.CreditCard;
import com.m4u.recharge.webservice.core.vo.CustomerDetail;
import com.m4u.recharge.webservice.core.vo.CustomerResponse;
import com.m4u.recharge.webservice.core.vo.SimpleRequest;
import com.m4u.recharge.webservice.core.vo.SimpleResponse;
import com.m4u.recharge.webservice.core.vo.TopupRequest;
import com.m4u.recharge.webservice.core.vo.TopupStatus;
import com.m4u.recharge.webservice.core.vo.TopupTransaction;
import com.m4u.recharge.webservice.core.vo.TransactionStatusRequest;
import com.m4u.recharge.webservice.core.vo.TransactionStatusResponse;
import com.m4u.recharge.webservice.core.vo.UpdateCreditcardRequest;

public class RequestAndResponseConverter {

    private static final Logger logger = Logger.getLogger(RequestAndResponseConverter.class);

    private Gson gson = new Gson();

    public Response createRestResponse(SimpleResponseRS response) {
        if (response != null) {
            Status httpStatus = ResponseMapping.getRightHttpStatusFor(response.getCode());
            Response resp = Response.status(httpStatus).type(MediaType.APPLICATION_JSON).entity(gson.toJson(response)).build();
            logger.info("createRestResponse: " + resp);
            return resp;
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

    public SimpleRequest convertRequest(SimpleRequestRS request) {
        SimpleRequest getCustomerRequest = new SimpleRequest();
        getCustomerRequest.setMsisdn(request.getMsisdn());
        getCustomerRequest.setChannel(request.getChannel());
        return getCustomerRequest;
    }
    
    public CreateCustomerRequest convertRequest(CreateCustomerRequestRS request) {
        CreateCustomerRequest createCustomerRequest = new CreateCustomerRequest();
        createCustomerRequest.setChannel(request.getChannel());
        createCustomerRequest.setMsisdn(request.getMsisdn());
        createCustomerRequest.setCardNumber(request.getCardNumber());
        createCustomerRequest.setExpirationDate(request.getExpirationDate());
        createCustomerRequest.setEmail(request.getEmail());
        createCustomerRequest.setSocialNetworkId(request.getSocialNetworkId());
        createCustomerRequest.setAdvertising(request.getAdvertising());
        return createCustomerRequest;
    }
    
    public UpdateCreditcardRequest convertRequest(UpdateCreditCardRequestRS request) {
        UpdateCreditcardRequest updateCreditCardRequest = new UpdateCreditcardRequest();
        updateCreditCardRequest.setChannel(request.getChannel());
        updateCreditCardRequest.setMsisdn(request.getMsisdn());
        updateCreditCardRequest.setCurrentCardType(request.getCardType());
        updateCreditCardRequest.setCurrentFinal4Digits(request.getLast4Digits());
        updateCreditCardRequest.setNewCardNumber(request.getCardNumber());
        updateCreditCardRequest.setNewExpirationDate(request.getExpirationDate());
        updateCreditCardRequest.setRandomAnswer(" ");
        updateCreditCardRequest.setRandomQuestion(0);
        return updateCreditCardRequest;
    }
    
    public CreateCustomerWithTokenRequest convertRequestCreateWithToken(CreateCustomerRequestRS request) {
        CreateCustomerWithTokenRequest createCustomerRequest = new CreateCustomerWithTokenRequest();
        createCustomerRequest.setChannel(request.getChannel());
        createCustomerRequest.setMsisdn(request.getMsisdn());
        createCustomerRequest.setCardToken(request.getCardToken());
        createCustomerRequest.setEmail(request.getEmail());
        createCustomerRequest.setSocialNetworkId(request.getSocialNetworkId());
        createCustomerRequest.setAdvertising(request.getAdvertising());
        return createCustomerRequest;
    }
    
    public CancelCustomerRequest convertRequestCancelCustomer(SimpleRequestRS request) {
        CancelCustomerRequest cancelCustomerRequest = new CancelCustomerRequest();
        cancelCustomerRequest.setChannel(request.getChannel());
        cancelCustomerRequest.setMsisdn(request.getMsisdn());
        cancelCustomerRequest.setRandomAnswer(" ");
        cancelCustomerRequest.setRandomQuestion(0);
        return cancelCustomerRequest;
    }

    public TransactionStatusRequest convertRequest(TopupStatusRequestRS request) {
        TransactionStatusRequest txStatusRequest = new TransactionStatusRequest();
        txStatusRequest.setChannel(request.getChannel());
        txStatusRequest.setMsisdn(request.getMsisdn());
        txStatusRequest.setExternalId(request.getExternalId());
        txStatusRequest.setChannel(request.getChannel());
        return txStatusRequest;
    }

    public TopupRequest convertRequest(TopupRequestRS request) {
        TopupRequest topupRequest = new TopupRequest();
        topupRequest.setChannel(request.getChannel());
        topupRequest.setMsisdn(request.getMsisdn());
        topupRequest.setExternalId(request.getExternalId());
        topupRequest.setChannel(request.getChannel());
        topupRequest.setPrepaid(request.getPrepaid());
        topupRequest.setAmount(request.getAmount());
        topupRequest.setCardType(request.getCardType());
        topupRequest.setFinal4Digits(request.getLast4Digits());
        topupRequest.setCvv(request.getCvv());
        topupRequest.setExternalId(request.getExternalId());
        return topupRequest;
    }

    public AddPrepaidRequest convertRequest(AddPrepaidRequestRS request) {
        AddPrepaidRequest addPrepaidRequest = new AddPrepaidRequest();
        addPrepaidRequest.setChannel(request.getChannel());
        addPrepaidRequest.setMsisdn(request.getMsisdn());
        addPrepaidRequest.setChannel(request.getChannel());
        addPrepaidRequest.setPrepaid(request.getPrepaid());
        return addPrepaidRequest;
    }
    
    public SimpleResponseRS convertResponse(SimpleResponse response) {
        Long code = response.getCode();
        String message = response.getMessage();
        return new SimpleResponseRS(code, message);
    }
    
    public TransactionStatusResponseRS convertResponse(TransactionStatusResponse response) {
        Long code = response.getCode();
        String message = response.getMessage();
        TopupStatusRS topupStatus = getTopupStatus(response.getTopupStatus());
        return new TransactionStatusResponseRS(code, message, topupStatus);
    }

    private TopupStatusRS getTopupStatus(TopupStatus topupStatus) {
        return new TopupStatusRS(topupStatus.getCode(), topupStatus.getDescription());
    }

    public CustomerResponseRS convertResponse(CustomerResponse response) {
        Long code = response.getCode();
        String message = response.getMessage();
        CustomerRS customer = getCustomer(response.getCustomerDetail());
        return new CustomerResponseRS(code, message, customer);
    }

    private CustomerRS getCustomer(CustomerDetail customerDetail) {
        CustomerRS customer = null;
        if (customerDetail != null) {
            customer = new CustomerRS();
            customer.setMsisdn(customerDetail.getMsisdn());
            customer.setRegisterDate(customerDetail.getRegisterDate());
            customer.setStatus(customerDetail.getStatus());
            customer.setPrepaids(getPrepaids(customerDetail.getPrepaid()));
            customer.setLastTransaction(getLastTransaction(customerDetail.getLastTransaction()));
            customer.setCreditCards(getCreditCards(customerDetail.getCreditCard()));
            customer.setProfile(getProfile(customerDetail));

            double topupAvailableBalance = customerDetail.getTopupAvailableBalance();
            List<Double> topupAvailableAmountHint = customerDetail.getTopupAmountHint();
            int prepaidQuantity = customer.getPrepaids().size();
            int prepaidChange = customerDetail.getPrepaidChange();
            int creditCardQuantity = customer.getCreditCards().size();
            int creditCardChange = customerDetail.getCreditCardChange();

            customer.setProfileUsage(new ProfileUsageRS(topupAvailableBalance, topupAvailableAmountHint, prepaidQuantity, prepaidChange,
                    creditCardQuantity, creditCardChange));
        }
        return customer;
    }

    private List<String> getPrepaids(List<String> prepaids) {
        List<String> result = null;
        if (prepaids != null) {
            result = new LinkedList<String>();
            for (String prepaid : prepaids) {
                result.add(prepaid);
            }
        }
        return result;
    }

    private List<CreditCardRS> getCreditCards(List<CreditCard> creditCards) {
        List<CreditCardRS> result = null;
        if (creditCards != null) {
            result = new LinkedList<CreditCardRS>();
            for (CreditCard cc : creditCards) {
                result.add(new CreditCardRS(cc.getCardType(), cc.getLast4Digits(), cc.getExpirationDate()));
            }
        }
        return result;
    }

    private TopupTransactionRS getLastTransaction(TopupTransaction tx) {
        TopupTransactionRS result = null;
        if (tx != null) {
            result = new TopupTransactionRS(tx.getDateTime(), tx.getPrepaid(), tx.getAmount(), tx.getCardType(), tx.getLast4Digits(),
                    tx.getNsu(), tx.getChannel());
        }
        return result;
    }

    private ProfileRS getProfile(CustomerDetail customerDetail) {
        Double minimumTopupAmount = customerDetail.getMinimumTopupAmount();
        Double topupMonthlyLimit = customerDetail.getTopupMonthlyLimit();
        List<Double> topupAmountHint = customerDetail.getTopupProfileAmountHint();
        Integer maximumPrepaidQuantity = customerDetail.getMaximumPrepaidQuantity();
        Integer maximumPrepaidChange = customerDetail.getMaximumPrepaidChange();
        Integer maximumCreditCardQuantity = customerDetail.getMaximumCreditCardQuantity();
        Integer maximumCreditCardChange = customerDetail.getMaximumCreditCardChange();

        ProfileRS result = new ProfileRS(minimumTopupAmount, topupMonthlyLimit, topupAmountHint, maximumPrepaidQuantity,
                maximumPrepaidChange, maximumCreditCardQuantity, maximumCreditCardChange);
        return result;
    }

    public List<ScheduleRechargeRS> convertList(List<ScheduledRecharge> schedules) {
        List<ScheduleRechargeRS> result = null;

        if (schedules != null && !schedules.isEmpty()) {
            result = new LinkedList<ScheduleRechargeRS>();
            for (ScheduledRecharge schedule : schedules) {
                String id = schedule.getId();
                String prepaid = schedule.getCustomerPrepaid().getPrepaid();
                String cardType = schedule.getCustomerCreditCard().getCardType();
                String last4Digits = schedule.getCustomerCreditCard().getLast4Digits();
                Double amount = schedule.getAmount();
                Byte day = schedule.getRechargeDay();
                ScheduleRechargeRS rechargeRS = new ScheduleRechargeRS(id, prepaid, cardType, last4Digits, amount, day);
                result.add(rechargeRS);
            }
        }

        return result;
    }

}
