package com.m4u.recharge.restservice.corews.impl;

import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.Form;

import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.service.MainService;
import com.m4u.recharge.restservice.commons.vo.RequestAndResponseConverter;
import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;
import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;
import com.m4u.recharge.restservice.corews.vo.AddPrepaidRequestRS;
import com.m4u.recharge.restservice.corews.vo.CreateCustomerRequestRS;
import com.m4u.recharge.restservice.corews.vo.CustomerResponseRS;
import com.m4u.recharge.restservice.corews.vo.HistoryRequestRS;
import com.m4u.recharge.restservice.corews.vo.HistoryResponseRS;
import com.m4u.recharge.restservice.corews.vo.TopupRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupStatusRequestRS;
import com.m4u.recharge.restservice.corews.vo.TransactionStatusResponseRS;
import com.m4u.recharge.restservice.corews.vo.UpdateCreditCardRequestRS;
import com.m4u.recharge.util.MsisdnUtil;
import com.m4u.recharge.webservice.core.CoreWebService;
import com.m4u.recharge.webservice.core.vo.CustomerResponse;
import com.m4u.recharge.webservice.core.vo.SimpleResponse;
import com.m4u.recharge.webservice.core.vo.TransactionStatusResponse;

@Path("/channel/{channel}")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public class CoreRestServiceImpl {
    private static final Logger logger = Logger.getLogger(CoreRestServiceImpl.class);
    private RequestAndResponseConverter converter = new RequestAndResponseConverter();

    @GET
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@Form SimpleRequestRS request) {

        logger.info("getCustomer request " + request);
        CustomerResponse customerResponse = getCoreWS().getCustomer(converter.convertRequest(request));

        CustomerResponseRS response = converter.convertResponse(customerResponse);
        this.appendSchedulesIfExist(response);

        return converter.createRestResponse(response);

    }
    
    private void appendSchedulesIfExist(CustomerResponseRS response) {

        Long code = response.getCode();
        // Evitando chamadas desnecessarias ao Banco
        if (code == 1000 || code == 1001 || code == 1002) {
            List<ScheduledRecharge> schedules = getMainService().getSchedulesByMsisdn(response.getCustomer().getMsisdn());
            response.getCustomer().setSchedules(converter.convertList(schedules));
        }

    }
    
    @POST
    @Path("/customers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(@Form CreateCustomerRequestRS request) {
        
        CustomerResponse customerResponse = null;
        
        if (request.getCardToken() != null) {
            
            if (!request.getCardToken().isEmpty()) {
                customerResponse = getCoreWS().createCustomerWithToken(converter.convertRequestCreateWithToken(request));
            } 
        } else {
            customerResponse = getCoreWS().createCustomer(converter.convertRequest(request));
        }
        
        CustomerResponseRS response = converter.convertResponse(customerResponse);
        return converter.createRestResponse(response);
    }

    @DELETE
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelCustomer(@Form SimpleRequestRS request) {
        
        logger.info("cancelCustomer request " + request);
        SimpleResponse simpleResponse = getCoreWS().cancelCustomer(converter.convertRequestCancelCustomer(request));
        
        SimpleResponseRS response = converter.convertResponse(simpleResponse);
        return converter.createRestResponse(response);
    }

    @GET
    @Path("/customers/{msisdn}/history")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHistory(@Form HistoryRequestRS request) {
        
        logger.info("getHistory request " + request);
        String msisdn = MsisdnUtil.formatMsisdn(request.getMsisdn());
        HistoryResponseRS response = getMainService().getHistoryByMsisdnAndPeriod(msisdn, request.getMonth());
        
        return converter.createRestResponse(response);
        
    }
    
    @GET
    @Path("/customers/{msisdn}/schedules")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSchedules(@Form SimpleRequestRS request) {
        
        logger.info("getSchedules request " + request);
        // TODO Obter historico do novo servico a ser implementado
        HistoryResponseRS response = null;
        return converter.createRestResponse(response);
        
    }
    
    @GET
    @Path("/customers/{msisdn}/topups/{externalId}/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTopupStatus(@Form TopupStatusRequestRS request) {

        logger.info("getTopupStatus request " + request);
        TransactionStatusResponse transactionStatusResponse = getCoreWS().getTransactionStatus(converter.convertRequest(request));
        TransactionStatusResponseRS response = converter.convertResponse(transactionStatusResponse);
        return converter.createRestResponse(response);

    }

    @POST
    @Path("/customers/{msisdn}/topups")
    @Produces(MediaType.APPLICATION_JSON)
    public Response topupRequest(@Form TopupRequestRS request) {

        logger.info("topupRequest request " + request);
        SimpleResponse simpleResponse = getCoreWS().requestTopup(converter.convertRequest(request));
        SimpleResponseRS response = converter.convertResponse(simpleResponse);
        return converter.createRestResponse(response);

    }

    @POST
    @Path("/customers/{msisdn}/prepaids")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPrepaid(@Form AddPrepaidRequestRS request) {
        
        logger.info("addPrepaid request " + request);
        CustomerResponse customerResponse = getCoreWS().addPrepaid(converter.convertRequest(request));
        CustomerResponseRS response = converter.convertResponse(customerResponse);
        this.appendSchedulesIfExist(response);
        return converter.createRestResponse(response);
        
    }
    
    @PUT
    @Path("/customers/{msisdn}/creditcards")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCreditCard(@Form UpdateCreditCardRequestRS request) {

        logger.info("updateCreditCard request " + request);
        CustomerResponse customerResponse = getCoreWS().updateCreditCard(converter.convertRequest(request));
        CustomerResponseRS response = converter.convertResponse(customerResponse);
        this.appendSchedulesIfExist(response);
        return converter.createRestResponse(response);

    }

    private CoreWebService getCoreWS() {
        try {
            CoreWebService coreWS = (CoreWebService) new InitialContext().lookup(CoreWebService.JNDI_REMOTE);
            return coreWS;
        } catch (NamingException e) {
            logger.error("delegate(): " + e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private MainService getMainService() {
        InitialContext context;
        try {
            context = new InitialContext();
            return (MainService) context.lookup(MainService.JNDI_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Erro ao realizar lookup no JNDI: " + MainService.JNDI_NAME, e);
        }
    }

}
