package com.m4u.recharge.restservice.sms;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import com.m4u.recharge.core.service.SMSManager;
import com.m4u.recharge.core.service.vo.SMSMessage;

@Path("/channel/{channel}/sms")
@Consumes(MediaType.APPLICATION_JSON)
public class SMSRestService {
    
    //private static final Logger logger = Logger.getLogger(SMSService.class);

    @PUT
    @Path("/outbox")
    public void sendSMS(@FormParam("from") String from, @FormParam("to") String to, @FormParam("text") String text) {

        SMSMessage sms = new SMSMessage(from, to, text);
        getSMSService().sendSMS(sms);
        
    }

    @PUT
    @Path("/inbox")
    public void receiveSMS(@FormParam("from") String from, @FormParam("to") String to, @FormParam("text") String text) {
        
        SMSMessage sms = new SMSMessage(from, to, text);
        getSMSService().receiveSMS(sms);

    }
    
    private SMSManager getSMSService() {
        InitialContext context;
        try {
            context = new InitialContext();
            return (SMSManager) context.lookup(SMSManager.JNDI_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Erro ao realizar lookup no JNDI: " + SMSManager.JNDI_NAME, e);
        }
    }

}
