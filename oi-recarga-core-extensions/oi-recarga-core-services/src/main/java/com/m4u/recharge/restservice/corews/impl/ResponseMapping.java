package com.m4u.recharge.restservice.corews.impl;

import javax.ws.rs.core.Response.Status;

import com.m4u.recharge.webservice.util.ServiceMessage;

public enum ResponseMapping {
	
	CUSTOMER_NEW(ServiceMessage.CUSTOMER_NEW, Status.NOT_FOUND),
	CUSTOMER_REGISTERED_WITHOUT_RECHARGE(ServiceMessage.CUSTOMER_REGISTERED_WITHOUT_RECHARGE, Status.OK),
	CUSTOMER_REGISTERED_WITH_RECHARGE(ServiceMessage.CUSTOMER_REGISTERED_WITH_RECHARGE, Status.OK),
	REGISTER_PENDENT(ServiceMessage.REGISTER_PENDENT, Status.BAD_REQUEST),
	CUSTOMER_BLACKLIST(ServiceMessage.CUSTOMER_BLACKLIST, Status.BAD_REQUEST),
	CUSTOMER_TEMPORARY_BLOCK(ServiceMessage.CUSTOMER_TEMPORARY_BLOCK, Status.BAD_REQUEST),
	CUSTOMER_CHANGES_LIMIT_EXCEEDS(ServiceMessage.CUSTOMER_CHANGES_LIMIT_EXCEEDS, Status.BAD_REQUEST),
	REGISTER_INTERVAL_MIN(ServiceMessage.REGISTER_INTERVAL_MIN, Status.BAD_REQUEST),
	SUCCESS_REGISTER(ServiceMessage.SUCCESS_REGISTER, Status.CREATED),
	CARD_REQUIRED(ServiceMessage.CARD_REQUIRED, Status.BAD_REQUEST),
	CARD_INVALID(ServiceMessage.CARD_INVALID, Status.BAD_REQUEST),
	CARD_WAS_REGISTER(ServiceMessage.CARD_WAS_REGISTER, Status.BAD_REQUEST),
	EXPIRATION_DATE_REQUIRED(ServiceMessage.EXPIRATION_DATE_REQUIRED, Status.BAD_REQUEST),
	EXPIRATION_DATE_INVALID(ServiceMessage.EXPIRATION_DATE_INVALID, Status.BAD_REQUEST),
	EXPIRATION_DATE_EXPIRED(ServiceMessage.EXPIRATION_DATE_EXPIRED, Status.BAD_REQUEST),
	CPF_REQUIRED(ServiceMessage.CPF_REQUIRED, Status.BAD_REQUEST),
	CPF_INVALID(ServiceMessage.CPF_INVALID, Status.BAD_REQUEST),
	CPF_DUPLICATED(ServiceMessage.CPF_DUPLICATED, Status.BAD_REQUEST),
	BIRTH_REQUIRED(ServiceMessage.BIRTH_REQUIRED, Status.BAD_REQUEST),
	BIRTH_INVALID(ServiceMessage.BIRTH_INVALID, Status.BAD_REQUEST),
	BIRTH_DENIED(ServiceMessage.BIRTH_DENIED, Status.BAD_REQUEST),
	MSISDN_REQUIRED(ServiceMessage.MSISDN_REQUIRED, Status.BAD_REQUEST),
	MSISDN_INVALID(ServiceMessage.MSISDN_INVALID, Status.BAD_REQUEST),
	ID_SOCIAL_NETWORK_REQUIRED(ServiceMessage.ID_SOCIAL_NETWORK_REQUIRED, Status.BAD_REQUEST),
	EMAIL_REQUIRED(ServiceMessage.EMAIL_REQUIRED, Status.BAD_REQUEST),
	EMAIL_INVALID(ServiceMessage.EMAIL_INVALID, Status.BAD_REQUEST),
	SOCIAL_NETWORK_ID_ALREADY_ASSOCIATED(ServiceMessage.SOCIAL_NETWORK_ID_ALREADY_ASSOCIATED, Status.CONFLICT),
	CARD_VALIDATION_MAX_LIMIT(ServiceMessage.CARD_VALIDATION_MAX_LIMIT, Status.BAD_REQUEST),
	CUSTOMER_ALREADY_REGISTERED(ServiceMessage.CUSTOMER_ALREADY_REGISTERED, Status.CONFLICT),
	SUCCESS_RECHARGE(ServiceMessage.SUCCESS_RECHARGE, Status.ACCEPTED),
	PROCESSING_RECHARGE(ServiceMessage.PROCESSING_RECHARGE, Status.OK),
	LAST_RECHARGE_FAULT(ServiceMessage.LAST_RECHARGE_FAULT, Status.OK),
	CUSTOMER_NOT_FOUND(ServiceMessage.CUSTOMER_NOT_FOUND, Status.NOT_FOUND),
	UNSUPPORTED_CARD(ServiceMessage.UNSUPPORTED_CARD, Status.BAD_REQUEST),
	PREPAID_BLACKLIST(ServiceMessage.PREPAID_BLACKLIST, Status.BAD_REQUEST),
	CREDIT_CARD_BLACKLIST(ServiceMessage.CREDIT_CARD_BLACKLIST, Status.BAD_REQUEST),
	CREDIT_CARD_NOT_FOUND(ServiceMessage.CREDIT_CARD_NOT_FOUND, Status.BAD_REQUEST),
	PREPAID_NOT_FOUND(ServiceMessage.PREPAID_NOT_FOUND, Status.BAD_REQUEST),
	RECHARGE_REPEAT_INTERVAL_MIN(ServiceMessage.RECHARGE_REPEAT_INTERVAL_MIN, Status.BAD_REQUEST),
	RECHARGE_MAX_VALUE_EXCEED(ServiceMessage.RECHARGE_MAX_VALUE_EXCEED, Status.BAD_REQUEST),
	RECHARGE_MAX_QUANTITY_EXCEED(ServiceMessage.RECHARGE_MAX_QUANTITY_EXCEED, Status.BAD_REQUEST),
	RECHARGE_MIN_VALUE(ServiceMessage.RECHARGE_MIN_VALUE, Status.BAD_REQUEST),
	RECHARGE_TRANSACTION_MAX_VALUE(ServiceMessage.RECHARGE_TRANSACTION_MAX_VALUE, Status.BAD_REQUEST),
	CARD_TYPE_REQUIRED(ServiceMessage.CARD_TYPE_REQUIRED, Status.BAD_REQUEST),
	CARD_END_NUMBER_REQUIRED(ServiceMessage.CARD_END_NUMBER_REQUIRED, Status.BAD_REQUEST),
	CARD_END_NUMBER_INVALID(ServiceMessage.CARD_END_NUMBER_INVALID, Status.BAD_REQUEST),
	CVV_REQUIRED(ServiceMessage.CVV_REQUIRED, Status.BAD_REQUEST),
	EXTERNAL_ID_DUPLICATED(ServiceMessage.EXTERNAL_ID_DUPLICATED, Status.CONFLICT),
	SCHEDULED_RECHARGE_FOUND(ServiceMessage.SCHEDULED_RECHARGE_FOUND, Status.OK),
	SCHEDULED_RECHARGE_NOT_FOUND(ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND, Status.NOT_FOUND),
	SCHEDULED_RECHARGE_DOES_NOT_MATCH(ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND, Status.NOT_FOUND),
	SCHEDULED_RECHARGE_ADDED(ServiceMessage.SCHEDULED_RECHARGE_ADDED, Status.CREATED),
	SCHEDULED_RECHARGE_ALREADY_REGISTERED(ServiceMessage.SCHEDULED_RECHARGE_ALREADY_REGISTERED, Status.CONFLICT),
	SCHEDULED_RECHARGE_REMOVED(ServiceMessage.SCHEDULED_RECHARGE_REMOVED, Status.OK),
	SCHEDULED_RECHARGE_ELEGIBILITY(ServiceMessage.SCHEDULED_RECHARGE_ELEGIBILITY, Status.OK),
	SCHEDULED_RECHARGE_NOT_ELEGIBILITY(ServiceMessage.SCHEDULED_RECHARGE_NOT_ELEGIBILITY, Status.FORBIDDEN),
	SCHEDULED_RECHARGE_WITH_INACTIVE_CARD(ServiceMessage.SCHEDULED_RECHARGE_WITH_INACTIVE_CARD, Status.NOT_ACCEPTABLE),
	SCHEDULED_RECHARGE_WITH_INACTIVE_PREPAID(ServiceMessage.SCHEDULED_RECHARGE_WITH_INACTIVE_PREPAID, Status.NOT_ACCEPTABLE),
	SCHEDULED_RECHARGE_CALLBACK_RECEIVED(ServiceMessage.SCHEDULED_RECHARGE_CALLBACK_RECEIVED, Status.OK),
	SCHEDULED_RECHARGE_MAX_DAY_CHANGES_REACHED(ServiceMessage.SCHEDULED_RECHARGE_MAX_DAY_CHANGES_REACHED, Status.BAD_REQUEST),
	SCHEDULED_RECHARGE_MAX_AMOUNT_CHANGES_REACHED(ServiceMessage.SCHEDULED_RECHARGE_MAX_AMOUNT_CHANGES_REACHED, Status.BAD_REQUEST),
	SCHEDULED_RECHARGE_UPDATED(ServiceMessage.SCHEDULED_RECHARGE_UPDATED, Status.OK),
	RECHARGE_INTERVAL_MIN(ServiceMessage.RECHARGE_INTERVAL_MIN, Status.BAD_REQUEST),
	CREDIT_CARD_ADDED(ServiceMessage.CREDIT_CARD_ADDED, Status.OK),
	CREDIT_CARD_REPLACED(ServiceMessage.CREDIT_CARD_REPLACED, Status.OK),
	CREDIT_CARD_REMOVED(ServiceMessage.CREDIT_CARD_REMOVED, Status.OK),
	CREDIT_CARD_UPDATED(ServiceMessage.CREDIT_CARD_UPDATED, Status.OK),
	CREDIT_CARD_TYPE_NOT_MATCH(ServiceMessage.CREDIT_CARD_TYPE_NOT_MATCH, Status.BAD_REQUEST),
	CARD_CHANGE_LIMIT(ServiceMessage.CARD_CHANGE_LIMIT, Status.BAD_REQUEST),
	CARD_SIZE_LIMIT(ServiceMessage.CARD_SIZE_LIMIT, Status.BAD_REQUEST),
	CARD_REGISTER_ON_USER(ServiceMessage.CARD_REGISTER_ON_USER, Status.CONFLICT),
	TOKEN_SENT(ServiceMessage.TOKEN_SENT, Status.OK),
	TOKEN_NOT_SENT(ServiceMessage.TOKEN_NOT_SENT, Status.NOT_MODIFIED),
	TOKEN_VALID(ServiceMessage.TOKEN_VALID, Status.OK),
	TOKEN_INVALID(ServiceMessage.TOKEN_INVALID, Status.BAD_REQUEST),
	TOKEN_REQUIRED(ServiceMessage.TOKEN_REQUIRED, Status.BAD_REQUEST),
	PREPAID_SUCCESS_ADDED(ServiceMessage.PREPAID_SUCCESS_ADDED, Status.OK),
	DDD_NOT_MATCH(ServiceMessage.DDD_NOT_MATCH, Status.BAD_REQUEST),
	PREPAID_REQUIRED(ServiceMessage.PREPAID_REQUIRED, Status.BAD_REQUEST),
	PREPAID_INVALID(ServiceMessage.PREPAID_INVALID, Status.BAD_REQUEST),
	PREPAID_VALIDATION_LIMIT(ServiceMessage.PREPAID_VALIDATION_LIMIT, Status.BAD_REQUEST),
	PREPAID_ALREADY_REGISTER(ServiceMessage.PREPAID_ALREADY_REGISTER, Status.CONFLICT),
	PREPAID_VALIDATION_MAX_LIMIT(ServiceMessage.PREPAID_VALIDATION_MAX_LIMIT, Status.CONFLICT),
	PREPAID_SUCCESS_REMOVED(ServiceMessage.PREPAID_SUCCESS_REMOVED, Status.OK),
	PREPAID_SUCCESS_UPDATED(ServiceMessage.PREPAID_SUCCESS_UPDATED, Status.OK),
	PREPAID_UPDATES_LIMIT_REACHED(ServiceMessage.PREPAID_UPDATES_LIMIT_REACHED, Status.BAD_REQUEST),
	PREPAID_REMOVES_LIMIT_REACHED(ServiceMessage.PREPAID_REMOVES_LIMIT_REACHED, Status.BAD_REQUEST),
	REMOVE_CARD_SUCCESS(ServiceMessage.REMOVE_CARD_SUCCESS, Status.OK),
	UNREGISTER_SUCCESS(ServiceMessage.UNREGISTER_SUCCESS, Status.OK),
	VALIDATION_REQUIRED(ServiceMessage.VALIDATION_REQUIRED, Status.BAD_REQUEST),
	VALIDATION_FIRED(ServiceMessage.VALIDATION_FIRED, Status.BAD_REQUEST),
	VALIDATION_TRY(ServiceMessage.VALIDATION_TRY, Status.BAD_REQUEST),
	VALIDATION_INVALID(ServiceMessage.VALIDATION_INVALID, Status.BAD_REQUEST),
	TRANSACTION_HISTORY_SUCCESS(ServiceMessage.TRANSACTION_HISTORY_SUCCESS, Status.OK),
	TRANSACTION_HISTORY_NO_RESULT(ServiceMessage.TRANSACTION_HISTORY_NO_RESULT, Status.NOT_FOUND),
	TRANSACTION_HISTORY_INVALID_PERIOD(ServiceMessage.TRANSACTION_HISTORY_INVALID_PERIOD, Status.BAD_REQUEST),
	TRANSACTION_STATUS_SUCCESS(ServiceMessage.TRANSACTION_STATUS_SUCCESS, Status.OK),
	TRANSACTION_STATUS_NO_RESULT(ServiceMessage.TRANSACTION_STATUS_NO_RESULT, Status.NOT_FOUND),
	CHANNEL_UNSUPPORTED(ServiceMessage.CHANNEL_UNSUPPORTED, Status.FORBIDDEN),
	INTERNAL_ERROR(ServiceMessage.INTERNAL_ERROR, Status.INTERNAL_SERVER_ERROR);

	
	private ServiceMessage rechargeResponse;
	private Status httpResponse;

	ResponseMapping(ServiceMessage rechargeResponse, Status httpResponse) {
		this.rechargeResponse = rechargeResponse;
		this.httpResponse = httpResponse;
	}
	
	public ServiceMessage rechargeResponse() {
		return rechargeResponse;
	}

	public Status httpResponse() {
		return httpResponse;
	}

	public static Status getRightHttpStatusFor(long rechargeCode) {
		for (ResponseMapping item : ResponseMapping.values()) {
			if (item.rechargeResponse().getCode() == rechargeCode) {
				return item.httpResponse();
			}
		}
		return Status.INTERNAL_SERVER_ERROR;
	}

}
