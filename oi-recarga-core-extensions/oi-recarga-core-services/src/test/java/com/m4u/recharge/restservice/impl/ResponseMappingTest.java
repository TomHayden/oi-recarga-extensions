package com.m4u.recharge.restservice.impl;

import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;

import com.m4u.recharge.restservice.corews.impl.ResponseMapping;

public class ResponseMappingTest {

    @Test
    public void deveRetornarHTTPStatusAccepted() {
        long RECARGA_SOLICITADA_COM_SUCESSO = 3000L;
        Status status = ResponseMapping.getRightHttpStatusFor(RECARGA_SOLICITADA_COM_SUCESSO);
        Assert.assertEquals(Status.ACCEPTED, status);
    }

    @Test
    public void deveRetornarHTTPStatusNotFound() {
        long STATUS_TRANSACAO_NAO_ENCONTRADO = 8501L;
        Status status = ResponseMapping.getRightHttpStatusFor(STATUS_TRANSACAO_NAO_ENCONTRADO);
        Assert.assertEquals(Status.NOT_FOUND, status);
    }

    @Test
    public void deveRetornarHTTPStatusOK() {
        long STATUS_TRANSACAO_ENCONTRADO = 8500L;
        Status status = ResponseMapping.getRightHttpStatusFor(STATUS_TRANSACAO_ENCONTRADO);
        Assert.assertEquals(Status.OK, status);
    }

}
