package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

public class TransactionStatusResponse extends SimpleResponse implements Serializable {

    private static final long serialVersionUID = 97569186698597807L;

    private TopupStatus topupStatus;

    public TopupStatus getTopupStatus() {
        return topupStatus;
    }

    public void setTopupStatus(TopupStatus topupStatus) {
        this.topupStatus = topupStatus;
    }

    @Override
    public String toString() {
        return "TransactionStatusResponse [code=" + getCode() + ", message=" + getMessage() + ", topupStatus=" + topupStatus + "]";
    }

}
