package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

public class UpdatePrepaidRequest extends SimpleRequest implements Serializable{

    private static final long serialVersionUID = -8167387117869391790L;

    @TransactionField(type = TransactionFieldType.PREPAID, required = true)
    private String currentPrepaid;
    
    @TransactionField(customType = "NEW_PREPAID", required = true)
    private String newPrepaid;
    
    private Integer randomQuestion;
    private String randomAnswer;

    public String getNewPrepaid() {
		return newPrepaid;
	}

	public void setNewPrepaid(String newPrepaid) {
		this.newPrepaid = newPrepaid;
	}

	public String getCurrentPrepaid() {
		return currentPrepaid;
	}

	public void setCurrentPrepaid(String currentPrepaid) {
		this.currentPrepaid = currentPrepaid;
	}

	public Integer getRandomQuestion() {
        return randomQuestion;
    }

    public void setRandomQuestion(Integer randomQuestion) {
        this.randomQuestion = randomQuestion;
    }

    public String getRandomAnswer() {
        return randomAnswer;
    }

    public void setRandomAnswer(String randomAnswer) {
        this.randomAnswer = randomAnswer;
    }

	@Override
	public String toString() {
		return "UpdatePrepaidRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", currentPrepaid=" + currentPrepaid
				+ ", newPrepaid=" + newPrepaid + ", randomQuestion="
				+ randomQuestion + ", randomAnswer=" + randomAnswer + "]";
	}
    
    

}
