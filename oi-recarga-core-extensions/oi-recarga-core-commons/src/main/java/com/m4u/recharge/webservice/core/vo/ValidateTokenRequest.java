package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

public class ValidateTokenRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = 8330970827063955137L;

    @TransactionField(type = TransactionFieldType.CC_TOKEN, required = true)
    private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "ValidateTokenRequest [channel=" + getChannel()
				+ ", msisdn=" + getMsisdn() + ", token=" + token + "]";
	}
	
	

}
