package com.m4u.recharge.webservice.core.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

@XmlAccessorType(XmlAccessType.FIELD)
public class CreateCustomerWithTokenRequest extends SimpleRequest implements CustomerRequest {

    private static final long serialVersionUID = -7976589924457399011L;

    private String cpf;

    private String birthDate;

    @TransactionField(customType = "EMAIL", required = true)
    private String email;

    @TransactionField(customType = "SOCIAL_NETWORK_ID", required = true)
    private String socialNetworkId;

    @TransactionField(type = TransactionFieldType.CC_TOKEN, required = true)
    private String cardToken;

    @TransactionField(customType = "ADVERTISING", required = true)
    public Boolean advertising;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSocialNetworkId() {
        return socialNetworkId;
    }

    public void setSocialNetworkId(String socialNetworkId) {
        this.socialNetworkId = socialNetworkId;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public Boolean isAdvertising() {
        return advertising;
    }

    public void setAdvertising(Boolean advertising) {
        this.advertising = advertising;
    }

    @Override
    public String toString() {
        return "CreateCustomerRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", cpf=" + cpf + ", birthDate=" + birthDate
                + ", cardToken=" + cardToken + ", email=" + email + ", socialNetworkId=" + socialNetworkId + ", advertising=" + advertising + "]";
    }

}
