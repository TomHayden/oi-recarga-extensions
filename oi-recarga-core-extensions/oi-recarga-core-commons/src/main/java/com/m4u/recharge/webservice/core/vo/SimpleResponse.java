package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class SimpleResponse implements Serializable {

    private static final Logger logger = Logger.getLogger(SimpleResponse.class);

    private static final long serialVersionUID = 6237224999021871322L;

    protected Long code;
    protected String message;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toJSON() {
        return convertToJSON(this);
    }

    @Override
    public String toString() {
        return "SimpleResponse [code=" + code + ", message=" + message + "]";
    }

    private static String criarJson(Object obj) {
        Gson g = new Gson();
        String json = g.toJson(obj);
        return json;
    }

    public String convertToJSON(Object obj) {
        try {
            logger.debug(String.format("Iniciando conversao de Java para JSON."));
            String json = criarJson(obj);
            logger.debug(String.format("Conversao de Java para JSON efetuada. Conteudo:[%1s].", json));
            logger.debug(String.format("Finalizando conversao de Java para JSON."));
            return json;
        } catch (Exception e) {
            logger.debug(String.format("Finalizando conversao de Java para JSON com falha."));
            return null;
        }
    }

}
