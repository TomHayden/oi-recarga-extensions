package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerDetail implements Serializable {

    private static final long serialVersionUID = -190947201754664104L;

    private String msisdn;

    private String cpf;
    
    private String birthDate;

    private Integer status;
    
    private String email;

    private String socialNetworkId;

    private Boolean advertising;
    
    private List<CreditCard> creditCard;

    private List<String> prepaid;

    private TopupTransaction lastTransaction;

    private Double minimumTopupAmount; // valor minimo por transacao

    private Double maximumTopupAmount; // valor máximo por transacao

    private Double topupMonthlyLimit; // limite mensal de recarga

    private Double topupAvailableBalance; // Saldo disponível para recarga

    private List<Double> topupAmountHint; // Sugestao de valores para recarga

    private Integer minimumIntervalBetweenTopup; // Qtd de minutos de intervalo
    // entre uma recarga e outra

    private Integer maximumPrepaidQuantity; // qty de inclusões de Oi recebedores
    // limite (Inclusões de Oi Recebedores)

    private Integer maximumCreditCardQuantity;
    
    @XmlTransient
    private String registerDate;
    
    @XmlTransient
    private Integer maximumPrepaidChange;
    @XmlTransient
    private Integer prepaidChange;
    @XmlTransient
    private Integer maximumCreditCardChange;
    @XmlTransient
    private Integer creditCardChange;
    
    @XmlTransient
    private List<Double> topupProfileAmountHint; // Valores fixos do Perfil
    
    public CustomerDetail() {
        this.creditCard = new ArrayList<CreditCard>();
        this.prepaid = new ArrayList<String>();
        this.topupAmountHint = new ArrayList<Double>();
        this.topupProfileAmountHint = new ArrayList<Double>();
    }

    public void addCreditCard(CreditCard creditCard) {
        this.creditCard.add(creditCard);
    }

    public void addPrepaid(String prepaid) {
        this.prepaid.add(prepaid);
    }

    public void addTopupAmountHint(Double amount) {
        this.topupAmountHint.add(amount);
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<CreditCard> getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(List<CreditCard> creditCard) {
        this.creditCard = creditCard;
    }

    public List<String> getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(List<String> prepaid) {
        this.prepaid = prepaid;
    }

    public TopupTransaction getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(TopupTransaction lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    public Double getMinimumTopupAmount() {
        return minimumTopupAmount;
    }

    public void setMinimumTopupAmount(Double minimumTopupAmount) {
        this.minimumTopupAmount = minimumTopupAmount;
    }

    public Double getMaximumTopupAmount() {
        return maximumTopupAmount;
    }

    public void setMaximumTopupAmount(Double maximumTopupAmount) {
        this.maximumTopupAmount = maximumTopupAmount;
    }

    public Double getTopupMonthlyLimit() {
        return topupMonthlyLimit;
    }

    public void setTopupMonthlyLimit(Double topupMonthlyLimit) {
        this.topupMonthlyLimit = topupMonthlyLimit;
    }

    public Double getTopupAvailableBalance() {
        return topupAvailableBalance;
    }

    public void setTopupAvailableBalance(Double topupAvaiableBalance) {
        this.topupAvailableBalance = topupAvaiableBalance;
    }

    public List<Double> getTopupAmountHint() {
        return topupAmountHint;
    }

    public void setTopupAmountHint(List<Double> topupAmountHint) {
        this.topupAmountHint = topupAmountHint;
    }

    public Integer getMinimumIntervalBetweenTopup() {
        return minimumIntervalBetweenTopup;
    }

    public void setMinimumIntervalBetweenTopup(Integer minimumIntervalBetweenTopup) {
        this.minimumIntervalBetweenTopup = minimumIntervalBetweenTopup;
    }

    public Integer getMaximumPrepaidQuantity() {
        return maximumPrepaidQuantity;
    }

    public void setMaximumPrepaidQuantity(Integer maximumPrepaidQuantity) {
        this.maximumPrepaidQuantity = maximumPrepaidQuantity;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSocialNetworkId() {
        return socialNetworkId;
    }

    public void setSocialNetworkId(String socialNetworkId) {
        this.socialNetworkId = socialNetworkId;
    }

    public Integer getMaximumCreditCardQuantity() {
        return maximumCreditCardQuantity;
    }

    public void setMaximumCreditCardQuantity(Integer maximumCreditCardQuantity) {
        this.maximumCreditCardQuantity = maximumCreditCardQuantity;
    }

    public Double getTopupMonthTotalBalance() {
        return this.topupMonthlyLimit - this.topupAvailableBalance;
    }

    public Boolean isAdvertising() {
        return advertising;
    }

    public void setAdvertising(Boolean advertising) {
        this.advertising = advertising;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public Integer getMaximumPrepaidChange() {
        return maximumPrepaidChange;
    }

    public void setMaximumPrepaidChange(Integer maximumPrepaidChange) {
        this.maximumPrepaidChange = maximumPrepaidChange;
    }

    public Integer getPrepaidChange() {
        return prepaidChange;
    }

    public void setPrepaidChange(Integer prepaidChange) {
        this.prepaidChange = prepaidChange;
    }

    public Integer getMaximumCreditCardChange() {
        return maximumCreditCardChange;
    }

    public void setMaximumCreditCardChange(Integer maximumCreditCardChange) {
        this.maximumCreditCardChange = maximumCreditCardChange;
    }

    public Integer getCreditCardChange() {
        return creditCardChange;
    }

    public void setCreditCardChange(Integer creditCardChange) {
        this.creditCardChange = creditCardChange;
    }

    public List<Double> getTopupProfileAmountHint() {
        return topupProfileAmountHint;
    }

    public void setTopupProfileAmountHint(List<Double> topupProfileAmountHint) {
        this.topupProfileAmountHint = topupProfileAmountHint;
    }

    public void addTopupProfileAmountHint(Double amount) {
        this.topupProfileAmountHint.add(amount);
    }
    
}
