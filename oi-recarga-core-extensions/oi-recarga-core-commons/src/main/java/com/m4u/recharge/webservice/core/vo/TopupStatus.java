package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

public class TopupStatus implements Serializable {

    private static final long serialVersionUID = -5084522566911950598L;

    protected String code;
    protected String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "TopupStatus [code=" + code + ", description=" + description + "]";
    }
    
}
