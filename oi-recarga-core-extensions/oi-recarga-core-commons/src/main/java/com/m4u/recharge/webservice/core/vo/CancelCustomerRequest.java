package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

public class CancelCustomerRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = 9201388391325317572L;

    private Integer randomQuestion;
    private String randomAnswer;

    public Integer getRandomQuestion() {
        return randomQuestion;
    }

    public void setRandomQuestion(Integer randomQuestion) {
        this.randomQuestion = randomQuestion;
    }

    public String getRandomAnswer() {
        return randomAnswer;
    }

    public void setRandomAnswer(String randomAnswer) {
        this.randomAnswer = randomAnswer;
    }

    @Override
    public String toString() {
        return "CancelCustomerRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", randomQuestion=" + randomQuestion
                + ", randomAnswer=" + randomAnswer + "]";
    }

}
