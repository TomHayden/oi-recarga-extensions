package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;

public class TransactionStatusRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = 9148011136716146322L;
    
    @TransactionField(customType = "EXTERNAL_ID", required = true)
    private String externalId;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String toString() {
        return "TransactionStatusRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", externalId=" + externalId + "]";
    }

}
