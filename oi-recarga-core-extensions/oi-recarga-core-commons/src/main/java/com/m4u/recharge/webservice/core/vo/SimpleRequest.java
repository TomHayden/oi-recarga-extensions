package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

public class SimpleRequest implements Serializable {

    private static final long serialVersionUID = -365071552064608063L;

    @TransactionField(type = TransactionFieldType.CHANNEL, required = true)
    protected Integer channel;
    @TransactionField(type = TransactionFieldType.CUSTOMER, required = true)
    protected String msisdn;
    
    public SimpleRequest() {
    }
    
    public SimpleRequest(Integer channel, String msisdn) {
        super();
        this.channel = channel;
        this.msisdn = msisdn;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "SimpleRequest [channel=" + channel + ", msisdn=" + msisdn + "]";
    }

}
