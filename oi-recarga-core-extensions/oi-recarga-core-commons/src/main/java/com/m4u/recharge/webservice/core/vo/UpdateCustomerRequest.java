package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;

public class UpdateCustomerRequest extends SimpleRequest implements Serializable{
    
    private static final long serialVersionUID = -3782195361526015749L;

    @TransactionField(customType = "EMAIL", required = true)
    private String email;
    
    @TransactionField(customType = "SOCIAL_NETWORK_ID", required = true)
    private String socialNetworkId;
    
    @TransactionField(customType = "ADVERTISING", required = true)
    private Boolean advertising;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSocialNetworkId() {
        return socialNetworkId;
    }

    public void setSocialNetworkId(String socialNetworkId) {
        this.socialNetworkId = socialNetworkId;
    }

    public Boolean isAdvertising() {
        return advertising;
    }
    
    public void setAdvertising(Boolean advertising) {
        this.advertising = advertising;
    }
    
    @Override
    public String toString() {
        return "UpdateCustomerRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", email=" + email + ", socialNetworkId=" + socialNetworkId + ", advertising=" + advertising + "]";
    }
}
