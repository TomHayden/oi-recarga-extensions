package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

public class RemovePrepaidRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = 3667970717098368004L;

    @TransactionField(type = TransactionFieldType.PREPAID, required = true)
    private String prepaid;
    private Integer randomQuestion;
    private String randomAnswer;

    public String getPrepaid() {
        return this.prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    public Integer getRandomQuestion() {
        return randomQuestion;
    }

    public void setRandomQuestion(Integer randomQuestion) {
        this.randomQuestion = randomQuestion;
    }

    public String getRandomAnswer() {
        return randomAnswer;
    }

    public void setRandomAnswer(String randomAnswer) {
        this.randomAnswer = randomAnswer;
    }

    @Override
    public String toString() {
        return "RemovePrepaidRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", prepaid=" + prepaid
                + ", randomQuestion=" + randomQuestion + ", randomAnswer=" + randomAnswer + "]";
    }

}
