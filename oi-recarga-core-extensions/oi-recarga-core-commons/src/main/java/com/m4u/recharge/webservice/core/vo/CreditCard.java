package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

public class CreditCard implements Serializable {

    private static final long serialVersionUID = -2440403071856542738L;

    private String cardType;
    private String last4Digits;
    private String expirationDate;
    
    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

}
