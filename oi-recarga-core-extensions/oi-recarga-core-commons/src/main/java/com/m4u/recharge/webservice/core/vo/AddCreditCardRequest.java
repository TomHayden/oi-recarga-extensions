package com.m4u.recharge.webservice.core.vo;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.recharge.util.RechargeUtil;

public class AddCreditCardRequest extends SimpleRequest implements CreditCardRequest {

    private static final long serialVersionUID = -2646356477079720086L;

    private String cardNumber;
    
    @TransactionField(customType = "EXPIRATION_DT", required = true)
    private String expirationDate;

    public String getCardNumber() {
        return cardNumber;
    }
    
    public String getHiddenCardNumber() {
        return RechargeUtil.hideCardNumber(cardNumber);
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "AddCreditCardRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", cardNumber="
                + RechargeUtil.hideCardNumber(cardNumber) + ", expirationDate=" + expirationDate + "]";
    }

}
