package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

@XmlAccessorType(XmlAccessType.FIELD)
public class TopupRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = -5040699610094228598L;

    @TransactionField(type = TransactionFieldType.PREPAID, required = true)
    private String prepaid;
    
    @TransactionField(type = TransactionFieldType.AMOUNT, required = true)
    private Double amount;
    
    @TransactionField(type = TransactionFieldType.CC_BRAND, required = true)
    private String cardType;
    
    @TransactionField(type = TransactionFieldType.CC_LAST4DIGITS, required = true)
    private String final4Digits;
    
    private String cvv;
    private Integer randomQuestion;
    private String randomAnswer;
    
    @XmlTransient
    @TransactionField(customType = "EXTERNAL_ID", required = true)
    private String externalId;

    public String getPrepaid() {
        return this.prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getFinal4Digits() {
        return final4Digits;
    }

    public void setFinal4Digits(String final4Digits) {
        this.final4Digits = final4Digits;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public Integer getRandomQuestion() {
        return randomQuestion;
    }

    public void setRandomQuestion(Integer randomQuestion) {
        this.randomQuestion = randomQuestion;
    }

    public String getRandomAnswer() {
        return randomAnswer;
    }

    public void setRandomAnswer(String randomAnswer) {
        this.randomAnswer = randomAnswer;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Override
    public String toString() {
        return "TopupRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", prepaid=" + getPrepaid() + ", amount=" + amount
                + ", cardType=" + cardType + ", final4Digits=" + final4Digits + ", randomQuestion=" + randomQuestion + ", randomAnswer="
                + randomAnswer + ", externalId=" + externalId + "]";
    }
}
