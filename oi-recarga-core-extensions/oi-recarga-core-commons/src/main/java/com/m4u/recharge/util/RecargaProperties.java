package com.m4u.recharge.util;

import com.m4u.recharge.tools.common.util.PropertiesUtil;

public class RecargaProperties {
    
    private static final String FILE = "/properties/recarga.properties";
    
    public static PropertiesUtil getConf() {
        return new PropertiesUtil(FILE);
    }

}
