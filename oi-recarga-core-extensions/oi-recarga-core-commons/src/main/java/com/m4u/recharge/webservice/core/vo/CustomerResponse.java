package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

public class CustomerResponse extends SimpleResponse implements Serializable {
    
    private static final long serialVersionUID = 4084100054648947967L;

    private CustomerDetail customerDetail;
    
    private Integer randomQuestion;

    public CustomerDetail getCustomerDetail() {
        return customerDetail;
    }

    public void setCustomerDetail(CustomerDetail customerDetail) {
        this.customerDetail = customerDetail;
    }

    /**
     * 
     * @return 0 - 4 primeiros digitos CPF <br>
     *         1 - Dia do nascimento <br>
     *         2 - Mes do nascimento <br>
     *         3 - Ano do nascimento <br>
     *         4 - Idade
     * 
     */
    public Integer getRandomQuestion() {
        return randomQuestion;
    }
    
    public void setRandomQuestion(Integer randomQuestion) {
        this.randomQuestion = randomQuestion;
    }
    
    @Override
    public String toString() {
        return "CustomerResponse [code=" + getCode() + ", message=" + getMessage() + "]";
    }
    
    

}
