package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;

public class TransactionHistoryRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = -3726348124604942896L;

    @TransactionField(customType = "PERIOD", required = true)
    private Integer period;

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "TransactionHistoryRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", period=" + period + "]";
    }

}
