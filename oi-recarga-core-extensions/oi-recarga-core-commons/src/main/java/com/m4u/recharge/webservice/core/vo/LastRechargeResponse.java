package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

public class LastRechargeResponse extends SimpleResponse implements Serializable {

    private static final long serialVersionUID = -1762589400429950933L;

    private TopupTransaction lastRecharge;

    public TopupTransaction getLastRecharge() {
        return lastRecharge;
    }

    public void setLastRecharge(TopupTransaction lastRecharge) {
        this.lastRecharge = lastRecharge;
    }

    @Override
    public String toString() {
        return "LastRechargeResponse [lastRecharge=" + lastRecharge + ", code=" + code + ", message=" + message + "]";
    }

}
