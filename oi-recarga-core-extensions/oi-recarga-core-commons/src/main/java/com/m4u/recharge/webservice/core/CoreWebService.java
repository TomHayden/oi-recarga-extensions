package com.m4u.recharge.webservice.core;

import com.m4u.recharge.webservice.core.vo.AddCreditCardRequest;
import com.m4u.recharge.webservice.core.vo.AddCreditCardWithTokenRequest;
import com.m4u.recharge.webservice.core.vo.AddPrepaidRequest;
import com.m4u.recharge.webservice.core.vo.CancelCustomerRequest;
import com.m4u.recharge.webservice.core.vo.CreateCustomerRequest;
import com.m4u.recharge.webservice.core.vo.CreateCustomerWithTokenRequest;
import com.m4u.recharge.webservice.core.vo.CustomerBySocialNetworkIdRequest;
import com.m4u.recharge.webservice.core.vo.CustomerResponse;
import com.m4u.recharge.webservice.core.vo.RemoveCreditCardRequest;
import com.m4u.recharge.webservice.core.vo.RemovePrepaidRequest;
import com.m4u.recharge.webservice.core.vo.SimpleRequest;
import com.m4u.recharge.webservice.core.vo.SimpleResponse;
import com.m4u.recharge.webservice.core.vo.TopupRequest;
import com.m4u.recharge.webservice.core.vo.TransactionHistoryRequest;
import com.m4u.recharge.webservice.core.vo.TransactionHistoryResponse;
import com.m4u.recharge.webservice.core.vo.TransactionStatusRequest;
import com.m4u.recharge.webservice.core.vo.TransactionStatusResponse;
import com.m4u.recharge.webservice.core.vo.UpdateCreditcardRequest;
import com.m4u.recharge.webservice.core.vo.UpdateCustomerRequest;
import com.m4u.recharge.webservice.core.vo.UpdatePrepaidRequest;
import com.m4u.recharge.webservice.core.vo.ValidateTokenRequest;

public interface CoreWebService {
    
    public static final String JNDI_LOCAL = "recharge/corewebservice/local";

    public static final String JNDI_REMOTE = "recharge/corewebservice/remote";
    
    public CustomerResponse getCustomer(SimpleRequest request);

    public CustomerResponse createCustomer(CreateCustomerRequest request);

    public CustomerResponse createCustomerWithToken(CreateCustomerWithTokenRequest request);
    
    public SimpleResponse requestTopup(TopupRequest request);

    public CustomerResponse addCreditCard(AddCreditCardRequest request);

    public CustomerResponse addCreditCardWithToken(AddCreditCardWithTokenRequest request);
    
    public CustomerResponse addPrepaid(AddPrepaidRequest request);

    public CustomerResponse removeCreditCard(RemoveCreditCardRequest request);

    public CustomerResponse removePrepaid(RemovePrepaidRequest request);
    
    public CustomerResponse updatePrepaid(UpdatePrepaidRequest request);
    
    public SimpleResponse cancelCustomer(CancelCustomerRequest request);

    public TransactionStatusResponse getTransactionStatus(TransactionStatusRequest request);
    
    public TransactionHistoryResponse getTransactionHistory(TransactionHistoryRequest request);
    
    public CustomerResponse updateCreditCard(UpdateCreditcardRequest request);
    
    public SimpleResponse sendToken(SimpleRequest request);

    public SimpleResponse validateToken(ValidateTokenRequest request);

    public CustomerResponse getCustomerBySocialNetworkId(CustomerBySocialNetworkIdRequest request);
    
    public CustomerResponse updateCustomer(UpdateCustomerRequest request);

}
