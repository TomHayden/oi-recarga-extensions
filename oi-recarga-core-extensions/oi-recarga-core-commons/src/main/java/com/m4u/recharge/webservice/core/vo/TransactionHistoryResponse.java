package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryResponse extends SimpleResponse implements Serializable {
    
    private static final long serialVersionUID = 2736207975270712123L;

    private List<TopupTransaction> transaction;
    
    public TransactionHistoryResponse() {
        this.transaction = new ArrayList<TopupTransaction>();
    }

    public List<TopupTransaction> getTransaction() {
        return transaction;
    }

    public void setTransaction(List<TopupTransaction> transaction) {
        this.transaction = transaction;
    }
    
    public void addTransaction(TopupTransaction transaction) {
        this.transaction.add(transaction);
    }

    @Override
    public String toString() {
        return "TransactionHistoryResponse [code=" + getCode() + ", message=" + getMessage() + "]";
    }

}
