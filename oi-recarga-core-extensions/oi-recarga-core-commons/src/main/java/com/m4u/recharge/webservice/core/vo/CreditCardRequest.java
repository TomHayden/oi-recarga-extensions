package com.m4u.recharge.webservice.core.vo;

public interface CreditCardRequest {
    
    public Integer getChannel();

    public void setChannel(Integer channel);

    public String getMsisdn();

    public void setMsisdn(String msisdn);


}
