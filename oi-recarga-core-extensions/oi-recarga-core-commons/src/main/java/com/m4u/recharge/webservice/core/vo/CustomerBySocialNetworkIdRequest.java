package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

public class CustomerBySocialNetworkIdRequest implements Serializable {

	private static final long serialVersionUID = -8721570977464565474L;

	@TransactionField(type = TransactionFieldType.CHANNEL, required = true)
	protected Integer channel;
	
	@TransactionField(customType = "SOCIAL_NETWORK_ID", required = true)
	protected String socialNetworkId;

	public Integer getChannel() {
		return channel;
	}

	public void setChannel(Integer channel) {
		this.channel = channel;
	}

	public String getSocialNetworkId() {
		return socialNetworkId;
	}

	public void setSocialNetworkId(String socialNetworkId) {
		this.socialNetworkId = socialNetworkId;
	}

	@Override
	public String toString() {
		return "CustomerBySocialNetworkIdRequest [channel=" + channel
				+ ", socialNetworkId=" + socialNetworkId + "]";
	}
	
}
