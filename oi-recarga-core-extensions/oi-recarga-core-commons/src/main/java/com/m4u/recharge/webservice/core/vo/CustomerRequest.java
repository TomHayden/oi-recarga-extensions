package com.m4u.recharge.webservice.core.vo;

public interface CustomerRequest {

    public Integer getChannel();

    public void setChannel(Integer channel);

    public String getMsisdn();

    public void setMsisdn(String msisdn);
    
    public String getCpf();

    public void setCpf(String cpf);

    public String getBirthDate();

    public void setBirthDate(String birthDate);

    public String getEmail();

    public void setEmail(String email);

    public String getSocialNetworkId();

    public void setSocialNetworkId(String socialNetworkId);
    
    public Boolean isAdvertising();

    public void setAdvertising(Boolean advertising);
    
}
