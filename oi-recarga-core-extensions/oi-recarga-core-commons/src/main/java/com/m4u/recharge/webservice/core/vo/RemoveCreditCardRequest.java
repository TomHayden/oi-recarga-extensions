package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

public class RemoveCreditCardRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = -3391851977004282541L;

    @TransactionField(type = TransactionFieldType.CC_BRAND, required = true)
    private String cardType;
    
    @TransactionField(type = TransactionFieldType.CC_LAST4DIGITS, required = true)
    private String final4Digits;
    
    private Integer randomQuestion;
    private String randomAnswer;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getFinal4Digits() {
        return final4Digits;
    }

    public void setFinal4Digits(String final4Digits) {
        this.final4Digits = final4Digits;
    }

    public Integer getRandomQuestion() {
        return randomQuestion;
    }

    public void setRandomQuestion(Integer randomQuestion) {
        this.randomQuestion = randomQuestion;
    }

    public String getRandomAnswer() {
        return randomAnswer;
    }

    public void setRandomAnswer(String randomAnswer) {
        this.randomAnswer = randomAnswer;
    }

    @Override
    public String toString() {
        return "RemoveCreditCardRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", cardType=" + cardType
                + ", final4Digits=" + final4Digits + ", randomQuestion=" + randomQuestion + ", randomAnswer=" + randomAnswer + "]";
    }

}
