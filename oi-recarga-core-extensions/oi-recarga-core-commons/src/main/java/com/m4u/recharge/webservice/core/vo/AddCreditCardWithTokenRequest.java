package com.m4u.recharge.webservice.core.vo;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;


public class AddCreditCardWithTokenRequest extends SimpleRequest implements CreditCardRequest {

    private static final long serialVersionUID = -2646356477079720086L;

    @TransactionField(type = TransactionFieldType.CC_TOKEN, required = true)
    private String cardToken;

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    @Override
    public String toString() {
        return "AddCreditCardRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", cardToken=" + cardToken + "]";
    }

}
