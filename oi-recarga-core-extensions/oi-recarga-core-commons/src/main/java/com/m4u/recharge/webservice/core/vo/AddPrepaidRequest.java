package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;

public class AddPrepaidRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = 1119010076568292399L;

    @TransactionField(type = TransactionFieldType.PREPAID, required = true)
    private String prepaid;

    public String getPrepaid() {
        return this.prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    @Override
    public String toString() {
        return "AddPrepaidRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", prepaid=" + prepaid + "]";
    }

}
