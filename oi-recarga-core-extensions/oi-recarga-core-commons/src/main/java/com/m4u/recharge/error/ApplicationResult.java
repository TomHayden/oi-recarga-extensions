package com.m4u.recharge.error;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ApplicationResult {
    public final static long SUCCESS_CODE = 0L;
    public final static String SUCCESS_MESSAGE = "Sucesso";
    
    private String domain;
    private String subDomain;
    private long code;
    private String message;
    private Map<String, Object> params = new HashMap<String, Object>();
    
    public ApplicationResult(String domain, String subDomain) {
        this(domain, subDomain, SUCCESS_CODE, SUCCESS_MESSAGE);
    }
    
    public ApplicationResult(String domain, String subDomain, long code, String message) {
        this.domain = domain;
        this.subDomain = subDomain;
        this.code = code;
        this.message = message;
    }
    
    public String getDomain() {
        return domain;
    }
    
    public String getSubDomain() {
        return subDomain;
    }
    
    public long getCode() {
        return code;
    }
    
    public void setCode(long code) {
        this.code = code;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public void addParam(String name, Object value) {
        this.params.put(name, value);
    }
    
    public Map<String, Object> getParams() {
        return Collections.unmodifiableMap(this.params);
    }
    
    public ApplicationResult recycle() {
        recycle(this.domain, this.subDomain);
        return this;
    }
    
    public ApplicationResult recycle(String domain, String subDomain) {
        this.domain = domain;
        this.subDomain = subDomain;
        this.code = SUCCESS_CODE;
        this.message = SUCCESS_MESSAGE;
        return this;
    }
    
    public String log() {
        return String.format("%s:%s:cod[%d]:msg[%s]", this.domain, this.subDomain, this.code, this.message);
    }

    @Override
    public String toString() {
        return "ApplicationError [domain=" + domain + ", subDomain=" + subDomain + ", code=" + code + ", message=" + message + ", params="
                + params + "]";
    }
}
