package com.m4u.recharge.webservice.core.vo;

import java.io.Serializable;

import com.m4u.logging.annotation.TransactionField;
import com.m4u.logging.type.TransactionFieldType;
import com.m4u.recharge.util.RechargeUtil;

public class UpdateCreditcardRequest extends SimpleRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private String newCardNumber;
    
    @TransactionField(customType = "EXPIRATION_DT", required = true)
    private String newExpirationDate;
    
    @TransactionField(type = TransactionFieldType.CC_BRAND, required = true)
    private String currentCardType;
    
    @TransactionField(type = TransactionFieldType.CC_LAST4DIGITS, required = true)
    private String currentFinal4Digits;
    
    private Integer randomQuestion;
    private String randomAnswer;

    public String getNewCardNumber() {
		return newCardNumber;
	}

	public void setNewCardNumber(String newCardNumber) {
		this.newCardNumber = newCardNumber;
	}

	public String getNewExpirationDate() {
		return newExpirationDate;
	}

	public void setNewExpirationDate(String newExpirationDate) {
		this.newExpirationDate = newExpirationDate;
	}

	public String getCurrentCardType() {
		return currentCardType;
	}

	public void setCurrentCardType(String currentCardType) {
		this.currentCardType = currentCardType;
	}

	public String getCurrentFinal4Digits() {
		return currentFinal4Digits;
	}

	public void setCurrentFinal4Digits(String currentFinal4Digits) {
		this.currentFinal4Digits = currentFinal4Digits;
	}

	public Integer getRandomQuestion() {
        return randomQuestion;
    }

    public void setRandomQuestion(Integer randomQuestion) {
        this.randomQuestion = randomQuestion;
    }

    public String getRandomAnswer() {
        return randomAnswer;
    }

    public void setRandomAnswer(String randomAnswer) {
        this.randomAnswer = randomAnswer;
    }

	@Override
	public String toString() {
		return "UpdateCreditcardRequest [channel=" + getChannel() + ", msisdn=" + getMsisdn() + ", newCardNumber=" + RechargeUtil.hideCardNumber(newCardNumber)
				+ ", newExpirationDate=" + newExpirationDate
				+ ", currentCardType=" + currentCardType
				+ ", currentFinal4Digits=" + currentFinal4Digits
				+ ", randomQuestion=" + randomQuestion + ", randomAnswer="
				+ randomAnswer + "]";
	}

    
    
}
