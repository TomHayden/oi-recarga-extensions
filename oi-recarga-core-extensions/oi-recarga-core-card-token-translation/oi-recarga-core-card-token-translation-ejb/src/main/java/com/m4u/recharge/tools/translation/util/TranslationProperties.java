package com.m4u.recharge.tools.translation.util;

import com.m4u.recharge.tools.common.util.PropertiesUtil;

public class TranslationProperties {
    
    private static final String FILE = "/card-token-translation.properties";
    
    public static PropertiesUtil getConf() {
        return new PropertiesUtil(FILE);
    }

}
