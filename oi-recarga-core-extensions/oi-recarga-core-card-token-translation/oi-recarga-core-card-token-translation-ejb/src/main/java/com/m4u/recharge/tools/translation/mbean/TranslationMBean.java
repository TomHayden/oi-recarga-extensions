package com.m4u.recharge.tools.translation.mbean;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.ejb3.annotation.Service;

import com.m4u.recharge.tools.translation.service.TranslationService;

@Service(objectName = "com.m4u.recharge.tools:name=ITranslationMBean,service=TranslationMBean")
public class TranslationMBean implements ITranslationMBean {

    @Override
    public void translateCustomerCreditCard(int topRow) {
        this.getTranslationService().translateCustomerCreditCard(topRow);
    }

    @Override
    public void translateTransactionLog(int topRow) {
        this.getTranslationService().translateTransactionLog(topRow);
    }

    @Override
    public void translateLastMsisdnUpdate(int topRow) {
        this.getTranslationService().translateLastMsisdnUpdate(topRow);
    }

    @Override
    public void translateChargebackLog(int topRow) {
        this.getTranslationService().translateChargebackLog(topRow);
    }

    @Override
    public void translateBlackList(int topRow) {
        this.getTranslationService().translateBlackList(topRow);
    }
    
    @Override
    public void translateCreditCard(long cardId) {
        this.getTranslationService().translateCreditCard(cardId);
    }
    
    private TranslationService getTranslationService() {
        InitialContext context;
        try {
            context = new InitialContext();
            return (TranslationService) context.lookup(TranslationService.JNDI_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Erro ao realizar lookup no JNDI: " + TranslationService.JNDI_NAME, e);
        }
    }

}
