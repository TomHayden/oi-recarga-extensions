package com.m4u.recharge.tools.translation.queue;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.Pool;
import org.jboss.ejb3.annotation.defaults.PoolDefaults;

import com.m4u.recharge.tools.translation.service.TranslationService;
import com.m4u.recharge.tools.translation.service.TranslationServiceBean;

@MessageDriven(name = "TranslationQueue", description = "Fila para tradução de Cartões", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = TranslationMDB.JNDI_NAME),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "25"),
        @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable") })
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Pool(value = PoolDefaults.POOL_IMPLEMENTATION_STRICTMAX, maxSize = 25)
public class TranslationMDB implements MessageListener {

    private static final Logger LOGGER = Logger.getLogger(TranslationServiceBean.class);
    
    public static final String JNDI_NAME = "queue/TranslationQueue";

    @EJB(name = TranslationService.JNDI_NAME)
    private TranslationService translationService;
    
    @Override
    public void onMessage(Message message) {
        
        ObjectMessage objectMessage = (ObjectMessage) message;
        try {
            Long cardId = (Long) objectMessage.getObject();
            translationService.translateCreditCard(cardId);
        } catch (Exception e) {
            LOGGER.error("Erro durante processamento da Fila.", e);
        }
        
    }

}
