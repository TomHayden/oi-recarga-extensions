package com.m4u.recharge.tools.translation.service;

import javax.ejb.Local;

@Local
public interface TranslationService {
    
    String JNDI_NAME = "translationservice/local";

    void translateCustomerCreditCard(int topRow);

    void translateTransactionLog(int topRow);

    void translateLastMsisdnUpdate(int topRow);

    void translateChargebackLog(int topRow);

    void translateBlackList(int topRow);
    
    void enqueueToTranslation(Long cardId);
    
    void translateCreditCard(Long cardId);

}
