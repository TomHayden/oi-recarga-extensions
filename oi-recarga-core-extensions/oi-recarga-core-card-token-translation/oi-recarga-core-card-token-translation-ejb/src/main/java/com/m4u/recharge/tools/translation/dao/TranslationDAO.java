package com.m4u.recharge.tools.translation.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.m4u.recharge.tools.commons.dao.DataSourceService;
import com.m4u.recharge.tools.translation.util.TranslationProperties;

public class TranslationDAO {

    private static final String SELECT_CUSTOMER_CCARD = "SELECT DISTINCT TOP (?) ID_CCARD FROM CUSTOMER_CCARD (NOLOCK) where (ID_CCARD IS NOT NULL AND ID_CCARD > 0) AND (token_ccard IS NULL OR token_ccard = '')";

    private static final String SELECT_TRANSACTION_LOG = "SELECT DISTINCT TOP (?) ID_CCARD FROM Transaction_Log (NOLOCK) where (ID_CCARD IS NOT NULL AND ID_CCARD > 0) AND (token_ccard IS NULL OR token_ccard = '')";

    private static final String SELECT_LASTMSISDNUPDATE = "SELECT DISTINCT TOP (?) ID_CCARD FROM LastMsisdnUpdate (NOLOCK) where (ID_CCARD IS NOT NULL AND ID_CCARD > 0) AND (token_ccard IS NULL OR token_ccard = '')";

    private static final String SELECT_CHARGEBACK_LOG = "SELECT DISTINCT TOP(?) ID_CCARD FROM Chargeback_log (NOLOCK) where (ID_CCARD IS NOT NULL AND ID_CCARD > 0) AND (token_ccard IS NULL OR token_ccard = '')";

    private static final String SELECT_BLACKLIST_CC = "SELECT DISTINCT TOP (?) ID_CCARD FROM Blacklist_CC (NOLOCK) where (ID_CCARD IS NOT NULL AND ID_CCARD > 0) AND (token_ccard IS NULL OR token_ccard = '')";

    private static final String UPDATE_CUSTOMER_CCARD = "UPDATE Customer_CCard SET token_ccard = ? WHERE id_ccard = ?;";

    private static final String UPDATE_TRANSACTION_LOG = "UPDATE Transaction_Log SET token_ccard = ? WHERE id_ccard = ?;";

    private static final String UPDATE_LASTMSISDNUPDATE = "UPDATE LastMsisdnUpdate SET token_ccard = ? WHERE id_ccard = ?;";

    private static final String UPDATE_CHARGEBACK_LOG = "UPDATE Chargeback_log SET token_ccard = ? WHERE id_ccard = ?; ";

    private static final String UPDATE_BLACKLIST_CC = "UPDATE Blacklist_CC SET token_ccard = ? WHERE id_ccard = ?; ";
    
    private String jndiDataSource = null;
    
    public TranslationDAO() {
         jndiDataSource = TranslationProperties.getConf().getString("jndi.datasource", "java:/RechargeDS");
    }

    public List<Long> getCardsFromCustomerCreditCard(int topRow) {
        List<Long> result = new LinkedList<Long>();

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(SELECT_CUSTOMER_CCARD);
            pstmt.setInt(1, topRow);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getLong("ID_CCARD"));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar os Cartoes. Metodo: getCardsFromCustomerCreditCard.", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        return result;
    }

    public List<Long> getCardsFromTransactionLog(int topRow) {
        List<Long> result = new LinkedList<Long>();

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(SELECT_TRANSACTION_LOG);
            pstmt.setInt(1, topRow);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getLong("ID_CCARD"));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar os Cartoes. Metodo: getCardsFromTransactionLog.", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        return result;
    }

    public List<Long> getCardsFromLastMsisdnUpdate(int topRow) {
        List<Long> result = new LinkedList<Long>();

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(SELECT_LASTMSISDNUPDATE);
            pstmt.setInt(1, topRow);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getLong("ID_CCARD"));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar os Cartoes. Metodo: getCardsFromLastMsisdnUpdate.", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        return result;
    }

    public List<Long> getCardsFromChargebackLog(int topRow) {
        List<Long> result = new LinkedList<Long>();

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(SELECT_CHARGEBACK_LOG);
            pstmt.setInt(1, topRow);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getLong("ID_CCARD"));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar os Cartoes. Metodo: getCardsFromChargebackLog.", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        return result;
    }

    public List<Long> getCardsFromBlackListCC(int topRow) {
        List<Long> result = new LinkedList<Long>();

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(SELECT_BLACKLIST_CC);
            pstmt.setInt(1, topRow);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                result.add(rs.getLong("ID_CCARD"));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar os Cartoes. Metodo: getCardsFromBlackListCC.", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        return result;
    }

    public void updateCard(Long cardId, String cardToken) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(UPDATE_CUSTOMER_CCARD + UPDATE_TRANSACTION_LOG + UPDATE_LASTMSISDNUPDATE + UPDATE_CHARGEBACK_LOG
                    + UPDATE_BLACKLIST_CC);

            int index = 1;
            pstmt.setString(index++, cardToken);
            pstmt.setLong(index++, cardId);
            pstmt.setString(index++, cardToken);
            pstmt.setLong(index++, cardId);
            pstmt.setString(index++, cardToken);
            pstmt.setLong(index++, cardId);
            pstmt.setString(index++, cardToken);
            pstmt.setLong(index++, cardId);
            pstmt.setString(index++, cardToken);
            pstmt.setLong(index++, cardId);

            pstmt.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException("Erro ao atualizar os Cartoes. Metodo: updateCards", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
    }

}
