package com.m4u.recharge.tools.translation.service;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.LocalBinding;

import com.m4u.componentes.conector.eldorado.EldoradoConnector;
import com.m4u.componentes.conector.eldorado.EldoradoConnectorException;
import com.m4u.componentes.conector.eldorado.GetCreditCardResponse;
import com.m4u.componentes.conector.eldorado.impl.EldoradoConnectorFactory;
import com.m4u.recharge.tools.translation.dao.TranslationDAO;
import com.m4u.recharge.tools.translation.queue.TranslationMDB;
import com.m4u.recharge.tools.translation.util.TranslationProperties;

@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Stateless(mappedName = TranslationService.JNDI_NAME, name = "TranslationServiceBean")
@LocalBinding(jndiBinding = TranslationService.JNDI_NAME)
public class TranslationServiceBean implements TranslationService {

    private static final Logger LOGGER = Logger.getLogger(TranslationServiceBean.class);

    private EldoradoConnector eldorado;
    private String targetServer = TranslationProperties.getConf().getString("eldorado.target.server");
    private String serviceId = TranslationProperties.getConf().getString("eldorado.service.id");

    @Resource(mappedName = "java:/ConnectionFactory")
    ConnectionFactory factory = null;
    Session sessionSender = null;
    MessageProducer translationProducer = null;
    @Resource(mappedName = TranslationMDB.JNDI_NAME)
    Destination translationQueue = null;

    @PostConstruct
    public void init() {
        this.eldorado = EldoradoConnectorFactory.getConnector(targetServer, serviceId);

        Connection conn = null;
        try {
            conn = factory.createConnection();
            sessionSender = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            translationProducer = sessionSender.createProducer(translationQueue);
        } catch (Exception e) {
            throw new RuntimeException("Problemas ao inicializar as filas.", e);
        }

    }

    @Override
    public void translateCustomerCreditCard(int topRow) {
        TranslationDAO dao = new TranslationDAO();
        List<Long> cards = dao.getCardsFromCustomerCreditCard(topRow);
        for (Long cardId : cards) {
            this.enqueueToTranslation(cardId);
        }
    }

    @Override
    public void translateTransactionLog(int topRow) {
        TranslationDAO dao = new TranslationDAO();
        List<Long> cards = dao.getCardsFromTransactionLog(topRow);
        for (Long cardId : cards) {
            this.enqueueToTranslation(cardId);
        }
    }

    @Override
    public void translateLastMsisdnUpdate(int topRow) {
        TranslationDAO dao = new TranslationDAO();
        List<Long> cards = dao.getCardsFromLastMsisdnUpdate(topRow);
        for (Long cardId : cards) {
            this.enqueueToTranslation(cardId);
        }
    }

    @Override
    public void translateChargebackLog(int topRow) {
        TranslationDAO dao = new TranslationDAO();
        List<Long> cards = dao.getCardsFromChargebackLog(topRow);
        for (Long cardId : cards) {
            this.enqueueToTranslation(cardId);
        }
    }

    @Override
    public void translateBlackList(int topRow) {
        TranslationDAO dao = new TranslationDAO();
        List<Long> cards = dao.getCardsFromBlackListCC(topRow);
        for (Long cardId : cards) {
            this.enqueueToTranslation(cardId);
        }
    }

    @Override
    public void enqueueToTranslation(Long cardId) {
        Message message;
        try {
            message = sessionSender.createObjectMessage(cardId);
            long time = System.currentTimeMillis() + 10000;
            message.setLongProperty("JMS_JBOSS_SCHEDULED_DELIVERY", time);
            translationProducer.send(message);
        } catch (JMSException e) {
            LOGGER.error("Erro ao enviar cartão para fila de tradução.", e);
        }
    }

    @Override
    public void translateCreditCard(Long cardId) {
        TranslationDAO dao = new TranslationDAO();
        GetCreditCardResponse response = null;
        try {
            long t0 = System.currentTimeMillis();
            response = eldorado.getCreditCard(cardId);
            long t1 = System.currentTimeMillis();
            long t2 = 0;
            if (response != null) {
                String cardToken = response.getCard().getKey();
                dao.updateCard(cardId, cardToken);
                t2 = System.currentTimeMillis();
            }
            LOGGER.info("TRADUÇÃO para cardID " + cardId + ": Consulta no Eldorado=" + (t1 - t0) + "ms; Atualização em banco=" + (t2 - t1)
                    + "ms; Total=" + (t2 - t0) + "ms;");
        } catch (EldoradoConnectorException e) {
            LOGGER.error("Erro durante tradução de Cartão de Crédito " + cardId + ": ", e);
        } catch (Exception e) {
            throw new EJBException("Erro durante tradução de Cartão de Crédito: " + cardId, e);
        }

    }
}
