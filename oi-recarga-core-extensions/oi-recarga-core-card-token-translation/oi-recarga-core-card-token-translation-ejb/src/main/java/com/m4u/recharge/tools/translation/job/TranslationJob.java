package com.m4u.recharge.tools.translation.job;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.m4u.recharge.tools.translation.service.TranslationService;

public class TranslationJob implements Job {
    
    private static final Logger LOGGER = Logger.getLogger(TranslationJob.class);
    
    private static String TOP_ROW = "topRow";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LOGGER.info("Job de migracao iniciado");
        int topRow = Integer.parseInt(context.getJobDetail().getJobDataMap().getString(TOP_ROW));
        getTranslationService().translateCustomerCreditCard(topRow);
        getTranslationService().translateTransactionLog(topRow);
        getTranslationService().translateLastMsisdnUpdate(topRow);
        getTranslationService().translateChargebackLog(topRow);
        getTranslationService().translateBlackList(topRow);
    }

    private TranslationService getTranslationService() {
        InitialContext context;
        try {
            context = new InitialContext();
            return (TranslationService) context.lookup(TranslationService.JNDI_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Erro ao realizar lookup no JNDI: " + TranslationService.JNDI_NAME, e);
        }
    }

}
