package com.m4u.recharge.tools.translation.mbean;

import org.jboss.ejb3.annotation.Management;

@Management
public interface ITranslationMBean {

    public void translateCustomerCreditCard(int topRow);

    public void translateTransactionLog(int topRow);

    public void translateLastMsisdnUpdate(int topRow);

    public void translateChargebackLog(int topRow);

    public void translateBlackList(int topRow);
    
    public void translateCreditCard(long cardId);

}
