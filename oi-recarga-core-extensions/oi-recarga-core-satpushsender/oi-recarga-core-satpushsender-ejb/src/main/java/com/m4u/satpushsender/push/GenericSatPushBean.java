package com.m4u.satpushsender.push;

public class GenericSatPushBean {

    private String alias;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

}
