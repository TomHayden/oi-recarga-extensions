/*
 * Created on Feb 4, 2010 by alexandre.assis for satpushsender
 * 
 * Copyright M4U Solucoes 2001-2010
 */
package com.m4u.satpushsender.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.m4u.satpushsender.vo.EletricProfileVO;

public class EletricProfileParser {

    private static final String PATH = "http://#SERVER#:8080/oi/search?action=profile&msisdn=#MSISDN#";

    private static Logger logger = Logger.getLogger(EletricProfileParser.class);

    public static void main(String[] args) {
        EletricProfileVO eletricProfile = search("552188631842");
        System.out.println(eletricProfile);
    }

    public static EletricProfileVO search(String msisdn) {

        HttpURLConnection connection = null;
        InputStream stream = null;
        String finalPath = null;
        try {
            String bindAddress = System.getProperty("jboss.bind.address", "localhost");
            finalPath = PATH.replace("#SERVER#", bindAddress).replace("#MSISDN#", msisdn);
            URL url = new URL(finalPath);
            connection = (HttpURLConnection) url.openConnection();
            stream = connection.getInputStream();
            InputStreamReader reader = new InputStreamReader(stream);
            BufferedReader bf = new BufferedReader(reader);
            String line = null;
            Map<String, String> map = new HashMap<String, String>();
            while ((line = bf.readLine()) != null) {
                String vet[] = line.split("=");
                map.put(vet[0], vet[1]);
            }
            map.put("msisdn", msisdn);
            return new EletricProfileVO(map);

        } catch (MalformedURLException e) {
            logger.error("problemas acessando perfil eletrico: path:" + finalPath, e);
        } catch (IOException e) {
            logger.error("problemas acessando perfil eletrico: path:" + finalPath, e);

        } catch (Exception e) {
            logger.error("problemas acessando perfil eletrico: path:" + finalPath, e);
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
                if (connection != null) {
                    connection.disconnect();
                }
            } catch (Exception e) {

                logger.error("problemas fechado connexao com stream, path:" + finalPath, e);
            }
        }
        return null;

    }
}
