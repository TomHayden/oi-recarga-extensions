package com.m4u.satpushsender.push;



import com.m4u.net.sms.SMSMessage;

public class SMS implements MobileMessage {
    private static final long serialVersionUID = -2452251225398308033L;

    private SMSMessage smsMessage = null;

    private Long transactionId = null;

    public SMS(SMSMessage smsMessage, long transactionId) {
        this(smsMessage);
        this.transactionId = transactionId;
    }

    public Long getTransactionId() {
        return this.transactionId;
    }

    public SMS(SMSMessage smsMessage) {
        this.smsMessage = smsMessage;
    }

    public SMSMessage getSMSMessage() {
        return this.smsMessage;
    }
}
