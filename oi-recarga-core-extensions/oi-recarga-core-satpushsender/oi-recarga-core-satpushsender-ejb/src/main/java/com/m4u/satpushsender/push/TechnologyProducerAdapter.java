package com.m4u.satpushsender.push;


public interface TechnologyProducerAdapter
{
    public static final String PARAM_MSG_FROM = "from";

    public static final String PARAM_MSG_TO = "to";

    public static final String PARAM_MSG_DATA = "data";

    public static final String PARAM_MSG_RECEIVE_DATE = "receive_date";

    public static final String PARAM_MSG_IS_DLR = "isDlr";

    public static final String PARAM_MSG_TRANSACTION_TYPE = "transactionType";

    public MobileMessage produceGenericPush(SimDescriptor msisdnPushDestination, byte[] satPushPage, String LA);
}
