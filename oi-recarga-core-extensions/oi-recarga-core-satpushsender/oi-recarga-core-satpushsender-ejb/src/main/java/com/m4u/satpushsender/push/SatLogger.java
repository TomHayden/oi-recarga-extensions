package com.m4u.satpushsender.push;


import org.apache.log4j.Logger;

public class SatLogger
{
    private static Logger logger = Logger.getLogger(SatLogger.class);

    public static void logClientMessage(long transaction, SimDescriptor simcard, byte[] data, byte tipo, String[] params)
    {
        try
        {
            // logando o header a mensagem enviada pelo cliente
            StringBuffer sb = new StringBuffer().append("receive|from|").append(simcard.getMsisdn())
                    .append("|transaction ID|");

            // Tipo da transacao
            long transactionId = getLong(data, 0);
            sb.append(transactionId).append('|').append(getTipo(tipo)).append('|');

            if (params != null)
            {
                // logando os parametros
                for (int i = 0; i < params.length; i++)
                {
                    String message = params[i];
                    message = message.replaceAll("\n", " ");
                    message = message.replaceAll("[\\r\\f]", " ");
                    while (message.indexOf("|") >= 0)
                    {
                        message.replace('|', ',');
                    }
                    sb.append('|').append(message);
                }
            }

            logger.info(sb.toString());
        }
        catch (Exception e)
        {
            logger.error("Error logging oi s@t request", e);
        }
    }

    public static void logServerMessage(String msisdn, String from, byte[] data, boolean crypto, byte tipo,
            String[] params)
    {
        try
        {
            // logando o header a mensagem enviada pelo cliente
            StringBuffer sb = new StringBuffer().append("send|from|").append(from).append("|to|").append(msisdn)
                    .append("|encrypted:").append(crypto);
            sb.append('|').append(getTipo(tipo)).append('|');
            if (params != null)
            {
                // logando os parametros
                for (int i = 0; i < params.length; i++)
                {
                    String message = params[i];
                    if (message != null)
                    {
                        message = message.replaceAll("[\\n\\r\\f]", " ");
                        message = message.replace('|', ',');
                    }
                    sb.append(message);
                }
            }

            logger.info(sb.toString());
        }
        catch (Exception e)
        {
            logger.error("Error logging oi javacard request", e);
        }
    }

    private static String getTipo(byte codigo)
    {
        switch (codigo)
        {
        case 0x03:
            return "Solicitacao Exibir Mensagem";
        case 0x0E:
            return "Resultado Compra OK";
        case 0x0F:
            return "Resultado Cancelamento OK";
        case 0x10:
            return "Solicitacao Inicio Desbloqueio de Cartao";
        case 0x11:
            return "Solicitacao Desbloqueio de Cartao";
        case 0x12:
            return "Resposta Autorizacao OK de Compra";
        case 0x13:
            return "Resposta Autorizacao Negada de Compra";
        case 0x14:
            return "Solicitacao Listagem de Cartoes para Ultimas Transacoes";
        case 0x15:
            return "Solicitacao Consulta Ultimas Transacoes";
        case 0x16:
            return "Solicitacao Listagem de Cartoes para Saldo";
        case 0x17:
            return "Solicitacao Saldo";
        case 0x18:
            return "Solicitacao Reset de Senha";
        case 0x19:
            return "Solicitacao Listagem de Cartoes para Troca de Senha";
        case 0x1A:
            return "Solicitacao Troca de Senha";
        case 0x1C:
            return "Resposta Inicio Desbloqueio de Cartao";
        case 0x1D:
            return "Resposta Desbloqueio de Cartao";
        case 0x1E:
            return "Solicitacao Autorizacao de Compra";
        case 0x1F:
            return "Resultado Listagem de Cartoes para Ultimas Transacoes";
        case 0x20:
            return "Resultado Listagem de Cartoes para Saldo";
        case 0x21:
            return "Iniciar Processo de Reset de Senha";
        case 0x22:
            return "Resultado Listagem de Cartoes para Troca de Senha";
        case 0x23:
            return "Requisicao de Exibicao do Menu";
        case 0x29:
            return "Resultado Compra Falhou";
        case 0x2A:
            return "Resultado Cancelamento Falhou";
        case 0x2F:
            return "Troca de senha via Paggo";
        case 0x32:
            return "Resposta de Compra de Recarga com Paggo via SMS";
        case 0x33:
            return "Consulta de Saldo por SMS";
        case 0x34:
            return "Consulta por SMS de Melhor dia de Compra";
        case 0x35:
            return "Consulta por SMS de extrato";
        case 0x36:
            return "Ativacao por SMS";
        case 0x42:
            return "Troca de senha iniciada por SMS";
        case 0x43:
            return "Venda iniciada pelo Parceiro";
        case 0x47:
            return "Teste de Compatibilidade";
        case 0x49:
            return "Consulta de Fatura";
        case 0x52:
            return "Auto Servico";
        case 0x53:
            return "Ativacao de RFID";
        case 0x54:
            return "Sonda SAT";

        default:
            logger.warn("Tipo de transacao desconhecida: " + Integer.toHexString(codigo & 0xFF));
            return "Transacao desconhecida";
        }
    }

    private static long getLong(byte[] buffer, int offset)
    {
        long value = 0L;
        for (int i = offset; i < offset + 8; i++)
        {
            value <<= 8;
            value += buffer[i] & 0xFF;
        }
        return value;
    }

}
