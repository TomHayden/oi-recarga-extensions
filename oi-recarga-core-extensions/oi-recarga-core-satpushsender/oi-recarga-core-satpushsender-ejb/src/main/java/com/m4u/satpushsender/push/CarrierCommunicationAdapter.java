package com.m4u.satpushsender.push;

public interface CarrierCommunicationAdapter
{
    /**
     * Envia push generico
     * 
     * @param msisdnPushDestination
     * @param satPushPage
     * @param LA
     * @return
     */
    public boolean sendGenericPushByJMXConsole(SimDescriptor msisdnPushDestination, byte[] satPushPage, String LA);
}