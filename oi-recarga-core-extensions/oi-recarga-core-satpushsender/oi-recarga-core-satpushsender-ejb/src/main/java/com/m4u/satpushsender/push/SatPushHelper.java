/*
 * Created on Feb 8, 2010 by alexandre.assis for satpushsender
 * 
 * Copyright M4U Solucoes 2001-2010
 */
package com.m4u.satpushsender.push;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.log4j.Logger;

import com.m4u.net.sms.SMSException;
import com.m4u.net.sms.SMSMessage;
import com.m4u.satpushsender.vo.EletricProfileVO;

public class SatPushHelper {

    // private static final String URL = "http://vas.oi.m4u.com.br:84/dispatcherSMS.pl";

    private static final String ENCODING = "ISO8859-1";
    private static final String ENCODING7BITS = "X-GSM-VAS-OI-NON-EXTENDED";
    private static final String ENCODING8BITS = ENCODING;

    private static Logger log = Logger.getLogger(SatPushHelper.class);

    private String smsc;
    private String la;
    private String url;
    private byte[] pushFile;

    private Map<String, ByteArrayOutputStream> map;

    public SatPushHelper(String la, String smsc, String url, Map<String, ByteArrayOutputStream> map) {
        super();
        this.la = la;
        this.smsc = smsc;
        this.url = url;
        this.map = map;
    }

    public SatPushHelper(String la, String smsc, String url, byte[] pushFile) {
        super();
        this.la = la;
        this.smsc = smsc;
        this.url = url;
        this.pushFile = pushFile;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSmsc() {
        return smsc;
    }

    public void setSmsc(String smsc) {
        this.smsc = smsc;
    }

    public String getLa() {
        return la;
    }

    public void setLa(String la) {
        this.la = la;
    }

    public void sendPush(EletricProfileVO profile) {

        SimDescriptor descriptor = new SimDescriptor();
        descriptor.setCarrier(Carrier.OI);
        descriptor.setManufacturer(Integer.parseInt(profile.getManufacturer()));
        MobileMessage message = null;
        SatTechAdapter tech = new SatTechAdapter("");
        if (map != null) {

            ByteArrayOutputStream baos = map.get(profile.getManufacturer());
            if (baos == null) {
                baos = map.get("default");
            }
            pushFile = baos.toByteArray();
            message = tech.produceGenericPush(descriptor, pushFile, null);
        } else {
            message = tech.produceGenericPush(descriptor, pushFile, null);
        }

        SMS sms = (SMS) message;

        try {
            SMSMessage smsMessage = sms.getSMSMessage();
            smsMessage.setTo(profile.getMsisdn());
            send(smsMessage);

        } catch (SMSException e) {

            log.error("msisdn:[" + profile.getMsisdn() + "] problemas enviando sms", e);
        } catch (Exception e) {

            log.error("msisdn:[" + profile.getMsisdn() + "] problema interno enviando sms", e);
        }

    }

    private void send(SMSMessage smsMessage) throws SMSException {

        URL url;
        long ini = System.currentTimeMillis();
        String vasURL = this.getUrl();
        StringBuffer sb = new StringBuffer(512);
        try {

            sb.append(vasURL);
            sb.append("?smsc=");
            sb.append(this.getSmsc());
            sb.append("&from=");
            sb.append(this.getLa());
            sb.append("&to=");
            sb.append(smsMessage.getTo());

            // seta o header
            byte[] udh = smsMessage.getUDHAsArray();
            if (udh.length > 0) {
                sb.append("&udh=").append(new String(udh, "X-HEX"));
            }

            // determina o dcs
            byte smsMessageDataCoding = smsMessage.getDataCoding();
            byte vasCoding = getCoding(smsMessageDataCoding);
            sb.append("&coding=").append(vasCoding).append("&alt-dcs=").append(getAltDcs(smsMessageDataCoding)).append("&mclass=").append(
                    getMClass(smsMessageDataCoding)).

            // protocol id
                    append("&pid=").append(smsMessage.getProtocolId());

            // seta o texto
            byte[] contentArray = smsMessage.getContentAsArray();
            if (contentArray.length > 0) {
                sb.append("&text=");
                if (udh.length == 0) {
                    // nao tem header

                    switch (vasCoding) {
                    case (byte) 1: {
                        // Envio em 7 bits
                        String s = new String(contentArray, ENCODING7BITS);
                        // new String("a@$b".getBytes("X-GSM-VAS-OI-NON-EXTENDED"),"X-GSM-VAS-OI-NON-EXTENDED");
                        sb.append(URLEncoder.encode(new String(s.getBytes(), ENCODING7BITS), ENCODING));
                        break;
                    }
                    case (byte) 2: {
                        // Envio em 8 bits com encoding normal
                        sb.append(URLEncoder.encode(new String(contentArray, ENCODING8BITS), ENCODING));
                        break;
                    }
                    default: {
                        // mesagem codificada em UCS2
                        throw new UnsupportedEncodingException("UCS2 encoding not supported.");
                    }
                    }
                } else {
                    // tem header, enviamos em formato binario e retiramos o
                    // header já adicionado
                    // do corpo
                    String content = new String(contentArray, 0, contentArray.length, "X-HEX");

                    sb.append(URLEncoder.encode(content, ENCODING));
                }
            }

            log.debug(sb.toString());
            url = new URL(sb.toString());
        } catch (Exception e) {
            String msgErr = "Problemas na requisicao de envio de SMS. Msg[" + e.getClass().getCanonicalName() + " : " + e.getMessage()
                    + "}, url[" + sb + "]";

            throw new SMSException(msgErr, e);
        }

        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestMethod("GET");

            // le o resultado da requisicao
            int httpResponseCode = connection.getResponseCode();
            String httpResponse = readHttpResponse(connection);

            try {
                connection.disconnect();
            } catch (Exception e) {
            }

            if (httpResponseCode != HttpURLConnection.HTTP_OK && httpResponseCode != HttpURLConnection.HTTP_CREATED
                    && httpResponseCode != HttpURLConnection.HTTP_ACCEPTED) {
                String mErr = "Erro no Envio de SMS para OI, Tempo de Resposta[" + (System.currentTimeMillis() - ini)
                        + "], httpResponseCode[" + httpResponseCode + "], resposta[" + httpResponse + "], request[" + sb + "]";

                throw new SMSException(mErr);
            }
            if (!"Sent.".equals(httpResponse)) {
                String mErr = "Resposta Inesperada no Envio de SMS para OI, Tempo de Resposta[" + (System.currentTimeMillis() - ini)
                        + "], resposta[" + httpResponse + "], request[" + sb + "]";

                throw new SMSException(mErr);
            } else {
                String m = "SMS enviado para OI, Tempo de Resposta[" + (System.currentTimeMillis() - ini) + "], httpResponseCode["
                        + httpResponseCode + "], resposta[" + httpResponse + "], request[" + sb + "]";
                log.info(m);
            }
        } catch (IOException e) {
            String msgErr = "Problemas no envio de SMS. Tempo de Resposta[" + (System.currentTimeMillis() - ini) + "], Msg["
                    + e.getClass().getCanonicalName() + " : " + e.getMessage() + "}, url[" + sb + "]";

            throw new SMSException(msgErr, e);
        }
    }

    private static byte getCoding(byte smsMessageDataCoding) throws UnsupportedEncodingException {
        switch (smsMessageDataCoding) {
        case SMSMessage.DCS_CODING_7BIT_CLASS_0:
        case SMSMessage.DCS_CODING_7BIT_CLASS_1_ME_SPECIFIC:
        case SMSMessage.DCS_CODING_7BIT_CLASS_2_SIM_SPECIFIC:
        case SMSMessage.DCS_CODING_7BIT_CLASS_3_TE_SPECIFIC: {
            return 1; // DCS=0
        }
        case SMSMessage.DCS_CODING_8BIT_CLASS_0:
        case SMSMessage.DCS_CODING_8BIT_CLASS_1_ME_SPECIFIC:
        case SMSMessage.DCS_CODING_8BIT_CLASS_2_SIM_SPECIFIC:
        case SMSMessage.DCS_CODING_8BIT_CLASS_3_TE_SPECIFIC: {
            return 2; // DCS=4
        }
        default:
            throw new UnsupportedEncodingException("Data coding scheme not supported: " + Integer.toHexString(0xFF & smsMessageDataCoding));
        }
    }

    private static byte getAltDcs(byte smsMessageDataCoding) {
        return ((0xF0 & smsMessageDataCoding) == 0x00) ? (byte) 2 : (byte) 1;
    }

    private static byte getMClass(byte smsMessageDataCoding) throws UnsupportedEncodingException {
        switch (smsMessageDataCoding) {
        case SMSMessage.DCS_CODING_7BIT_CLASS_0:
        case SMSMessage.DCS_CODING_8BIT_CLASS_0: {
            return 1; // message class 0
        }
        case SMSMessage.DCS_CODING_7BIT_CLASS_1_ME_SPECIFIC:
        case SMSMessage.DCS_CODING_8BIT_CLASS_1_ME_SPECIFIC: {
            return 2; // message class 1 (default meaning: ME-specific)

        }
        case SMSMessage.DCS_CODING_7BIT_CLASS_2_SIM_SPECIFIC:
        case SMSMessage.DCS_CODING_8BIT_CLASS_2_SIM_SPECIFIC: {
            return 3; // message class 2 (SIM specific message)
        }
        case SMSMessage.DCS_CODING_7BIT_CLASS_3_TE_SPECIFIC:
        case SMSMessage.DCS_CODING_8BIT_CLASS_3_TE_SPECIFIC: {
            return 4; // message class 3 (default meaning: TE specific)
        }
        default:
            throw new UnsupportedEncodingException("Data coding scheme not supported: " + Integer.toHexString(0xFF & smsMessageDataCoding));
        }
    }

    private static String readHttpResponse(HttpURLConnection connection) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        StringBuffer sb = new StringBuffer();
        String line;
        while ((line = reader.readLine()) != null) {
            if (sb.length() > 0) {
                // sb.append( "<br/> ");
                sb.append('\n');
            }
            sb.append(line);
        }
        reader.close();

        return sb.toString();
    }

}
