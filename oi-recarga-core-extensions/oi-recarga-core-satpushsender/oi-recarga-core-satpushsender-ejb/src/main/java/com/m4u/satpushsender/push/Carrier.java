package com.m4u.satpushsender.push;



import org.apache.log4j.Logger;

public enum Carrier {
    OI, CLARO;

    private static final String CODIGO_OI = "001";

    private static final String CODIGO_CLARO = "002";

    private static Logger logger = Logger.getLogger(Carrier.class);

    /**
     * Retorna a operadora de acordo com o codigo informado
     * 
     * @param carrierCode
     * @return
     */
    public static Carrier getByCode(String carrierCode) {
        if (CODIGO_OI.equals(carrierCode)) {
            return OI;
        } else if (CODIGO_CLARO.equals(carrierCode)) {
            return CLARO;
        } else {
            logger.debug("Codigo da operadora desconhecido: " + carrierCode + ". Retornando default: " + OI);
            return OI;
        }
    }

    public String getCode() {
        if (this.equals(OI)) {
            return CODIGO_OI;
        } else if (this.equals(CLARO)) {
            return CODIGO_CLARO;
        } else {
            return null;
        }

    }
}
