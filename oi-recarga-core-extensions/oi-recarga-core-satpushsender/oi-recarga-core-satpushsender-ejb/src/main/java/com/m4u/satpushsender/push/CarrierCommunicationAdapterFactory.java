package com.m4u.satpushsender.push;

import org.apache.log4j.Logger;



public class CarrierCommunicationAdapterFactory
{
    private static Logger logger = Logger.getLogger(CarrierCommunicationAdapterFactory.class);

    public static CarrierCommunicationAdapter execute(SimDescriptor simDescriptor)
    {
        Carrier carrier = simDescriptor.getCarrier();
        if (carrier.equals(Carrier.OI))
        {
            return new OiCommunicationAdapter();
        }
        else
        {
            logger.warn("Carrier diferente");
            return null;
        }
    }
}
