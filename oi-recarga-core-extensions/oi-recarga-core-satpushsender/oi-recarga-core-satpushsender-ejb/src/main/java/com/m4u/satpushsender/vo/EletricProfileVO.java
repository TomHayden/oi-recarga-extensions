/*
 * Created on Feb 4, 2010 by alexandre.assis for satpushsender
 * 
 * Copyright M4U Solucoes 2001-2010
 */
package com.m4u.satpushsender.vo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class EletricProfileVO implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    private String iccid;
    private String keyid;
    private String manufacturer;
    private String satkeyset;
    private String satuid;
    private String tripledeskeyset;
    private String type;
    private String msisdn;
    private Date date;

    public EletricProfileVO(String iccid, String keyid, String manufacturer, String satkeyset, String satuid, String tripledeskeyset,
            String type, String msisdn) {
        super();
        this.iccid = iccid;
        this.keyid = keyid;
        this.manufacturer = manufacturer;
        this.satkeyset = satkeyset;
        this.satuid = satuid;
        this.tripledeskeyset = tripledeskeyset;
        this.type = type;
        this.msisdn = msisdn;
        this.date = new Date();
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public EletricProfileVO(Map<String, String> map) {
        this(map.get("iccid"), map.get("keyid"), map.get("manufacturer"), map.get("satkeyset"), map.get("satuid"), map
                .get("tripledeskeyset"), map.get("type"), map.get("msisdn"));
    }

    public String toString() {
        StringBuffer bf = new StringBuffer();
        bf.append("iccid=" + getIccid());
        bf.append("\n");
        bf.append("keyid=" + getKeyid());
        bf.append("\n");
        bf.append("manufacturer=" + getManufacturer());
        bf.append("\n");
        bf.append("satkeyset=" + getSatkeyset());
        bf.append("\n");
        bf.append("satuid=" + getSatuid());
        bf.append("\n");
        bf.append("tripledeskeyset=" + getTripledeskeyset());
        bf.append("\n");
        bf.append("type=" + getType());
        bf.append("\n");
        bf.append("msisdn=" + getMsisdn());
        bf.append("\n");
        bf.append("date=" + df.format(date));
        return bf.toString();
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getKeyid() {
        return keyid;
    }

    public void setKeyid(String keyid) {
        this.keyid = keyid;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSatkeyset() {
        return satkeyset;
    }

    public void setSatkeyset(String satkeyset) {
        this.satkeyset = satkeyset;
    }

    public String getSatuid() {
        return satuid;
    }

    public void setSatuid(String satuid) {
        this.satuid = satuid;
    }

    public String getTripledeskeyset() {
        return tripledeskeyset;
    }

    public void setTripledeskeyset(String tripledeskeyset) {
        this.tripledeskeyset = tripledeskeyset;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
