package com.m4u.satpushsender.push;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.m4u.net.sms.CommandPacket;
import com.m4u.net.sms.SMSException;
import com.m4u.net.sms.SMSMessage;
import com.m4u.satpush.SATPushException;
import com.m4u.satpush.SATPushPackagerFactory;
import com.m4u.satpush.packager.SATPushPackager;
import com.m4u.sbc.SBCCompiler;
import com.m4u.sbc.SBCException;

public class SatTechAdapter implements TechnologyProducerAdapter {
    private static Logger logger = Logger.getLogger(SatTechAdapter.class);

    private SBCCompiler scf = new SBCCompiler();

    private static DecimalFormat outputCurrencyFormat = new DecimalFormat("#,##0.00");

    private static SimpleDateFormat outputLongDateFormat = new SimpleDateFormat("dd/MM/yy");

    private static final int MAX_TEXT_LENGTH = 240;

    private static final int SMS_MAX_TEXT_LENGTH = 160;

    private static String tar;

    public SatTechAdapter(String tar) {
        SatTechAdapter.tar = tar;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');

        synchronized (outputCurrencyFormat) {
            outputCurrencyFormat.setDecimalFormatSymbols(symbols);
        }
    }

    public MobileMessage produceGenericPush(SimDescriptor msisdnPushDestination, byte[] satPushPage, String LA) {
        byte[] message = null;

        // xml do push
        message = satPushPage;

        GenericSatPushBean bean = new GenericSatPushBean();

        bean.setAlias("CR61");

        String[] logParam = new String[1];
        String from = LA;

        return produceMessage(msisdnPushDestination, message, bean, from, false);
    }

    private String longToHex(long number) {
        String hex = Long.toHexString(number);
        while (hex.length() < 16) {
            hex = "0" + hex;
        }
        return hex;
    }

    /**
     * @param orderVO
     * @param message
     * @param insert
     * @return mensagem SMS
     */
    private MobileMessage produceMessage(SimDescriptor simcard, byte[] message, Object insert, String from, boolean isConfirmed) {
        SATPushPackager packager;

        try {
            TextFormatter formatter = new TextFormatter();

            // Codigo inserido para configurar o LA de retorno do sat push, caso
            // o push deva ser enviado em ambiente de teste. Isso eh feito
            // verificando se existe a propriedade que representa o LA de teste
            // try {
            // if (insert != null) {
            // String testLA = PropertiesUtil.getString("la.test");
            // if (testLA != null && !testLA.isEmpty()) {
            // if (insert instanceof GenericSatPushBean) {
            // logger.debug("Ambiente de teste: Alterando o LA de retorno do push para " + testLA);
            // ((GenericSatPushBean) insert).setLA(StringUtil.reverseLa(testLA));
            // }
            // }
            // }
            // } catch (Exception e) {
            // logger.warn("Erro na configuracao do LA de teste como La de retorno do push", e);
            // }

            /*
             * TODO Inserir encoding
             */
            String newMessage = formatter.format(new String(message), insert);
            String msisdn = simcard.getMsisdn();

            // logger.debug("Mensagem sat:" + newMessage);
            StringReader reader = new StringReader(newMessage);
            byte[] compiledMessage = scf.compile(reader);
            // logger.debug("Mensagem sat compilada:" +
            // XGsmDefault.convert(compiledMessage));
            if (simcard.getManufacturer() == SimDescriptor.MANUFACTURER_AXALTO
                    || simcard.getManufacturer() == SimDescriptor.MANUFACTURER_ABNOTE) {
                logger.debug("Produzindo sat High pro msisdn " + msisdn);
                packager = SATPushPackagerFactory.create(SATPushPackagerFactory.CARRIER_OI, SATPushPackagerFactory.SIMTYPE_HIGH);
            } else { // gemplus, g&d burti, sagem orga
                logger.debug("Produzindo sat Low para o msisdn " + msisdn);
                packager = SATPushPackagerFactory.create(SATPushPackagerFactory.CARRIER_OI, SATPushPackagerFactory.SIMTYPE_LOW);
            }

            SMSMessage sms = packager.pack(msisdn, compiledMessage);

            if (simcard.getManufacturer() == SimDescriptor.MANUFACTURER_AXALTO)
            // TODO teste da gemalto do sim card Oi Paggo Oi PDV com S@T Push -
            // verificar se podemos fazer isto para todos os sim cards (axalto
            // antigos e gemplus)
            // if(msisdn.equals("552188815373") || msisdn.equals("552188815397")
            // || msisdn.equals("553188021989") || msisdn.equals("2188815373")
            // || msisdn.equals("2188815397") || msisdn.equals("3188021989"))
            {
                CommandPacket cp = sms.getContentAsCommandPacket();
                cp.setKIc((byte) (CommandPacket.KIC_FLAG_ALGORITHM_DES | CommandPacket.KIC_KEY_INDEX_03));
                cp.setKId((byte) (CommandPacket.KID_FLAG_ALGORITHM_DES | CommandPacket.KID_KEY_INDEX_03));
                sms.setContent(cp);
            }

            sms.setFrom(from);
            sms.setConfirmed(isConfirmed);
            return new SMS(sms);
        } catch (SATPushException e) {
            logger.error("Error creating sat push packager factory", e);
        } catch (IOException e) {
            logger.error("Error packing sat push file", e);
        } catch (SBCException e) {
            logger.error("Error compiling sat push file", e);
        } catch (SMSException e) {
            logger.error("Error generating sat push message", e);
        }
        return null;
    }
}
