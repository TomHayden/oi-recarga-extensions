/*
 * Created on Mar 17, 2010 by alexandre.assis for satpushsender
 * 
 * Copyright M4U Solucoes 2001-2010
 */
package com.m4u.satpushsender.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.resource.ResourceException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.m4u.satpushsender.vo.AnalysisVO;

public class AnalysisDAO {

    private Logger logger = Logger.getLogger(AnalysisDAO.class);
    private DataSource dataSource = null;
    private final String DATABASE = "java:/SatPushSenderDS";

    private static final String INSERT_WITH_PROFILE = "insert into analysis(MSISDN,LA,TEXT,STATUS,DETAIL,DATE,ICCID,KEYID,MANUFACTURER,SATKEYSET,SATUID,TRIPLEDESKEYSET,TYPE) values(?,?,?,?,?,getdate(),?,?,?,?,?,?,?)";
    private static final String INSERT_WITHOUT_PROFILE = "insert into analysis(MSISDN,LA,TEXT,STATUS,DETAIL,DATE) values(?,?,?,?,?,getdate())";
    private static AnalysisDAO dao = null;

    public void insert(AnalysisVO analisysVO) throws ResourceException, SQLException {

        Connection conn = null;
        PreparedStatement stmt = null;
        String sql = analisysVO.getEletricProfileVO() == null ? INSERT_WITHOUT_PROFILE : INSERT_WITH_PROFILE;

        try {
            conn = dataSource.getConnection();
        } catch (Exception e) {
            dao = null;
            throw new ResourceException("problemas buscando conexao em datasource", e);
        }

        try {

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, analisysVO.getFrom());
            stmt.setString(2, analisysVO.getTo());
            stmt.setString(3, analisysVO.getText());
            stmt.setBoolean(4, analisysVO.isStatus());
            stmt.setString(5, analisysVO.getDetail());
            if (analisysVO.getEletricProfileVO() != null) {

                stmt.setString(6, analisysVO.getEletricProfileVO().getIccid());
                stmt.setString(7, analisysVO.getEletricProfileVO().getKeyid());
                stmt.setString(8, analisysVO.getEletricProfileVO().getManufacturer());
                stmt.setString(9, analisysVO.getEletricProfileVO().getSatkeyset());
                stmt.setString(10, analisysVO.getEletricProfileVO().getSatuid());
                stmt.setString(11, analisysVO.getEletricProfileVO().getTripledeskeyset());
                stmt.setString(12, analisysVO.getEletricProfileVO().getType());
            }
            stmt.execute();

        } finally {

            try {
                stmt.close();
            } catch (Exception ex) {

                logger.error("problemas fechando stmt", ex);
            }
            try {
                conn.close();
            } catch (Exception ex) {

                logger.error("problemas fechando connection", ex);
            }
        }

    }

    public static final AnalysisDAO getInstance() throws ResourceException {
        if (dao == null) {
            synchronized (AnalysisDAO.class) {
                if (dao == null) {
                    dao = new AnalysisDAO();
                }
            }
        }

        return dao;
    }

    private AnalysisDAO() throws ResourceException {

        try {
            InitialContext cx = new InitialContext();
            dataSource = (DataSource) cx.lookup(DATABASE);
        } catch (NamingException e) {
            throw new ResourceException("problemas acessando base:" + DATABASE, e);

        }

    }
}
