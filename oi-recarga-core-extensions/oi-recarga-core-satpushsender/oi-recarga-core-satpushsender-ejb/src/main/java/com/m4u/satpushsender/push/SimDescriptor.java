package com.m4u.satpushsender.push;

import java.io.Serializable;

public class SimDescriptor implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final byte TYPE_WIB_OPEN = 0x21;

    public static final byte TYPE_WIB = 0x22;

    public static final byte TYPE_DYNAMIC = 0x02;

    public static final byte TYPE_CORP_V2 = (byte) 0xFD;

    public static final byte TYPE_STATIC = (byte) 0xFE;

    public static final byte TYPE_UNKNOWN = 0x00;

    public static final byte TYPE_SAT_OPEN = 0x01;

    public static final byte TYPE_SAT = 0x02;

    public static final byte TYPE_JAVACARD_MERCHANT_V10 = 0x03;

    public static final byte TYPE_JAVACARD_CARDHOLDER_V10 = 0x04;

    public static final byte TYPE_JAVACARD_MERCHANT_V20 = 0x05;

    public static final byte TYPE_JAVACARD_CARDHOLDER_V20 = 0x06;

    public static final byte TYPE_JAVACARD_MERCHANT_V30 = 0x07;

    public static final byte TYPE_JAVACARD_CARDHOLDER_V30 = 0x08;

    public static final byte TYPE_JAVACARD_MERCHANT_V40 = 0x09;

    public static final byte TYPE_JAVACARD_CARDHOLDER_V40 = 0x0A;

    public static final byte TYPE_JAVACARD_MERCHANT_V40_SAT = 0x0B;

    /**
     * merchant e portador java 5
     */
    public static final byte TYPE_JAVACARD_V50 = 0x0D;

    // javacard
    public static final byte TYPE_JAVACARD_V50_ABNOTE = 0x0E;

    public static final byte TYPE_JAVACARD_V50_ORGA = 0x0F;

    public static final byte TYPE_JAVACARD_V50_OBERTHUR = 0x10;
    
    public static final byte TYPE_NFC_J2ME = 0x11;

    public static final byte TYPE_SAT_AXALTO2 = (byte) 0xFE;

    public static final byte TYPE_UNSUPPORTED = (byte) 0xFF;

    public static final int MANUFACTURER_UNKNOWN = 0x00;

    /**
     * g&d burti
     */
    public static final int MANUFACTURER_GIESECKE_DEVRIENT = 0x06;

    public static final int MANUFACTURER_GEMPLUS = 0x02;

    /**
     * SAGEM ORGA
     */
    public static final int MANUFACTURER_ORGA = 0x10;

    public static final int MANUFACTURER_AXALTO = 0x04;

    // public static final int MANUFACTURER_OBERTHUR_CARD_SYSTEMS = 0x05;
    // public static final int MANUFACTURER_XPONCARD = 0x06;
    public static final int MANUFACTURER_OBERTHUR = 0x07;

    public static final int MANUFACTURER_ABNOTE = 0x0C;
    public static final String MANUFACTURER_ABNOTE_ELETRIC_PROFILE_NAME = "07.23";
    public static final String MANUFACTURER_ABNOTE_WRONG_KEYSET_ELETRIC_PROFILE_NAME = "66.66";
    public static final String MANUFACTURER_ABNOTE_WRONG_KEYSET_JAVA_3DESDUKPT_KEY = "107";

    public static final int MANUFACTURER_OTHERS = 0xFE;

    /**
     * Numero do telefone
     */
    private String msisdn;

    /**
     * Nome do lojista ou do cardholder
     */
    private String name;

    /**
     * ICCID do Simcard
     */
    private String iccid;

    /**
     * Utilizado pelo javacard v4
     */
    private byte[] dukptKsn;

    /**
     * Utilizado pelo javacard V1-V3 | sat-push
     */
    private int tripleDesKeyset;

    /**
     * Utilizado pelo S@T
     */
    private byte satKeyset;

    private byte type;

    private int manufacturer;

    /**
     * Identificador do perfil eletrico
     */
    private String eletricProfileName;

    /**
     * IMEI
     */
    private String imei;

    /**
     * Operadora do simcard
     */
    private Carrier carrier = Carrier.OI;

    public Carrier getCarrier() {
        return this.carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public SimDescriptor(String msisdn, String iccid, byte tripleDesKeyset, byte satKeyset, byte type, int manufacturer) {
        this.msisdn = msisdn;
        this.iccid = iccid;
        this.tripleDesKeyset = tripleDesKeyset;
        this.satKeyset = satKeyset;
        this.type = type;
        this.manufacturer = manufacturer;
    }

    public SimDescriptor(String msisdn, String iccid, byte tripleDesKeyset, byte satKeyset, byte type, int manufacturer, String imei) {
        this.msisdn = msisdn;
        this.iccid = iccid;
        this.tripleDesKeyset = tripleDesKeyset;
        this.satKeyset = satKeyset;
        this.type = type;
        this.manufacturer = manufacturer;
        this.imei = imei;
    }

    public SimDescriptor(String msisdn, String iccid, byte[] dukptkeyset, byte tripleDesKeyset, byte satKeyset, byte type,
            int manufacturer, String imei) {
        this.msisdn = msisdn;
        this.iccid = iccid;
        this.dukptKsn = dukptkeyset;
        this.tripleDesKeyset = tripleDesKeyset;
        this.satKeyset = satKeyset;
        this.type = type;
        this.manufacturer = manufacturer;
        this.imei = imei;
    }

    public SimDescriptor() {
    }

    public boolean isCardholder() {
        switch (type) {
        case TYPE_JAVACARD_V50:
        case TYPE_JAVACARD_V50_ABNOTE:
        case TYPE_JAVACARD_V50_ORGA:
        case TYPE_JAVACARD_V50_OBERTHUR:
        case TYPE_JAVACARD_MERCHANT_V40_SAT:
        case TYPE_SAT:
        case TYPE_JAVACARD_CARDHOLDER_V40:
        case TYPE_JAVACARD_CARDHOLDER_V30:
        case TYPE_JAVACARD_CARDHOLDER_V20:
        case TYPE_JAVACARD_CARDHOLDER_V10:
        case TYPE_SAT_OPEN:
        case TYPE_SAT_AXALTO2: {
            return true;
        }
        }
        return false;
    }

    public boolean isMerchant() {
        switch (type) {
        case TYPE_JAVACARD_V50:
        case TYPE_JAVACARD_V50_ABNOTE:
        case TYPE_JAVACARD_V50_ORGA:
        case TYPE_JAVACARD_V50_OBERTHUR:
        case TYPE_JAVACARD_MERCHANT_V40_SAT:
        case TYPE_JAVACARD_MERCHANT_V40:
        case TYPE_JAVACARD_MERCHANT_V30:
        case TYPE_JAVACARD_MERCHANT_V20:
        case TYPE_JAVACARD_MERCHANT_V10: {
            return true;
        }
        }
        return false;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public byte[] getDukptKsn() {
        return dukptKsn;
    }

    public void setDukptKsn(byte[] dukpt) {
        this.dukptKsn = dukpt;
    }

    public int getTripleDesKeyset() {
        return tripleDesKeyset;
    }

    public void setTripleDesKeyset(int tripleDesKeySet) {
        this.tripleDesKeyset = tripleDesKeySet;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte javacard) {
        this.type = javacard;
    }

    public int getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(int manufacturer) {
        this.manufacturer = manufacturer;
    }

    public byte getSatKeyset() {
        return satKeyset;
    }

    public void setSatKeyset(byte masterKey) {
        this.satKeyset = masterKey;
    }

    public String getEletricProfileName() {
        return eletricProfileName;
    }

    public void setEletricProfileName(String eletricProfileName) {
        this.eletricProfileName = eletricProfileName;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSatuid() {

        String satuid = "";

        switch (getManufacturer()) {

        case MANUFACTURER_AXALTO:
            satuid = "FE00";
            break;

        case MANUFACTURER_GEMPLUS:
            satuid = "FE01";
            break;

        case MANUFACTURER_GIESECKE_DEVRIENT:
            satuid = "FE00";
            break;

        case MANUFACTURER_ORGA:
            satuid = "FE00";
            break;

        case MANUFACTURER_OBERTHUR:
            satuid = "FE00";
            break;

        case MANUFACTURER_ABNOTE:
            satuid = "FE00";
            break;

        default:
            throw new RuntimeException("Manufacturer not available. " + getManufacturer());
        }

        return satuid;
    }

    public String getKeyid() {

        String keyid = "";

        switch (getManufacturer()) {

        case MANUFACTURER_AXALTO:
            keyid = "8";
            break;

        case MANUFACTURER_GEMPLUS:
            keyid = "7";
            break;

        case MANUFACTURER_GIESECKE_DEVRIENT:
            keyid = "8";
            break;

        case MANUFACTURER_ORGA:
            keyid = "7";
            break;

        case MANUFACTURER_OBERTHUR:
            keyid = "8";
            break;

        case MANUFACTURER_ABNOTE:
            keyid = "6";
            break;

        default:
            throw new RuntimeException("Manufacturer not available. " + getManufacturer());
        }

        return keyid;

    }

}