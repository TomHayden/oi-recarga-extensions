package com.m4u.satpushsender.ejb;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import com.m4u.net.sms.SMSMessage;
import com.m4u.satpushsender.push.SatPushHelper;
import com.m4u.satpushsender.util.EletricProfileParser;
import com.m4u.satpushsender.util.QueueHelper;
import com.m4u.satpushsender.vo.AnalysisVO;
import com.m4u.satpushsender.vo.EletricProfileVO;

public class SmsReceiverMDB implements MessageDrivenBean, MessageListener {

    private Logger logger = Logger.getLogger(SmsReceiverMDB.class);

    private static final long serialVersionUID = 1L;

    private SatPushHelper satPushHelper;

    private List<String> alias;

    /*
     * (non-Javadoc)
     * 
     * @see javax.ejb.MessageDrivenBean#ejbRemove()
     */
    public void ejbRemove() throws EJBException {
    }

    private void load(String parameter) {

        String file = "/satpushsender.properties";

        Map<String, String> mapPushfile = new HashMap<String, String>();
        Map<String, ByteArrayOutputStream> mapPushBaos = new HashMap<String, ByteArrayOutputStream>();

        alias = new ArrayList<String>();
        String url = null;
        String smsc = null;
        String la = null;
        String files[] = null;

        try {

            StringBuffer bf = new StringBuffer();
            InputStream stream = SmsReceiverMDB.class.getResourceAsStream(file);
            Properties properties = new Properties();
            properties.load(stream);

            String aliasValue = properties.getProperty("alias");
            if (aliasValue == null) {
                throw new Exception("alias nao configurado");
            } else {

                String alias[] = aliasValue.split(",");
                for (String string : alias) {
                    this.alias.add(string.toLowerCase().trim());
                }

            }
            url = properties.getProperty("url");
            smsc = properties.getProperty("smsc");
            la = properties.getProperty("la");
            files = properties.getProperty("pushfiles").split(",");

            bf.append("url:[" + url + "] smsc:[" + smsc + "] la:[" + la + "]");
            for (String s : files) {
                String id = properties.getProperty(s);
                mapPushfile.put(id, s);
                bf.append(s + ":[" + id + "]");

            }
            logger.info("load file:" + file + " ->" + bf.toString());

        } catch (Exception e) {

            logger.error("problemas lendo recurso:" + file, e);
            throw new EJBException("problemas lendo recurso:" + file);
        }
        String pushFile = null;

        try {

            Set<String> keys = mapPushfile.keySet();

            for (String manufacturerId : keys) {

                pushFile = mapPushfile.get(manufacturerId);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                InputStream stream = SmsReceiverMDB.class.getResourceAsStream("/" + pushFile);
                InputStreamReader reader = new InputStreamReader(stream);

                BufferedReader inputStream = new BufferedReader(reader);
                String inLine = null;

                while ((inLine = inputStream.readLine()) != null) {
                    // pesquisar na inline o teu interesse
                    if (inLine.contains("%s")) {
                        inLine = String.format(inLine, parameter);
                    }
                    baos.write(inLine.getBytes());
                }

                mapPushBaos.put(manufacturerId, baos);

            }

        } catch (Exception e) {

            logger.error("problemas lendo recurso:" + pushFile, e);
            throw new EJBException("problemas lendo recurso:" + pushFile);
        }

        satPushHelper = new SatPushHelper(la, smsc, url, mapPushBaos);

    }

    /**
     * Default create method
     */
    public void ejbCreate() {
    }

    public void onMessage(Message message) {
        try {
            long time = System.currentTimeMillis();
            SMSMessage vo = (SMSMessage) ((ObjectMessage) message).getObject();

            // recebendo parametro e configurando o arquivo XML para enviar SMS
            String parameter = "push";
            if (vo.getChannelId() != null && !vo.getChannelId().equals("push")) {
                parameter = vo.getChannelId();
            }

            load(parameter);

            logger.info("smsRequest:" + vo);
            String command = vo.getContentAsString().toLowerCase().trim();
            EletricProfileVO eletricProfile = null;
            boolean status = false;
            String detail = null;
            if (alias.contains(command)) {
                eletricProfile = EletricProfileParser.search(vo.getFrom());

                if (eletricProfile != null) {
                    logger.debug("profile:" + eletricProfile);
                    satPushHelper.sendPush(eletricProfile);
                    status = true;
                    detail = "sat push enviado";
                } else {
                    detail = "perfil eletrico nao encontrado";
                }
            } else {
                detail = "comando invalido:[" + command + "] lista de comandos validos:" + alias;
                logger.info("comando invalido:[" + command + "] lista de comandos validos:" + alias);
            }
            registerInTopticForFutureAnalisys(new AnalysisVO(eletricProfile, vo.getFrom(), vo.getContentAsString(), vo.getTo(), status,
                    detail));
            long diff = System.currentTimeMillis() - time;

            if (diff < 1000) {
                try {
                    long sleepTime = 1000 - diff;
                    logger.debug("sleeping sleepTime:" + sleepTime);
                    Thread.sleep(1000 - diff);

                } catch (InterruptedException e) {

                    logger.error("problemas no no controle de vazao", e);
                }
            }

        } catch (JMSException e) {
            logger.error("problemas processando mensagem", e);
        }

    }

    public void setMessageDrivenContext(MessageDrivenContext arg0) throws EJBException {

    }

    private void registerInTopticForFutureAnalisys(AnalysisVO analysisVO) {

        String topicName = "topic/pushsent";
        try {

            logger.debug("registrando analisyanalysisVOsVO:" + analysisVO + " em: " + topicName);
            QueueHelper.putInQueue("/topic/pushsent", analysisVO);
            logger.debug("registrado analysisVO:" + analysisVO + " em: " + topicName);

        } catch (Exception e) {

            logger.error("nao pode registrar analysisVO:" + analysisVO + " em: " + topicName, e);
        }
    }
}