package com.m4u.satpushsender.push;


import java.io.Serializable;
import java.util.Date;

public class ReportVO implements Serializable
{

    private static final long serialVersionUID = 1L;

    private long transactionId;

    private Date startDate;

    private Date endDate;

    private String cardholder;

    private String merchant;

    private int transactionType;

    private int transactionStep;

    private int stepStatus;

    private byte acquirerCode;

    private int status;

    private Integer error;

    private String errorMessage;

    private String cardholderImei;

    private String merchantImei;

    private String cardholderEletricProfile;

    private byte merchantType;

    private Integer sentSmsCount;

    private Double amount;

    private boolean isRemoteTransaction = false;

    private String benefitedMsisdn;

    private String requestChannel;

    private Integer partnerId;

    private String merchantName;

    private String stickerId;

    public ReportVO()
    {
        error = null;
    }

    public ReportVO(Date startDate, long transactionId, String cardholder, String merchant, int type, int step,
            int stepStatus, byte acquirerCode, int status, Double amount, boolean remoteTransaction)
    {
        this.startDate = startDate;
        this.endDate = startDate;
        this.transactionId = transactionId;
        this.cardholder = cardholder;
        this.merchant = merchant;
        this.transactionType = type;
        this.transactionStep = step;
        this.stepStatus = stepStatus;
        this.acquirerCode = acquirerCode;
        this.status = status;
        this.amount = amount;
        this.setRemoteTransaction(remoteTransaction);
    }

    public ReportVO(Date startDate, long transactionId, String cardholder, String merchant, int type, int step,
            int stepStatus, byte acquirerCode, int status, String cardholderImei, String merchantImei,
            String cardholderEletricProfile, byte merchantType, String errorMessage, Integer sentSmsCount,
            Double amount, boolean remoteTransaction)
    {
        this.startDate = startDate;
        this.endDate = startDate;
        this.transactionId = transactionId;
        this.cardholder = cardholder;
        this.merchant = merchant;
        this.transactionType = type;
        this.transactionStep = step;
        this.stepStatus = stepStatus;
        this.acquirerCode = acquirerCode;
        this.status = status;
        this.cardholderImei = cardholderImei;
        this.merchantImei = merchantImei;
        this.cardholderEletricProfile = cardholderEletricProfile;
        this.merchantType = merchantType;
        this.errorMessage = errorMessage;
        this.sentSmsCount = sentSmsCount;
        this.amount = amount;
        this.setRemoteTransaction(remoteTransaction);
    }

    public ReportVO(Date startDate, long transactionId, String cardholder, String merchant, int type, int step,
            int stepStatus, byte acquirerCode, int status)
    {
        this.startDate = startDate;
        this.endDate = startDate;
        this.transactionId = transactionId;
        this.cardholder = cardholder;
        this.merchant = merchant;
        this.transactionType = type;
        this.transactionStep = step;
        this.stepStatus = stepStatus;
        this.acquirerCode = acquirerCode;
        this.status = status;
    }

    public ReportVO(Date startDate, long transactionId, String cardholder, String merchant, int type, int step,
            int stepStatus, byte acquirerCode, int status, String cardholderImei, String merchantImei,
            String cardholderEletricProfile, byte merchantType, String errorMessage, Integer sentSmsCount)
    {
        this.startDate = startDate;
        this.endDate = startDate;
        this.transactionId = transactionId;
        this.cardholder = cardholder;
        this.merchant = merchant;
        this.transactionType = type;
        this.transactionStep = step;
        this.stepStatus = stepStatus;
        this.acquirerCode = acquirerCode;
        this.status = status;
        this.cardholderImei = cardholderImei;
        this.merchantImei = merchantImei;
        this.cardholderEletricProfile = cardholderEletricProfile;
        this.merchantType = merchantType;
        this.errorMessage = errorMessage;
        this.sentSmsCount = sentSmsCount;
    }

    public String getMerchantName()
    {
        return merchantName;
    }

    public void setMerchantName(String merchantName)
    {
        this.merchantName = merchantName;
    }

    public byte getAcquirerCode()
    {
        return acquirerCode;
    }

    public void setAcquirerCode(byte acquirerCode)
    {
        this.acquirerCode = acquirerCode;
    }

    public String getCardholder()
    {
        return cardholder;
    }

    public void setCardholder(String cardholder)
    {
        this.cardholder = cardholder;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Integer getError()
    {
        return error;
    }

    public void setError(Integer error)
    {
        this.error = error;
    }

    public String getMerchant()
    {
        return merchant;
    }

    public void setMerchant(String merchant)
    {
        this.merchant = merchant;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public long getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(long transactionId)
    {
        this.transactionId = transactionId;
    }

    public int getTransactionStep()
    {
        return transactionStep;
    }

    public void setTransactionStep(int transactionStep)
    {
        this.transactionStep = transactionStep;
    }

    public int getStepStatus()
    {
        return stepStatus;
    }

    public void setStepStatus(int stepStatus)
    {
        this.stepStatus = stepStatus;
    }

    public int getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(int transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getCardholderEletricProfile()
    {
        return cardholderEletricProfile;
    }

    public void setCardholderEletricProfile(String cardholderEletricProfile)
    {
        this.cardholderEletricProfile = cardholderEletricProfile;
    }

    public String getCardholderImei()
    {
        return cardholderImei;
    }

    public void setCardholderImei(String cardholderImei)
    {
        this.cardholderImei = cardholderImei;
    }

    public String getMerchantImei()
    {
        return merchantImei;
    }

    public void setMerchantImei(String merchantImei)
    {
        this.merchantImei = merchantImei;
    }

    public byte getMerchantType()
    {
        return merchantType;
    }

    public void setMerchantType(byte merchantType)
    {
        this.merchantType = merchantType;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public Integer getSentSmsCount()
    {
        return sentSmsCount;
    }

    public void setSentSmsCount(Integer sentSmsCount)
    {
        this.sentSmsCount = sentSmsCount;
    }

    public Double getAmount()
    {
        return amount;
    }

    public void setAmount(Double amount)
    {
        this.amount = amount;
    }

    public Integer getPartnerId()
    {
        return this.partnerId;
    }

    public void setPartnerId(Integer partnerId)
    {
        this.partnerId = partnerId;
    }

    public void copyFrom(ReportVO reportVO)
    {

        this.transactionId = reportVO.getTransactionId();
        this.startDate = reportVO.getStartDate();
        this.endDate = reportVO.getEndDate();
        this.cardholder = reportVO.getCardholder();
        this.merchant = reportVO.getMerchant();
        this.transactionType = reportVO.getTransactionType();
        this.transactionStep = reportVO.getTransactionStep();
        this.stepStatus = reportVO.getStepStatus();
        this.acquirerCode = reportVO.getAcquirerCode();
        this.status = reportVO.getStatus();
        this.error = reportVO.getError();
        this.errorMessage = reportVO.getErrorMessage();
        this.cardholderImei = reportVO.getCardholderImei();
        this.merchantImei = reportVO.getMerchantImei();
        this.cardholderEletricProfile = reportVO.getCardholderEletricProfile();
        this.merchantType = reportVO.getMerchantType();
        this.sentSmsCount = reportVO.getSentSmsCount();
        this.amount = reportVO.getAmount();
        this.isRemoteTransaction = reportVO.isRemoteTransaction();
        this.partnerId = reportVO.getPartnerId();
        this.benefitedMsisdn = reportVO.getBenefitedMsisdn();
        this.merchantName = reportVO.getMerchantName();
        this.stickerId = reportVO.getStickerId();
    }

    public void setStickerId(String stickerId)
    {
        this.stickerId = stickerId;
    }

    public String getStickerId()
    {
        return this.stickerId;
    }

    public void setRemoteTransaction(boolean remoteTransaction)
    {
        this.isRemoteTransaction = remoteTransaction;
    }

    public boolean isRemoteTransaction()
    {
        return isRemoteTransaction;
    }

    /**
     * @param requestChannel
     *            the requestChannel to set
     */
    public void setRequestChannel(String requestChannel)
    {
        this.requestChannel = requestChannel;
    }

    /**
     * @return the requestChannel
     */
    public String getRequestChannel()
    {
        return requestChannel;
    }

    /**
     * @param benefitedMsisdn
     *            the benefitedMsisdn to set
     */
    public void setBenefitedMsisdn(String benefitedMsisdn)
    {
        this.benefitedMsisdn = benefitedMsisdn;
    }

    /**
     * @return the benefitedMsisdn
     */
    public String getBenefitedMsisdn()
    {
        return benefitedMsisdn;
    }
}
