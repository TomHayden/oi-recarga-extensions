package com.m4u.satpushsender.push;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import org.apache.log4j.Logger;

/**
 * @author gabriel
 */
public class TextFormatter {
    public String format(String text, Object bean) throws IOException {
        return format(new StringReader(text), bean);
    }

    public InputStream format(InputStream is, String charset, Object bean) throws IOException {
        String text = format(new BufferedReader(new InputStreamReader(is, charset)), bean);
        return new ByteArrayInputStream(text.getBytes(charset));
    }

    private String format(Reader reader, Object bean) throws IOException {
        StringBuffer sbOut = new StringBuffer();

        StringBuffer match = new StringBuffer();
        boolean matched = false;

        for (int c = reader.read(); c >= 0; c = reader.read()) {
            char ch = (char) c;
            if (!matched) {
                if (ch != '*') {
                    sbOut.append(ch);
                } else {
                    // found opening #
                    matched = true;
                }
            } else {
                if (ch != '*') {
                    match.append(ch);
                } else {
                    // found closing #
                    if (match.length() == 0) {
                        // found ##
                        sbOut.append('*');
                    } else {
                        Class clazz = bean.getClass();
                        String property = match.toString(); // bug maldito do
                                                            // caractere que eu
                                                            // escolhi
                        try {
                            Object value = clazz.getMethod(property, null).invoke(bean, null);
                            sbOut.append(value);
                        } catch (Exception e) {
                            String msg = new StringBuffer(128).append("Erro executando o metodo \"").append(match)
                                    .append("()\" da classe \"").append(clazz.getName()).append('"').toString();
                            Logger.getLogger(TextFormatter.class).warn(msg, e);
                        }
                    }
                    matched = false;
                    match.setLength(0);
                }
            }
        }
        return sbOut.toString();
    }
}
