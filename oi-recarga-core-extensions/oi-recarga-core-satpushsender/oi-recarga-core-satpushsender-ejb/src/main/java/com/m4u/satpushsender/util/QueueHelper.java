package com.m4u.satpushsender.util;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

public class QueueHelper {

    private static Logger logger = Logger.getLogger(QueueHelper.class);

    public static void putInQueue(String queueName, Serializable jmsMessage) {

        Connection conn = null;
        Session session = null;
        Destination destination = null;
        MessageProducer producer = null;

        try {

            InitialContext in = new InitialContext();
            ConnectionFactory connectionFactory = (ConnectionFactory) in.lookup("java:/ConnectionFactory");
            destination = (Destination) in.lookup(queueName);
            conn = connectionFactory.createConnection();
            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            ObjectMessage obj = session.createObjectMessage(jmsMessage);
            // obj.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);
            producer = session.createProducer(destination);
            producer.send(obj);

        } catch (NamingException e) {

            logger.error("problems putting in queue", e);
        } catch (JMSException e) {

            logger.error("problems putting in queue", e);
        } finally {

            try {
                close(conn, session);
            } catch (Exception e) {

                logger.error("problems closing resources", e);
            }

        }

    }

    private static void close(Connection conn, Session session) {
        if (conn != null) {
            try {
                conn.close();
            } catch (JMSException e) {
                logger.error("problems closing connection", e);
            }
        }

        if (session != null) {
            try {
                session.close();
            } catch (JMSException e) {
                logger.error("problems closing session", e);
            }
        }
    }
}
