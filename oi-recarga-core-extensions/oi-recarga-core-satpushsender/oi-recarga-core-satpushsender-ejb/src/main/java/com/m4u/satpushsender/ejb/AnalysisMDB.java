package com.m4u.satpushsender.ejb;

import java.sql.SQLException;

import javax.ejb.EJBException;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.resource.ResourceException;

import org.apache.log4j.Logger;

import com.m4u.satpushsender.dao.AnalysisDAO;
import com.m4u.satpushsender.vo.AnalysisVO;

public class AnalysisMDB implements MessageDrivenBean, MessageListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Logger logger = Logger.getLogger(AnalysisMDB.class);
    private AnalysisDAO analysisDAO = null;

    /*
     * (non-Javadoc)
     * 
     * @see javax.ejb.MessageDrivenBean#ejbRemove()
     */
    public void ejbRemove() throws EJBException {
    }

    /**
     * Default create method
     */
    public void ejbCreate() {

        try {
            analysisDAO = AnalysisDAO.getInstance();
        } catch (ResourceException e) {
            logger.error("problemas carregando analysisDAO", e);
            throw new EJBException(e);
        }
    }

    public void onMessage(Message message) {

        AnalysisVO vo = null;
        try {
            vo = (AnalysisVO) ((ObjectMessage) message).getObject();
            logger.debug("registering: " + vo);
            analysisDAO.insert(vo);
            logger.info("registered: " + vo);

        } catch (JMSException e) {
            logger.error("problemas lendo mensagem", e);
            throw new EJBException(e);
        } catch (ResourceException e) {

            logger.error("problemas lendo recursos", e);
            throw new EJBException(e);

        } catch (SQLException e) {
            logger.error("problemas registrando analysis:" + vo, e);
         
        }
    }

    public void setMessageDrivenContext(MessageDrivenContext arg0) throws EJBException {

    }

}