package com.m4u.satpushsender.push;



public class SimDescriptorException extends Exception {
    private static final long serialVersionUID = 1L;

    private long transactionId;

    public SimDescriptorException() {
        super();
    }

    public SimDescriptorException(String arg0) {
        super(arg0);
    }

    public SimDescriptorException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public SimDescriptorException(Throwable arg0) {
        super(arg0);
    }

    public SimDescriptorException(long arg0) {
        super();
        this.setTransactionId(arg0);
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public long getTransactionId() {
        return transactionId;
    }

}
