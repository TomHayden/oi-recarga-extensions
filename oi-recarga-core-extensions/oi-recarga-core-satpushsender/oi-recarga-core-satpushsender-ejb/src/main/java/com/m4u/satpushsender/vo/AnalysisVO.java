/*
 * Created on Feb 4, 2010 by alexandre.assis for satpushsender
 * 
 * Copyright M4U Solucoes 2001-2010
 */
package com.m4u.satpushsender.vo;

import java.io.Serializable;

public class AnalysisVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String from = null;
    private String to = null;
    private String text = null;
    private EletricProfileVO eletricProfileVO = null;
    private boolean status;
    private String detail;

    public AnalysisVO(EletricProfileVO eletricProfileVO, String from, String text, String to, boolean status, String detail) {
        super();
        this.eletricProfileVO = eletricProfileVO;
        this.from = from;
        this.text = text;
        this.to = to;
        this.status = status;
        this.detail = detail;
    }

    public String toString() {
        StringBuffer bf = new StringBuffer();
        bf.append("from:[" + from + "]");
        bf.append("to:[" + to + "]");
        bf.append("text:[" + text + "]");
        bf.append("status:[" + status + "]");
        bf.append("detail:[" + detail + "]");
        bf.append("eletricProfile:[" + eletricProfileVO + "]");
        return bf.toString();

    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public EletricProfileVO getEletricProfileVO() {
        return eletricProfileVO;
    }

    public void setEletricProfileVO(EletricProfileVO eletricProfileVO) {
        this.eletricProfileVO = eletricProfileVO;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

}
