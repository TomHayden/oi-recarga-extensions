package com.m4u.satpushsender.util;

import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

public class PropertiesUtil {
    private static final String FILE = "satpushsender.properties";

    private static Properties property = null;
    private static String alias;

    private static Logger logger = Logger.getLogger(PropertiesUtil.class);

    public static Properties loadProperty() {
        try {
            property = new Configuration(FILE);
        } catch (InstantiationException e) {
            logger.error("problemas carregando properties", e);
            return null;
        }
        return property;
    }

    public static String getString(String key) {
        if (property == null) {
            property = loadProperty();

            if (property == null) {
                return null;
            }
        }
        return property.getProperty(key);
    }

    public static String[] getStringsComaSepareted(String key) {
        if (property == null) {
            property = loadProperty();

            if (property == null) {
                return null;
            }
        }
        String tmp = property.getProperty(key);
        StringTokenizer st = new StringTokenizer(tmp, ",");
        String[] strs = new String[st.countTokens()];
        int i = 0;
        while (st.hasMoreTokens()) {
            strs[i] = st.nextToken();
            i++;
        }
        return strs;
    }

    /**
     * Obtem alias
     * 
     * @return
     */
    public static String getAlias() {
        if (alias == null) {
            alias = getString("alias");
        }

        if (alias == null || alias.equals("")) {
            logger.warn("Alias nao encontrado. Usando configuracao Default: s.do");
            alias = "s.do";
        }

        return alias;
    }

    /**
     * Recarrega alias do arquivo de propriedades (Recarga.properties)
     * 
     * @return
     */
    public static String reloadAlias() {
        alias = loadProperty().getProperty("alias");

        if (alias == null || alias.equals("")) {
            logger.warn("Alias nao encontrado. Usando configuracao Default: s.do");
            alias = "s.do";
        }

        logger.info("ALIAS ATUALIZADO:" + alias);

        return alias;
    }

    public static void reloadProperties() {
        property = loadProperty();
    }

    public static long getLong(String key) {
        if (property == null) {
            property = loadProperty();

            if (property == null) {
                return 0;
            }
        }
        return Long.parseLong(property.getProperty(key));
    }

    public static Boolean getBoolean(String key) {
        if (property == null) {
            property = loadProperty();

            if (property == null) {
                return false;
            }
        }
        return Boolean.parseBoolean(property.getProperty(key).trim());
    }

}
