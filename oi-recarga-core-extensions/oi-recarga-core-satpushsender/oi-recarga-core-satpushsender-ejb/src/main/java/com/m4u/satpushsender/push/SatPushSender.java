package com.m4u.satpushsender.push;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;

/**
 * Created on Jan 13, 2010 by felipe.goldin for SatPushSender
 *
 * Copyright M4U Solu��es S.A.
 */

/**
 * @author felipe.goldin
 */
public class SatPushSender
{
    private Logger logger = Logger.getLogger(SatPushSender.class);

    public String sendPushFromURL(String msisdn, String filePath, String LA)
    {
        InputStream pushStream = null;
        SimDescriptor sim = new SimDescriptor();
        sim.setMsisdn(msisdn);
        sim.setCarrier(Carrier.OI);

        try
        {
            if (msisdn != null && filePath != null && LA != null)
            {
                byte[] satPushPage = {};

                try
                {
                    logger.debug("Obtendo perfil eletrico do msisdn: " + msisdn);
                    SimDescriptorHelper helper = new SimDescriptorHelper();
                    sim = helper.getSimDescriptor(sim, new ReportVO());
                    if (sim == null)
                    {
                        throw new SimDescriptorException();
                    }
                }
                catch (SimDescriptorException se)
                {
                    logger.info("Perfil eletrico nao encontrado para o msisdn " + msisdn);
                    return "Sim Descriptor nao encontrado";
                }
                catch (Exception e)
                {
                    logger.error("Erro ao buscar perfil eletrico para o msisdn " + msisdn);
                    return "Erro na busca do Sim Descriptor";
                }

                logger.debug("Lendo arquivo do path: " + filePath);
                URL push = new URL(filePath);
                URLConnection urlConnection = push.openConnection();
                pushStream = urlConnection.getInputStream();
                satPushPage = this.toByteArray(urlConnection.getInputStream());

                logger.debug("Iniciando envio do sat push para o msisdn: " + msisdn);
                CarrierCommunicationAdapter carrierCommunicationAdapter = (CarrierCommunicationAdapter) CarrierCommunicationAdapterFactory
                        .execute(sim);
                carrierCommunicationAdapter.sendGenericPushByJMXConsole(sim, satPushPage, LA);
                return "Sucesso";
            }
            return "Preencher todos os parametros";
        }
        catch (Exception e)
        {
            logger.debug("Erro ao enviar PUSH", e);
            return "Falha";
        }
        finally
        {
            if (pushStream != null)
            {
                try
                {
                    pushStream.close();
                }
                catch (IOException e)
                {
                    logger.warn("Erro ao fechar conexao da input stream", e);
                }
            }
        }
    }

    /**
     * Transforma em array de bytes a stream lida
     * 
     * @param is
     * @return
     * @throws IOException
     */
    private byte[] toByteArray(InputStream is) throws IOException
    {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int length;
        while ((length = is.read(buffer)) >= 0)
        {
            bos.write(buffer, 0, length);
        }
        return bos.toByteArray();
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub

    }

}
