package com.m4u.satpushsender.push;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.apache.log4j.Logger;

import com.m4u.net.sms.SMSException;
import com.m4u.net.sms.SMSMessage;
import com.m4u.net.sms.SMSResponseMessage;
import com.m4u.util.M4UApp;

public class SMSSenderClient {
    // private static final int DEFAULT_TIMEOUT = 600000;
    //
    // private static final int MAX_SMS_ERROR_MESSAGE = 38;
    //
    // private Properties jndiProperties = null;
    //
    // private static final String DEFAULT_JNDI_RESOURCE_NAME = "sms-jndi.properties";
    //
    // private ConfirmedSMSProducerHome confirmedSMSHome;// TODO usando
    //
    // // ConfirmedSMSProducer
    //
    // // para confirmacao sincrona.
    // // Alterar para SMSProducer para
    // // DLR assincrono
    //
    // private SMSProducerHome smsHome;
    //
    // private AssyncSMSProducerHome assyncSMSHome;
    //
    // private InitialContext ic;
    //
    // private String smsServiceAlias;
    //
    // private String smsServiceAlias72446;
    //
    // private final Logger logger = Logger.getLogger(SMSSenderClient.class);
    //
    // private static SMSSenderClient instance = null;
    //
    // // synchronized creator to defend against multi-threading issues
    // private synchronized static void createInstance() throws NamingException {
    // if (instance == null) {
    // instance = new SMSSenderClient();
    // }
    // }
    //
    // public static SMSSenderClient getInstance() throws NamingException {
    // if (instance == null) {
    // createInstance();
    // }
    // return instance;
    // }
    //
    // private SMSSenderClient() throws NamingException {
    // init(DEFAULT_JNDI_RESOURCE_NAME);
    // ic = new InitialContext(jndiProperties);
    // Object o = ic.lookup("ejb/ConfirmedSMSProducer");
    // confirmedSMSHome = (ConfirmedSMSProducerHome) PortableRemoteObject.narrow(o, ConfirmedSMSProducerHome.class);
    //
    // o = ic.lookup("ejb/SMSProducer");
    // smsHome = (SMSProducerHome) PortableRemoteObject.narrow(o, SMSProducerHome.class);
    //
    // Object ao = ic.lookup("ejb/AssyncSMSProducer");
    // assyncSMSHome = (AssyncSMSProducerHome) PortableRemoteObject.narrow(ao, AssyncSMSProducerHome.class);
    //
    // smsServiceAlias = M4UApp.getString("SMSProducer.jmsServiceAlias");
    // smsServiceAlias72446 = M4UApp.getString("SMSProducer.jmsServiceAlias72446");
    // }
    //
    // public SMSResponseMessage sendMessage(SMSMessage smsMsg, int priority, long timeout, boolean isDLRSynchronous, Integer
    // transactionType)
    // throws SMSException, RemoteException, NamingException {
    // return sendSMSMessage(smsMsg, priority, timeout, isDLRSynchronous, transactionType);
    // }
    //
    // public SMSResponseMessage sendMessage(SMSMessage smsMsg, int priority, long timeout) throws SMSException, RemoteException,
    // NamingException {
    // return sendSMSMessage(smsMsg, priority, timeout, true, null);
    // }
    //
    // private String getMonitorErrorMessage(String errorMessage) {
    // if (errorMessage != null && errorMessage.length() > MAX_SMS_ERROR_MESSAGE) {
    // return errorMessage.substring(0, MAX_SMS_ERROR_MESSAGE);
    // } else {
    // if (errorMessage == null) {
    // return "null";
    // } else {
    // return errorMessage;
    // }
    //
    // }
    // }
    //
    // private SMSResponseMessage sendSMSMessage(SMSMessage smsMsg, int priority, long timeout, boolean isDLRSynchronous,
    // Integer transactionType) throws SMSException, NamingException, RemoteException {
    // String activeAlias = smsServiceAlias;
    //
    // String from = smsMsg.getFrom();
    //
    // if (OiCommunicationAdapter.getNonEmptyParameter("com.m4u.mPayment.LA7").equals(smsMsg.getFrom())
    // || OiCommunicationAdapter.getNonEmptyParameter("com.m4u.mPayment.LA8").equals(smsMsg.getFrom())
    // || OiCommunicationAdapter.getNonEmptyParameter("com.m4u.mPayment.LA10").equals(smsMsg.getFrom())) {
    // activeAlias = smsServiceAlias72446;
    // }
    //
    // if (OiCommunicationAdapter.getNonEmptyParameter("com.m4u.mPayment.LA2").equals(from)) {
    // sendOrderAcceptanceMonitor = Monitoring.start(MonitorName.SEND_SMS_THROUGH_4621);
    // }
    //
    // try {
    // SMSResponseMessage response = dispatchSMS(smsMsg, priority, timeout, isDLRSynchronous, activeAlias, transactionType);
    // monitorResponse(activeAlias, sendSMSMonitor, response);
    // monitorResponse(activeAlias, sendOrderAcceptanceMonitor, response);
    // monitorResponseQTD(activeAlias, sendSMSMonitorQtd, response);
    // return response;
    // } catch (RemoteException e) {
    // logger.info("Trying to reconnect to SMSProducer after a RemoteException", e);
    // instance = new SMSSenderClient();
    // SMSResponseMessage response = dispatchSMS(smsMsg, priority, timeout, isDLRSynchronous, activeAlias, transactionType);
    // monitorResponse(activeAlias, sendSMSMonitor, response);
    // monitorResponse(activeAlias, sendOrderAcceptanceMonitor, response);
    // monitorResponseQTD(activeAlias, sendSMSMonitorQtd, response);
    // return response;
    // } finally {
    // sendSMSMonitor.end();
    // sendSMSMonitorQtd.end();
    // if (sendOrderAcceptanceMonitor != null) {
    // sendOrderAcceptanceMonitor.end();
    // }
    // }
    // }
    //
    // private void monitorResponse(String activeAlias, Monitor sendSMSMonitor, SMSResponseMessage response) {
    //
    // if (sendSMSMonitor != null) {
    // String errorMessage = null;
    // boolean isSuccess = false;
    //
    // if (response != null) {
    // isSuccess = response.isSuccess();
    //
    // errorMessage = response.getErrorMessage();
    // }
    //
    // String monitorErrorMessage = this.getMonitorErrorMessage(errorMessage);
    //
    // if (isSuccess) {
    // sendSMSMonitor.setEvent("Sucesso:" + activeAlias);
    // } else {
    // sendSMSMonitor.setEvent("Erro:" + activeAlias + " - " + monitorErrorMessage);
    // }
    // }
    // }
    //
    // /**
    // *
    // * Esse metodo leva em consideracao cada SMS concatenado. Ou seja, um push que tenha 4 SMS concatenados, este metodo marcar&aacute; 4
    // * envios.
    // *
    // * @param activeAlias
    // * @param sendSMSMonitor
    // * @param response
    // * @param smsQTDMonitor
    // */
    // private void monitorResponseQTD(String activeAlias, Monitor smsQTDMonitor, SMSResponseMessage response) {
    //
    // String errorMessage = null;
    // boolean isSuccess = false;
    // int qtdSMS = 0;
    //
    // if (response != null) {
    // isSuccess = response.isSuccess();
    //
    // qtdSMS = response.getSmsCount();
    //
    // errorMessage = response.getErrorMessage();
    // }
    //
    // String monitorErrorMessage = this.getMonitorErrorMessage(errorMessage);
    // /*
    // * Sao usados dois monitores, para que um marque a quantidade real de sms enviadas e o outro marque a quantidade "logica" de sms
    // * enviados, ou seja, este ultimo nao contempla os sms concatenados
    // */
    // for (int i = 0; i < qtdSMS; i++) {
    //
    // if (isSuccess) {
    // smsQTDMonitor.setEvent("Sucesso:" + activeAlias);
    // } else {
    // smsQTDMonitor.setEvent("Erro:" + activeAlias + " - " + monitorErrorMessage);
    // }
    // }
    //
    // }
    //
    // private SMSResponseMessage dispatchSMS(SMSMessage smsMsg, int priority, long timeout, boolean isDLRSynchronous, String activeAlias,
    // Integer transactionType) throws CreateException, RemoteException, SMSException {
    // if (timeout > 0L) {
    // if (smsMsg.isConfirmed() && isDLRSynchronous) {
    // ConfirmedSMSProducer smsProducer = confirmedSMSHome.create();
    // return smsProducer.sendMessage(activeAlias, smsMsg, priority, (int) timeout);
    // } else {
    // SMSProducer smsProducer = smsHome.create();
    //
    // SMSResponseMessage response = smsProducer.sendMessage(activeAlias, smsMsg, priority, (int) timeout);
    //
    // if (response != null && smsMsg.isConfirmed()) {
    // /*
    // * TODO permitir que o dlr assincrono seja usado por todas os outros trechos do codigo alem do fluxo 1. Para isso,
    // * provavelmente serah necessario postar no TreeCacheManager fora desse metodo, para que, quando chegue o DLR, saibamos
    // * para que ele serve.
    // */
    // CacheVO cacheVO = new CacheVO();
    // long messageId = response.getMessageId();
    // /*
    // * TODO EXTERNALIZAR essas chaves para algum arquivo, interface, algo assim... Ou mesmo herdar de CacheVO e criar um
    // * especifico para guardar esse tipo de cache
    // */
    // HashMap map = new HashMap();
    // map.put("LA", smsMsg.getFrom());
    // String destinationMsisdn = smsMsg.getTo();
    // map.put("MSISDN", destinationMsisdn.substring(2));
    // map.put("SEND_DATE", new Date());
    // map.put("TRANSACTION_TYPE", transactionType);
    // cacheVO.setObject(map);
    // try {
    // TreeCacheManager cacheManager = new TreeCacheManager(TreeCacheManager.DLR_NODE);
    // cacheManager.add(messageId, cacheVO);
    // logger.info("SMS Enviado com DLR. Message id: " + messageId + ". Msisdn: " + destinationMsisdn);
    // } catch (Exception e) {
    // logger.warn("Erro ao gravar o messageId no cache, para capturar o DLR depois", e);
    // }
    // }
    //
    // return response;
    // }
    // } else {
    // AssyncSMSProducer assyncSMSProducer = assyncSMSHome.create();
    // assyncSMSProducer.sendMessageAssync(activeAlias, smsMsg, priority, DEFAULT_TIMEOUT);
    // return null;
    // }
    // }
    //
    // private void init(String jndiResourceName) {
    // InputStream jndiPropsFile = (jndiResourceName != null) ? getClass().getClassLoader().getResourceAsStream(jndiResourceName) : null;
    //
    // try {
    // if (jndiPropsFile != null) {
    // jndiProperties = new Properties();
    //
    // jndiProperties.load(jndiPropsFile);
    //
    // logger.info("Usando as seguintes propriedades jndi: " + jndiProperties.toString());
    // } else
    // logger.info("Usando propriedades jndi default");
    // } catch (IOException exc) {
    // logger.error("Erro ao carregar propriedades JNDI \"" + jndiResourceName + '\"', exc);
    // }
    //
    // }
}
