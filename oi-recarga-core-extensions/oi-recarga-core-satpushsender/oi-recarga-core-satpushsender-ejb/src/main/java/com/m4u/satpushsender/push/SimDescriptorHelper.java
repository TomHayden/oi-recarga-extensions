package com.m4u.satpushsender.push;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.m4u.satpushsender.util.PropertiesUtil;

public class SimDescriptorHelper {

    // TODO simcard.oi.m4u.com.br -> contaempresa.oiloja.com.br
    // FIXME PARAMETRIZAR
    private static final String URL_OI = "http://apipaggo.novaoi.com.br/oicorp/api_paggo.jsp?msisdn=";

    // FIXME PARAMETRIZAR
    private static final String URL_CLARO = "http://localhost:8080/visa006/EletricProfile?msisdn=";

    private static final String MSISDN_NOT_FOUND = "MSISDN nao encontrado.";

    public SimDescriptorHelper() {
    }

    public SimDescriptor getSimDescriptor(String msisdn, String iccid, byte tripleDesKeyset, byte satKeyset, byte type, int manufacturer) {
        return new SimDescriptor(msisdn, iccid, tripleDesKeyset, satKeyset, type, manufacturer);
    }

    public SimDescriptor getSimDescriptor(String msisdn, String iccid, byte tripleDesKeyset, byte satKeyset, byte type, int manufacturer,
            String imei) {
        return new SimDescriptor(msisdn, iccid, tripleDesKeyset, satKeyset, type, manufacturer, imei);
    }

    public SimDescriptor getSimDescriptor(String msisdn, String iccid, byte[] dukptKeyset, byte tripleDesKeyset, byte satKeyset, byte type,
            int manufacturer, String imei) {
        return new SimDescriptor(msisdn, iccid, dukptKeyset, tripleDesKeyset, satKeyset, type, manufacturer, imei);
    }

    /**
     * Pega busca o simDescriptor completo para o sim com o msisdn e carrier preenchidos. <br>
     * 1- Busca na base local <br>
     * 2- Caso cache desatualizado ou erro na busca local, consulta operadora<br>
     * 3- Caso consulta operadora falhe - utiliza valor desatualizado
     * 
     * @param sim
     *            SimDescriptor com o msisdn e carrier preenchidos
     * @param reportVO
     * @return
     * @throws SimDescriptorException
     */
    public SimDescriptor getSimDescriptor(SimDescriptor sim, ReportVO reportVO) throws SimDescriptorException {
        return this.getSimDescriptor(sim.getMsisdn(), reportVO, sim.getCarrier());
    }

    private SimDescriptor getSimDescriptor(String msisdn, ReportVO reportVO, Carrier carrier) throws SimDescriptorException {
        if (msisdn == null) {
            throw new NullPointerException("msisdn == null");
        }

        return getSimDescriptorFromCarrier(msisdn, reportVO, carrier);
    }

    /**
     * retorna null para tipo inexistente (msisdn nao encontrado)
     */
    public SimDescriptor getSimDescriptorFromCarrier(String msisdn, ReportVO reportVO, Carrier carrier) throws SimDescriptorException {

        String httpResponseMessage = null;

        reportVO.setStartDate(new Date());

        try {
            String url = URL_OI + msisdn;
            switch (carrier) {
            case OI:
                url = URL_OI;
                break;
            case CLARO:
                url = URL_CLARO;
                break;
            default:
                break;
            }
            url += msisdn;
            Logger.getLogger(SimDescriptorHelper.class).debug(url);
            HttpURLConnection connection = (HttpURLConnection) ((new URL(url)).openConnection());
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(2000);

            // Le o resultado da transacao.
            httpResponseMessage = readHttpResponse(connection, msisdn);
            reportVO.setEndDate(new Date());

            if (httpResponseMessage == null || httpResponseMessage.length() > 150) {
                throw new SimDescriptorException("Erro ao tentar obter informacoes sobre o sim card na operadora (resposta muito grande): "
                        + msisdn);
            }
            // montando o vo para validar o resultado da Oi
            SimDescriptor simdescriptor;
            try {
                simdescriptor = buildSimDescriptor(msisdn, httpResponseMessage);
                if (simdescriptor != null) {
                    simdescriptor.setCarrier(carrier);
                }
            } catch (Exception e) {
                throw new SimDescriptorException("Erro ao tentar obter informacoes sobre o sim card na operada (resposta invalida): "
                        + msisdn, e);
            }

            // "MSISDN nao encontrado" ou perfil eletrico com tipo reconhecido -
            // vamos salvar
            if (simdescriptor == null || simdescriptor.getType() != SimDescriptor.TYPE_UNKNOWN) {
                // Leitura na operadora terminada com sucesso.
                // Grava a resposta do carrier na tabela de cache.

            } else {
                Logger.getLogger(SimDescriptorHelper.class).error(
                        "Erro ao tentar obter informacoes sobre o sim card na operada (resposta invalida): " + msisdn + " - "
                                + httpResponseMessage);
            }

            return simdescriptor;
        } catch (SimDescriptorException e) {
            // Falha na comunicacao com o carrier.
            reportVO.setEndDate(new Date());
            throw e;
        } catch (IOException e) {
            // Falha na comunicacao com o carrier.
            reportVO.setEndDate(new Date());
            throw new SimDescriptorException("Erro ao tentar obter informacoes sobre o sim card na operadora: " + msisdn, e);
        }
    }

    private SimDescriptor buildSimDescriptor(String msisdn, String httpResponseMessage) {
        if (httpResponseMessage.startsWith(MSISDN_NOT_FOUND)) {
            return null;
        }

        StringTokenizer tokenizer = new StringTokenizer(httpResponseMessage, ";");
        String iccid = tokenizer.nextToken().trim();

        // Verificacao criada devido ao bug do keyset errado dos chips ABNOTE. Se o iccid do chip estiver na lista de chips com erro, sera
        // atribuido a eles um novo perfil eletrico
        String electricProfileName = tokenizer.nextToken().trim();

        iccid += generateCheckDigit(iccid);

        ElectricProfileVO electricProfileVO = ElectricProfileHelper.getElectricProfile(electricProfileName);

        SimDescriptor vo = new SimDescriptor();

        if (electricProfileVO != null) {
            electricProfileVO.copyInto(vo);
        } else {
            // sabemos qual o terminal profile deste msisdn
            Logger.getLogger(SimDescriptorHelper.class).warn(
                    "Perfil eletrico nao reconhecido para o msisdn " + msisdn + ": " + electricProfileName);
            vo.setType(SimDescriptor.TYPE_UNKNOWN);
        }

        vo.setIccid(iccid);
        vo.setMsisdn(msisdn);
        vo.setEletricProfileName(electricProfileName);
        return vo;
    }

    private String readHttpResponse(HttpURLConnection connection, String msisdn) throws SimDescriptorException {
        try {
            int httpResponseCode = connection.getResponseCode();
            if (httpResponseCode != HttpURLConnection.HTTP_OK) {
                throw new SimDescriptorException("Codigo de resposta HTTP nao esperado durante verificacao de tipo de sim card para "
                        + msisdn + ": " + httpResponseCode);
            }

            byte[] buffer = new byte[128];
            ByteArrayOutputStream baos = new ByteArrayOutputStream(128);
            InputStream is = connection.getInputStream();

            int i = 0;
            for (int len = is.read(buffer, 0, buffer.length); len >= 0 && ++i < 5; len = is.read(buffer, 0, buffer.length)) {
                baos.write(buffer, 0, len);
            }

            if (i >= 5) {
                throw new SimDescriptorException("Tamanho da resposta HTTP muito grande durante verificacao de tipo de sim card para "
                        + msisdn);
            }

            return baos.toString("ISO8859-1").trim();
        } catch (IOException e) {
            throw new SimDescriptorException("Erro durante a comunicacao com a operadora durante verificacao de tipo de sim card para "
                    + msisdn, e);
        }
    }

    private int generateCheckDigit(String aCardNumber) {
        int sum = 0, added;
        boolean doubleFlag = true;

        for (int i = aCardNumber.length() - 1; i >= 0; i--) {
            if (doubleFlag) {
                added = (Integer.parseInt(aCardNumber.substring(i, i + 1))) << 1;
                if (added > 9) {
                    added -= 9;
                }
            } else {
                added = Integer.parseInt(aCardNumber.substring(i, i + 1));
            }
            sum += added;
            doubleFlag = !doubleFlag;
        }
        return (10 - sum % 10) % 10;
    }

    /**
     * Retorna o valor dos parâmetros armazenados no arquivo M4UApp.properties
     * 
     * @param name
     *            Nome do parametro
     * @return Valor dos parâmetros
     */
    static String getNonEmptyParameter(String name) {
        String temp = PropertiesUtil.getString(name);
        if (temp == null || temp.length() == 0) {
            throw new RuntimeException("Parametro \"" + name + "\" nao foi definido");
        }
        return temp;
    }

    public static byte[] decodeIccid(byte[] param, int offset) {
        byte[] iccid = new byte[8];
        int i = 0;
        do {
            byte b = param[i + offset];
            byte high = (byte) (b << 4 & 0xF0);
            byte low = (byte) (b >>> 4 & 0x0F);
            iccid[i] = (byte) (high | low);
        } while (++i < 8);
        return iccid;
    }

    public static boolean msisdnIsValid(String msisdn) {
        if (msisdn != null) {
            if (!msisdn.matches("\\d{10}")) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

}
