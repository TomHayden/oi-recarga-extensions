package com.m4u.satpushsender.push;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.m4u.satpushsender.util.PropertiesUtil;

public class ElectricProfileHelper
{
    private static final String PROPERTY_FILE_NAME = "com.m4u.mpayment.carriercommunicationadapter.oiadapter.simdescriptor.ElectricProfileHelper.electricProfileFilename";

    private static HashMap<String, ElectricProfileVO> electricProfiles = null;

    private static Logger logger = Logger.getLogger(ElectricProfileHelper.class);

    static
    {
        reloadNoError();
    }

    public ElectricProfileHelper()
    {
    }

    public static ElectricProfileVO getElectricProfile(String electricProfileName)
    {
        ElectricProfileVO vo = electricProfiles.get(electricProfileName);

        if (vo == null)
        {
            vo = new ElectricProfileVO();
            logger.error("Perfil eletrico desconhecido: " + electricProfileName);
        }
        return vo;
    }

    public static boolean reload() throws IOException
    {
        boolean isSuccess = true;
        String file = getNonEmptyParameter(PROPERTY_FILE_NAME);
        FileReader fr = new FileReader(file);
        BufferedReader in = new BufferedReader(fr);
        String temp;
        HashMap<String, ElectricProfileVO> tempHash = new HashMap<String, ElectricProfileVO>();
        while ((temp = in.readLine()) != null)
        {
            temp = temp.trim();

            if (!temp.startsWith("#") && temp.length() > 2)
            {
                try
                {

                    if (temp.indexOf('#') > 0)
                    {
                        temp = temp.substring(0, temp.indexOf('#') - 1);
                    }
                    String[] simcard = temp.split("(\\s)+");// separa por
                                                            // whitespace
                                                            // character
                    if (simcard.length == 5)
                    {
                        ElectricProfileVO vo = new ElectricProfileVO();
                        String name = simcard[0];
                        vo.setName(name);
                        vo.setType((byte) Integer.parseInt(simcard[1], 16));
                        vo.setManufacturer(Integer.parseInt(simcard[2], 16));

                        temp = simcard[3];
                        if (!temp.equalsIgnoreCase("null"))
                        {
                            vo.setSatKeyset(Integer.parseInt(temp, 16));
                        }

                        temp = simcard[4];
                        if (!temp.equalsIgnoreCase("null"))
                        {
                            vo.setTripleDesKeyset(Integer.parseInt(temp, 16));
                        }
                        tempHash.put(name, vo);
                    }
                    else
                    {
                        logger.warn("Erro lendo arquivo de perfil eletrico. Linha invalida: " + temp);
                        isSuccess = false;
                    }
                }
                catch (NullPointerException npe)
                {
                    isSuccess = false;
                    logger.warn("Erro lendo lista de perfil eletrico de arquivo.", npe);
                }
            }
        }
        electricProfiles = tempHash;
        in.close();
        return isSuccess;
    }

    public static void reloadNoError()
    {
        try
        {
            if (!reload())
            {
                String message = "Leitura do arquivo de configuracao do perfil eletrico nao foi concluida com sucesso";
                Logger.getLogger(ElectricProfileHelper.class).warn(message);
            }
        }
        catch (Exception e)
        {
            String message = "Erro ao tentar carregar arquivos de configuracao do perfil eletrico: "
                    + getNonEmptyParameter(PROPERTY_FILE_NAME);
            Logger.getLogger(ElectricProfileHelper.class).fatal(message, e);
            throw new Error(message, e);
        }
    }

    /**
     * Retorna o valor dos parametros armazenados no arquivo M4UApp.properties
     * 
     * @param name
     *            Nome do parametro
     * @return Valor dos par�metros
     */
    private static String getNonEmptyParameter(String name)
    {
        String temp = PropertiesUtil.getString(name);
        if (temp == null || temp.length() == 0)
        {
            throw new RuntimeException("Parametro \"" + name + "\" nao foi definido");
        }
        return temp;
    }
}
