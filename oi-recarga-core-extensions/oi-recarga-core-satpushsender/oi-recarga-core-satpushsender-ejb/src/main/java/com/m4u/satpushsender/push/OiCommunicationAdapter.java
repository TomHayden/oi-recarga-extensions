package com.m4u.satpushsender.push;

import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Adaptador para as requisicoes recebidas pelo OiCommunicationServletListener
 * 
 * @author enzo.motta
 * 
 */
public class OiCommunicationAdapter implements CarrierCommunicationAdapter
{
    public static final int TAR_MERCHANT = 0x110501;

    public static final int TAR_CARDHOLDER = 0x110502;

    public static final boolean CONFIRMED = true;

    public static final boolean NOT_CONFIRMED = false;

    public static final long TIMEOUT_DATA = 30000L;

    private static final long SEND_MESSAGE_TIMEOUT = 30000L;

    private final Logger logger = Logger.getLogger(OiCommunicationAdapter.class);

    public OiCommunicationAdapter()
    {
    }

    @Override
    public boolean sendGenericPushByJMXConsole(SimDescriptor msisdnPushDestination, byte[] satPushPage, String LA) {
        // TODO Auto-generated method stub
        return false;
    }

//    public boolean sendGenericPushByJMXConsole(SimDescriptor msisdnPushDestination, byte[] satPushPage, String LA)
//    {
//        // pega o technologyAdapter
//        TechnologyProducerAdapterFactory factory = new TechnologyProducerAdapterFactory();
//        TechnologyProducerAdapter adapter = factory.getTechnologyAdapter(msisdnPushDestination);
//
//        ReportVO repVO = null;
//        try
//        {
//            logger.debug("Produz a mensagem que vai ser enviada");
//            SMS message = (SMS) adapter.produceGenericPush(msisdnPushDestination, satPushPage, LA);
//
//            logger.debug("Faz o envio da mensagem");
//            boolean result = sendMessage(message, repVO, OiCommunicationAdapter.TIMEOUT_DATA);
//            return result;
//        }
//        catch (SMSException e)
//        {
//            logger.error("Nao foi possivel enviar o SMS", e);
//        }
//        return false;
//    }
//    
//    private boolean sendMessage(SMS sms, ReportVO reportVO, long validityPeriod) throws SMSException {
//        SMSMessage message = sms.getSMSMessage();
//        message.setValidityPeriod(validityPeriod);
//        return this.sendMessage(sms, reportVO, true, null);
//    }

    /**
     * 
     * @param message
     * @param reportVO
     * @param sendWithSynchronousDLR
     *            Deve ser marcado como false quando se deseja obter um DLR de forma assincrona. Para isso, eh necessario marcar o sms como
     *            setConfirmed(true). <br /> So vai gerar DLR se o message estiver com setConfirmed(true)
     * @param transactionType
     * @return
     * @throws SMSException
     */
//    private boolean sendMessage(SMS message, ReportVO reportVO, boolean sendWithSynchronousDLR, Integer transactionType)
//            throws SMSException
//    {
//        /*
//         * TODO tirar esse sendWithSynchronousDLR e fazer todo mundo pegar DLR de forma assincrona
//         */
//        SMSResponseMessage response = null;
//        SMSMessage smsMessage = null;
//        smsMessage = message.getSMSMessage();
//        response = this.sendSMSMessage(smsMessage, sendWithSynchronousDLR, transactionType);
//
//        if (response == null)
//        {
//            if (reportVO != null)
//            {
//                reportVO.setErrorMessage("send sms failed");
//                reportVO.setStartDate(new Date());
//                reportVO.setEndDate(new Date());
//            }
//
//            logger.warn("send sms failed. Response is null");
//            return false;
//        } 
//        else 
//        {
//            boolean success = response.isSuccess();
//
//            if (!success) 
//            {
//                String errorMessage = response.getErrorMessage();
//
//                StringBuilder loggerMessage = new StringBuilder();
//                loggerMessage.append("Erro ao enviar SMS ").append(errorMessage);
//                logger.error(loggerMessage.toString());
//            }
//
//            return success;
//        }
//    }
    
//    private SMSResponseMessage sendSMSMessage(SMSMessage smsMessage, boolean sendWithSynchronousDLR, Integer transactionType) 
//    {
//        System.out.println(smsMessage.getTo());
//        return null;
//        /*try 
//        {
//            return SMSSenderClient.getInstance().sendMessage(smsMessage, Message.DEFAULT_PRIORITY, SEND_MESSAGE_TIMEOUT,
//                    sendWithSynchronousDLR, transactionType);
//        } 
//        catch (Exception e) 
//        {
//            logger.warn("send sms failed. MSISDN: " + smsMessage.getTo(), e);
//            return null;
//        }*/
//    }

}