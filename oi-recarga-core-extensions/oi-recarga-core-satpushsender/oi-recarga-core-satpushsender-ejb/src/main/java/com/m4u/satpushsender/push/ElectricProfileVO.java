package com.m4u.satpushsender.push;



public class ElectricProfileVO {
    String name;
    byte type = SimDescriptor.TYPE_UNKNOWN;
    int manufacturer = SimDescriptor.MANUFACTURER_UNKNOWN;
    Integer satKeyset = null;
    Integer tripleDesKeyset = null;

    public ElectricProfileVO() {
    }

    public int getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(int manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Integer getTripleDesKeyset() {
        return tripleDesKeyset;
    }

    public void setTripleDesKeyset(Integer masterKeyJavacard) {
        this.tripleDesKeyset = masterKeyJavacard;
    }

    public Integer getSatKeyset() {
        return satKeyset;
    }

    public void setSatKeyset(Integer masterKeySat) {
        this.satKeyset = masterKeySat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public void copyInto(SimDescriptor simDescriptor) {
        simDescriptor.setManufacturer(manufacturer);
        if (tripleDesKeyset != null) {
            simDescriptor.setTripleDesKeyset(tripleDesKeyset.byteValue());
        }
        if (satKeyset != null) {
            simDescriptor.setSatKeyset(satKeyset.byteValue());// TODO padronizar nomes dos key set's javacard e sat
        }
        simDescriptor.setType(type);
    }
}
