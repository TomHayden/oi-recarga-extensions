create database SatPushSenderDS
create table analysis
(
id bigint primary key identity,
MSISDN varchar(12),
LA varchar(5),
TEXT varchar(50),
STATUS tinyint,
DETAIL varchar(200),
DATE datetime,
ICCID varchar(20),
KEYID varchar(20),
MANUFACTURER varchar(20),
SATKEYSET varchar(20),
SATUID varchar(20),
TRIPLEDESKEYSET varchar(20),
TYPE varchar(20)
) 