package com.m4u.recharge.tools.cpfencryption.util;

import com.m4u.recharge.tools.common.util.PropertiesUtil;


public class CPFEncryptionProperties {
    
    private static final String FILE = "/cpf-encryption.properties";

    public static PropertiesUtil getConf() {
        return new PropertiesUtil(FILE);
    }
}
