package com.m4u.recharge.tools.cpfencryption.vo;

public class Customer {
    
    private Long id;
    private String plainCPF;

    public Customer(Long id, String plainCPF) {
        super();
        this.id = id;
        this.plainCPF = plainCPF;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlainCPF() {
        return plainCPF;
    }

    public void setPlainCPF(String plainCPF) {
        this.plainCPF = plainCPF;
    }
    
}
