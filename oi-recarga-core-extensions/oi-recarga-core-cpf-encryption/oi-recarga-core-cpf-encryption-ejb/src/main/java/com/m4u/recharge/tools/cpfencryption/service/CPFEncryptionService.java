package com.m4u.recharge.tools.cpfencryption.service;

import javax.ejb.Local;

@Local
public interface CPFEncryptionService {
    
    String JNDI_NAME = "cpfencryptionservice/local";

    void encryptCPF(int topRow);

}
