package com.m4u.recharge.tools.cpfencryption.mbean;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.ejb3.annotation.Service;

import com.m4u.recharge.tools.cpfencryption.service.CPFEncryptionService;

@Service(objectName = "com.m4u.recharge.tools:name=ICPFEncryptionMBean,service=CPFEncryptionMBean")
public class CPFEncryptionMBean implements ICPFEncryptionMBean {

    @Override
    public void encryptCPF(int topRow) {
        this.getCPFEncryptionService().encryptCPF(topRow);
    }
    
    private CPFEncryptionService getCPFEncryptionService() {
        InitialContext context;
        try {
            context = new InitialContext();
            return (CPFEncryptionService) context.lookup(CPFEncryptionService.JNDI_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Erro ao realizar lookup no JNDI: " + CPFEncryptionService.JNDI_NAME, e);
        }
    }

}
