package com.m4u.recharge.tools.cpfencryption.service;

import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.LocalBinding;

import com.m4u.recharge.tools.cpfencryption.dao.CustomerDAO;
import com.m4u.recharge.tools.cpfencryption.vo.Customer;

@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Stateless(mappedName = CPFEncryptionService.JNDI_NAME, name = "TranslationServiceBean")
@LocalBinding(jndiBinding = CPFEncryptionService.JNDI_NAME)
public class CPFEncryptionServiceBean implements CPFEncryptionService {

    private static final Logger LOGGER = Logger.getLogger(CPFEncryptionServiceBean.class);

    @Override
    public void encryptCPF(int topRow) {
        CustomerDAO dao = new CustomerDAO();
        try {
            List<Customer> customers = dao.findCustomersToEncryptCPF(topRow);
            for (Customer customer : customers) {
                dao.updateCustomer(customer);
            }
        } catch (Exception e) {
            throw new EJBException("Erro durante encriptacao de CPF.", e);
        }

        
    }
}
