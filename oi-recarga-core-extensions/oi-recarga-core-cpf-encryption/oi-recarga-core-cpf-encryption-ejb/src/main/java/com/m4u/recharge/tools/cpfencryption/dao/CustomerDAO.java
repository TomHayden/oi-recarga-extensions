package com.m4u.recharge.tools.cpfencryption.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import com.m4u.recharge.tools.commons.dao.DataSourceService;
import com.m4u.recharge.tools.cpfencryption.util.CPFEncryptionProperties;
import com.m4u.recharge.tools.cpfencryption.vo.Customer;

public class CustomerDAO {

    private static final String FIND_CUSTOMERS_TO_ENCRYPT_CPF = "SELECT TOP (?) ID_CUSTOMER, MOTHER FROM CUSTOMER (NOLOCK) WHERE STATUS = 9";

    private static final String UPDATE_CUSTOMER = "UPDATE CUSTOMER SET CPF = ? WHERE ID_CUSTOMER = ?";
    
    private static final String cryptoKeyset = "42";

    private String jndiDataSource = null;

    public CustomerDAO() {
        jndiDataSource = CPFEncryptionProperties.getConf().getString("jndi.datasource", "java:/RechargeDS");
    }

    public List<Customer> findCustomersToEncryptCPF(int topRow) {
        List<Customer> result = new LinkedList<Customer>();

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(FIND_CUSTOMERS_TO_ENCRYPT_CPF);
            pstmt.setInt(1, topRow);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Long customerId = rs.getLong("ID_CUSTOMER");
                String plainCPF = rs.getString("MOTHER");
                result.add(new Customer(customerId, plainCPF));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar os Clientes. Metodo: findCustomersToEncryptCPF.", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        return result;
    }

    public void updateCustomer(Customer customer) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(UPDATE_CUSTOMER);

//            CryptoRMIClient cryptoClient = CryptoRMIClient.getInstance();
//            String cpfEncrypted = new String(cryptoClient.encrypt3Des(cryptoKeyset, customer.getPlainCPF().getBytes("ISO8859-1"), 0, customer.getPlainCPF().getBytes("ISO8859-1").length),
//                    "X-HEX");
            String cpfEncrypted = "";
            
            int index = 1;
            pstmt.setString(1, cpfEncrypted);
            pstmt.setLong(2, customer.getId());

            pstmt.executeUpdate();

        } catch (Exception e) {
            throw new RuntimeException("Erro ao atualizar o Cliente. Metodo: updateCustomer", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        
    }

}
