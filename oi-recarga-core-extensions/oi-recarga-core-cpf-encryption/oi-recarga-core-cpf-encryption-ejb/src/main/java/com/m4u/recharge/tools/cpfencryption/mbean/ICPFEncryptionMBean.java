package com.m4u.recharge.tools.cpfencryption.mbean;

import org.jboss.ejb3.annotation.Management;

@Management
public interface ICPFEncryptionMBean {

    public void encryptCPF(int topRow);

}
