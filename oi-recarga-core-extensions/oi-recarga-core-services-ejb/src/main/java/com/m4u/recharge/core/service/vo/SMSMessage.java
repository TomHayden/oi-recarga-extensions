package com.m4u.recharge.core.service.vo;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

public class SMSMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private String from;
    private String to;
    private byte[] text;

    public SMSMessage(String from, String to, String text) {
        this.from = from;
        this.to = to;
        try {
            this.text = text.getBytes("X-GSM-DEFAULT");
        } catch (UnsupportedEncodingException e) {
            throw new Error("X-GSM-DEFAULT ENCODING NOT FOUND");
        }
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getText() {
        try {
            return new String(text, "X-GSM-DEFAULT");
        } catch (UnsupportedEncodingException e) {
            throw new Error("X-GSM-DEFAULT ENCODING NOT FOUND");
        }
    }

    @Override
    public String toString() {
        return "SMSMessage [from=" + from + ", to=" + to + ", text=" + text + "]";
    }

}
