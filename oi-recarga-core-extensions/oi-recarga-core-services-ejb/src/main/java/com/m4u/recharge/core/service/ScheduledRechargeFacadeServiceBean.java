package com.m4u.recharge.core.service;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.jboss.ejb3.annotation.LocalBinding;

@Stateless(mappedName = ScheduledRechargeFacadeService.JNDI_NAME, name = "ScheduledRechargeFacadeServiceBean")
@LocalBinding(jndiBinding = ScheduledRechargeFacadeService.JNDI_NAME)
public class ScheduledRechargeFacadeServiceBean implements ScheduledRechargeFacadeService {

    @EJB
    private MainService mainService;

    @EJB
    private ScheduleRechargeManager scheduleRechargeManager;

    @EJB
    private RechargeLegacyManager rechargeLegacyManager;
    
    
    @Override
    public MainService getMainService() {
        return mainService;
    }

    @Override
    public ScheduleRechargeManager getScheduleRechargeManager() {
        return scheduleRechargeManager;
    }

    @Override
    public RechargeLegacyManager getRechargeLegacyManager() {
        return rechargeLegacyManager;
    }

}
