package com.m4u.recharge.core.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.log4j.Logger;

import com.m4u.recharge.core.service.vo.SMSMessage;

public class SMSSenderVASImpl implements SMSSender {

    private static final Logger logger = Logger.getLogger(SMSSenderVASImpl.class);

    private String vasURL;
    private String vasInterface;
    private String vasLA;

    SMSSenderVASImpl(String vasURL, String vasInterface, String vasLA) {
        this.vasURL = vasURL;
        this.vasInterface = vasInterface;
        this.vasLA = vasLA;
    }

    @Override
    public boolean send(SMSMessage sms) {
        try {

            String expectedHttpResponse = "Sent.";

            // send via get

            StringBuffer sb = new StringBuffer(512).append(vasURL).append("?smsc=").append(URLEncoder.encode(vasInterface, "ISO8859-1"));
            sb.append("&from=").append(URLEncoder.encode(vasLA, "ISO8859-1")).append("&to=")
                    .append(URLEncoder.encode(sms.getTo(), "ISO8859-1"));

            // seta o texto
            sb.append("&text=");

            // mensagem codificada em 7 bits, vamos usar URLEncoder apenas uma
            // vez
            String text = sms.getText();
            text = new String(text.getBytes("X-GSM-VAS-OI-NON-EXTENDED"), "X-GSM-VAS-OI-NON-EXTENDED");
            sb.append(URLEncoder.encode(text, "ISO8859_1").replace("%A4", "%24"));

            logger.info("SMS a enviar: " + sb.toString());
            
            URL url = new URL(sb.toString());

            int httpResponseCode = 0;
            String httpResponse = null;
            HttpURLConnection connection = null;
            try {

                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setRequestMethod("GET");

                // le o resultado da requisicao
                httpResponseCode = connection.getResponseCode();
                httpResponse = readHttpResponse(connection);
                if (httpResponseCode != HttpURLConnection.HTTP_OK || !expectedHttpResponse.equals(httpResponse)) {
                    String logMessage = new StringBuffer(1024).append("SMS to ").append(sms.getTo())
                            .append(" not sent. Could not send sms. VAS response code: ").append(connection.getResponseCode())
                            .append(". VAS response message: ").append(httpResponse).toString();
                    logger.error("INT1016: " + logMessage);
                    return false;
                }
            } catch (IOException e1) {
                logger.error("INT1016: SMS to " + sms.getTo() + " not sent.");
                return false;
            }

        } catch (UnsupportedEncodingException e) {
            logger.error("problemas de encoding", e);
            return false;
        } catch (MalformedURLException e) {
            logger.error("problemas de url malformada", e);
            return false;
        }

        return true;
    }

    private String readHttpResponse(HttpURLConnection connection) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        StringBuffer sb = new StringBuffer();
        String line;
        while ((line = reader.readLine()) != null) {
            if (sb.length() > 0) {
                sb.append('\n');
            }
            sb.append(line);
        }
        reader.close();

        return sb.toString();
    }

}
