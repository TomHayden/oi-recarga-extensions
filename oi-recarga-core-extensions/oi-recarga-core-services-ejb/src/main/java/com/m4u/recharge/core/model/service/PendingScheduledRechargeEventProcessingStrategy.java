package com.m4u.recharge.core.model.service;

import com.m4u.recharge.core.model.ScheduledRechargeEvent;

public interface PendingScheduledRechargeEventProcessingStrategy {
    
    public abstract boolean preProcess(ScheduledRechargeEvent event);

    public abstract boolean postProcess(ScheduledRechargeEvent event);
}
