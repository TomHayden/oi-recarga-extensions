package com.m4u.recharge.core.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.LocalBinding;

import com.m4u.logging.ApplicationLogger;
import com.m4u.multirecarga.pegasus.client.PegasusCustomerRequest;
import com.m4u.multirecarga.pegasus.client.exception.PegasusClientURLInvalidaException;
import com.m4u.multirecarga.pegasus.to.CustomerTO;
import com.m4u.recharge.core.helper.SMSMessageFactory;
import com.m4u.recharge.core.model.CallCenterEvent;
import com.m4u.recharge.core.model.CallCenterEventType;
import com.m4u.recharge.core.model.Channel;
import com.m4u.recharge.core.model.Customer;
import com.m4u.recharge.core.model.CustomerCreditCard;
import com.m4u.recharge.core.model.CustomerPrepaid;
import com.m4u.recharge.core.model.PegasusCallbackEnum;
import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.model.ScheduledRechargeEvent;
import com.m4u.recharge.core.model.TransactionLog;
import com.m4u.recharge.core.model.TransactionLogType;
import com.m4u.recharge.core.model.service.CheckForBenefitAuthorizationStrategy;
import com.m4u.recharge.core.model.service.CheckForBillingAuthorizationStrategy;
import com.m4u.recharge.core.model.service.CheckForEligibilityStrategy;
import com.m4u.recharge.core.model.service.NoRequiredProcessingStrategy;
import com.m4u.recharge.core.model.service.PendingScheduledRechargeEventProcessingStrategy;
import com.m4u.recharge.core.service.validation.ServiceValidator;
import com.m4u.recharge.core.service.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.core.service.vo.GetScheduledRechargeResponse;
import com.m4u.recharge.core.service.vo.SMSMessage;
import com.m4u.recharge.core.service.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.core.service.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.error.ApplicationResult;
import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;
import com.m4u.recharge.restservice.corews.vo.EventRS;
import com.m4u.recharge.restservice.corews.vo.HistoryResponseRS;
import com.m4u.recharge.util.DateUtil;
import com.m4u.recharge.util.RecargaProperties;
import com.m4u.recharge.util.RechargeUtil;
import com.m4u.recharge.webservice.core.CoreWebService;
import com.m4u.recharge.webservice.core.vo.CustomerResponse;
import com.m4u.recharge.webservice.core.vo.SimpleRequest;
import com.m4u.recharge.webservice.util.ServiceMessage;

@Stateless(mappedName = MainServiceBean.JNDI_NAME, name = "MainServiceBean")
@LocalBinding(jndiBinding = MainServiceBean.JNDI_NAME)
public class MainServiceBean implements MainService {

    private static final Logger logger = Logger.getLogger(MainServiceBean.class);

    private static final Integer CHANNEL_SISTEMA = 3;

    private static NumberFormat _amountFormatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    private static NumberFormat _dayFormatter = new DecimalFormat("00");

    private String url = null;
    private String perfil = null;
    private String servico = null;

    @EJB
    private ScheduleRechargeManager scheduleRechargeManager;

    @EJB
    private RechargeLegacyManager rechargeLegacyManager;

    @EJB
    private SMSManager smsManager;

    private PegasusCustomerRequest pegasusConnector() {

        this.url = RecargaProperties.getConf().getString("schedule.pegasus.server", "http://stg.pegasus.m4u.com.br:8080/");
        this.perfil = RecargaProperties.getConf().getString("schedule.pegasus.profile", "OIRECPRG");
        this.servico = RecargaProperties.getConf().getString("schedule.pegasus.service.id", "203");

        try {
            if (RecargaProperties.getConf().getBoolean("schedule.pegasus.integration.enable", true) == true) {
                return new PegasusCustomerRequest(url, perfil, servico);
            }
            return new PegasusCustomerRequestMock(url, perfil, servico);

        } catch (PegasusClientURLInvalidaException e) {
            logger.error("URL para servico Pegasus nao foi definida corretamente.", e);
            return null;
        }
    }

    private SessionContext getSessionContext() {
        try {
            InitialContext ic = new InitialContext();
            SessionContext sctxLookup = (SessionContext) ic.lookup("java:comp/EJBContext");
            return sctxLookup;
        } catch (NamingException ex) {
            logger.error("Erro durante lookup do SessionContext.", ex);
            return null;
        }
    }

    @Override
    public SimpleResponseRS addScheduledRecharge(AddScheduledRechargeRequest req) {

        Channel channel = rechargeLegacyManager.findChannelById(req.getChannel());
        if (channel == null) {
            return new SimpleResponseRS(ServiceMessage.CHANNEL_UNSUPPORTED.getCode(), ServiceMessage.CHANNEL_UNSUPPORTED.getMessage());
        }
        Customer customer = rechargeLegacyManager.findCustomerByMsisdn(req.getMsisdn());
        if (customer == null) {
            return new SimpleResponseRS(ServiceMessage.CUSTOMER_NOT_FOUND.getCode(), ServiceMessage.CUSTOMER_NOT_FOUND.getMessage());
        }
        CustomerPrepaid customerPrepaid = rechargeLegacyManager.findCustomerPrepaid(req.getPrepaid(), customer);
        if (customerPrepaid == null) {
            return new SimpleResponseRS(ServiceMessage.PREPAID_NOT_FOUND.getCode(), ServiceMessage.PREPAID_NOT_FOUND.getMessage());
        }
        CustomerCreditCard creditCard = rechargeLegacyManager.findCustomerCreditCard(req.getCardType(), req.getLast4Digits(), customer);
        if (creditCard == null) {
            return new SimpleResponseRS(ServiceMessage.CREDIT_CARD_NOT_FOUND.getCode(), ServiceMessage.CREDIT_CARD_NOT_FOUND.getMessage());
        }

        // .Fase 2. Uma programada para o recebedor de um MSISDN
        List<ScheduledRecharge> schedules = this.getSchedulesByMsisdn(req.getMsisdn());
        if (schedules != null && !schedules.isEmpty()) {
            for (ScheduledRecharge scheduled : schedules) {
                if (scheduled.getCustomerPrepaid().getPrepaid().equals(req.getPrepaid())) {
                    return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_ALREADY_REGISTERED.getCode(),
                            ServiceMessage.SCHEDULED_RECHARGE_ALREADY_REGISTERED.getMessage());
                }
            }
        }

        ScheduledRecharge schedule = new ScheduledRecharge();
        schedule.setAmount(req.getAmount());
        schedule.setRechargeDay(req.getDay());
        schedule.setRegisterDate(new Date(System.currentTimeMillis()));
        schedule.setCustomerPrepaid(customerPrepaid);
        schedule.setCreditCard(creditCard);
        schedule.setChannel(channel);
        schedule.setSubscriptionType(req.getSubscriptionType());

        String scheduledRechargeId = scheduleRechargeManager.addScheduledRecharge(schedule);

        if (scheduledRechargeId != null && scheduledRechargeId.trim().length() > 0) {

            // Se a programada for persistida, entao agendar no pegasus
            CustomerTO cadastro = createPegasusRequest(req, creditCard, scheduledRechargeId);
            logger.info("Requisicao de cadastro de recarga programada no pegasus: " + cadastro);

            ApplicationLogger.step("REQUISICAO_ADESAO_PEGASUS").customer(cadastro.getAccount()).ccToken(cadastro.getCardToken())
                    .param("amount", cadastro.getValue()).param("birthday", cadastro.getBirthday())
                    .param("externalid", cadastro.getExternalId()).log();

            boolean pegasusResponse = pegasusConnector().cadastrar(cadastro);
            logger.info("Resposta do cadastro no pegasus: " + pegasusResponse);

            // Gravar Evento para CallCenter
            if (pegasusResponse == true) {
                rechargeLegacyManager.createEventForNewScheduledRecharge(schedule);

                return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_ADDED.getCode(),
                        ServiceMessage.SCHEDULED_RECHARGE_ADDED.getMessage());
            } else {
                getSessionContext().setRollbackOnly();
            }

        }
        return new SimpleResponseRS(ServiceMessage.INTERNAL_ERROR.getCode(), ServiceMessage.INTERNAL_ERROR.getMessage());

    }

    private CustomerTO createPegasusRequest(AddScheduledRechargeRequest req, CustomerCreditCard creditCard, String scheduledRechargeId) {
        CustomerTO cadastro = new CustomerTO(scheduledRechargeId, getBirthday(req.getDay()), req.getPrepaid(), creditCard.getToken(),
                formatAmount(req.getAmount()));
        return cadastro;
    }

    private Long getBirthday(Byte day) {
        Calendar cal = Calendar.getInstance();

        if (cal.get(Calendar.DATE) >= day) {
            cal.add(Calendar.MONTH, 1);
        } else {
            cal.add(Calendar.MONTH, 0);
        }

        cal.set(Calendar.DATE, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    // TODO Mover este metodo para classe utilitaria do recarga
    private String formatAmount(Double topupAmount) {
        NumberFormat t = NumberFormat.getNumberInstance();
        t.setMinimumFractionDigits(2);
        t.setMaximumFractionDigits(2);

        String str = (t.format(topupAmount)).replace(",", "").replace(".", "");
        while (str.length() < 5) {
            str = "0" + str;
        }
        return str;
    }

    @Override
    public List<ScheduledRecharge> getSchedulesByMsisdn(String msisdn) {

        List<ScheduledRecharge> result = scheduleRechargeManager.getSchedulesByMsisdn(msisdn);

        return result;
    }

    @Override
    public GetScheduledRechargeResponse getScheduleById(String scheduleId) {
        ScheduledRecharge schedule = scheduleRechargeManager.findScheduledRechargeById(scheduleId);
        GetScheduledRechargeResponse result = null;
        if (schedule != null) {
            String msisdn = schedule.getCustomerPrepaid().getCustomer().getMsisdn();
            String prepaid = schedule.getCustomerPrepaid().getPrepaid();
            String cardType = schedule.getCustomerCreditCard().getCardType();
            String last4Digits = schedule.getCustomerCreditCard().getLast4Digits();
            Double amount = schedule.getAmount();
            byte day = schedule.getRechargeDay();
            result = new GetScheduledRechargeResponse(ServiceMessage.SCHEDULED_RECHARGE_FOUND.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_FOUND.getMessage(), msisdn, prepaid, cardType, last4Digits, amount, day);
        } else {
            result = new GetScheduledRechargeResponse(ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND.getMessage());
        }
        return result;

    }

    @Override
    public SimpleResponseRS removeSchedule(String scheduleId, Integer channelId) {

        SimpleResponseRS result = null;
        ScheduledRecharge schedule = scheduleRechargeManager.findScheduledRechargeById(scheduleId);
        if (schedule != null) {

            boolean isRemoved = scheduleRechargeManager.removeScheduledRecharge(schedule);

            // remover a recarga programada do Pegasus
            logger.info("Requisicao de cancelamento de recarga programada no pegasus: " + schedule);
            boolean pegasusResponse = pegasusConnector().desativarCadastroCliente(scheduleId);
            logger.info("Resposta do cancelamento no pegasus: " + pegasusResponse);

            if (isRemoved && pegasusResponse) {
                result = new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_REMOVED.getCode(),
                        ServiceMessage.SCHEDULED_RECHARGE_REMOVED.getMessage());

                // Logando evento para CallCenter
                String origem = "OPERADOR";
                if (!CHANNEL_SISTEMA.equals(channelId)) {
                    Channel channel = rechargeLegacyManager.findChannelById(channelId);
                    origem = channel.getDescription();
                }
                rechargeLegacyManager.createEventForRemovedScheduledRecharge(schedule, channelId,
                        "Cancelamento da Recarga Programada efetuado via " + origem);
                // ----

                final String SMS_LA = RecargaProperties.getConf().getString("sms.vas.from", "3000");
                // Enviando SMS confirmando cancelamento da Recarga Programada
                String msisdn = schedule.getCustomerPrepaid().getCustomer().getMsisdn();
                String prepaid = schedule.getCustomerPrepaid().getPrepaid();
                String cardType = schedule.getCustomerCreditCard().getCardType();
                String last4Digits = schedule.getCustomerCreditCard().getLast4Digits();
                String amount = _amountFormatter.format(schedule.getAmount());
                String day = _dayFormatter.format(schedule.getRechargeDay());
                String msgTemplate = RecargaProperties.getConf().getString("sms.message.schedule.recharge.cancellation.success");
                String text = String.format(msgTemplate, null, prepaid, amount, cardType, last4Digits, day);
                logger.info("Mensagem de Sucesso Cancelamento Recarga Programada: " + text);
                SMSMessage sms = new SMSMessage(SMS_LA, msisdn, text);
                smsManager.sendSMS(sms);
                // ----

            } else {
                getSessionContext().setRollbackOnly();
                result = new SimpleResponseRS(ServiceMessage.INTERNAL_ERROR.getCode(), ServiceMessage.INTERNAL_ERROR.getMessage());
            }

        } else {
            result = new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND.getMessage());
        }

        return result;
    }

    @Override
    public SimpleResponseRS verifyScheduleEligibitiy(Integer channel, Long cycleId, String serviceId, String scheduleId,
            String cycleStepId, String cardToken, String prepaid, Double reloadAmount) {

        // pesquisar programada
        ScheduledRecharge schedule = scheduleRechargeManager.findScheduledRechargeById(scheduleId);

        // TRATAR AGENDAMENTO NAO ENCONTRADO
        if (schedule == null) {
            return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND.getMessage());
        }

        String cardType = schedule.getCustomerCreditCard().getCardType();
        String last4Digits = schedule.getCustomerCreditCard().getLast4Digits();
        String msisdn = schedule.getCustomerPrepaid().getCustomer().getMsisdn();

        if (!validateParametersAgainstStoredScheduledRechargeInfo(schedule, channel, cycleId, serviceId, cycleStepId, cardToken, prepaid,
                reloadAmount)) {
            return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getMessage());
        }

        ScheduledRechargeEvent event = storeEligibilityRequest(schedule, channel, cycleId, serviceId, scheduleId, cycleStepId, cardToken,
                prepaid, reloadAmount);

        // TRATAR CARTAO INATIVO
        if (!schedule.getCustomerCreditCard().isActive()) {

            // TODO: Confimar!!!
            scheduleRechargeManager.removeScheduledRecharge(schedule);

            boolean shallNotifyViaSms = RecargaProperties.getConf().getBoolean(
                    "scheduled.recharge.notify.when.cc.inactivated.during.pegasus.cycle", true);

            if (shallNotifyViaSms) {
                final String SMS_LA = RecargaProperties.getConf().getString("sms.vas.from", "3000");
                String msgTemplate = RecargaProperties.getConf().getString("sms.message.schedule.recharge.change.card");
                String text = String.format(msgTemplate, null, null, null, cardType, last4Digits);
                SMSMessage sms = new SMSMessage(SMS_LA, msisdn, text);
                smsManager.sendSMS(sms);
            }

            return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_WITH_INACTIVE_CARD.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_WITH_INACTIVE_CARD.getMessage());
        }

        // TRATAR CONSULTAS FORA DO PERIODO
        Calendar now = Calendar.getInstance();
        byte today = (byte) now.get(Calendar.DATE);
        if (schedule.getRechargeDay() != today) {
            return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_NOT_ELEGIBILITY.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_NOT_ELEGIBILITY.getMessage());
        }

        // Obter cliente da programada
        CustomerResponse customerResponse = getCoreWS().getCustomer(new SimpleRequest(channel, msisdn));

        String receiver = schedule.getCustomerPrepaid().getPrepaid();
        List<String> prepaids = customerResponse.getCustomerDetail().getPrepaid();
        if (!prepaids.contains(receiver)) {
            return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_WITH_INACTIVE_PREPAID.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_WITH_INACTIVE_PREPAID.getMessage());
        }

        // Validar R. Programada
        SimpleResponseRS response = new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_NOT_ELEGIBILITY.getCode(),
                ServiceMessage.SCHEDULED_RECHARGE_NOT_ELEGIBILITY.getMessage());
        Long code = customerResponse.getCode();
        if (code == 1000 || code == 1001 || code == 1002) {
            Double topupAvailableBalance = customerResponse.getCustomerDetail().getTopupAvailableBalance();
            Double amount = schedule.getAmount();

            String cycleIdAsString = cycleId.toString();
            if (amount <= topupAvailableBalance) {

                rechargeLegacyManager.createEventToScheduleElegibilityValid(schedule, event);
                response = new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_ELEGIBILITY.getCode(),
                        ServiceMessage.SCHEDULED_RECHARGE_ELEGIBILITY.getMessage());
            } else {

                final String SMS_LA = RecargaProperties.getConf().getString("sms.vas.from", "3000");
                String msgTemplate = RecargaProperties.getConf().getString("sms.message.schedule.recharge.change.balance");
                String text = String.format(msgTemplate, null, null, null, cardType, last4Digits);
                SMSMessage sms = new SMSMessage(SMS_LA, msisdn, text);
                smsManager.sendSMS(sms);

                rechargeLegacyManager.createEventToScheduleElegibilityBalanceError(schedule, event, cycleIdAsString);
            }
        }

        return response;
    }

    private ScheduledRechargeEvent storeEligibilityRequest(ScheduledRecharge schedule, Integer channel, Long cycleId, String serviceId,
            String scheduleId, String cycleStepId, String cardToken, String prepaid, Double reloadAmount) {

        try {
            ScheduledRechargeEvent scheduleEvent = new ScheduledRechargeEvent();
            scheduleEvent.setDescription(PegasusCallbackEnum.ELIGIBILITY_REQUEST.getDescription());
            scheduleEvent.setCycleId(cycleId);
            scheduleEvent.setCycleStepId(cycleStepId);
            scheduleEvent.setScheduleRecharge(schedule);
            Date now = new Date();
            scheduleEvent.setDtReceived(now);
            scheduleEvent.setDtCreated(now);
            scheduleEvent.setIdServicePegasus(serviceId);

            scheduleRechargeManager.addScheduledRechargeEvent(scheduleEvent);

            return scheduleEvent;
        } catch (RuntimeException e) {
            ApplicationLogger.step("CONSULTA_ELEGIBILIDADE:GUARDAR_EVENTO").channel(channel.toString()).param("cycleId", cycleId)
                    .param("serviceId", serviceId).param("scheduleId", schedule.getId()).param("cycleExtId", cycleStepId)
                    .ccToken(cardToken).receiver(prepaid).amount(reloadAmount).obs("Falha ao guardar evento").log();
            throw e;
        }
    }

    @Override
    public SimpleResponseRS processCallbackNotifications(ScheduledRechargeEventRequest request) {
        try {
            ScheduledRecharge scheduledRecharge = scheduleRechargeManager.findScheduledRechargeById(request.getScheduleid());
            ScheduledRechargeEvent event = null;
            PegasusCallbackEnum eventType = PegasusCallbackEnum.getCallbackByDescription(request.getDescription());
            if (request.isReprocessed()) {
                event = this.scheduleRechargeManager.findScheduledRechargeEventById(request.getEventId());
                event.setPendingEventProcessingStrategy(defineStrategy(request.getDescription()));
                event.setReprocessed(true);
            } else {
                event = convertRequestToEvent(request, scheduledRecharge);
                if (!eventType.shallCreateLegacyLog()) {
                    event.processed();
                }
                scheduleRechargeManager.addScheduledRechargeEvent(event);
            }

            if (scheduledRecharge != null) {
                TransactionLog transactionLog = null;
                if (eventType.shallCreateLegacyLog()) {
                    transactionLog = rechargeLegacyManager.addTransactionLogStep(event);
                }

                if (event.wasProcessed()) {
                    postProcessingEvent(event, transactionLog);
                }
            }
            return new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_CALLBACK_RECEIVED.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_CALLBACK_RECEIVED.getMessage());

        } catch (Exception e) {
            logger.error("Erro durante processamento de callback de recarga programada", e);
            return new SimpleResponseRS(ServiceMessage.INTERNAL_ERROR.getCode(), ServiceMessage.INTERNAL_ERROR.getMessage());
        }

    }

    private void postProcessingEvent(ScheduledRechargeEvent event, TransactionLog transactionLog) {
        // Tratamentos extras para notificacoes que nao vao pro legado
        PegasusCallbackEnum eventType = PegasusCallbackEnum.getCallbackByDescription(event.getDescription());
        ScheduledRecharge scheduledRecharge = event.getScheduleRecharge();
        if (scheduledRecharge != null) {

            // Evitar enviar SMS quando CREATED for menor que HOJE
            Date now = new Date();
            boolean isCallbackOutOfPeriod = DateUtil.dateDiff(event.getDtCreated(), now) >= 1;

            String last4Digits = scheduledRecharge.getCustomerCreditCard().getLast4Digits();
            String paymentType = scheduledRecharge.getCustomerCreditCard().getCardType();
            String msisdn = scheduledRecharge.getCustomerPrepaid().getCustomer().getMsisdn();
            switch (eventType) {
            case CYCLE_DEACTIVATED: {
                if (!isCallbackOutOfPeriod) {
                    String msgTemplate = RecargaProperties.getConf().getString("sms.message.schedule.recharge.payment.error");
                    String text = String.format(msgTemplate, null, null, null, paymentType, last4Digits);
                    sendSmsMessage(msisdn, text);
                }
            }
                break;

            case BENEFIT_NOTIFY_CONFIRM: {
                if (!isCallbackOutOfPeriod) {
                    String msgTemplate = RecargaProperties.getConf().getString("sms.message.schedule.recharge.confirm.sucess");
                    Double amount = transactionLog.getAmount();
                    String formattedAmount = RechargeUtil.moneyToString(amount);
                    String carrierNsuCode = transactionLog.getCarrierNsuCode();
                    String paymentAuthorizeCode = transactionLog.getPaymentAuthorizeCode();
                    String text = String.format(msgTemplate, null, null, formattedAmount, paymentType, last4Digits, carrierNsuCode,
                            paymentAuthorizeCode);
                    sendSmsMessage(msisdn, text);
                }
            }
                break;
            default:
                break;
            }
        }

    }

    private void sendSmsMessage(String msisdn, String text) {
        final String SMS_LA = RecargaProperties.getConf().getString("sms.vas.from", "3000");
        SMSMessage sms = new SMSMessage(SMS_LA, msisdn, text);
        smsManager.sendSMS(sms);
    }

    public SimpleResponseRS updateScheduledRecharge(UpdateScheduledRechargeRequest updateScheduledRecharge) {

        ServiceValidator validator = new ServiceValidator(rechargeLegacyManager);
        SMSMessageFactory messageFormatHelper = new SMSMessageFactory();
        SimpleResponseRS result = null;

        Customer customer = rechargeLegacyManager.findCustomerByMsisdn(updateScheduledRecharge.getMsisdn());

        ScheduledRecharge schedule = scheduleRechargeManager.findScheduledRechargeById(updateScheduledRecharge.getScheduledId());
        Double amountRegistered = schedule.getAmount();
        Byte birthdayRegistered = schedule.getRechargeDay();

        ServiceMessage messageScheduleValidator = validator.validateScheduleRecharge(schedule);

        if (messageScheduleValidator == ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND) {
            return new SimpleResponseRS(messageScheduleValidator.getCode(), messageScheduleValidator.getMessage());
        }

        ServiceMessage message = validator.validateUpdateScheduleRecharge(customer.getId(), updateScheduledRecharge, schedule);

        if (message == ServiceMessage.SCHEDULED_RECHARGE_UPDATED) {

            if (updateScheduledRecharge.getAmount() != null) {
                String amountMessage = messageFormatHelper.createMessageAmountChangeSuccess(amountRegistered,
                        updateScheduledRecharge.getAmount());
                logger.info("Mensagem de atualizacao do valor da recarga programada: " + amountMessage);
                schedule.setAmount(updateScheduledRecharge.getAmount());
                rechargeLegacyManager.createEventToCountScheduleRechargeChanges(schedule,
                        TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_AMOUNT_CHANGE, amountMessage,
                        updateScheduledRecharge.getChannel());
            }

            if (updateScheduledRecharge.getBirthday() != null) {
                String birthdayMessage = messageFormatHelper.createMessageBirthdayChangeSuccess(birthdayRegistered,
                        updateScheduledRecharge.getBirthday());
                logger.info("Mensagem de atualizacao da data da recarga programada: " + birthdayMessage);
                schedule.setRechargeDay(updateScheduledRecharge.getBirthday());
                rechargeLegacyManager.createEventToCountScheduleRechargeChanges(schedule,
                        TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_DATE_CHANGE, birthdayMessage,
                        updateScheduledRecharge.getChannel());
            }

        } else {
            return new SimpleResponseRS(message.getCode(), message.getMessage());
        }

        if (updateScheduledRecharge.getCardType() != null && updateScheduledRecharge.getLast4Digits() != null) {

            CustomerCreditCard cc = new CustomerCreditCard();

            cc = rechargeLegacyManager.findCustomerCreditCard(updateScheduledRecharge.getCardType(),
                    updateScheduledRecharge.getLast4Digits(), customer);

            schedule.setCreditCard(cc);

        }

        boolean updateResult = scheduleRechargeManager.updateScheduledRecharge(schedule);
        logger.info("Resposta da atualizacao de dados da Recarga Programada: " + updateResult);

        Long dateInMillis = updateScheduledRecharge.getBirthday() != null ? getBirthday(updateScheduledRecharge.getBirthday()) : null;
        String amountFormatted = updateScheduledRecharge.getAmount() != null ? formatAmount(updateScheduledRecharge.getAmount()) : null;

        CustomerTO customerTO = new CustomerTO(updateScheduledRecharge.getScheduledId(), dateInMillis, null, schedule
                .getCustomerCreditCard().getToken(), amountFormatted);
        boolean pegasusResponse = pegasusConnector().alterar(customerTO);
        logger.info("Resposta de atualizacao de recarga programada no Pegasus: " + pegasusResponse);

        if (updateResult && pegasusResponse) {
            result = new SimpleResponseRS(ServiceMessage.SCHEDULED_RECHARGE_UPDATED.getCode(),
                    ServiceMessage.SCHEDULED_RECHARGE_UPDATED.getMessage());

            // String text = String.format(msgTemplate, null, null, null, updateScheduledRecharge.getCardType(),
            // updateScheduledRecharge.getLast4Digits());

            String msgFormatted = messageFormatHelper.findSuccessMessageToSend(amountRegistered, updateScheduledRecharge.getAmount(),
                    birthdayRegistered, updateScheduledRecharge.getBirthday(), updateScheduledRecharge.getCardType(),
                    updateScheduledRecharge.getLast4Digits());

            final String SMS_LA = RecargaProperties.getConf().getString("sms.vas.from", "3000");
            SMSMessage sms = new SMSMessage(SMS_LA, updateScheduledRecharge.getMsisdn(), msgFormatted);
            smsManager.sendSMS(sms);

        } else {
            result = new SimpleResponseRS(ServiceMessage.INTERNAL_ERROR.getCode(), ServiceMessage.INTERNAL_ERROR.getMessage());
            getSessionContext().setRollbackOnly();
        }

        return result;
    }

    private ScheduledRechargeEvent convertRequestToEvent(ScheduledRechargeEventRequest request, ScheduledRecharge scheduledRecharge) {
        ScheduledRechargeEvent event = new ScheduledRechargeEvent();
        event.setDescription(request.getDescription());
        event.setPendingEventProcessingStrategy(defineStrategy(request.getDescription()));
        event.setPayload(request.getPayload());
        event.setCycleId(request.getCycleId());
        event.setCycleStepId(request.getCycleStepId());
        event.setIdServicePegasus(request.getServiceId());
        event.setDtReceived(new Date());
        event.setDtCreated(getDtCreatedFromScheduledEvent(request.getCreated()));
        event.setReprocessed(request.isReprocessed());
        if (scheduledRecharge == null) {
            event.setScheduleRecharge(new ScheduledRecharge(request.getScheduleid()));
        } else {
            event.setScheduleRecharge(scheduledRecharge);
        }
        return event;
    }

    private Date getDtCreatedFromScheduledEvent(String created) {
        try {
            Date dtCreated;
            if (created == null) {
                dtCreated = new Date();
            } else {
                dtCreated = DateUtil.stringToDate(created, "yyyy-MM-dd HH:mm:ss.SSS");
            }
            return dtCreated;
        } catch (ParseException e) {
            logger.warn("Erro no processamento da data de criacao de callback de recarga programada. Data recebida: " + created);
            return new Date();
        }

    }

    private PendingScheduledRechargeEventProcessingStrategy defineStrategy(String description) {
        PendingScheduledRechargeEventProcessingStrategy strategy = null;

        if (description.equals(PegasusCallbackEnum.BILLING_AUTHORIZATION.getDescription())) {
            strategy = new CheckForEligibilityStrategy(getScheduledRechargeFacadeService());
        } else if (description.equals(PegasusCallbackEnum.BENEFIT_AUTHORIZATION.getDescription())) {
            strategy = new CheckForBillingAuthorizationStrategy(getScheduledRechargeFacadeService());
        } else if (description.equals(PegasusCallbackEnum.BENEFIT_NOTIFY_CONFIRM.getDescription())) {
            strategy = new CheckForBenefitAuthorizationStrategy(getScheduledRechargeFacadeService());
        } else {
            strategy = new NoRequiredProcessingStrategy();
        }

        return strategy;
    }

    private ScheduledRechargeFacadeService getScheduledRechargeFacadeService() {
        InitialContext context;
        try {
            context = new InitialContext();
            return (ScheduledRechargeFacadeService) context.lookup(ScheduledRechargeFacadeService.JNDI_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Erro ao realizar lookup no JNDI: " + ScheduledRechargeFacadeService.JNDI_NAME, e);
        }
    }

    private CoreWebService getCoreWS() {
        try {
            CoreWebService coreWS = (CoreWebService) new InitialContext().lookup(CoreWebService.JNDI_REMOTE);
            return coreWS;
        } catch (NamingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public HistoryResponseRS getHistoryByMsisdnAndPeriod(String msisdn, Integer month) {

        HistoryResponseRS response = null;

        if (month == null || month < 1 || month > 12) {
            response = new HistoryResponseRS(ServiceMessage.TRANSACTION_HISTORY_INVALID_PERIOD.getCode(),
                    ServiceMessage.TRANSACTION_HISTORY_INVALID_PERIOD.getMessage(), null);
        } else {

            List<CallCenterEvent> callCenterEvents = rechargeLegacyManager.findCustomerEventsInPeriod(msisdn, month);

            if (callCenterEvents == null || callCenterEvents.size() == 0) {
                response = new HistoryResponseRS(ServiceMessage.TRANSACTION_HISTORY_NO_RESULT.getCode(),
                        ServiceMessage.TRANSACTION_HISTORY_NO_RESULT.getMessage(), null);
            } else {
                List<EventRS> events = parseCallCenterEvents(callCenterEvents);
                response = new HistoryResponseRS(ServiceMessage.TRANSACTION_HISTORY_SUCCESS.getCode(),
                        ServiceMessage.TRANSACTION_HISTORY_SUCCESS.getMessage(), events);
            }
        }
        return response;
    }

    private List<EventRS> parseCallCenterEvents(List<CallCenterEvent> callCenterEvents) {
        List<EventRS> list = new LinkedList<EventRS>();
        for (CallCenterEvent item : callCenterEvents) {
            String dateTime = DateUtil.dateToString(item.getDateTime(), "dd/MM/yyyy HH:mm:ss");
            String description = CallCenterEventType.parse(item.getIdTransactionType()).decription();
            String status = getStatusInfo(item);
            Double amount = item.getAmount();
            String paymentType = getPaymentType(item);
            String prepaid = item.getPrepaid();
            String origem = item.getOrigem();
            String obs = item.getObs();
            list.add(new EventRS(dateTime, description, status, amount, paymentType, prepaid, origem, obs));
        }
        return list;
    }

    private String getStatusInfo(CallCenterEvent item) {
        Integer type = item.getIdTransactionType();
        if (type < 16 || type > 19) {
            // Tratamento padrao para eventos diferente de Recarga
            return "ERROR".equals(item.getReturnCode()) ? "Nao realizada" : "OK";
        } else {
            return item.isRechargeDone() ? "OK" : "Nao realizada";
        }
    }

    private String getPaymentType(CallCenterEvent item) {
        return (blankIfNull(item.getPaymentType()) + " " + blankIfNull(item.getMask())).trim();
    }

    private String blankIfNull(String mask) {
        if (mask == null)
            return "";
        return mask;
    }

    private boolean validateParametersAgainstStoredScheduledRechargeInfo(ScheduledRecharge schedule, Integer channel, Long cycleId,
            String serviceId, String cycleStepId, String cardToken, String prepaid, Double reloadAmount) {
        boolean result = true;
        ApplicationResult applicationResult = new ApplicationResult("CONSULTA_ELIGIBILIDADE", "VALIDACAO_DE_CONSISTENCIA");
        try {
            String storedPrepaid = schedule.getCustomerPrepaid().getPrepaid();
            String storedCardToken = schedule.getCustomerCreditCard().getToken();
            Double storedAmount = schedule.getAmount();

            result = validatePrepaid(prepaid, storedPrepaid, applicationResult.recycle())
                    && validateCardToken(cardToken, storedCardToken, applicationResult.recycle())
                    && validateReloadAmount(reloadAmount, storedAmount, applicationResult.recycle());
            return result;
        } finally {
            if (!result) {
                ApplicationLogger.step("CONSULTA_ELEGIBILIDADE:VALIDAR_CONSISTENCIA").channel(channel.toString()).param("cycleId", cycleId)
                        .param("serviceId", serviceId).param("scheduleId", schedule.getId()).param("cycleExtId", cycleStepId)
                        .ccToken(cardToken).receiver(prepaid).amount(reloadAmount).obs(applicationResult.log()).log();
            }
        }
    }

    private boolean validateReloadAmount(Double reloadAmount, Double storedAmount, ApplicationResult appResult) {
        return ensureEquals(reloadAmount, storedAmount, appResult, ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getCode(),
                String.format("%s Valor de recarga recebida [%.2f] não é igual a programada [%.2f]",
                        ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getMessage(), reloadAmount, storedAmount));
    }

    private boolean validateCardToken(String cardToken, String storedCardToken, ApplicationResult appResult) {
        return ensureEquals(cardToken, storedCardToken, appResult, ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getCode(),
                String.format("%s Token do Cartão recebido [%s] não é igual a token do cartão programado [%s]",
                        ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getMessage(), cardToken, storedCardToken));
    }

    private boolean validatePrepaid(String prepaid, String storedPrepaid, ApplicationResult appResult) {
        return ensureEquals(prepaid, storedPrepaid, appResult, ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getCode(), String.format(
                "%s Assinante destino da recarga recebido [%s] não é igual ao Assinante destino da programada [%s]",
                ServiceMessage.SCHEDULED_RECHARGE_DOES_NOT_MATCH.getMessage(), prepaid, storedPrepaid));
    }

    private boolean ensureEquals(Object obj1, Object obj2, ApplicationResult appResult, long code, String message) {
        boolean result = obj1 != null && obj1.equals(obj2);
        if (!result) {
            appResult.setCode(code);
            appResult.setMessage(message);
        }

        return result;
    }
}
