package com.m4u.recharge.core.service.validation;

import org.apache.log4j.Logger;

import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.model.TransactionLogType;
import com.m4u.recharge.core.service.RechargeLegacyManager;
import com.m4u.recharge.core.service.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.util.RecargaProperties;
import com.m4u.recharge.webservice.util.ServiceMessage;

public class ServiceValidator {

    private static final Logger logger = Logger.getLogger(ServiceValidator.class);

    private RechargeLegacyManager rechargeLegacyManager;

    private Integer period = RecargaProperties.getConf().getInt("scheduled.recharge.period", 1);
    private Integer qtdMaxAmountChanges = RecargaProperties.getConf().getInt("scheduled.recharge.qtd.amount.changes", 2);
    private Integer qtdMaxBirthdayChanges = RecargaProperties.getConf().getInt("scheduled.recharge.qtd.day.changes", 2);
    private String maxAmountChangesReached = RecargaProperties.getConf().getString("scheduled.recharge.max.amount.changes.reached");
    private String maxBirthdayChangesReached = RecargaProperties.getConf().getString("scheduled.recharge.max.birthday.changes.reached");

    public ServiceValidator(RechargeLegacyManager rechargeLegacyManager) {
        this.rechargeLegacyManager = rechargeLegacyManager;
    }

    private Integer validateScheduleRechargeQtdChanges(Integer customerId, Integer qtdMonths, Integer idTransactionType) {
        logger.info("Parametros da validacao de quantidade de changes da programada: " + customerId + " " + qtdMonths + " "
                + idTransactionType);
        Integer qtdChanges = rechargeLegacyManager.callGetQtdChangesProcedure(customerId, qtdMonths, idTransactionType);
        logger.info("Quantidade de changes para o idTransactionType " + idTransactionType + ": " + qtdChanges);
        return qtdChanges;
    }

    public boolean validateQtdAmountChanges(Integer customerId) {

        boolean result = false;

        Integer qtdAmountChanges = validateScheduleRechargeQtdChanges(customerId, period,
                TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_AMOUNT_CHANGE.getCode());

        if (qtdAmountChanges < qtdMaxAmountChanges) {
            result = true;
        }

        return result;
    }

    public boolean validateQtdDateChanges(Integer customerId) {

        boolean result = false;

        Integer qtdDateChanges = validateScheduleRechargeQtdChanges(customerId, period,
                TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_DATE_CHANGE.getCode());

        if (qtdDateChanges < qtdMaxBirthdayChanges) {
            result = true;
        }

        return result;
    }

    public ServiceMessage validateUpdateScheduleRecharge(Integer customerId, UpdateScheduledRechargeRequest updateScheduledRecharge,
            ScheduledRecharge schedule) {

        if (updateScheduledRecharge.getAmount() != null) {

            boolean canAmountBeUpdated = this.validateQtdAmountChanges(customerId);

            if (canAmountBeUpdated == false) {

                logger.info("Cliente realizou mais de duas trocas de valor da recarga programada.");
                rechargeLegacyManager.createEventForMaxAmountChangesForScheduledRechargeReached(updateScheduledRecharge, schedule,
                        maxAmountChangesReached);

                return ServiceMessage.SCHEDULED_RECHARGE_MAX_AMOUNT_CHANGES_REACHED;

            }
        }

        if (updateScheduledRecharge.getBirthday() != null) {

            boolean canDateBeUpdated = this.validateQtdDateChanges(customerId);

            if (canDateBeUpdated == false) {

                logger.info("Cliente realizou mais de duas trocas de data da recarga programada.");
                rechargeLegacyManager.createEventForMaxBirthdayChangesForScheduledRechargeReached(updateScheduledRecharge, schedule,
                        maxBirthdayChangesReached);

                return ServiceMessage.SCHEDULED_RECHARGE_MAX_DAY_CHANGES_REACHED;

            }
        }

        return ServiceMessage.SCHEDULED_RECHARGE_UPDATED;
    }

    public ServiceMessage validateScheduleRecharge(ScheduledRecharge scheduleRecharge) {

        if (scheduleRecharge == null) {
            return ServiceMessage.SCHEDULED_RECHARGE_NOT_FOUND;
        } else {
            return ServiceMessage.SCHEDULED_RECHARGE_FOUND;
        }

    }

}
