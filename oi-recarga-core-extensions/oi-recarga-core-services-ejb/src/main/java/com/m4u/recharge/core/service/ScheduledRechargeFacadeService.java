package com.m4u.recharge.core.service;

import javax.ejb.Local;


@Local
public interface ScheduledRechargeFacadeService {

    public static final String JNDI_NAME = "scheduledrechargefacadeservice/local";

    public abstract MainService getMainService();
    public abstract ScheduleRechargeManager getScheduleRechargeManager();
    public abstract RechargeLegacyManager getRechargeLegacyManager();


}
