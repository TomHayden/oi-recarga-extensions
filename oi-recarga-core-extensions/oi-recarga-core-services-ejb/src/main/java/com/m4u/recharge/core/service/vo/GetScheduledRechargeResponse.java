package com.m4u.recharge.core.service.vo;

import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;

public class GetScheduledRechargeResponse extends SimpleResponseRS {

    private ScheduledRecharge scheduledRecharge;
    
    public GetScheduledRechargeResponse(Long code, String message) {
        super(code, message);
        this.scheduledRecharge = null;
    }
    
    public GetScheduledRechargeResponse(Long code, String message, String msisdn, String prepaid, String cardType, String last4Digits,
            Double amount, byte day) {
        super(code, message);
        this.scheduledRecharge = new ScheduledRecharge(msisdn, prepaid, cardType, last4Digits, amount, day);
    }

    public ScheduledRecharge getScheduledRecharge(){
        return this.scheduledRecharge;
    }
    
    public String getMsisdn() {
        return scheduledRecharge.msisdn;
    }

    public String getPrepaid() {
        return scheduledRecharge.prepaid;
    }

    public String getCardType() {
        return scheduledRecharge.cardType;
    }

    public String getLast4Digits() {
        return scheduledRecharge.last4Digits;
    }

    public Double getAmount() {
        return scheduledRecharge.amount;
    }

    public byte getDay() {
        return scheduledRecharge.day;
    }
    
    public class ScheduledRecharge {

        String msisdn;
        String prepaid;
        String cardType;
        String last4Digits;
        Double amount;
        byte day;

        public ScheduledRecharge(String msisdn, String prepaid, String cardType, String last4Digits, Double amount, byte day) {
            super();
            this.msisdn = msisdn;
            this.prepaid = prepaid;
            this.cardType = cardType;
            this.last4Digits = last4Digits;
            this.amount = amount;
            this.day = day;
        }

    }

}