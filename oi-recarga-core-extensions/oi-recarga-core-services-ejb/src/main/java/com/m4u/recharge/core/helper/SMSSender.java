package com.m4u.recharge.core.helper;

import com.m4u.recharge.core.service.vo.SMSMessage;

public interface SMSSender {

    boolean send(SMSMessage sms);

}
