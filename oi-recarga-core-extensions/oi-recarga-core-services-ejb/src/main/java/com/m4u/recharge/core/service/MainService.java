package com.m4u.recharge.core.service;

import java.util.List;

import javax.ejb.Local;

import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.service.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.core.service.vo.GetScheduledRechargeResponse;
import com.m4u.recharge.core.service.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.core.service.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.restservice.commons.vo.SimpleResponseRS;
import com.m4u.recharge.restservice.corews.vo.HistoryResponseRS;


@Local
public interface MainService {

    public static final String JNDI_NAME = "mainservice/local";

    public abstract SimpleResponseRS addScheduledRecharge(AddScheduledRechargeRequest request);

    public abstract List<ScheduledRecharge> getSchedulesByMsisdn(String msisdn);

    public abstract GetScheduledRechargeResponse getScheduleById(String scheduleId);

    public abstract SimpleResponseRS removeSchedule(String scheduleId, Integer channelId);

    public abstract SimpleResponseRS verifyScheduleEligibitiy(Integer channel, Long cycleId, String serviceId, String scheduleId,
            String cycleStepId, String cardToken, String prepaid, Double reloadAmount);
    

    public abstract SimpleResponseRS processCallbackNotifications(ScheduledRechargeEventRequest request);
    
    public abstract SimpleResponseRS updateScheduledRecharge(UpdateScheduledRechargeRequest updateScheduledRecharge);

    public abstract HistoryResponseRS getHistoryByMsisdnAndPeriod(String msisdn, Integer month);


}
