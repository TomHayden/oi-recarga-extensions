package com.m4u.recharge.core.model.service;

import java.util.List;

import com.m4u.recharge.core.model.PegasusCallbackEnum;
import com.m4u.recharge.core.model.ScheduledRechargeEvent;
import com.m4u.recharge.core.model.TransactionLog;
import com.m4u.recharge.core.model.TransactionLogType;
import com.m4u.recharge.core.service.ScheduledRechargeFacadeService;
import com.m4u.recharge.core.service.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.util.DateUtil;

public class CheckForBillingAuthorizationStrategy implements PendingScheduledRechargeEventProcessingStrategy {

    private ScheduledRechargeFacadeService facade;

    public CheckForBillingAuthorizationStrategy(ScheduledRechargeFacadeService scheduledRechargeFacadeService) {
        this.facade = scheduledRechargeFacadeService;
    }

    @Override
    public boolean preProcess(ScheduledRechargeEvent event) {
        boolean result = true;
        Long cycleId = event.getCycleId();
        String cycleIdAsString = cycleId.toString();

        if (!thereIsBillingAuthorizationWithSucessInTransactionLog(cycleIdAsString)) {
            result = false;

            PegasusCallbackEnum eventType = PegasusCallbackEnum
                    .getCallbackByTransactionType(TransactionLogType.TRANSACTION_RECHARGE_CC_DEBITED);
            List<ScheduledRechargeEvent> pedingEvents = facade.getScheduleRechargeManager().findPendingScheduledEventByCycleIdAndEventType(cycleId,
                    eventType);
            for (ScheduledRechargeEvent pendingEvent : pedingEvents) {
                ScheduledRechargeEventRequest request = createScheduledRechargeEventRequestBasedOnPendingEvent(pendingEvent);
                facade.getMainService().processCallbackNotifications(request);
            }
             result = thereIsBillingAuthorizationWithSucessInTransactionLog(cycleIdAsString);
        }
        return result;
    }



    @Override
    public boolean postProcess(ScheduledRechargeEvent event) {
        boolean result = true;

        Long cycleId = event.getCycleId();
        String cycleIdAsString = cycleId.toString();
        TransactionLog transactionLog = findBillingAuthorizationInTransactionLog(cycleIdAsString, TransactionLogType.TRANSACTION_RECHARGE_DONE);

        if (transactionLog == null) {
            result = false;

            PegasusCallbackEnum eventType = PegasusCallbackEnum
                    .getCallbackByTransactionType(TransactionLogType.TRANSACTION_RECHARGE_DONE);
            List<ScheduledRechargeEvent> pedingEvents = facade.getScheduleRechargeManager().findPendingScheduledEventByCycleIdAndEventType(cycleId,
                    eventType);
            for (ScheduledRechargeEvent pendingEvent : pedingEvents) {
                ScheduledRechargeEventRequest request = createScheduledRechargeEventRequestBasedOnPendingEvent(pendingEvent);
                facade.getMainService().processCallbackNotifications(request);
            }

             transactionLog = findBillingAuthorizationInTransactionLog(cycleIdAsString, TransactionLogType.TRANSACTION_RECHARGE_DONE);
             
             if ( transactionLog != null ) {
                 result = true;
             } else {
                 result = false;
             }

        }
        return result;
    }
    
    private ScheduledRechargeEventRequest createScheduledRechargeEventRequestBasedOnPendingEvent(ScheduledRechargeEvent event) {
        String dtCreateAsString = DateUtil.dateToString(event.getDtCreated(), "yyyy-MM-dd HH:mm:ss.SSS");
        ScheduledRechargeEventRequest request = new ScheduledRechargeEventRequest(event.getCycleId(), event.getDescription(), event.getPayload(), event.getScheduleRecharge().getId(), event.getCycleStepId(), event.getIdServicePegasus(), dtCreateAsString);
        request.setEventId(event.getId());
        request.setReprocessed(true);
        return request;
    }
    
    
    private boolean thereIsBillingAuthorizationWithSucessInTransactionLog(String cycleIdAsString) {
        boolean result;
        TransactionLog transactionLog = findBillingAuthorizationInTransactionLog(cycleIdAsString, TransactionLogType.TRANSACTION_RECHARGE_CC_DEBITED);
         if ( transactionLog != null && transactionLog.isSuccess() ) {
             result = true;
         } else {
             result = false;
         }
        return result;
    }
    
    private TransactionLog findBillingAuthorizationInTransactionLog(String cycleId, TransactionLogType transactionType) {
        TransactionLog transactionLog = facade.getRechargeLegacyManager().findTransactionLogByExternalIdAndTransactionType(cycleId,
                transactionType);
        return transactionLog;
    }

}
