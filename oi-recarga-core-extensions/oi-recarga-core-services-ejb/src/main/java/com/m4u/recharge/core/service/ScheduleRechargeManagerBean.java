package com.m4u.recharge.core.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.LocalBinding;

import com.m4u.logging.ApplicationLogger;
import com.m4u.recharge.core.model.Channel;
import com.m4u.recharge.core.model.PegasusCallbackEnum;
import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.model.ScheduledRechargeEvent;
import com.m4u.recharge.core.model.TransactionLogType;

@Stateless(mappedName = ScheduleRechargeManager.JNDI_NAME, name = "ScheduleRechargeManagerBean")
@LocalBinding(jndiBinding = ScheduleRechargeManager.JNDI_NAME)
public class ScheduleRechargeManagerBean implements ScheduleRechargeManager {

    private static final Logger logger = Logger.getLogger(ScheduleRechargeManagerBean.class);

    @PersistenceContext(unitName = "oi-recarga-pessoal-PU")
    private EntityManager em;

    @Override
    public String addScheduledRecharge(ScheduledRecharge schedule) {

        em.persist(schedule);

        return schedule.getId();

    }

    @Override
    public boolean addScheduledRechargeEvent(ScheduledRechargeEvent scheduleEvent) {

        em.persist(scheduleEvent);

        return true;

    }

    @Override
    public ScheduledRecharge findScheduledRechargeById(String id) {
        ScheduledRecharge schedule = null;
        try {
            schedule = em.find(ScheduledRecharge.class, id);
        } catch (Exception e) {
            logger.error("Recarga programada nao encontrada");
            return null;
        }
        return schedule;
    }

    @Override
    public boolean removeScheduledRecharge(ScheduledRecharge schedule) {

        try {

            em.remove(schedule);
            return true;

        } catch (Exception e) {

            logger.error("Erro durante remocao de Recarga Programada.", e);
            return false;
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ScheduledRecharge> getSchedulesByMsisdn(String msisdn) {

        Query qry = em.createNamedQuery("findSchedulesByMsisdn");
        qry.setParameter("msisdn", msisdn);
        List<ScheduledRecharge> result = qry.getResultList();
        return result;

    }

    @Override
    public boolean updateScheduledRecharge(ScheduledRecharge schedule) {

        boolean result = false;

        try {
            em.merge(schedule);
            em.flush();
            return true;
        } catch (Exception e) {
            logger.error("Erro durante atualizacao de dados da Recarga Programada.", e);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ScheduledRechargeEvent> findPendingScheduledEventByCycleIdAndEventType(Long cycleId, PegasusCallbackEnum eventType) {

        long startedAt = System.currentTimeMillis();
        Query qry = em
                .createQuery("SELECT sre FROM ScheduledRechargeEvent sre WHERE sre.cycleId = :cycleId AND sre.description = :description AND sre.pending = true");
        qry.setParameter("cycleId", cycleId);
        qry.setParameter("description", eventType.getDescription());
        List<ScheduledRechargeEvent> result = qry.getResultList();
        long finishedAt = startedAt - System.currentTimeMillis();
        ApplicationLogger.step("CONSULTA DE EVENTOS PENDENTES").param("cycleId", cycleId).param("eventType", eventType.getDescription())
                .obs(String.format("Tempo de Consulta: %dms", finishedAt));
        return result;

    }
    
    public ScheduledRechargeEvent findScheduledRechargeEventById(Integer eventId) {
        ScheduledRechargeEvent result = em.find(ScheduledRechargeEvent.class, eventId);
        return result;
    }

}
