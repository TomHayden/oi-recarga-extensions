package com.m4u.recharge.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;

@Entity
@NamedNativeQuery(name = "findCallcenterEvents", query = "SELECT ID_TRANSACTION_LOG, DTLOG, MSISDN, PREPAID, AMOUNT, DESCRIPTION, RETURN_CODE, ID_TRANSACTION_TYPE, OBS, ORIGEM, PAYMENT_TYPE, MASK FROM TRANSACTION_LOG (NOLOCK) WHERE (ID_TRANSACTION_TYPE IN (0,1,2,3,4,5,6,7,8,13,15,20,21,22,23,24,32,33,34,35,36,42,43)) AND (DTLOG >= :beginDate) AND (MSISDN = :msisdn) UNION SELECT ID_TRANSACTION_LOG, DTLOG, MSISDN, PREPAID, AMOUNT, DESCRIPTION, RETURN_CODE, ID_TRANSACTION_TYPE, OBS, ORIGEM, PAYMENT_TYPE, MASK FROM TRANSACTION_LOG (NOLOCK) WHERE (ID_TRANSACTION_TYPE IN (16,17,18,19,30,31,40)) AND (DTLOG >= :beginDate) AND (MSISDN = :msisdn) AND DTLOG IN (SELECT MAX(DTLOG) FROM TRANSACTION_LOG WHERE (ID_TRANSACTION_TYPE IN (16,17,18,19,30,31,40)) AND (DTLOG >= :beginDate) AND (MSISDN = :msisdn) AND (PAYMENT_TYPE IN ('MASTERCARD', 'VISA', 'DINERS', 'ELO', 'ITAU SHOPLINE', 'CC OI') OR PAYMENT_TYPE IS NULL) GROUP BY ID_TRANSACTION_LOG) ORDER BY DTLOG", resultClass = CallCenterEvent.class)
public class CallCenterEvent {

    @Id
    @Column(name = "id_transaction_log")
    private Long id;

    @Column(name = "dtlog")
    private Date dateTime;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "prepaid")
    private String prepaid;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "return_code")
    private String returnCode;

    @Column(name = "id_transaction_type")
    private Integer idTransactionType;

    @Column(name = "obs")
    private String obs;

    @Column(name = "origem")
    private String origem;

    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "mask")
    private String mask;

    public boolean isRechargeDone() {
        return (idTransactionType == 19 && ("0".equals(returnCode) || "00".equals(returnCode) || "000".equals(returnCode) || "0000".equals(returnCode)));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public Integer getIdTransactionType() {
        return idTransactionType;
    }

    public void setIdTransactionType(Integer idTransactionType) {
        this.idTransactionType = idTransactionType;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

}
