package com.m4u.recharge.core.model;

public enum PegasusCallbackEnum {
    BENEFIT_AUTHORIZATION("BENEFIT_AUTHORIZATION", true, TransactionLogType.TRANSACTION_RECHARGE_CARRIER_AUTHORIZED),
    BENEFIT_NOTIFY_CANCEL("BENEFIT_NOTIFY_CANCEL", false, TransactionLogType.TRANSACTION_UNDEFINED),
    BENEFIT_NOTIFY_CONFIRM("BENEFIT_NOTIFY_CONFIRM", true, TransactionLogType.TRANSACTION_RECHARGE_DONE),
    BILLING_AUTHORIZATION("BILLING_AUTHORIZATION", true, TransactionLogType.TRANSACTION_RECHARGE_CC_DEBITED),
    BILLING_NOTIFY_CANCEL("BILLING_NOTIFY_CANCEL", false, TransactionLogType.TRANSACTION_PAYMENT_CANCELED),
    BILLING_NOTIFY_CONFIRM("BILLING_NOTIFY_CONFIRM", false, TransactionLogType.TRANSACTION_UNDEFINED),
    CUSTOMER_DEACTIVATED("CUSTOMER_DEACTIVATED", false, TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_UNREGISTERED),
    CYCLE_DEACTIVATED("CYCLE_DEACTIVATED", false, TransactionLogType.TRANSACTION_UNDEFINED),
    CYCLE_RESET("CYCLE_RESET", false, TransactionLogType.TRANSACTION_UNDEFINED),
    ELIGIBILITY_REQUEST("ELIGIBILITY_REQUEST", false, TransactionLogType.TRANSACTION_RECHARGE_STARTED),
    NA("N/A", false, TransactionLogType.TRANSACTION_UNDEFINED), // TODO Verificar se pode significar fim de ciclo
    REFUND("REFUND", false, TransactionLogType.TRANSACTION_UNDEFINED),
    UNDEFINED_CALLBACK("UNDEFINED_CALLBACK", false, TransactionLogType.TRANSACTION_UNDEFINED); // Callback nao definido na integracao

    private String description;
    private Boolean generateLegacyLog;
    private TransactionLogType transactionType;

    private PegasusCallbackEnum(String description, Boolean generateLegacyLog, TransactionLogType transactionType) {
        this.description = description;
        this.generateLegacyLog = generateLegacyLog;
        this.transactionType = transactionType;
    }

    public String getDescription() {
        return description;
    }

    public TransactionLogType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionLogType transactionType) {
        this.transactionType = transactionType;
    }

    public Boolean shallCreateLegacyLog() {
        return generateLegacyLog;
    }

    public static PegasusCallbackEnum getCallbackByDescription(String description) {
        for (PegasusCallbackEnum callback : PegasusCallbackEnum.values()) {
            if (callback.getDescription().equals(description)) {
                return callback;
            }

        }
        return UNDEFINED_CALLBACK;
    }

    public static PegasusCallbackEnum getCallbackByTransactionType(TransactionLogType transactionType) {
        PegasusCallbackEnum result = UNDEFINED_CALLBACK;
        for (PegasusCallbackEnum callback : PegasusCallbackEnum.values()) {
            if (callback.getTransactionType().equals(transactionType)) {
                return callback;
            }
        }
        return result;
    }

}
