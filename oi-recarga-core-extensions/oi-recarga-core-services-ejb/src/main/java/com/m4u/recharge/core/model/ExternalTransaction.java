package com.m4u.recharge.core.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "External_Transaction")
@NamedQueries(value = { @NamedQuery(name = "findExternalTransactionByChannelAndExternalId", query = "SELECT et FROM ExternalTransaction et WHERE et.channel = :channel and et.idExternal = :idExternal") })
public class ExternalTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_external_transaction")
    private Integer id;

    @Column(name ="id_transaction_log")
    private Integer idTrasactionLog;

    @Column(name = "id_external")
    private String idExternal;

    @ManyToOne
    @JoinColumn(name = "id_channel")
    private Channel channel;

    @Column(name = "dt_last_update")
    private Date dtLastUpdate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getIdExternal() {
        return idExternal;
    }

    public void setIdExternal(String idExternal) {
        this.idExternal = idExternal;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Date getDtLastUpdate() {
        return dtLastUpdate;
    }

    public void setDtLastUpdate(Date dtLastUpdate) {
        this.dtLastUpdate = dtLastUpdate;
    }

    public Integer getIdTrasactionLog() {
        return idTrasactionLog;
    }

    public void setIdTrasactionLog(Integer idTrasactionLog) {
        this.idTrasactionLog = idTrasactionLog;
    }



}
