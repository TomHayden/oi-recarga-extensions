package com.m4u.recharge.core.service.vo;

import java.io.Serializable;

public class UpdateScheduledRechargeRequest implements Serializable {
    
    private static final long serialVersionUID = 1L;

    private Integer channel;

    private String msisdn;

    private String scheduledId;

    private Double amount;

    private Byte birthday;

//    private String ccard;
    
    private String cardType;
    
    private String last4Digits;

    public UpdateScheduledRechargeRequest (Integer channel, String msisdn, String scheduledId, Double amount, Byte birthday, String cardType, String last4Digits) {
        super();
        this.channel = channel;
        this.msisdn = msisdn;
        this.scheduledId = scheduledId;
        this.amount = amount;
        this.birthday = birthday;
        this.cardType = cardType;
        this.last4Digits = last4Digits;

    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getScheduledId() {
        return scheduledId;
    }

    public void setScheduledId(String scheduledId) {
        this.scheduledId = scheduledId;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Byte getBirthday() {
        return birthday;
    }

    public void setBirthday(Byte birthday) {
        this.birthday = birthday;
    }
    
}
