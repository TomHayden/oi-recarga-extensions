package com.m4u.recharge.core.service;

import javax.ejb.Local;

import com.m4u.recharge.core.service.vo.SMSMessage;

@Local
public interface SMSManager {
    
    public static final String JNDI_NAME = "smsmanager/local";
    
    public boolean sendSMS(SMSMessage sms);
    
    public boolean receiveSMS(SMSMessage sms);

}
