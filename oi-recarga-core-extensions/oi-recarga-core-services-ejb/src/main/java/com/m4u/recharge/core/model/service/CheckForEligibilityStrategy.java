package com.m4u.recharge.core.model.service;

import java.util.List;

import com.m4u.recharge.core.model.PegasusCallbackEnum;
import com.m4u.recharge.core.model.ScheduledRechargeEvent;
import com.m4u.recharge.core.model.TransactionLog;
import com.m4u.recharge.core.model.TransactionLogType;
import com.m4u.recharge.core.service.MainService;
import com.m4u.recharge.core.service.RechargeLegacyManager;
import com.m4u.recharge.core.service.ScheduleRechargeManager;
import com.m4u.recharge.core.service.ScheduledRechargeFacadeService;
import com.m4u.recharge.core.service.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.util.DateUtil;

public class CheckForEligibilityStrategy implements PendingScheduledRechargeEventProcessingStrategy {

    private MainService mainService;
    private ScheduleRechargeManager scheduleRechargeManager;
    private RechargeLegacyManager rechargeLegacyManager;

    public CheckForEligibilityStrategy(ScheduledRechargeFacadeService scheduledRechargeFacadeService) {
        this.mainService = scheduledRechargeFacadeService.getMainService();
        this.scheduleRechargeManager = scheduledRechargeFacadeService.getScheduleRechargeManager();
        this.rechargeLegacyManager = scheduledRechargeFacadeService.getRechargeLegacyManager();
    }

    @Override
    public boolean preProcess(ScheduledRechargeEvent event) {
        boolean result = true;
        
        // O PASSO ANTERIOR, CONSULTA DE ELEGIBILIDADE, E' O
        // UNICO PASSO QUE NAO VEM FORA DE ORDEM, PORTANTO
        // SEU PROCESSAMENTO JA FOI REALIZADO
        
        return result;
    }

    @Override
    public boolean postProcess(ScheduledRechargeEvent event) {
        boolean result = true;

        Long cycleId = event.getCycleId();
        String cycleIdAsString = cycleId.toString();
        TransactionLog transactionLog = findTransactionLog(cycleIdAsString);

        // Se tiver registro na TransactionLog do tipo 17 OK retorno
        if (transactionLog == null) {
            result = false;

            // Caso contratio
            // Procuro por 17 pendente na tabela de eventos
            PegasusCallbackEnum eventType = PegasusCallbackEnum
                    .getCallbackByTransactionType(TransactionLogType.TRANSACTION_RECHARGE_CARRIER_AUTHORIZED);
            List<ScheduledRechargeEvent> pedingEvents = scheduleRechargeManager.findPendingScheduledEventByCycleIdAndEventType(cycleId,
                    eventType);
            // Para todo pendente 17 da tabela de event eu mando processar
            for (ScheduledRechargeEvent pendingEvent : pedingEvents) {
                // gerar a partir do evento um ScheduledRechargeEventRequest
                ScheduledRechargeEventRequest request = createScheduledRechargeEventRequestBasedOnPendingEvent(pendingEvent);
                // chamo o ejb Main.processCallbackNotifications
                this.mainService.processCallbackNotifications(request);
            }

            // Se tiver registro na TransactionLog do tipo 17 OK retorno
             transactionLog = findTransactionLog(cycleIdAsString);
             
             if ( transactionLog != null ) {
                 result = true;
             } else {
                 // Caso contrario
                 // false
                 result = false;
             }

        }
        return result;
    }
    
    private ScheduledRechargeEventRequest createScheduledRechargeEventRequestBasedOnPendingEvent(ScheduledRechargeEvent event) {
        String dtCreateAsString = DateUtil.dateToString(event.getDtCreated(), "yyyy-MM-dd HH:mm:ss.SSS");
        ScheduledRechargeEventRequest request = new ScheduledRechargeEventRequest(event.getCycleId(), event.getDescription(), event.getPayload(), event.getScheduleRecharge().getId(), event.getCycleStepId(), event.getIdServicePegasus(), dtCreateAsString);
        request.setEventId(event.getId());        
        request.setReprocessed(true);
        return request;
    }
    
    private TransactionLog findTransactionLog(String cycleId) {
        TransactionLog transactionLog = rechargeLegacyManager.findTransactionLogByExternalIdAndTransactionType(cycleId,
                TransactionLogType.TRANSACTION_RECHARGE_CARRIER_AUTHORIZED);
        return transactionLog;
    }
}
