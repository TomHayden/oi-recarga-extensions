package com.m4u.recharge.core.model;

public enum CallCenterEventType {

    TRANSACTION_ADD_CUSTOMER(TransactionLogType.TRANSACTION_ADD_CUSTOMER, "Criacao de Cadastro", false),
    TRANSACTION_CANCEL_CUSTOMER(TransactionLogType.TRANSACTION_CANCEL_CUSTOMER, "Cancelamento", false),
    TRANSACTION_ADD_NUMBER_PREPAID(TransactionLogType.TRANSACTION_ADD_NUMBER_PREPAID, "Cadastro de Recebedor", false),
    TRANSACTION_MODIFY_NUMBER_PREPAID(TransactionLogType.TRANSACTION_MODIFY_NUMBER_PREPAID, "Alteracao de Recebedor", false),
    TRANSACTION_DELETE_NUMBER_PREPAID(TransactionLogType.TRANSACTION_DELETE_NUMBER_PREPAID, "Exclusao de Recebedor", false),
    TRANSACTION_MODIFY_PASSWORD(TransactionLogType.TRANSACTION_MODIFY_PASSWORD, "Alteracao de Senha", false),
    TRANSACTION_ADD_CCARD(TransactionLogType.TRANSACTION_ADD_CCARD, "Cadastro de Cartao", false),
    TRANSACTION_DELETE_CCARD(TransactionLogType.TRANSACTION_DELETE_CCARD, "Exclusao de Cartao", false),
    TRANSACTION_ADD_BLACK_LIST(TransactionLogType.TRANSACTION_ADD_BLACK_LIST, "Bloqueio", false),
    TRANSACTION_DELETE_BLACK_LIST(TransactionLogType.TRANSACTION_DELETE_BLACK_LIST, "Desbloqueado", false),

    TRANSACTION_RECHARGE_STARTED(TransactionLogType.TRANSACTION_RECHARGE_STARTED, "Recarga", true),
    TRANSACTION_RECHARGE_CARRIER_AUTHORIZED(TransactionLogType.TRANSACTION_RECHARGE_CARRIER_AUTHORIZED, "Recarga", true),
    TRANSACTION_RECHARGE_CC_DEBITED(TransactionLogType.TRANSACTION_RECHARGE_CC_DEBITED, "Recarga", true),
    TRANSACTION_RECHARGE_DONE(TransactionLogType.TRANSACTION_RECHARGE_DONE, "Recarga", true),
    TRANSACTION_RECHARGE_FINAL_FAILURE(TransactionLogType.TRANSACTION_RECHARGE_FINAL_FAILURE, "Recarga", true),
    TRANSACTION_RECHARGE_CC_PREAUTHORIZED(TransactionLogType.TRANSACTION_RECHARGE_CC_PREAUTHORIZED, "Recarga", true),
    TRANSACTION_PAYMENT_CANCELED(TransactionLogType.TRANSACTION_PAYMENT_CANCELED, "Recarga", true),

    TRANSACTION_PROFILE_CHANGED(TransactionLogType.TRANSACTION_PROFILE_CHANGED, "Scoring", false),

    TRANSACTION_BUSINESS_VALIDATION_ERROR(TransactionLogType.TRANSACTION_BUSINESS_VALIDATION_ERROR, "Regra de Negocio", false),
    TRANSACTION_UPDATE_CCARD(TransactionLogType.TRANSACTION_UPDATE_CCARD, "Alteracao de Cartao de Credito", false),
    TRANSACTION_SCHEDULED_RECHARGE_REGISTERED(
            TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_REGISTERED,
            "Adesao Recarga Programada",
            false),
    TRANSACTION_SCHEDULED_RECHARGE_UNREGISTERED(
            TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_UNREGISTERED,
            "Cancelamento Recarga Programada",
            false),
    TRANSACTION_SCHEDULED_RECHARGE_AMOUNT_CHANGE(
            TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_AMOUNT_CHANGE,
            "Alteracao Valor Recarga Programada",
            false),
    TRANSACTION_SCHEDULED_RECHARGE_DATE_CHANGE(
            TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_DATE_CHANGE,
            "Alteracao Data Recarga Programada",
            false);

    private TransactionLogType type;
    private String description;
    private boolean isGroupedEvent;

    CallCenterEventType(TransactionLogType type, String description, boolean isGroupedEvent) {
        this.type = type;
        this.description = description;
        this.isGroupedEvent = isGroupedEvent;
    }

    public TransactionLogType type() {
        return type;
    }

    public String decription() {
        return description;
    }

    public boolean isGroupedEvent() {
        return isGroupedEvent;
    }

    public static String getNotGroupedEventCommaSeparated() {
        String result = null;
        for (CallCenterEventType item : CallCenterEventType.values()) {
            if (!item.isGroupedEvent) {
                if (result == null) {
                    result = item.type().getCode().toString();
                } else {
                    result += "," + item.type().getCode().toString();
                }
            }
        }
        return result;
    }

    public static String getGroupedEventCommaSeparated() {
        String result = null;
        for (CallCenterEventType item : CallCenterEventType.values()) {
            if (item.isGroupedEvent) {
                if (result == null) {
                    result = item.type().getCode().toString();
                } else {
                    result += "," + item.type().getCode().toString();
                }
            }
        }
        return result;
    }

    public static CallCenterEventType parse(Integer idTransactionLogType) {
        for (CallCenterEventType item : CallCenterEventType.values()) {
            if (item.type().getCode().equals(idTransactionLogType)) {
                return item;
            }
        }
        return null;
    }
}
