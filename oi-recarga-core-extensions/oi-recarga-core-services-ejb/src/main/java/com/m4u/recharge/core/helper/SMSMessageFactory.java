package com.m4u.recharge.core.helper;

import com.m4u.recharge.util.RecargaProperties;
import com.m4u.recharge.util.RechargeUtil;

public class SMSMessageFactory {

    public String createMessageAmountChangeSuccess(Double amountRegistered, Double amountToChange) {

        String text = RecargaProperties.getConf().getString("scheduled.recharge.amount.changes");
        String message = String.format(text, null, null, RechargeUtil.moneyToString(amountRegistered), null, null, null, null,
                RechargeUtil.moneyToString(amountToChange));

        return message;
    }

    public String createMessageBirthdayChangeSuccess(Byte birthdayRegistered, Byte birthdayToChange) {

        String text = RecargaProperties.getConf().getString("scheduled.recharge.birthday.changes");
        String message = String.format(text, null, null, null, null, null, null, null, null, birthdayRegistered, birthdayToChange);

        return message;

    }

    private String createMessageCCardChangeSuccess(String paymentType, String last4Digits) {

        String text = RecargaProperties.getConf().getString("sms.message.schedule.recharge.replaced.credit.card");
        String message = String.format(text, null, null, null, paymentType, last4Digits);

        return message;
    }

    public String findSuccessMessageToSend(Double amountRegistered, Double amountToChange, Byte birthdayRegistered, Byte birthdayToChange,
            String paymentType, String last4Digits) {

        String message = null;

        if (amountToChange != null) {
            message = createMessageAmountChangeSuccess(amountRegistered, amountToChange);
        }

        if (birthdayToChange != null) {
            message = createMessageBirthdayChangeSuccess(birthdayRegistered, birthdayToChange);
        }

        if (paymentType != null && last4Digits != null) {
            message = createMessageCCardChangeSuccess(paymentType, last4Digits);
        }

        return message;
    }

}
