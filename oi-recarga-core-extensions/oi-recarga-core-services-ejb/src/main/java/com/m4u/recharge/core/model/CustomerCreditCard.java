package com.m4u.recharge.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Customer_CCard")
@NamedQueries(value = { 
    @NamedQuery(name = "findCreditCardByMaskAndCustomer", query = "SELECT ccc FROM CustomerCreditCard ccc WHERE ccc.cardType = :cardType and ccc.last4Digits = :last4Digits and ccc.customer = :customer"),
    @NamedQuery(name = "findCreditCardByCCId", query = "SELECT ccc FROM CustomerCreditCard ccc WHERE ccc.id = :ccardId")
})
public class CustomerCreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_customer_ccard")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_customer")
    private Customer customer;

    @Column(name = "mask")
    private String last4Digits;

    @Column(name = "payment_type")
    private String cardType;

    @Column(name = "status")
    private boolean active;
    
    @Column(name = "token_ccard")
    private String token;

    @Column(name = "id_ccard")
    private Integer eldoradoCardId;

    @Column(name = "id_pan")
    private Integer eldoradoPanId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getEldoradoCardId() {
        return eldoradoCardId;
    }

    public void setEldoradoCardId(Integer eldoradoCardId) {
        this.eldoradoCardId = eldoradoCardId;
    }

    public Integer getEldoradoPanId() {
        return eldoradoPanId;
    }

    public void setEldoradoPanId(Integer eldoradoPanId) {
        this.eldoradoPanId = eldoradoPanId;
    }

}
