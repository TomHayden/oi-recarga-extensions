package com.m4u.recharge.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.Service;

import com.m4u.recharge.core.service.vo.SMSMessage;

@Service(objectName = "jmsProducer:service=com.m4u")
public class JMSProducerImpl implements JMSProducer {

    private static final Logger logger = Logger.getLogger(JMSProducerImpl.class);

    @Override
    public void sendMessage(String queueName, String text, int quantity) {

        try {
            Connection conn = getConnectionFactory().createConnection();
            Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(getQueue(queueName));

            SMSMessage sms = new SMSMessage("3000", "21988887777", text);
            
            Message message = session.createTextMessage(text);
            
            for (int i = 0; i < quantity; i++) {
                producer.send(message);
            }
            
        } catch (JMSException e) {
            logger.error("Erro ao enviar SMS para fila.", e);
        }

    }

    private ConnectionFactory getConnectionFactory() {
        try {
            InitialContext ic = new InitialContext();
            ConnectionFactory connectionFactory = (ConnectionFactory) ic.lookup("java:/ConnectionFactory");
            return connectionFactory;
        } catch (NamingException ex) {
            logger.error("Erro durante lookup do ConnectionFactory.", ex);
            return null;
        }
    }

    private Destination getQueue(String queueName) {
        try {
            InitialContext ic = new InitialContext();
            Destination destination = (Destination) ic.lookup(queueName);
            return destination;
        } catch (NamingException ex) {
            logger.error("Erro durante lookup da Queue: " + queueName, ex);
            return null;
        }
    }

}
