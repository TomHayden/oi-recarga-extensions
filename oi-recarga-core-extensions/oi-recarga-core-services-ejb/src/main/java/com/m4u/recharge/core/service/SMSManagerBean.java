package com.m4u.recharge.core.service;

import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.LocalBinding;

import com.m4u.recharge.core.service.vo.SMSMessage;

@Stateless(mappedName = SMSManager.JNDI_NAME, name = "SMSManagerBean")
@LocalBinding(jndiBinding = SMSManager.JNDI_NAME)
public class SMSManagerBean implements SMSManager {
    
    private static final Logger logger = Logger.getLogger(SMSManagerBean.class);

//    @PostConstruct
//    public void init() {
//
//        try {
//            Connection conn = factory.createConnection();
//            session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
//            producer = session.createProducer(destination);
//        } catch (Exception e) {
//            throw new EJBException("Probemas construindo bean", e);
//        }
//
//    }

    @Override
    public boolean sendSMS(SMSMessage sms) {

        try {
            Connection conn = getConnectionFactory().createConnection();
            Session session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(getSmsSenderQueue());
            
            Message message = session.createObjectMessage(sms);
            producer.send(message);
        } catch (JMSException e) {
            logger.error("Erro ao enviar SMS para fila.", e);
            return false;
        }

        return true;
    }

    @Override
    public boolean receiveSMS(SMSMessage sms) {
        return false;
    }
    
    private ConnectionFactory getConnectionFactory() {
        try {
            InitialContext ic = new InitialContext();
            ConnectionFactory connectionFactory = (ConnectionFactory) ic.lookup("java:/ConnectionFactory");
            return connectionFactory;
        } catch (NamingException ex) {
            logger.error("Erro durante lookup do ConnectionFactory.", ex);
            return null;
        }
    }
    
    private Destination getSmsSenderQueue() {
        try {
            InitialContext ic = new InitialContext();
            Destination destination = (Destination) ic.lookup(NewSMSSenderMDB.QUEUE_NAME);
            return destination;
        } catch (NamingException ex) {
            logger.error("Erro durante lookup do ConnectionFactory.", ex);
            return null;
        }
    }
    
}
