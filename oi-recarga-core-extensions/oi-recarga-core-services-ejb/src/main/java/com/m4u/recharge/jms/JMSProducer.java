package com.m4u.recharge.jms;

import org.jboss.ejb3.annotation.Management;

@Management
public interface JMSProducer {
    
    public void sendMessage(String queueName, String message, int quantity);

}
