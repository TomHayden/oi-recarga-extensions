package com.m4u.recharge.core.service;

import java.util.List;

import javax.ejb.Local;

import com.m4u.recharge.core.model.CallCenterEvent;
import com.m4u.recharge.core.model.Channel;
import com.m4u.recharge.core.model.Customer;
import com.m4u.recharge.core.model.CustomerCreditCard;
import com.m4u.recharge.core.model.CustomerPrepaid;
import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.model.ScheduledRechargeEvent;
import com.m4u.recharge.core.model.TransactionLog;
import com.m4u.recharge.core.model.TransactionLogType;
import com.m4u.recharge.core.service.vo.UpdateScheduledRechargeRequest;

@Local
public interface RechargeLegacyManager {

    public static final String JNDI_NAME = "rechargelegacymanager/local";

    public abstract String getCustomerFromBase(String msisdn);

    public abstract Channel findChannelById(Integer channel);

    public abstract Customer findCustomerByMsisdn(String msisdn);

    public abstract CustomerPrepaid findCustomerPrepaid(String prepaid, Customer customer);

    public abstract CustomerCreditCard findCustomerCreditCard(String cardType, String last4Digits, Customer customer);

    public abstract CustomerCreditCard findCustomerCreditCardByCCId(String ccardId);
    
    public abstract TransactionLog findTransactionLogByExternalIdAndTransactionType(String idExternal, TransactionLogType type);

    public abstract void createEventToScheduleElegibilityValid(ScheduledRecharge schedule, ScheduledRechargeEvent event);

    public abstract void createEventToScheduleElegibilityBalanceError(ScheduledRecharge schedule, ScheduledRechargeEvent event, String idExternal);

    public abstract TransactionLog addTransactionLogStep(ScheduledRechargeEvent scheduledRechargeEvent);

    public abstract void createEventForNewScheduledRecharge(ScheduledRecharge schedule);

    public abstract void createEventForRemovedScheduledRecharge(ScheduledRecharge schedule, Integer channelId, String obs);

    public abstract void createEventForMaxAmountChangesForScheduledRechargeReached(UpdateScheduledRechargeRequest updateSchedule,
            ScheduledRecharge schedule, String obs);

    public abstract void createEventForMaxBirthdayChangesForScheduledRechargeReached(UpdateScheduledRechargeRequest updateSchedule,
            ScheduledRecharge schedule, String obs);

    public abstract List<CallCenterEvent> findCustomerEventsInPeriod(String msisdn, Integer month);

    public abstract Integer callGetQtdChangesProcedure(int idCustomer, int monthRange, Integer idTransactionType);

    public abstract void createEventToCountScheduleRechargeChanges(ScheduledRecharge schedule, TransactionLogType idTransactionType,
            String obs, Integer channel);


}
