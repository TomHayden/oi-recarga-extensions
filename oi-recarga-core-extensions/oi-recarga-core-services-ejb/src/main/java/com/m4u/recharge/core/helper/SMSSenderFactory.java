package com.m4u.recharge.core.helper;

import com.m4u.recharge.util.RecargaProperties;

public class SMSSenderFactory {

    public static SMSSender create() {
        
        String vasURL = RecargaProperties.getConf().getString("sms.vas.url", "http://vas.oi.m4u.com.br:84/dispatcherSMS.pl");
        String vasInterface = RecargaProperties.getConf().getString("sms.vas.interface", "SMSC827");
        String vasLA = RecargaProperties.getConf().getString("sms.vas.from", "3000");
        
        return new SMSSenderVASImpl(vasURL, vasInterface, vasLA);
        
    }
}
