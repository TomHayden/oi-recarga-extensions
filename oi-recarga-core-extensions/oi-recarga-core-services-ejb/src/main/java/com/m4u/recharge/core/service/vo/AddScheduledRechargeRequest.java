package com.m4u.recharge.core.service.vo;


public class AddScheduledRechargeRequest {

    private Integer channel;

    private String msisdn;

    private String prepaid;

    private String cardType;

    private String last4Digits;

    private Double amount;

    private Byte day;
    
    private String subscriptionType;
    
    public AddScheduledRechargeRequest(Integer channel, String msisdn, String prepaid, String cardType, String last4Digits, Double amount,
            Byte day, String subscriptionType) {
        super();
        this.channel = channel;
        this.msisdn = msisdn;
        this.prepaid = prepaid;
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.amount = amount;
        this.day = day;
        this.subscriptionType = subscriptionType;
    }

    public Integer getChannel() {
        return channel;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public String getCardType() {
        return cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public Double getAmount() {
        return amount;
    }

    public Byte getDay() {
        return day;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    @Override
    public String toString() {
        return "AddScheduledRechargeRequest [channel=" + channel + ", msisdn=" + msisdn + ", prepaid=" + prepaid + ", cardType=" + cardType
                + ", last4Digits=" + last4Digits + ", amount=" + amount + ", day=" + day + ", subscriptionType=" + subscriptionType + "]";
    }

}
