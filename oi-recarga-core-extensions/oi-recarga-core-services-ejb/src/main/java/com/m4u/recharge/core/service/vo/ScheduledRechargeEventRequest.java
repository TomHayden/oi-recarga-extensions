package com.m4u.recharge.core.service.vo;

public class ScheduledRechargeEventRequest {

    private Integer eventId;
    private Long cycleId;
    private String description;
    private String payload;
    private String scheduleid;
    private String cycleStepId;
    private String serviceId;
    private String created;
    private boolean reprocessed = false;

    public ScheduledRechargeEventRequest(Long cycleId, String description, String payload, String scheduleid, String cycleStepId,
            String serviceId, String created) {
        super();
        this.cycleId = cycleId;
        this.description = description;
        this.payload = payload;
        this.scheduleid = scheduleid;
        this.cycleStepId = cycleStepId;
        this.serviceId = serviceId;
        this.created = created;
    }

    public Long getCycleId() {
        return cycleId;
    }

    public String getDescription() {
        return description;
    }

    public String getPayload() {
        return payload;
    }

    public String getScheduleid() {
        return scheduleid;
    }

    public String getCycleStepId() {
        return cycleStepId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getCreated() {
        return created;
    }
    
    public boolean isReprocessed() {
        return reprocessed;
    }
    
    public void setReprocessed(boolean reprocessed) {
        this.reprocessed = reprocessed;
    }
    
    public Integer getEventId() {
        return eventId;
    }
    
    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "ScheduledRechargeEventRequest [cycleId=" + cycleId + ", description=" + description + ", payload=" + payload
                + ", scheduleid=" + scheduleid + ", cycleStepId=" + cycleStepId + ", serviceId=" + serviceId + ", created=" + created
                + ", reprocessed=" + reprocessed + "]";
    }
    
    
}
