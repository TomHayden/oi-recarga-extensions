package com.m4u.recharge.core.service;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.SessionContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.jboss.ejb3.annotation.Pool;
import org.jboss.ejb3.annotation.defaults.PoolDefaults;

import com.m4u.recharge.core.helper.SMSSenderFactory;
import com.m4u.recharge.core.service.vo.SMSMessage;

@MessageDriven(mappedName = "NewSMSSenderMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = NewSMSSenderMDB.QUEUE_NAME),
        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "4") })
@Pool(value = PoolDefaults.POOL_IMPLEMENTATION_STRICTMAX, maxSize = 3)
public class NewSMSSenderMDB implements MessageListener {

    private static final Logger logger = Logger.getLogger(NewSMSSenderMDB.class);

    public static final String QUEUE_NAME = "queue/NewSMSSenderQueue";

    private SessionContext getSessionContext() {
        try {
            InitialContext ic = new InitialContext();
            SessionContext sctxLookup = (SessionContext) ic.lookup("java:comp/EJBContext");
            return sctxLookup;
        } catch (NamingException ex) {
            logger.error("Erro durante lookup do SessionContext.", ex);
            return null;
        }
    }
    
    @Override
    public void onMessage(Message msg) {

        try {
            
            SMSMessage sms = (SMSMessage) ((ObjectMessage) msg).getObject();
            boolean isSent = SMSSenderFactory.create().send(sms);
            
            if (!isSent) {
                logger.warn("SMS nao enviado. Voltara para a fila para retentativa.");
                getSessionContext().setRollbackOnly();
            }

        } catch (JMSException e) {
            logger.error("Problema na fila " + QUEUE_NAME, e);

        }

    }

}
