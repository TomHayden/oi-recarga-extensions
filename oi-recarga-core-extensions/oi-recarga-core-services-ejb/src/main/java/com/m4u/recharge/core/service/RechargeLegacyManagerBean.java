package com.m4u.recharge.core.service;

import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.jboss.ejb3.annotation.LocalBinding;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.m4u.recharge.core.model.CallCenterEvent;
import com.m4u.recharge.core.model.CallCenterEventType;
import com.m4u.recharge.core.model.Channel;
import com.m4u.recharge.core.model.Customer;
import com.m4u.recharge.core.model.CustomerCreditCard;
import com.m4u.recharge.core.model.CustomerPrepaid;
import com.m4u.recharge.core.model.ExternalTransaction;
import com.m4u.recharge.core.model.PegasusCallbackEnum;
import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.model.ScheduledRechargeEvent;
import com.m4u.recharge.core.model.SeqRecharge;
import com.m4u.recharge.core.model.TransactionLog;
import com.m4u.recharge.core.model.TransactionLog.Builder;
import com.m4u.recharge.core.model.TransactionLogPK;
import com.m4u.recharge.core.model.TransactionLogType;
import com.m4u.recharge.core.service.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.util.DateUtil;
import com.m4u.recharge.util.RecargaProperties;

@Stateless(mappedName = RechargeLegacyManager.JNDI_NAME, name = "RechargeLegacyServiceBean")
@LocalBinding(jndiBinding = RechargeLegacyManager.JNDI_NAME)
public class RechargeLegacyManagerBean implements RechargeLegacyManager {

    private static final Integer CANAL_RECARGA_PROGRAMADA = 29;

    @EJB
    private SMSManager smsManager;

    @PersistenceContext(unitName = "oi-recarga-pessoal-PU")
    private EntityManager em;

    public String getCustomerFromBase(String msisdn) {
        Query qry = em.createNativeQuery("SELECT msisdn FROM Customer where Msisdn = :msisdn");
        qry.setParameter("msisdn", msisdn);
        @SuppressWarnings("unchecked")
        List<String> values = qry.getResultList();
        return values.get(0);
    }

    @Override
    public Channel findChannelById(Integer id) {
        Channel result = em.find(Channel.class, id);
        return result;
    }

    @Override
    public Customer findCustomerByMsisdn(String msisdn) {
        Query qry = em.createNamedQuery("findCustomerByMsisdn");
        qry.setParameter("msisdn", msisdn);
        try {
            Customer result = (Customer) qry.getSingleResult();
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public CustomerPrepaid findCustomerPrepaid(String prepaid, Customer customer) {
        Query qry = em.createNamedQuery("findPrepaidByPrepaidAndCustomer");
        qry.setParameter("prepaid", prepaid);
        qry.setParameter("customer", customer);
        try {
            CustomerPrepaid result = (CustomerPrepaid) qry.getSingleResult();
            return result;
        } catch (Exception e) {
            return null;
        }

    }

    @Override
    public CustomerCreditCard findCustomerCreditCard(String cardType, String last4Digits, Customer customer) {
        Query qry = em.createNamedQuery("findCreditCardByMaskAndCustomer");
        qry.setParameter("cardType", cardType);
        qry.setParameter("last4Digits", last4Digits);
        qry.setParameter("customer", customer);
        try {
            CustomerCreditCard result = (CustomerCreditCard) qry.getSingleResult();
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public CustomerCreditCard findCustomerCreditCardByCCId(String ccardId) {

        Query qry = em.createNamedQuery("findCreditCardByCCId");
        qry.setParameter("ccardId", ccardId);

        try {
            CustomerCreditCard result = (CustomerCreditCard) qry.getSingleResult();
            return result;
        } catch (Exception e) {
            return null;
        }

    }

    @Override
    public TransactionLog findTransactionLogByExternalIdAndTransactionType(String idExternal, TransactionLogType type) {

        TransactionLog result = null;
        Query qry = em
                .createNativeQuery(
                        "SELECT tl.* FROM Transaction_Log tl "
                                + "JOIN External_Transaction et ON tl.id_transaction_log = et.id_transaction_log AND tl.id_transaction_type = :id_transaction_type "
                                + "WHERE et.id_external = :id_external ",
                        // + defineStatusOkay(type),
                        TransactionLog.class);

        qry.setParameter("id_external", idExternal);
        qry.setParameter("id_transaction_type", type.getCode());

        try {
            result = (TransactionLog) qry.getSingleResult();
        } catch (NoResultException e) {
            // ignored
        }
        return result;
    }

    private String defineStatusOkay(TransactionLogType type) {
        String result = null;
        switch (type) {
        case TRANSACTION_RECHARGE_CARRIER_AUTHORIZED:
            result = " AND nsu_code IS NOT NULL ";
            break;
        case TRANSACTION_RECHARGE_CC_DEBITED:
            result = " AND return_code = '000' ";
            break;
        case TRANSACTION_RECHARGE_DONE:
            result = " AND return_code = '000' ";
            break;
        default:
            result = "";
            break;
        }
        return result;
    }

    @Override
    public void createEventToScheduleElegibilityValid(ScheduledRecharge schedule, ScheduledRechargeEvent event) {
        Channel channel = findChannelById(CANAL_RECARGA_PROGRAMADA);
        String idExternal = event.getCycleId().toString();
        ExternalTransaction externalTransaction = saveOrUpdateExternalTransaction(idExternal, channel);
        String observation = RecargaProperties.getConf().getString("schedule.callcenter.eligibility.request");
        saveOrUpdateTransactionLogBasedOnEligibilityCheckEvent(schedule, event, externalTransaction, observation);
        markEventAsProcessed(event);
    }

    private void markEventAsProcessed(ScheduledRechargeEvent event) {
        event.processed();
        em.merge(event);
    }

    private ExternalTransaction saveOrUpdateExternalTransaction(String idExternal, Channel channel) {
        boolean foundExternalTransactionLog = true;
        ExternalTransaction externalTransaction = findExternalTransation(channel, idExternal);

        if (externalTransaction == null) {
            foundExternalTransactionLog = false;
            externalTransaction = new ExternalTransaction();
            externalTransaction.setIdTrasactionLog(this.createTransactionLogId());
            externalTransaction.setChannel(findChannelById(CANAL_RECARGA_PROGRAMADA));
        }

        externalTransaction.setDtLastUpdate(new Date());
        externalTransaction.setIdExternal(idExternal);

        if (!foundExternalTransactionLog) {
            em.persist(externalTransaction);
        } else {
            em.merge(externalTransaction);
        }

        return externalTransaction;
    }

    public void createEventToScheduleElegibilityBalanceError(ScheduledRecharge schedule, ScheduledRechargeEvent event, String idExternal) {
        Channel channel = findChannelById(CANAL_RECARGA_PROGRAMADA);

        String observation = "Limite de recargas atingido no mes";
        ExternalTransaction externalTransaction = saveOrUpdateExternalTransaction(idExternal, channel);
        saveOrUpdateTransactionLogBasedOnEligibilityCheckEvent(schedule, event, externalTransaction, observation);
        markEventAsProcessed(event);
    }

    private void saveOrUpdateTransactionLogBasedOnEligibilityCheckEvent(ScheduledRecharge schedule, ScheduledRechargeEvent event,
            ExternalTransaction externalTransaction, String observation) {
        TransactionLog transactionLog = this.findTransactionLogByKey(externalTransaction.getIdTrasactionLog(),
                TransactionLogType.TRANSACTION_RECHARGE_STARTED.getCode());

        if (transactionLog == null) {
            Integer transactionLogId = externalTransaction.getIdTrasactionLog();
            transactionLog = new TransactionLog.Builder(transactionLogId, TransactionLogType.TRANSACTION_RECHARGE_STARTED)
                    .withMsisdn(schedule.getCustomerPrepaid().getCustomer().getMsisdn())
                    .withPrepaid(schedule.getCustomerPrepaid().getPrepaid())
                    .withPaymentType(schedule.getCustomerCreditCard().getCardType())
                    .withLast4Digits(schedule.getCustomerCreditCard().getLast4Digits()).withAmount(schedule.getAmount())
                    .withChannelId(CANAL_RECARGA_PROGRAMADA).withObs(observation).build();
            em.persist(transactionLog);
        } else {
            transactionLog.setDateTime(event.getDtCreated());
            em.merge(transactionLog);
        }
    }

    private TransactionLog findTransactionLogByKey(Integer idTransactionLog, Integer transactionType) {
        try {
            return em.find(TransactionLog.class, new TransactionLogPK(idTransactionLog, transactionType));
        } catch (NoResultException e) {
            return null;
        }
    }

    private ExternalTransaction findExternalTransation(Channel channel, String idExternal) {
        ExternalTransaction result = null;
        Query qry = em.createNamedQuery("findExternalTransactionByChannelAndExternalId");
        qry.setParameter("channel", channel);
        qry.setParameter("idExternal", idExternal);
        try {
            result = (ExternalTransaction) qry.getSingleResult();
        } catch (NoResultException e) {
            // ignored
        }
        return result;
    }

    @Override
    public TransactionLog addTransactionLogStep(ScheduledRechargeEvent event) {

        // TODO: Não deveria ser findOrCreate... Somente find
        ExternalTransaction externalTransaction = findOrCreateExternalTransaction(event);

        // General log parameters
        String originalPayload = event.getPayload();
        // PayloadParser para Payload com queryString nao recupera primeiro parametro quando inicia com "?"
        String modifiedPayload = modifyPayloadForBenefitParse(originalPayload);
        Integer idTransactionLog = externalTransaction.getIdTrasactionLog();
        Date dateTime = event.getDtCreated();
        ScheduledRecharge scheduledRecharge = event.getScheduleRecharge();
        String msisdn = scheduledRecharge.getCustomerPrepaid().getCustomer().getMsisdn();
        String prepaid = scheduledRecharge.getCustomerPrepaid().getPrepaid();
        String last4Digits = scheduledRecharge.getCustomerCreditCard().getLast4Digits();
        String paymentType = scheduledRecharge.getCustomerCreditCard().getCardType();
        Double amount = scheduledRecharge.getAmount();
        Customer customer = scheduledRecharge.getCustomerPrepaid().getCustomer();
        CustomerCreditCard customerCreditCard = this.findCustomerCreditCard(paymentType, last4Digits, customer);
        Integer eldoradoPanId = customerCreditCard.getEldoradoPanId();
        Integer eldoradoCardId = customerCreditCard.getEldoradoCardId();
        String description = event.getDescription();
        String obs = getCallcenterMessage(modifiedPayload, description);

        TransactionLogType type = null;

        String paymentResponseCode = null;
        String paymentAuthorizeCode = null;
        String paymentResponseDescription = null;
        String paymentTid = null;

        TransactionLog transactionLog = null;

        if (event.preProcessPendingEvent()) {
            if (PegasusCallbackEnum.BILLING_AUTHORIZATION.getDescription().equals(description)) {
                type = TransactionLogType.TRANSACTION_RECHARGE_CC_DEBITED;
                Gson gson = new Gson();
                JsonObject payload = gson.fromJson(originalPayload, JsonObject.class);
                JsonElement resultJson = payload.get("result");
                if (!(resultJson instanceof JsonNull)) {
                    paymentResponseCode = resultJson.getAsJsonObject().get("resultCode").getAsString();
                    paymentResponseDescription = resultJson.getAsJsonObject().get("resultMessage").getAsString();

                }
                JsonElement authorizationJson = payload.get("authorizationNumber");
                if (!(authorizationJson instanceof JsonNull)) {
                    paymentAuthorizeCode = authorizationJson.getAsString();
                }
                JsonElement nsuJson = payload.get("nsu");
                if (!(nsuJson instanceof JsonNull)) {
                    paymentTid = nsuJson.getAsString();
                }
            }

            Long carrierId = null;
            String carrierNsuCode = null;
            if (PegasusCallbackEnum.BENEFIT_AUTHORIZATION.getDescription().equals(description)) {
                boolean isRechargeAuthorized = false;
                type = TransactionLogType.TRANSACTION_RECHARGE_CARRIER_AUTHORIZED;
                List<NameValuePair> payloadList = URLEncodedUtils.parse(modifiedPayload, Charset.defaultCharset());
                for (NameValuePair element : payloadList) {
                    if (element.getName().equalsIgnoreCase("nsu")) {
                        carrierNsuCode = element.getValue();
                    }
                    if (element.getName().equalsIgnoreCase("isAuthorized")) {
                        isRechargeAuthorized = "true".equals(element.getValue());
                    }
                    if (element.getName().equalsIgnoreCase("jsonResponseProperties")) {
                        String jsonResponseProperties = element.getValue();
                        Gson gson = new Gson();
                        JsonObject jsonObj = gson.fromJson(jsonResponseProperties, JsonObject.class);
                        carrierId = jsonObj.get("PDV_ID") instanceof JsonNull ? null : jsonObj.get("PDV_ID").getAsLong();
                    }
                }
                if (!isRechargeAuthorized) {
                    // Para adequar os dados ao historico do legado, o nsu sera descartado
                    // quando a recarga nao for autorizada
                    carrierNsuCode = null;
                }
            }

            if (PegasusCallbackEnum.BENEFIT_NOTIFY_CONFIRM.getDescription().equals(description)) {
                type = TransactionLogType.TRANSACTION_RECHARGE_DONE;
                TransactionLog billingAutorizationLog = em.find(TransactionLog.class, new TransactionLogPK(idTransactionLog,
                        TransactionLogType.TRANSACTION_RECHARGE_CC_DEBITED.getCode()));
                paymentAuthorizeCode = billingAutorizationLog.getPaymentAuthorizeCode();
                paymentResponseCode = billingAutorizationLog.getPaymentResponseCode();
                paymentResponseDescription = billingAutorizationLog.getPaymentResponseDescription();
                paymentTid = billingAutorizationLog.getPaymentTid();

                TransactionLog rechargeAutorizationLog = em.find(TransactionLog.class, new TransactionLogPK(idTransactionLog,
                        TransactionLogType.TRANSACTION_RECHARGE_CARRIER_AUTHORIZED.getCode()));
                carrierId = rechargeAutorizationLog.getCarrierId();
                carrierNsuCode = rechargeAutorizationLog.getCarrierNsuCode();

                // Adicionando diferenca de tempo para que o ultimo passo esteja sempre na frente
                dateTime = ensureCurrentEventTimeIsAfterThatPrior(rechargeAutorizationLog.getDateTime(), dateTime);

            }

            transactionLog = findTransactionLogByExternalIdAndTransactionType(externalTransaction.getIdExternal(), type);

            // Logando evento na transactionLog para visibilidade no callcenter
            if (transactionLog == null) {
                transactionLog = new Builder(idTransactionLog, type).withMsisdn(msisdn).withPrepaid(prepaid)
                        .withChannelId(CANAL_RECARGA_PROGRAMADA).withLast4Digits(last4Digits).withAmount(amount)
                        .withEldoradoCardId(eldoradoCardId).withEldoradoPanId(eldoradoPanId).withObs(obs)
                        .withPaymentAuthorizeCode(paymentAuthorizeCode).withPaymentResponseCode(paymentResponseCode)
                        .withPaymentResponseDescription(paymentResponseDescription).withPaymentTid(paymentTid).withCarrierId(carrierId)
                        .withCarrierNsuCode(carrierNsuCode).withPaymentType(paymentType).withDateTime(dateTime).build();
                em.persist(transactionLog);
            } else {

                // Nao faz isso SEMPRE... Só quando a versao que estiver lá tiver falhada
                // ou a versao que estiver lá estiver Ok, porém é mais velha que a nova versao.
                if (!transactionLog.isSuccess()
                        || (transactionLog.isSuccess() && event.getDtCreated().getTime() > transactionLog.getDateTime().getTime())) {
                    transactionLog.setObs(obs);
                    transactionLog.setPaymentAuthorizeCode(paymentAuthorizeCode);
                    transactionLog.setPaymentResponseCode(paymentResponseCode);
                    transactionLog.setPaymentTid(paymentTid);
                    transactionLog.setCarrierId(carrierId);
                    transactionLog.setCarrierNsuCode(carrierNsuCode);
                    em.merge(transactionLog);
                }
            }

            markEventAsProcessed(event);

            if (transactionLog.isSuccess()) {
                event.postProcessPendingEvent();
            }
        }

        return transactionLog;

    }

    private String modifyPayloadForBenefitParse(String originalPayload) {
        String result = originalPayload;
        if (originalPayload != null) {
            result = originalPayload.startsWith("?") ? originalPayload.substring(1) : originalPayload;
        }
        return result;
    }

    private Date ensureCurrentEventTimeIsAfterThatPrior(Date priorEventDateTime, Date currentEventDateTime) {

        Long dateDiffInSeconds = DateUtil.dateDiffInSeconds(priorEventDateTime, currentEventDateTime);

        if (dateDiffInSeconds <= 1)
            return new Date(priorEventDateTime.getTime() + 666);
        else {
            return currentEventDateTime;
        }
    }

    private ExternalTransaction findOrCreateExternalTransaction(ScheduledRechargeEvent event) {
        // Give External Transaction
        Channel channel = findChannelById(CANAL_RECARGA_PROGRAMADA);
        Query qry = em.createNamedQuery("findExternalTransactionByChannelAndExternalId");
        qry.setParameter("channel", channel);
        qry.setParameter("idExternal", event.getCycleId().toString());
        ExternalTransaction externalTransaction = null;
        try {
            externalTransaction = (ExternalTransaction) qry.getSingleResult();
        } catch (NoResultException e) {
            externalTransaction = createExternalTransaction(event, channel);
        }
        return externalTransaction;
    }

    private ExternalTransaction createExternalTransaction(ScheduledRechargeEvent event, Channel channel) {
        ExternalTransaction externalTransaction;
        // cria nova linha na ExternalTransation caso nao exita
        externalTransaction = new ExternalTransaction();
        externalTransaction.setIdTrasactionLog(this.createTransactionLogId());
        externalTransaction.setChannel(channel);
        externalTransaction.setDtLastUpdate(event.getDtReceived());
        externalTransaction.setIdExternal(event.getCycleStepId());
        em.persist(externalTransaction);
        return externalTransaction;
    }

    private String getCallcenterMessage(String payload, String description) {
        String callcenterMSG = null;
        if (payload != null) {
            if (description.equals(PegasusCallbackEnum.BILLING_AUTHORIZATION.getDescription())) {
                Gson gson = new Gson();
                JsonObject payloadObj = gson.fromJson(payload, JsonObject.class);
                boolean resultSuccess = payloadObj.get("result").getAsJsonObject().get("resultSuccess").getAsBoolean();
                if (resultSuccess == false) {
                    callcenterMSG = RecargaProperties.getConf().getString("schedule.callcenter.billing.autorization.error");
                } else {
                    callcenterMSG = RecargaProperties.getConf().getString("schedule.callcenter.waiting.for.benefit.confirm");
                }
            } else if (description.equals(PegasusCallbackEnum.BENEFIT_AUTHORIZATION.getDescription())) {
                List<NameValuePair> payloadList = URLEncodedUtils.parse(payload, Charset.defaultCharset());
                boolean isRechargeAuthorized = false;
                for (NameValuePair element : payloadList) {
                    if (element.getName().equalsIgnoreCase("responseMessage")) {
                        callcenterMSG = element.getValue().trim();
                    }
                    if (element.getName().equalsIgnoreCase("isAuthorized")) {
                        isRechargeAuthorized = "true".equals(element.getValue());
                    }
                }
                if (isRechargeAuthorized) {
                    callcenterMSG = RecargaProperties.getConf().getString("schedule.callcenter.waiting.for.benefit.confirm");
                }
            }

        }
        if (description.equals(PegasusCallbackEnum.BENEFIT_NOTIFY_CONFIRM.getDescription())) {
            callcenterMSG = RecargaProperties.getConf().getString("schedule.callcenter.notify.benefit.success");
        }
        return callcenterMSG;
    }

    private Integer createTransactionLogId() {
        SeqRecharge seq = new SeqRecharge();
        seq.setDummy(1);
        em.persist(seq);
        em.flush();
        return seq.getId();
    }

    @Override
    public void createEventForNewScheduledRecharge(ScheduledRecharge schedule) {

        Integer idTxLog = this.createTransactionLogId();

        String msisdn = schedule.getCustomerPrepaid().getCustomer().getMsisdn();
        String prepaid = schedule.getCustomerPrepaid().getPrepaid();
        String paymentType = schedule.getCustomerCreditCard().getCardType();
        String last4Digits = schedule.getCustomerCreditCard().getLast4Digits();
        Double amount = schedule.getAmount();
        Integer channelId = schedule.getChannel().getId();
        String obs = String.format("Dia da Recarga: %02d", schedule.getRechargeDay());

        TransactionLog tx = new TransactionLog.Builder(idTxLog, TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_REGISTERED)
                .withMsisdn(msisdn).withPrepaid(prepaid).withPaymentType(paymentType).withLast4Digits(last4Digits).withAmount(amount)
                .withChannelId(channelId).withObs(obs).build();

        em.persist(tx);

    }

    @Override
    public void createEventForRemovedScheduledRecharge(ScheduledRecharge schedule, Integer channelId, String obs) {

        Integer idTxLog = this.createTransactionLogId();

        String msisdn = schedule.getCustomerPrepaid().getCustomer().getMsisdn();
        String prepaid = schedule.getCustomerPrepaid().getPrepaid();
        String paymentType = schedule.getCustomerCreditCard().getCardType();
        String last4Digits = schedule.getCustomerCreditCard().getLast4Digits();
        Double amount = schedule.getAmount();

        TransactionLog tx = new TransactionLog.Builder(idTxLog, TransactionLogType.TRANSACTION_SCHEDULED_RECHARGE_UNREGISTERED)
                .withMsisdn(msisdn).withPrepaid(prepaid).withPaymentType(paymentType).withLast4Digits(last4Digits).withAmount(amount)
                .withChannelId(channelId).withObs(obs).build();

        em.persist(tx);

    }

    @Override
    public void createEventForMaxAmountChangesForScheduledRechargeReached(UpdateScheduledRechargeRequest updateSchedule,
            ScheduledRecharge schedule, String obs) {

        Integer idTxLog = this.createTransactionLogId();
        TransactionLog tx = new TransactionLog.Builder(idTxLog, TransactionLogType.TRANSACTION_BUSINESS_VALIDATION_ERROR)
                .withMsisdn(updateSchedule.getMsisdn()).withPrepaid(schedule.getCustomerPrepaid().getPrepaid())
                .withPaymentType(schedule.getCustomerCreditCard().getCardType())
                .withLast4Digits(schedule.getCustomerCreditCard().getLast4Digits()).withAmount(schedule.getAmount())
                .withChannelId(updateSchedule.getChannel()).withObs(obs).build();

        em.persist(tx);
    }

    @Override
    public void createEventForMaxBirthdayChangesForScheduledRechargeReached(UpdateScheduledRechargeRequest updateSchedule,
            ScheduledRecharge schedule, String obs) {

        Integer idTxLog = this.createTransactionLogId();
        TransactionLog tx = new TransactionLog.Builder(idTxLog, TransactionLogType.TRANSACTION_BUSINESS_VALIDATION_ERROR)
                .withMsisdn(updateSchedule.getMsisdn()).withPrepaid(schedule.getCustomerPrepaid().getPrepaid())
                .withPaymentType(schedule.getCustomerCreditCard().getCardType())
                .withLast4Digits(schedule.getCustomerCreditCard().getLast4Digits()).withAmount(schedule.getAmount())
                .withChannelId(updateSchedule.getChannel()).withObs(obs).build();

        em.persist(tx);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Integer callGetQtdChangesProcedure(int idCustomer, int monthRange, Integer idTransactionType) {

        List<Object[]> result = null;
        Query qry = em.createNativeQuery("{call recharge.spGetQTDCustomer(?,?,?)}").setParameter(1, idCustomer).setParameter(2, monthRange)
                .setParameter(3, idTransactionType);

        result = qry.getResultList();
        return (Integer) result.get(0)[0];

    }

    @Override
    public void createEventToCountScheduleRechargeChanges(ScheduledRecharge schedule, TransactionLogType idTransactionType, String obs,
            Integer channel) {
        Integer idTxLog = this.createTransactionLogId();

        TransactionLog tx = new TransactionLog.Builder(idTxLog, idTransactionType)
                .withMsisdn(schedule.getCustomerPrepaid().getCustomer().getMsisdn()).withAmount(schedule.getAmount())
                .withPrepaid(schedule.getCustomerPrepaid().getPrepaid()).withPaymentType(schedule.getCustomerCreditCard().getCardType())
                .withLast4Digits(schedule.getCustomerCreditCard().getLast4Digits()).withChannelId(channel).withObs(obs).build();

        em.persist(tx);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CallCenterEvent> findCustomerEventsInPeriod(String msisdn, Integer month) {
        Calendar dateBegin = Calendar.getInstance();
        dateBegin.add(Calendar.MONTH, month * -1);
        Query qry = em
                .createNativeQuery(
                        "SELECT ID_TRANSACTION_LOG, DTLOG, MSISDN, PREPAID, AMOUNT, RETURN_CODE, ID_TRANSACTION_TYPE, OBS, ch.DESCRIPTION AS ORIGEM, PAYMENT_TYPE, MASK FROM TRANSACTION_LOG TL (NOLOCK) JOIN CHANNEL CH (NOLOCK) ON TL.ORIGEM = CH.ID_CHANNEL WHERE (ID_TRANSACTION_TYPE IN ("
                                + CallCenterEventType.getNotGroupedEventCommaSeparated()
                                + ")) AND (DTLOG >= :beginDate) AND (MSISDN = :msisdn) "
                                + "UNION SELECT ID_TRANSACTION_LOG, DTLOG, MSISDN, PREPAID, AMOUNT, RETURN_CODE, ID_TRANSACTION_TYPE, OBS, ch.DESCRIPTION AS ORIGEM, PAYMENT_TYPE, MASK FROM TRANSACTION_LOG TL (NOLOCK) JOIN CHANNEL CH (NOLOCK) ON TL.ORIGEM = CH.ID_CHANNEL WHERE (ID_TRANSACTION_TYPE IN ("
                                + CallCenterEventType.getGroupedEventCommaSeparated()
                                + ")) AND (DTLOG >= :beginDate) AND (MSISDN = :msisdn) AND DTLOG IN (SELECT MAX(DTLOG) FROM TRANSACTION_LOG WHERE (ID_TRANSACTION_TYPE IN ("
                                + CallCenterEventType.getGroupedEventCommaSeparated()
                                + ")) AND (DTLOG >= :beginDate) AND (MSISDN = :msisdn) AND (PAYMENT_TYPE IN ('MASTERCARD', 'VISA', 'DINERS', 'ELO', 'ITAU SHOPLINE', 'CC OI') OR PAYMENT_TYPE IS NULL) GROUP BY ID_TRANSACTION_LOG) ORDER BY DTLOG DESC",
                        CallCenterEvent.class);
        qry.setParameter("msisdn", msisdn);
        qry.setParameter("beginDate", dateBegin.getTime());
        List<CallCenterEvent> result = qry.getResultList();
        return result;
    }

}