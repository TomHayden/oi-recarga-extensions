package com.m4u.recharge.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Transaction_Log")
public class TransactionLog {

    public static class Builder {

        private Integer id;
        private TransactionLogType transactionLogType;
        private Date dateTime = new Date(System.currentTimeMillis());
        private String msisdn;
        private String prepaid;
        private Double amount;
        private String obs;
        private String paymentResponseCode;
        private String paymentAuthorizeCode;
        private String paymentResponseDescription;
        private String paymentTid;
        private String paymentType;
        private String carrierNsuCode;
        private Long carrierId;
        private String last4Digits;
        private Integer eldoradoCardId;
        private Integer eldoradoPanId;
        private Integer channelId;

        public Builder(Integer id, TransactionLogType transactionLogType) {
            this.id = id;
            this.transactionLogType = transactionLogType;
        }

        public Builder withDateTime(Date dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public Builder withMsisdn(String msisdn) {
            this.msisdn = msisdn;
            return this;
        }

        public Builder withPrepaid(String prepaid) {
            this.prepaid = prepaid;
            return this;
        }

        public Builder withAmount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Builder withObs(String obs) {
            this.obs = obs;
            return this;
        }

        public Builder withPaymentResponseCode(String paymentResponseCode) {
            this.paymentResponseCode = paymentResponseCode;
            return this;
        }

        public Builder withPaymentAuthorizeCode(String paymentAuthorizeCode) {
            this.paymentAuthorizeCode = paymentAuthorizeCode;
            return this;
        }

        public Builder withPaymentResponseDescription(String paymentResponseDescription) {
            this.paymentResponseDescription = paymentResponseDescription;
            return this;
        }

        public Builder withPaymentTid(String paymentTid) {
            this.paymentTid = paymentTid;
            return this;
        }

        public Builder withPaymentType(String paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public Builder withCarrierNsuCode(String carrierNsuCode) {
            this.carrierNsuCode = carrierNsuCode;
            return this;
        }

        public Builder withCarrierId(Long carrierId) {
            this.carrierId = carrierId;
            return this;
        }

        public Builder withLast4Digits(String last4Digits) {
            this.last4Digits = last4Digits;
            return this;
        }

        public Builder withEldoradoCardId(Integer eldoradoCardId) {
            this.eldoradoCardId = eldoradoCardId;
            return this;
        }

        public Builder withEldoradoPanId(Integer eldoradoPanId) {
            this.eldoradoPanId = eldoradoPanId;
            return this;
        }

        public Builder withChannelId(Integer channelId) {
            this.channelId = channelId;
            return this;
        }

        public TransactionLog build() {
            TransactionLogPK pk = new TransactionLogPK(id, transactionLogType.getCode());
            TransactionLog txLog = new TransactionLog();
            txLog.setPrimaryKey(pk);
            txLog.setDescription(transactionLogType.getDecription());
            txLog.setDateTime(dateTime);
            txLog.setMsisdn(msisdn);
            txLog.setPrepaid(prepaid);
            txLog.setAmount(amount);
            txLog.setObs(obs);
            txLog.setPaymentResponseCode(paymentResponseCode);
            txLog.setPaymentAuthorizeCode(paymentAuthorizeCode);
            txLog.setPaymentResponseDescription(paymentResponseDescription);
            txLog.setPaymentTid(paymentTid);
            txLog.setPaymentType(paymentType);
            txLog.setCarrierNsuCode(carrierNsuCode);
            txLog.setCarrierId(carrierId);
            txLog.setLast4Digits(last4Digits);
            txLog.setEldoradoCardId(eldoradoCardId);
            txLog.setEldoradoPanId(eldoradoPanId);
            txLog.setChannelId(channelId);
            return txLog;
        }

    }

    @EmbeddedId
    private TransactionLogPK primaryKey;

    private String description;

    @Column(name = "dtlog")
    private Date dateTime;

    private String msisdn;

    private String prepaid;

    private Double amount;

    private String obs;

    @Column(name = "return_code")
    private String paymentResponseCode;

    @Column(name = "cc_auth_code")
    private String paymentAuthorizeCode;

    @Column(name = "cc_message")
    private String paymentResponseDescription;

    @Column(name = "tid")
    private String paymentTid;

    @Column(name = "cc_nsu_code")
    private String ccNsuCode;

    @Column(name = "payment_type")
    private String paymentType;

    @Column(name = "nsu_code")
    private String carrierNsuCode;

    @Column(name = "pdv_code")
    private Long carrierId;

    @Column(name = "mask")
    private String last4Digits;

    @Column(name = "id_ccard")
    private Integer eldoradoCardId;

    @Column(name = "id_pan")
    private Integer eldoradoPanId;

    @Column(name = "origem")
    private Integer channelId;
    
    public TransactionLogPK getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(TransactionLogPK primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getPaymentResponseCode() {
        return paymentResponseCode;
    }

    public void setPaymentResponseCode(String paymentResponseCode) {
        this.paymentResponseCode = paymentResponseCode;
    }

    public String getPaymentAuthorizeCode() {
        return paymentAuthorizeCode;
    }

    public void setPaymentAuthorizeCode(String paymentAuthorizeCode) {
        this.paymentAuthorizeCode = paymentAuthorizeCode;
    }

    public String getPaymentResponseDescription() {
        return paymentResponseDescription;
    }

    public void setPaymentResponseDescription(String paymentResponseDescription) {
        this.paymentResponseDescription = paymentResponseDescription;
    }

    public String getPaymentTid() {
        return paymentTid;
    }

    public void setPaymentTid(String paymentTid) {
        this.paymentTid = paymentTid;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCarrierNsuCode() {
        return carrierNsuCode;
    }

    public void setCarrierNsuCode(String carrierNsuCode) {
        this.carrierNsuCode = carrierNsuCode;
    }

    public Long getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(Long carrierId) {
        this.carrierId = carrierId;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public Integer getEldoradoCardId() {
        return eldoradoCardId;
    }

    public void setEldoradoCardId(Integer eldoradoCardId) {
        this.eldoradoCardId = eldoradoCardId;
    }

    public Integer getEldoradoPanId() {
        return eldoradoPanId;
    }

    public void setEldoradoPanId(Integer eldoradoPanId) {
        this.eldoradoPanId = eldoradoPanId;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public String getCcNsuCode() {
        return ccNsuCode;
    }

    public void setCcNsuCode(String ccNsuCode) {
        this.ccNsuCode = ccNsuCode;
    }
    
    public boolean isSuccess() {
        boolean result = true;
        
        TransactionLogType type = TransactionLogType.valueOf(this.primaryKey.getType());
        switch (type) {
        case TRANSACTION_RECHARGE_STARTED:
            result = true;
            break;
        case TRANSACTION_RECHARGE_CC_DEBITED:
            result = ("000".equals(paymentResponseCode));
            break;
        case TRANSACTION_RECHARGE_CARRIER_AUTHORIZED:
            result = (carrierNsuCode != null);
            break;
        case TRANSACTION_RECHARGE_DONE:
            result = true;
            break;
        default:
            result = true;
            break;
        }
        return result;
    }
}
