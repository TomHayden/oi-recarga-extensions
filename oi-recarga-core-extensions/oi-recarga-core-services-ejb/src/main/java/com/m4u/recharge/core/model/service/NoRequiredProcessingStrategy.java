package com.m4u.recharge.core.model.service;

import com.m4u.recharge.core.model.ScheduledRechargeEvent;

public class NoRequiredProcessingStrategy implements PendingScheduledRechargeEventProcessingStrategy {

    @Override
    public boolean preProcess(ScheduledRechargeEvent event) {
        return true;
    }

    @Override
    public boolean postProcess(ScheduledRechargeEvent event) {
        return true;
    }
}
