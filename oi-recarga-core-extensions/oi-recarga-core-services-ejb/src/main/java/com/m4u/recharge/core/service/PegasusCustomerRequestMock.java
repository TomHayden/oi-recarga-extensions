package com.m4u.recharge.core.service;

import org.apache.log4j.Logger;

import com.m4u.multirecarga.pegasus.client.PegasusCustomerRequest;
import com.m4u.multirecarga.pegasus.client.exception.PegasusClientURLInvalidaException;
import com.m4u.multirecarga.pegasus.to.CustomerTO;

public class PegasusCustomerRequestMock extends PegasusCustomerRequest {
    
    private static final Logger logger = Logger.getLogger(MainServiceBean.class);

    public PegasusCustomerRequestMock(String url, String perfil, String servico) throws PegasusClientURLInvalidaException {
        super(url, perfil, servico);
        logger.warn("Conexao com o Pegasus esta desativada. Utilizando MOCK. Para alterar, ver propriedade: schedule.pegasus.integration.enable");
    }

    @Override
    public boolean alterar(CustomerTO customer) {
        return true;
    }

    @Override
    public boolean desativarCadastroCliente(String externalId) {
        return true;
    }

    @Override
    public boolean cadastrar(CustomerTO cadastro) {
        return true;
    }

    @Override
    public boolean ativarCadastroCliente(String externalId) {
        return true;
    }

    @Override
    public String consultarCliente(String externalId) {
        return super.consultarCliente(externalId);
    }
}
