package com.m4u.recharge.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Scheduled_Recharge")
@NamedQueries({ @NamedQuery(name = "findSchedulesByMsisdn", query = "SELECT sr FROM ScheduledRecharge sr, CustomerPrepaid cp, Customer c WHERE sr.customerPrepaid = cp and cp.customer = c and c.msisdn = :msisdn") })
public class ScheduledRecharge {

    @Id
    @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
    @GeneratedValue(generator = "generator")
    @Column(name = "id_scheduled_recharge", columnDefinition = "uniqueidentifier")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_channel")
    private Channel channel;

    @ManyToOne
    @JoinColumn(name = "id_customer_prepaid")
    private CustomerPrepaid customerPrepaid;

    @ManyToOne
    @JoinColumn(name = "id_customer_ccard")
    private CustomerCreditCard customerCreditCard;

    @Column(name = "recharge_day")
    private Byte rechargeDay;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "dt_register")
    private Date registerDate;

    @Column(name = "subscription_type")
    private String subscriptionType;

    public ScheduledRecharge() {
        super();
    }

    public ScheduledRecharge(String id) {
        super();
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public CustomerPrepaid getCustomerPrepaid() {
        return customerPrepaid;
    }

    public void setCustomerPrepaid(CustomerPrepaid prepaid) {
        this.customerPrepaid = prepaid;
    }

    public CustomerCreditCard getCustomerCreditCard() {
        return customerCreditCard;
    }

    public void setCreditCard(CustomerCreditCard creditCard) {
        this.customerCreditCard = creditCard;
    }

    public Byte getRechargeDay() {
        return rechargeDay;
    }

    public void setRechargeDay(Byte rechargeDay) {
        this.rechargeDay = rechargeDay;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

}
