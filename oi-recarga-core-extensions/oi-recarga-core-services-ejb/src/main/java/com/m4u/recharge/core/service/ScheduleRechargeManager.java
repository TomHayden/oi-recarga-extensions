package com.m4u.recharge.core.service;

import java.util.List;

import javax.ejb.Local;

import com.m4u.recharge.core.model.PegasusCallbackEnum;
import com.m4u.recharge.core.model.ScheduledRecharge;
import com.m4u.recharge.core.model.ScheduledRechargeEvent;

@Local
public interface ScheduleRechargeManager {

    public static final String JNDI_NAME = "schedulerechargemanager/local";
    
    public abstract String addScheduledRecharge(ScheduledRecharge schedule);

    public abstract ScheduledRecharge findScheduledRechargeById(String id);

    public abstract boolean removeScheduledRecharge(ScheduledRecharge schedule);
    
    public abstract List<ScheduledRecharge> getSchedulesByMsisdn(String msisdn);
    
    public abstract boolean addScheduledRechargeEvent(ScheduledRechargeEvent scheduleEvent);

    public abstract boolean updateScheduledRecharge(ScheduledRecharge schedule);
    
    public abstract List<ScheduledRechargeEvent> findPendingScheduledEventByCycleIdAndEventType(Long cycleId, PegasusCallbackEnum eventType);

    public abstract ScheduledRechargeEvent findScheduledRechargeEventById(Integer eventId);

}
