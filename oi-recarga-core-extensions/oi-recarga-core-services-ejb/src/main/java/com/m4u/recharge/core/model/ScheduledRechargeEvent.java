package com.m4u.recharge.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.m4u.recharge.core.model.service.NoRequiredProcessingStrategy;
import com.m4u.recharge.core.model.service.PendingScheduledRechargeEventProcessingStrategy;

@Entity
@Table(name = "Scheduled_Recharge_Event")
@NamedQueries(value = { @NamedQuery(name = "findEventsByIdScheduleRecharge", query = "SELECT sre FROM ScheduledRechargeEvent sre WHERE sre.scheduleRecharge = :scheduleRecharge and sre.cycleStepId = :cycleStepId") })
public class ScheduledRechargeEvent {
    

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_scheduled_recharge_event")
    private Integer id;

    private String description;
    
    private String payload;

    @ManyToOne(optional = true)
    @JoinColumn(name = "id_scheduled_recharge")
    private ScheduledRecharge scheduleRecharge;

    @Column(name = "id_cycle")
    private Long cycleId;

    @Column(name = "id_step")
    private String cycleStepId;
    
    @Column(name = "id_service_pegasus")
    private String idServicePegasus;

    @Column(name = "dt_created")
    private Date dtCreated;

    @Column(name = "dt_received")
    private Date dtReceived;
    
    @Column(name = "pending")
    private boolean pending = true;
    
    @Transient
    private PendingScheduledRechargeEventProcessingStrategy pendingEventProcessingStrategy = new NoRequiredProcessingStrategy();
    
    @Transient
    private boolean reprocessed;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public ScheduledRecharge getScheduleRecharge() {
        return scheduleRecharge;
    }

    public void setScheduleRecharge(ScheduledRecharge scheduleRecharge) {
        this.scheduleRecharge = scheduleRecharge;
    }

    public String getIdServicePegasus() {
        return idServicePegasus;
    }

    public void setIdServicePegasus(String idServicePegasus) {
        this.idServicePegasus = idServicePegasus;
    }

    public Date getDtCreated() {
        return dtCreated;
    }

    public void setDtCreated(Date dtCreated) {
        this.dtCreated = dtCreated;
    }

    public Date getDtReceived() {
        return dtReceived;
    }

    public void setDtReceived(Date dtReceived) {
        this.dtReceived = dtReceived;
    }

    public Long getCycleId() {
        return cycleId;
    }

    public void setCycleId(Long cycleId) {
        this.cycleId = cycleId;
    }

    public String getCycleStepId() {
        return cycleStepId;
    }

    public void setCycleStepId(String cycleStepId) {
        this.cycleStepId = cycleStepId;
    }
    
    public boolean isPending() {
        return pending;
    }
    
    public boolean wasProcessed() {
        return !pending;
    }
    
    public void processed() {
        this.pending = false;
    }
    
    public void setPendingEventProcessingStrategy(PendingScheduledRechargeEventProcessingStrategy pendingEventProcessingStrategy) {
        this.pendingEventProcessingStrategy = pendingEventProcessingStrategy;
    }
    
    public boolean preProcessPendingEvent() {
        return pendingEventProcessingStrategy.preProcess(this);
    }
    
    public boolean postProcessPendingEvent() {
        return pendingEventProcessingStrategy.postProcess(this);
    }
    
    public boolean isReprocessed() {
        return reprocessed;
    }
    
    public void setReprocessed(boolean reprocessed) {
        this.reprocessed = reprocessed;
    }
    
}
