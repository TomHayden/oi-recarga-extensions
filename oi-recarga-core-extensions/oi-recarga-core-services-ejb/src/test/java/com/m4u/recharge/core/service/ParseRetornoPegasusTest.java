package com.m4u.recharge.core.service;

import java.nio.charset.Charset;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class ParseRetornoPegasusTest {

    public static final String BILLING_AUTHORIZATION_CALLBACK = "{\"result\":{\"resultHint\":\"00\",\"resultSuccess\":true,\"resultCode\":\"000\",\"properties\":{\"MOCK\":\"Mocked\",\"PROXY\":\"M[toString - 1409721015388]\",\"MOCK_CLASS\":\"class com.m4u.eldorado.acquirer.integration.mock.driver.mbean.MockedPos\",\"DATE\":\"Wed Sep 03 02:10:15 BRT 2014\"},\"resultMessage\":\"Sem detalhamento\",\"resultDetailing\":0},\"authorizationNumber\":\"M24745\",\"nsu\":\"M[getNSU - 1409721015388]\"}";
    public static final String BENEFIT_AUTHORIZATION = "type=AUTHORIZE_RESPONSE&serviceId=1&externalId=2014080000567225%7C179886120658790&requestProperties=%7B%7D&nsu=214552&responseProperties=%7B%7D&response=PR000&responseMessage=MOCK+MOCK+MOCK+MOCK+MOCK&isAuthorized=true";
    public static void main(String args[]) {
        Gson gson = new Gson();
        JsonObject payload = gson.fromJson(BILLING_AUTHORIZATION_CALLBACK, JsonObject.class);
        String paymentResponseCode = payload.get("result").getAsJsonObject().get("resultCode").getAsString();
        String paymentAuthorizeCode = payload.get("authorizationNumber").getAsString();
        String paymentResponseDescription = payload.get("result").getAsJsonObject().get("resultMessage").getAsString();
        String paymentTid = payload.get("nsu").getAsString();
        String ccNsuCode = paymentTid;

       
        List<NameValuePair> payloadList = URLEncodedUtils.parse(BENEFIT_AUTHORIZATION, Charset.defaultCharset());
        String nsu=null;
        String pdv_code=null;
        String isAuthorized = null;
        String message = null;
        for(NameValuePair element:payloadList){
            if(element.getName().equalsIgnoreCase("nsu")){
                nsu = element.getValue();
            }
            if(element.getName().equalsIgnoreCase("externalId")){
                pdv_code= element.getValue();
            }
            if(element.getName().equals("isAuthorized")){
                isAuthorized = element.getValue();
            }
            if(element.getName().equals("responseMessage")){
                message= element.getValue();
            }
        }
        System.out.println("BENEFIT_AUTHORIZATION:");
        System.out.println(nsu);
        System.out.println(pdv_code);
        System.out.println(isAuthorized);
        System.out.println(message);
    }

}
