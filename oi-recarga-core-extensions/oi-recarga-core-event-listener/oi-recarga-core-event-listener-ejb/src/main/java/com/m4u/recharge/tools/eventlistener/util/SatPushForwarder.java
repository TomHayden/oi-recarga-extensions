package com.m4u.recharge.tools.eventlistener.util;

import com.m4u.net.sms.SMSMessage;
import com.m4u.recharge.tools.common.util.QueueHelper;

public class SatPushForwarder {

    public static void sendSatPush(String msisdn, String token) {

        final SMSMessage sms = new SMSMessage();
        sms.setFrom(msisdn);
        sms.setTo("3000");
        sms.setContent("OI");
        sms.setChannelId(token);

        QueueHelper.putInQueue("queue/smsReceiverQueue", sms);

    }

}
