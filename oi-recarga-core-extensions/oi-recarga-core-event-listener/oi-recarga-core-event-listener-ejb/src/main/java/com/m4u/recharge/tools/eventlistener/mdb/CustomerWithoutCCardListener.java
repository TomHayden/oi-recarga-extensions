package com.m4u.recharge.tools.eventlistener.mdb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.m4u.recharge.tools.eventlistener.dao.ChannelDAO;
import com.m4u.recharge.tools.eventlistener.util.SatPushForwarder;
import com.m4u.recharge.tools.eventlistener.vo.Channel;
import com.m4u.recharge.tools.eventlistener.vo.CustomerWithoutCCardEvent;

@MessageDriven(name = "CustomerWithoutCCardListener", description = "Encaminha Usuario para o Fluxo Sat", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "topic/CustomerWithoutCCardEventTopic"),
        @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "NonDurable") })
public class CustomerWithoutCCardListener implements MessageListener{
    
    private static final Logger LOGGER = Logger.getLogger(CustomerWithoutCCardListener.class);

    @Override
    public void onMessage(Message message) {

        String stringMessage;
        try {
            stringMessage = ((TextMessage) message).getText();
            LOGGER.info("Object received: " + stringMessage);

            Gson gson = new Gson();
            CustomerWithoutCCardEvent event = gson.fromJson(stringMessage, CustomerWithoutCCardEvent.class);
            LOGGER.info("Object converted: " + event);

            ChannelDAO dao = new ChannelDAO();
            Channel channel = dao.getChannel(event.getChannel());

            if (channel != null && channel.isForwardToMenuSat()) {
                SatPushForwarder.sendSatPush(event.getMsisdn(), channel.getToken());
            }

        } catch (JMSException e) {
            LOGGER.error("Nao foi possivel utilizar objeto do Topico.", e);
        }

    }
}
