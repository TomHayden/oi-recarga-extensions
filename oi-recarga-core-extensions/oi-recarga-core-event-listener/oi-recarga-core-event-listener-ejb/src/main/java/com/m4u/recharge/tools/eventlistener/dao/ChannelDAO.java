package com.m4u.recharge.tools.eventlistener.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.m4u.recharge.tools.commons.dao.DataSourceService;
import com.m4u.recharge.tools.eventlistener.util.EventListenerProperties;
import com.m4u.recharge.tools.eventlistener.vo.Channel;

public class ChannelDAO {

    private static final String FIND_CHANNEL_BY_ID = "SELECT * FROM CHANNEL WHERE ID_CHANNEL = ?";

    private String jndiDataSource = null;

    public ChannelDAO() {
        jndiDataSource = EventListenerProperties.getConf().getString("jndi.datasource", "java:/RechargeDS");
    }

    public Channel getChannel(int id) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DataSourceService.getPool(jndiDataSource).getConnection();
            pstmt = conn.prepareStatement(FIND_CHANNEL_BY_ID);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return new Channel(rs.getInt("id_channel"), rs.getString("description"), rs.getString("token"),
                        rs.getBoolean("forward_to_menu_sat"));
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar os Cartoes. Metodo: getCardsFromCustomerCreditCard.", e);
        } finally {
            DataSourceService.closeConnection(conn, pstmt, rs);
        }
        return null;
    }

}
