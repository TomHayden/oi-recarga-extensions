package com.m4u.recharge.tools.eventlistener.vo;

public class Channel {

    private Integer id;
    private String description;
    private String token;
    private boolean forwardToMenuSat;

    public Channel(Integer id, String description, String token, boolean forwardToMenuSat) {
        super();
        this.id = id;
        this.description = description;
        this.token = token;
        this.forwardToMenuSat = forwardToMenuSat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isForwardToMenuSat() {
        return forwardToMenuSat;
    }

    public void setForwardToMenuSat(boolean forwardToMenuSat) {
        this.forwardToMenuSat = forwardToMenuSat;
    }

    @Override
    public String toString() {
        return "Channel [id=" + id + ", description=" + description + ", token=" + token + ", forwardToMenuSat=" + forwardToMenuSat + "]";
    }

}
