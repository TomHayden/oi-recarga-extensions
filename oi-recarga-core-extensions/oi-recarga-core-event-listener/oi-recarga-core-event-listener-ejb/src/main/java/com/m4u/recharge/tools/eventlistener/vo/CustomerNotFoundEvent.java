package com.m4u.recharge.tools.eventlistener.vo;

import java.io.Serializable;

public class CustomerNotFoundEvent implements Serializable {
    
    private static final long serialVersionUID = 7119383913831361919L;

    protected Integer channel;
    protected String msisdn;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "CustomerNotFoundEvent [channel=" + channel + ", msisdn=" + msisdn + "]";
    }

}
