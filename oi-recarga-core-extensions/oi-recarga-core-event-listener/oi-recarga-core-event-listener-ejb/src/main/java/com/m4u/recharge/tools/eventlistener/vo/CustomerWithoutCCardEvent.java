package com.m4u.recharge.tools.eventlistener.vo;

import java.io.Serializable;

public class CustomerWithoutCCardEvent implements Serializable {
    
    private static final long serialVersionUID = 7665653092433606821L;

    protected Integer channel;
    protected String msisdn;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "CustomerWithoutCCardEvent [channel=" + channel + ", msisdn=" + msisdn + "]";
    }

}