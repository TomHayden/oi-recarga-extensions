package com.m4u.recharge.tools.eventlistener.util;

import com.m4u.recharge.tools.common.util.PropertiesUtil;

public class EventListenerProperties {
    
    private static final String FILE = "/event-listener.properties";
    
    public static PropertiesUtil getConf() {
        return new PropertiesUtil(FILE);
    }

}
