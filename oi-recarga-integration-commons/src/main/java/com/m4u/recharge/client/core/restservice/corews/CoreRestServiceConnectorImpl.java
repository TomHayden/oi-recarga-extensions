package com.m4u.recharge.client.core.restservice.corews;

import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;
import com.m4u.recharge.restservice.corews.vo.AddPrepaidRequestRS;
import com.m4u.recharge.restservice.corews.vo.CreateCustomerRequestRS;
import com.m4u.recharge.restservice.corews.vo.HistoryRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupStatusRequestRS;
import com.m4u.recharge.restservice.corews.vo.UpdateCreditCardRequestRS;

public class CoreRestServiceConnectorImpl implements CoreRestServiceConnector {

    private static final String URL_BASE = "/recharge/core";
    private String targetServer;

    public CoreRestServiceConnectorImpl(String targetServer) {
        this.targetServer = targetServer;
    }

    private CoreRestServiceClient getCoreRestClient() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(targetServer + URL_BASE);
        return target.proxy(CoreRestServiceClient.class);
    }

    @Override
    public Response getCustomer(SimpleRequestRS request) {
        return getCoreRestClient().getCustomer(request);
    }
    
    @Override
    public Response createCustomer(CreateCustomerRequestRS request) {
        return getCoreRestClient().createCustomer(request);
    }
    
    @Override
    public Response createCustomerWithToken(CreateCustomerRequestRS request) {
        return getCoreRestClient().createCustomerWithToken(request);
    }
    
    @Override
    public Response cancelCustomer(SimpleRequestRS request) {
        return getCoreRestClient().cancelCustomer(request);
    }
    
    @Override
    public Response getTopupStatus(TopupStatusRequestRS request) {
        return getCoreRestClient().getTopupStatus(request);
    }

    @Override
    public Response getHistory(HistoryRequestRS request) {
        return getCoreRestClient().getHistory(request);
    }
    
    @Override
    public Response topupRequest(TopupRequestRS request) {
        return getCoreRestClient().topupRequest(request);
    }

    
    @Override
    public Response addPrepaid(AddPrepaidRequestRS request) {
        return getCoreRestClient().addPrepaid(request);
    }
    
    @Override
    public Response updateCreditCard(UpdateCreditCardRequestRS request) {
        return getCoreRestClient().updateCreditCard(request);
    }
}
