package com.m4u.recharge.client.core.restservice.schedule.vo;

import javax.ws.rs.PathParam;

public class RemoveScheduleRequest {

    @PathParam(value = "channel")
    private Integer channel;

    @PathParam("scheduleid")
    private String scheduleId;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Override
    public String toString() {
        return "RemoveScheduleRequest [channel=" + channel + ", scheduleId=" + scheduleId + "]";
    }

}
