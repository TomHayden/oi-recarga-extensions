package com.m4u.recharge.client.core.restservice.schedule.vo;

import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;

public class VerifyElegibilityRequest {
    @PathParam(value = "channel")
    private Integer channel;

    @FormParam(value = "cycleId")
    private Long cycleId;

    @FormParam(value = "serviceId")
    private String serviceId;

    @FormParam(value = "cycleStepId")
    private String cycleStepId;

    @PathParam(value = "scheduleid")
    private String scheduleId;

    @FormParam(value = "cardToken")
    private String cardToken;

    @FormParam(value = "prepaid")
    private String prepaid;

    @FormParam(value = "reloadAmount")
    private Double reloadAmount;

    public VerifyElegibilityRequest(Integer channel, Long cycleId, String serviceId, String scheduleId, String cycleStepId,
            String cardToken, String prepaid, Double reloadAmount) {
        this.channel = channel;
        this.cycleId = cycleId;
        this.serviceId = serviceId;
        this.scheduleId = scheduleId;
        this.cycleStepId = cycleStepId;
        this.cardToken = cardToken;
        this.prepaid = prepaid;
        this.reloadAmount = reloadAmount;
    }

    public Integer getChannel() {
        return channel;
    }

    public Long getCycleId() {
        return cycleId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getCycleStepId() {
        return cycleStepId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public String getCardToken() {
        return cardToken;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public Double getReloadAmount() {
        return reloadAmount;
    }

    @Override
    public String toString() {
        return "VerifyElegibilityRequest [channel=" + channel + ", cycleId=" + cycleId + ", serviceId=" + serviceId + ", cycleStepId="
                + cycleStepId + ", scheduleId=" + scheduleId + ", cardToken=" + cardToken + ", prepaid=" + prepaid + ", reloadAmount="
                + reloadAmount + "]";
    }

}
