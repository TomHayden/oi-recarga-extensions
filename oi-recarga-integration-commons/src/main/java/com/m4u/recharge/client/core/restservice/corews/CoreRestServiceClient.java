package com.m4u.recharge.client.core.restservice.corews;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;
import com.m4u.recharge.restservice.corews.vo.AddPrepaidRequestRS;
import com.m4u.recharge.restservice.corews.vo.CreateCustomerRequestRS;
import com.m4u.recharge.restservice.corews.vo.HistoryRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupStatusRequestRS;
import com.m4u.recharge.restservice.corews.vo.UpdateCreditCardRequestRS;

@Path("/channel/{channel}")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
public interface CoreRestServiceClient {

    @GET
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@Form SimpleRequestRS request);
    
    @POST
    @Path("/customers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(@Form CreateCustomerRequestRS request);
    
    @POST
    @Path("/customers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomerWithToken(@Form CreateCustomerRequestRS request);
    
    @DELETE
    @Path("/customers/{msisdn}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelCustomer(@Form SimpleRequestRS request);
    
    @GET
    @Path("/customers/{msisdn}/topups/{externalId}/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTopupStatus(@Form TopupStatusRequestRS request);
    
    @GET
    @Path("/customers/{msisdn}/history")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHistory(@Form HistoryRequestRS request);
    
    @POST
    @Path("/customers/{msisdn}/topups")
    @Produces(MediaType.APPLICATION_JSON)
    public Response topupRequest(@Form TopupRequestRS request);

    @POST
    @Path("/customers/{msisdn}/prepaids")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPrepaid(@Form AddPrepaidRequestRS request);

    @PUT
    @Path("/customers/{msisdn}/creditcards")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCreditCard(@Form UpdateCreditCardRequestRS request);
    
}


