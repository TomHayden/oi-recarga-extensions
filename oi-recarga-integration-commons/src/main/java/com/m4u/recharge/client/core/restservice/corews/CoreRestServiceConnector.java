package com.m4u.recharge.client.core.restservice.corews;

import javax.ws.rs.core.Response;

import com.m4u.recharge.restservice.commons.vo.SimpleRequestRS;
import com.m4u.recharge.restservice.corews.vo.AddPrepaidRequestRS;
import com.m4u.recharge.restservice.corews.vo.CreateCustomerRequestRS;
import com.m4u.recharge.restservice.corews.vo.HistoryRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupRequestRS;
import com.m4u.recharge.restservice.corews.vo.TopupStatusRequestRS;
import com.m4u.recharge.restservice.corews.vo.UpdateCreditCardRequestRS;

public interface CoreRestServiceConnector {

    public Response getCustomer(SimpleRequestRS request);

    public Response createCustomer(CreateCustomerRequestRS request);

    public Response createCustomerWithToken(CreateCustomerRequestRS request);

    public Response cancelCustomer(SimpleRequestRS request);

    public Response getTopupStatus(TopupStatusRequestRS request);

    public Response getHistory(HistoryRequestRS request);

    public Response topupRequest(TopupRequestRS request);

    public Response addPrepaid(AddPrepaidRequestRS request);

    public Response updateCreditCard(UpdateCreditCardRequestRS request);

}
