package com.m4u.recharge.client.core.restservice.schedule.vo;

import javax.ws.rs.PathParam;

public class RemoveScheduleByIdRequest {

    @PathParam(value = "channel")
    private Integer channel;

    @PathParam("scheduleid")
    private String scheduleId;

    @PathParam("msisdn")
    private String msisdn;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "RemoveScheduleByIdRequest [channel=" + channel + ", scheduleId=" + scheduleId + ", msisdn=" + msisdn + "]";
    }
}
