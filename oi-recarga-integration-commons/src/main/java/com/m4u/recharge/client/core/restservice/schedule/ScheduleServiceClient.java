package com.m4u.recharge.client.core.restservice.schedule;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;

import com.m4u.recharge.client.core.restservice.schedule.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.GetScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.VerifyElegibilityRequest;

@Path("/channel/{channel}")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public interface ScheduleServiceClient {

    @POST
    @Path("/customers/{msisdn}/schedules")
    public Response addScheduleRecharge(@Form AddScheduledRechargeRequest request);

    @PUT
    @Path("/schedules/{scheduleid}/evaluation")
    public Response verifyTopupElegibility(@Form VerifyElegibilityRequest request);

    @PUT
    @Path("/schedules/{scheduleid}/notify")
    public Response proccessCallbackNotifications(@Form ScheduledRechargeEventRequest request);

    @GET
    @Path("/schedules/{scheduleid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getScheduleById(@Form GetScheduleByIdRequest request);
    
    @PUT
    @Path("/customers/{msisdn}/schedules/{scheduleid}")
    public Response updateScheduledRecharge(@Form UpdateScheduledRechargeRequest request);

    @DELETE
    @Path("/customers/{msisdn}/schedules/{scheduleid}")
    public Response removeScheduleById(@Form RemoveScheduleByIdRequest request);
    
    @DELETE
    @Path("/schedules/{scheduleid}")
    public Response removeSchedule(@Form RemoveScheduleRequest request);
    
}
