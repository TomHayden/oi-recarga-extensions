package com.m4u.recharge.client.core.restservice.schedule.vo;

import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;

public class ScheduledRechargeEventRequest {

    @PathParam("channel")
    private Integer channel;

    @FormParam(value = "cycleId")
    private Long cycleId;

    @FormParam(value = "serviceId")
    private String serviceId;

    @PathParam(value = "scheduleid")
    private String scheduleId;

    @FormParam(value = "cycleStepId")
    private String cycleStepId;

    @FormParam(value = "description")
    private String description;
    
    @FormParam(value = "payload")
    private String payload;

    @FormParam(value = "created")
    private String created;

    public ScheduledRechargeEventRequest(Integer channel, Long cycleId, String serviceId, String scheduleId, String payload,
            String cycleStepId, String description, String created) {
        this.channel = channel;
        this.cycleId = cycleId;
        this.serviceId = serviceId;
        this.scheduleId = scheduleId;
        this.payload = payload;
        this.cycleStepId = cycleStepId;
        this.description = description;
        this.created = created;
    }

    public Integer getChannel() {
        return channel;
    }

    public Long getCycleId() {
        return cycleId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public String getCycleStepId() {
        return cycleStepId;
    }

    public String getDescription() {
        return description;
    }

    public String getPayload() {
        return payload;
    }

    public String getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return "ScheduledRechargeEventRequest [channel=" + channel + ", cycleId=" + cycleId + ", serviceId=" + serviceId + ", scheduleId="
                + scheduleId + ", cycleStepId=" + cycleStepId + ", description=" + description + ", payload=" + payload + ", created="
                + created + "]";
    }
    
}
