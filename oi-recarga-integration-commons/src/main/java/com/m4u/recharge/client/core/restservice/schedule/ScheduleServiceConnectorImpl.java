package com.m4u.recharge.client.core.restservice.schedule;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.m4u.recharge.client.core.restservice.schedule.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.GetScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.VerifyElegibilityRequest;

public class ScheduleServiceConnectorImpl implements ScheduleServiceConnector {

    private static final Logger LOGGER = Logger.getLogger(ScheduleServiceConnector.class);
    private static final String URL_BASE = "/recharge/core";
    private String targetServer;

    public ScheduleServiceConnectorImpl(String targetServer) {
        this.targetServer = targetServer;
    }

    private ScheduleServiceClient getScheduleServiceClient() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(targetServer + URL_BASE);
        return target.proxy(ScheduleServiceClient.class);
    }

    @Override
    public Response addScheduleRecharge(AddScheduledRechargeRequest request) {
        return getScheduleServiceClient().addScheduleRecharge(request);
    }

    @Override
    public Response verifyTopupElegibility(VerifyElegibilityRequest scheduleid) {
        return getScheduleServiceClient().verifyTopupElegibility(scheduleid);
    }

    @Override
    public Response proccessCallbackNotifications(ScheduledRechargeEventRequest request) {
        return getScheduleServiceClient().proccessCallbackNotifications(request);
    }

    @Override
    public Response getScheduleById(GetScheduleByIdRequest request) {
        LOGGER.info("ScheduleServiceConnectorImpl.getScheduleById: " + request.toString());
        return getScheduleServiceClient().getScheduleById(request);
    }

    @Override
    public Response updateScheduledRecharge(UpdateScheduledRechargeRequest request) {
        LOGGER.info("ScheduleServiceConnectorImpl.updateScheduledRecharge:" + request.toString());
        return getScheduleServiceClient().updateScheduledRecharge(request);
    }
    
    @Override
    public Response removeScheduleById(RemoveScheduleByIdRequest request) {
        LOGGER.info("ScheduleServiceConnectorImpl.removeScheduleById: " + request.toString());
        return getScheduleServiceClient().removeScheduleById(request);
    }

    @Override
    public Response removeSchedule(RemoveScheduleRequest request) {
        LOGGER.info("ScheduleServiceConnectorImpl.removeSchedule: " + request);
        return getScheduleServiceClient().removeSchedule(request);
    }


}
