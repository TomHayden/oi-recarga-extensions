package com.m4u.recharge.client.core.restservice.schedule.vo;

import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;

public class AddScheduledRechargeRequest {

    @PathParam(value = "channel")
    private Integer channel;

    @PathParam(value = "msisdn")
    private String msisdn;

    @FormParam(value = "prepaid")
    private String prepaid;

    @FormParam(value = "cardType")
    private String cardType;

    @FormParam(value = "last4Digits")
    private String last4Digits;

    @FormParam(value = "amount")
    private Double amount;

    @FormParam(value = "day")
    private Byte day;
    
    @FormParam(value = "subscriptionType")
    private String subscriptionType;
    
    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Byte getDay() {
        return day;
    }

    public void setDay(Byte day) {
        this.day = day;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    @Override
    public String toString() {
        return "AddScheduledRechargeRequest [channel=" + channel + ", msisdn=" + msisdn + ", prepaid=" + prepaid + ", cardType=" + cardType
                + ", last4Digits=" + last4Digits + ", amount=" + amount + ", day=" + day + ", subscriptionType=" + subscriptionType + "]";
    }

}
