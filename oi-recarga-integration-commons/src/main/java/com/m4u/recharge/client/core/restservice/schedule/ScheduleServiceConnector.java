package com.m4u.recharge.client.core.restservice.schedule;

import javax.ws.rs.core.Response;

import com.m4u.recharge.client.core.restservice.schedule.vo.AddScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.GetScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.RemoveScheduleRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.UpdateScheduledRechargeRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.VerifyElegibilityRequest;

public interface ScheduleServiceConnector {

    public Response addScheduleRecharge(AddScheduledRechargeRequest request);
    public Response verifyTopupElegibility(VerifyElegibilityRequest scheduleid);
    public Response proccessCallbackNotifications(ScheduledRechargeEventRequest request);
    public Response getScheduleById(GetScheduleByIdRequest request);
    public Response updateScheduledRecharge(UpdateScheduledRechargeRequest request);
    public Response removeSchedule(RemoveScheduleRequest request);
    public Response removeScheduleById(RemoveScheduleByIdRequest request);
}