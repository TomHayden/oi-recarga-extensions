package com.m4u.recharge.client.core.util;

import com.m4u.recharge.tools.common.util.PropertiesUtil;

public class LookupCoreProperties {
    
    private static final String FILE = "/lookup-core.properties";
    
    public static PropertiesUtil getConf() {
        return new PropertiesUtil(FILE);
    }

}
