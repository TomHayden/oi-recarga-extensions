package com.m4u.recharge.client.core;

import com.m4u.recharge.client.core.restservice.corews.CoreRestServiceConnector;
import com.m4u.recharge.client.core.restservice.corews.CoreRestServiceConnectorImpl;
import com.m4u.recharge.client.core.restservice.schedule.ScheduleServiceConnector;
import com.m4u.recharge.client.core.restservice.schedule.ScheduleServiceConnectorImpl;
import com.m4u.recharge.client.core.util.LookupCoreProperties;

public class CoreRestConnectorFactory {

    public static CoreRestServiceConnector createCoreRSConnector() {
        String targetServer = getTargetServer();
        return new CoreRestServiceConnectorImpl(targetServer);
    }
    
    public static CoreRestServiceConnector createCoreRSConnector(String targetServer) {
        return new CoreRestServiceConnectorImpl(targetServer);
    }
    
    public static ScheduleServiceConnector createSheduleServiceConnector(String targetServer) {
        return new ScheduleServiceConnectorImpl(targetServer);
    }
    
    public static ScheduleServiceConnector createSheduleServiceConnector() {
        String targetServer = getTargetServer();
        return new ScheduleServiceConnectorImpl(targetServer);
    }
    
    private static String getTargetServer() {
        String port = LookupCoreProperties.getConf().getString("core.rest.port", "8080");
        String hostname = LookupCoreProperties.getConf().getString("core.rest.server", "core.m4u.recargaoi");
        String protocol = LookupCoreProperties.getConf().getString("core.rest.protocol", "http");
        String targetServer = protocol + "://" + hostname + ":" + port;
        return targetServer;
    }
}
