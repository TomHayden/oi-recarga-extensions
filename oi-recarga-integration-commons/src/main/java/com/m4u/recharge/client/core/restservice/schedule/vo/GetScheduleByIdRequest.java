package com.m4u.recharge.client.core.restservice.schedule.vo;

import javax.ws.rs.PathParam;

public class GetScheduleByIdRequest {

    @PathParam(value = "scheduleid")
    private String scheduleId;

    @PathParam(value = "channel")
    private Integer channel;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Override
    public String toString() {
        return "GetScheduleByIdRequest [scheduleId=" + scheduleId + ", channel=" + channel + "]";
    }

}
