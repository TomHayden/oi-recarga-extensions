package com.m4u.recharge.client.core.restservice.schedule.vo;

import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;

public class UpdateScheduledRechargeRequest {
    
    @PathParam(value = "channel")
    private Integer channel;
    
    @PathParam(value = "msisdn")
    private String msisdn;

    @PathParam(value = "scheduleid")
    private String scheduledId;

    @FormParam(value = "amount")
    private Double amount;

    @FormParam(value = "day")
    private Byte birthday;
    
    @FormParam(value = "cardType")
    private String cardType;
    
    @FormParam(value = "last4Digits")
    private String last4Digits;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getScheduledId() {
        return scheduledId;
    }

    public void setScheduledId(String scheduledId) {
        this.scheduledId = scheduledId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Byte getBirthday() {
        return birthday;
    }

    public void setBirthday(Byte birthday) {
        this.birthday = birthday;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getLast4Digits() {
        return last4Digits;
    }

    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }

    @Override
    public String toString() {
        return "UpdateScheduledRechargeRequest [channel=" + channel + ", msisdn=" + msisdn + ", scheduledId=" + scheduledId + ", amount="
                + amount + ", birthday=" + birthday + ", cardType=" + cardType + ", last4Digits=" + last4Digits + "]";
    }

  
}
