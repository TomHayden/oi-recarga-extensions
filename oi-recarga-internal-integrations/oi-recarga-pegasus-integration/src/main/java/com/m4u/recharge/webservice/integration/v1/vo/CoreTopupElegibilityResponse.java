package com.m4u.recharge.webservice.integration.v1.vo;

public class CoreTopupElegibilityResponse {


    private Long code;
    private String message;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CoreTopupElegibilityResponse [code=" + code + ", message=" + message + "]";
    }
}
