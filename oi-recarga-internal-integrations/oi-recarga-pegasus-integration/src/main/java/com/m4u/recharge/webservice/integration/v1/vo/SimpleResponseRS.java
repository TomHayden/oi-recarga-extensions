package com.m4u.recharge.webservice.integration.v1.vo;

public class SimpleResponseRS {

    protected Long code;
    protected String message;

    public Long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
