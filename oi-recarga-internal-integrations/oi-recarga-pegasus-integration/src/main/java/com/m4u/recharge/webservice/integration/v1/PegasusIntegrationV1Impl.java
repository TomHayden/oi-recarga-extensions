package com.m4u.recharge.webservice.integration.v1;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.m4u.logging.ApplicationLogger;
import com.m4u.recharge.client.core.CoreRestConnectorFactory;
import com.m4u.recharge.client.core.restservice.schedule.ScheduleServiceConnector;
import com.m4u.recharge.client.core.restservice.schedule.vo.GetScheduleByIdRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.ScheduledRechargeEventRequest;
import com.m4u.recharge.client.core.restservice.schedule.vo.VerifyElegibilityRequest;
import com.m4u.recharge.util.RechargeUtil;
import com.m4u.recharge.webservice.integration.v1.vo.GetScheduledRechargeResponse;
import com.m4u.recharge.webservice.integration.v1.vo.ScheduledRecharge;

@Path("/pegasus/v1")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class PegasusIntegrationV1Impl {
    private static final Integer PEGASUS_CHANNEL = 29;
    private static final Logger logger = Logger.getLogger(PegasusIntegrationV1Impl.class);
    private ScheduleServiceConnector coreIntegration = CoreRestConnectorFactory.createSheduleServiceConnector();

    @PUT
    @Path("/schedule/validation")
    public Response verifyTopupElegibility(@FormParam("cycle_id") Long cycleId, @FormParam("service_id") String serviceId,
            @FormParam("external_id") String scheduleId, @FormParam("cycle_extid") String cycleStepId,
            @FormParam("profile_extid") String profileExtId, @FormParam("card_token") String cardToken,
            @FormParam("account") String prepaid, @FormParam("customer_value") String reloadAmount,
            @FormParam("profile_value") String defaultReloadAmount) {
        
        double reloadAmountAsDouble = RechargeUtil.stringToDecimal(reloadAmount);

        VerifyElegibilityRequest request = new VerifyElegibilityRequest(PEGASUS_CHANNEL, cycleId, serviceId, scheduleId, cycleStepId,
                cardToken, prepaid, reloadAmountAsDouble);

        logger.info("verifyTopupElegibility coreRequest: " + request);

        ApplicationLogger.step("CONSULTA_ELEGIBILIDADE").channel(request.getChannel().toString())
                .param("cycleId", cycleId)
                .param("serviceId", serviceId)
                .param("scheduleId", scheduleId)
                .param("cycleStepId", cycleStepId)
                .ccToken(cardToken)
                .receiver(prepaid)
                .amount(reloadAmountAsDouble)
                .log();

        Response response = coreIntegration.verifyTopupElegibility(request);
        String entity = response.readEntity(String.class);
        logger.info("verifyTopupElegibility coreResponse:" + entity);
        return createRestResponse(response.getStatusInfo(), entity);
    }

    private Response createRestResponse(StatusType statusInfo, String entity) {
        return Response.status(statusInfo).type(MediaType.APPLICATION_JSON).entity(entity).build();
    }

    @PUT
    @Path("/schedule/notify")
    @Produces(MediaType.APPLICATION_JSON)
    public Response proccessCallbackNotifications(
            @FormParam(value = "cycle_id") Long cycleId, 
            @FormParam(value = "service_id") String serviceId,
            @FormParam(value = "external_id") String scheduleid,
            @FormParam(value = "payload") String payload,
            @FormParam(value = "cycle_extid") String cycleStepId,
            @FormParam(value = "description") String description,
            @FormParam(value = "created") String created            
            ) {
        
        ScheduledRechargeEventRequest request = new ScheduledRechargeEventRequest(PEGASUS_CHANNEL, cycleId, serviceId, scheduleid, payload,
                cycleStepId, description, created);
        logger.info("proccessCallbackNotifications request" + request);

        ApplicationLogger.step("CALLBACK_PEGASUS").channel(request.getChannel().toString())
                .param("cycleId", cycleId)
                .param("serviceId", request.getServiceId())
                .param("scheduleId", request.getScheduleId())
                .param("payload", request.getPayload())
                .param("cycleStepId", request.getCycleStepId())
                .param("description", request.getDescription())
                .param("created", request.getCreated()).log();

        Response response = coreIntegration.proccessCallbackNotifications(request);
        String entity = response.readEntity(String.class);
        logger.info("proccessCallbackNotifications coreResponse:" + entity);
        return createRestResponse(response.getStatusInfo(), entity);
    }

}
