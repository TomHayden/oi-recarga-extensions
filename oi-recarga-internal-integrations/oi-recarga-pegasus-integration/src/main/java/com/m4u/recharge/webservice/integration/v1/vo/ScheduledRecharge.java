package com.m4u.recharge.webservice.integration.v1.vo;

public class ScheduledRecharge {
    private String msisdn;
    private String prepaid;
    private String cardType;
    private String last4Digits;
    private Double amount;
    private byte day;
    public String getMsisdn() {
        return msisdn;
    }
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    public String getPrepaid() {
        return prepaid;
    }
    public void setPrepaid(String prepaid) {
        this.prepaid = prepaid;
    }
    public String getCardType() {
        return cardType;
    }
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    public String getLast4Digits() {
        return last4Digits;
    }
    public void setLast4Digits(String last4Digits) {
        this.last4Digits = last4Digits;
    }
    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }
    public byte getDay() {
        return day;
    }
    public void setDay(byte day) {
        this.day = day;
    }


}
