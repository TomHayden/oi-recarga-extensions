package com.m4u.recharge.webservice.integration.v1.vo;

import com.m4u.recharge.webservice.integration.v1.vo.SimpleResponseRS;

public class GetScheduledRechargeResponse extends SimpleResponseRS {

    private ScheduledRecharge scheduledRecharge;

    public ScheduledRecharge getScheduledRecharge() {
        return scheduledRecharge;
    }

    public void setScheduledRecharge(ScheduledRecharge scheduledRecharge) {
        this.scheduledRecharge = scheduledRecharge;
    } 
}
